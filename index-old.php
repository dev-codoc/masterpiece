<?php session_start(); ?>
<?php require_once './layout/db.php'; ?>
<?php require_once './Artist/lat_long_conversion.php' ?>
<?php require_once './layout/functions.php'; ?>
<?php require_once './auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler(); ?>
<html>
<head>
    <!-- SEO Start-->
    <title>Your Masterpieces</title>
    <meta charset="UTF-8"/>
    <meta name="description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings.">
    <meta name="keywords"
          content="Paintings,Your Masterpieces, Non Digital Art, Buy paintings, Sell paintings, Sell Art, Buy Art, Bulk paintings buying, Foreign paintings, Art collectors, Collect Art, Buy painting online, Sell painting online, Painter profile, Artist profile"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta property="og:title" content="Your Masterpieces"/>
    <meta property="og:type" content=""/>
    <meta property="og:url" content="<?php echo $webroot; ?>"/>
    <meta property="og:image" content="http://www.yourmasterpieces.com/images/favicon.jpeg"/>
    <meta property="og:site_name" content="Your Masterpieces"/>
    <meta property="fb:admins" content="Your Masterpieces"/>
    <meta property="og:locale" content="en_us"/>
    <meta property="og:description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings."/>
    <link rel="canonical" href="<?php echo $webroot; ?>"/>
    <meta name="google-site-verification" content="70XCszZbzQwiVI-nFBVngixEft2YBhHBb-gt2i77oe4"/>

    <!-- CSS Styles -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="assets/css/animate.min.css">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="assets/js/plugins/revolution/css/settings.css">
    <link rel="stylesheet" href="assets/js/plugins/revolution/css/layers.css">
    <link rel="stylesheet" href="assets/js/plugins/revolution/css/navigation.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="assets/js/plugins/owl-carousel/owl.carousel.css">

    <!-- Google Web Fonts -->

    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png"
          type="image/x-icon">

   <!-- --><?php /*include 'Artist/artist_page_external_style.php'; */?>

    <link rel="stylesheet" href="Artist/commoncss.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <link rel="stylesheet" href="assets/css/homepage_stylesheet.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php include 'google-analytics.php'; ?>

</head>
<body>


<!-- Loader Start -->
<div id="preloader">
    <div class="preloader-container">
        <div class="ws-spinner"></div>
    </div>
</div>
<!-- End Loader Start -->

<!-- Top Bar Start -->

<?php require_once './layout/header.php'; ?>
<!-- End Header -->


<!-- Hero Content -->
<div id="ws-hero-fullscreen" class="rev_slider">
    <ul>
        <li data-transition="fade" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut"
            data-masterspeed="2000">
            <!-- Background Image -->
            <img src="assets/img/backgrounds/masterpiece.jpg" alt="your masterpiece" data-bgposition="center center"
                 data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10">

            <!-- Background Overlay -->
            <div class="tp-caption"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                 data-width="full"
                 data-height="full"
                 data-whitespace="nowrap"
                 data-transform_idle="o:1;"
                 data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                 data-transform_out="s:300;s:300;"
                 data-start="750"
                 data-basealign="slide"
                 data-responsive_offset="on"
                 data-responsive="off"
                 style="z-index: 5;background-color:rgba(0, 0, 0, 0.40);border-color:rgba(0, 0, 0, 0.50);">
            </div>
            a
            <!-- Layer -->
            <div class="tp-caption ws-hero-title"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','1']"
                 data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                 data-mask_in="x:0px;y:0px;"
                 data-mask_out="x:0;y:0;"
                 data-start="200"
                 left="55px" ;
                 data-responsive_offset="on"
                 style="z-index: 6;"><h1>Handmade Paintings <br class="rwd-break"> From Artists all <br
                            class="rwd-break"> Around The World </h1>
            </div>

            <!-- Layer -->
            <div class="tp-caption ws-hero-description"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['-72','-72','-72','-48']"
                 data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                 data-mask_in="x:0px;y:0px;"
                 data-mask_out="x:0;y:0;"
                 data-start="1000"
                 data-responsive_offset="on"
                 style="z-index: 7;"><h4 style="padding-bottom:5px">A platform to explore, get inspiration and collect
                    paintings from upcoming as well as professional artists</h4>
            </div>

            <!-- Button -->
            <div class="tp-caption"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['92','92','92','76']"
                 data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeInOut;"
                 data-start="1000"
                 data-responsive_offset="on"
                 data-responsive="off"
                 style="z-index: 8;"><a class="btn ws-big-btn" href="Paintings/paintings.php">View Paintings</a>
            </div>
        </li>
    </ul>
    <div class="tp-static-layers"></div>
    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
</div>
<!-- Hero Content -->


<!-- WHO WE ARE SECTION START -->
<div class="padding-top-x70"></div>
<div class="container">
    <div class="row vertical-align">
        <div class="col-sm-6" style="padding-bottom: 25px ">
            <h3 class="who_we">Who We Are</h3>
            <div class="ws-footer-separator"></div>
            <p class="inner_Who">We are an open platform for artists, art collectors, art galleries and NGOs.</p>
            <br>
            <p class="inner_Who">Started in 2016 with an idea to enable Artists from all over the world, irrespective of
                their age, expertise or experience, to share their paintings using mobile technology.</p>
            <br>
            <p class="inner_Who">It helped enable 550 Artists from over 65 Countries to share their creations on the
                Masterpiece mobile application. We are 3K users on our platform and still growing.</p>
        </div>

        <div class="col-sm-6" style="padding-bottom: 25px">
            <img src="<?php echo $webroot ?>/images/index_bg.jpg" alt="your masterpiece"
                 class="img-responsive">
        </div>
    </div>
</div>
<!-- WHO WE ARE SECTION ENDED -->


<!-- REGISTER ARTIST SECTION START -->


<section class="ws-about-section">
    <div class="container">
        <div class="row">

            <div class="ws-about-content clearfix">
                <div class="col-sm-8 col-sm-offset-2">
                    <h3 class="who_we">How to join Masterpiece platform </h3>
                    <div class="ws-separator"></div>
                </div>

                <div class="padding-top-x70 hidden-xs"></div>
                <div class="ws-featured-collections clearfix">

                    <div class="col-sm-4 featured-collections-item">
                        <b class="step">Step 1</b><br>
                        <div class="ws-separator"></div>
                        <p class="inner_Who"> Download the Masterpiece mobile application on android or iOS </p>
                        <br>
                    </div>

                    <div class="col-sm-4 featured-collections-item">
                        <b class="step">Step 2</b><br>
                        <div class="ws-separator"></div>
                        <p class="inner_Who"> Register and tell us about yourself in your bio
                            section
                        </p>
                        <br>
                    </div>

                    <div class="col-sm-4 featured-collections-item">
                        <b class="step">Step 3</b><br>
                        <div class="ws-separator"></div>
                        <p class="inner_Who"> If you are an artist, click a picture & upload your paintings
                        </p>
                    </div>
                </div>

            </div>
        </div>
</section>

<!-- BUTTON FOR DOWNLOAD START ANDROID OR IOS -->
<section class="ws-call-section">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder" style="background: rgba(0,0,0,0.5);">
                <div class="col-sm-6 col-sm-offset-3">
                    <h2 class="app_heading">Download the Masterpiece App</h2>
                    <div class="ws-separator"></div>
                    <!--<p>Our blog covering interesting stories around culture and design.<br>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
                    -->
                    <div class="col-md-12">

                        <div class="col-md-6">
                            <div class="ws-call-btn">
                                <a class="btn ws-btn-subscribe android_btn"
                                   href="https://play.google.com/store/apps/details?id=com.codon.masterpiece&amp;hl=en"><i
                                            class="fa fa-download" style="padding-right: 5px"></i>Android App</a>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="ws-call-btn">
                                <a class="btn ws-btn-subscribe android_btn"
                                   href="https://itunes.apple.com/in/app/masterpiece-platform-for-non/id1089890769?mt=8"><i
                                            class="fa fa-download" style="padding-right: 5px"></i>IOS App</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- END DOWNLAOD BUTTON SECTION -->


<div class="padding-top-x40"></div>


<!-- FEATURED ARTIST Collection Start  -->
<section class="ws-works-section">
    <div class="container">
        <div class="row">

            <div class="ws-works-title artist_heading">
                <div class="col-sm-12">
                    <h3>Featured Artists</h3>
                    <div class="ws-separator"></div>
                </div>
            </div>

            <!-- Item -->


            <?php
            //$sql = "SELECT * FROM `users` WHERE id=162 OR id=1877 OR id=1827"; //fetching 32 Result Only from Users Table

            $sql = "SELECT * FROM `users` Where created_at BETWEEN '2018-02-01' AND '2018-05-01' ORDER BY totLikes DESC LIMIT 3";
            $result = $conn->query($sql);
            $artist_id = '';  // varibale is used for load more button
            $lat = '';
            $lng = '';
            $address = '';

            if ($result->num_rows > 0) {
                // output data of each row
                while ($row = $result->fetch_assoc()) {

                    $id = $row['id'];  // id for detail page of artist
                    $artist_name = $row['firstname'];
                    $fb_id = $row['fb_id'];
                    $location = $row['location']; //storing latlong value into a variable
                    ?>


                    <!-- FIRST ARTIST HARDCODED -->
                    <div class="col-sm-4">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                    <div class="wpb_wrapper">

                                        <a href="Artist/artist_details.php/<?php echo friendlyURL($id, $artist_name); ?>">

                                            <img src="<?php get_artist_profile_image($conn, $id, $fb_id); ?>"
                                                 alt="Your Masterpieces || <?php echo $row['firstname']; ?>"
                                                 class="img_circle img_artist">
                                        </a>

                                        <h3 class="artist_name"> <?php echo $row['firstname']; ?> </h3>
                                        <div class="ws-separator-small"></div>
                                        <h5 class="location">

                                            <?php
                                            get_artist_location($location);
                                            ?>
                                        </h5>
                                        <br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php }
            } ?>
            <!--FIRST ARTIST HARDCODED END -->
        </div>
    </div>
</section>
<!-- FEATURED ARTIST Collection End  -->


<!-- FEATURED PAINTING Collection Start  -->
<section class="ws-works-section">
    <div class="container">
        <div class="row">
            <div class="ws-works-title">
                <div class="col-sm-12">
                    <h3>Featured Paintings</h3>
                    <div class="ws-separator"></div>
                </div>
            </div>

            <!-- Item -->
            <?php
            //$sql = "SELECT * FROM tasks WHERE id='1562' OR id='587' OR id='803' OR id='144' OR id='898' OR id='532' OR id='166' OR id='411' OR id='478' ORDER BY id DESC"; //fetching 12 Result Only from Users Table

            $sql = "SELECT * FROM `tasks` Where created_at BETWEEN '2018-02-01' AND '2018-04-01' ORDER BY tot_likes DESC LIMIT 3";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while ($row = $result->fetch_assoc()) {
                    $product_id = $row['id'];
                    $art_image = $row['image_path'];
                    $art_name = $row['task'];
                    $art_description = $row['task_description'];
                    $art_technique = $row['technique'];
                    $art_price = $row['price'];
                    ?>

                    <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.1s, ease-in 20px'>

                        <!-- LIKE SECTION START -->
                        <div class="pull-right for_like">
                            <?php if (isset($_SESSION['login_user'])) { // Checking Session user
                                $user_id = $_SESSION['login_user'];
                                $db->hasUserLiked($user_id, $product_id); // Checking that user has liked painting or not
                                if (($db->hasUserLiked($user_id, $product_id)) == true) {
                                    echo "<a class='go_dislike' href='javascript:void(0)' data-id='$product_id'><i class=\"fa fa-heart heart\" ></i></a>";
                                } else {
                                    echo "<a class='go_like' href='javascript:void(0)' data-id='$product_id'><i class=\"fa fa-heart-o heart\" ></i></a>";
                                }
                            } else { //When user is not logined
                                echo " <a class='login-required' href='#login-modal-shortlist' data-toggle='modal' ><i class=\"fa fa-heart-o heart\" ></i></a>";
                            }
                            ?>
                            <span class="countlike<?php echo $product_id ?>"
                                  id="countlike"><?php $db->countlikesArt($product_id); ?></span>
                        </div>
                        <!-- LIKE SECTION END -->

                        <a href="Paintings/painting_details.php/<?php echo friendlyURL($product_id, $art_name); ?>">

                            <div class="ws-item-offer product_background">


                                <!-- Image -->
                                <figure>


                                    <img src="<?php echo $art_image; ?>"
                                         alt="Your Masterpieces || <?php echo $art_name; ?>"
                                         class="img-responsive margin_set">
                                </figure>
                            </div>

                            <div class="ws-works-caption text-center">
                                <!-- Item Category -->
                                <div class="ws-item-category one_line"><?php echo $art_technique; ?></div>
                                <!-- Title -->
                                <h3 class="ws-item-title font_17"><?php echo $art_name; ?></h3>
                                <div class="ws-item-separator"></div>
                                <!-- Price -->
                                <div class="ws-item-price">
                                    <?php get_painting_price($art_price); ?>
                                </div>
                            </div>
                        </a>
                    </div>
                    <br>
                <?php }
            } ?>
        </div>
    </div>
</section>
<!-- FEATURED PAINTING Collection End  -->


<!-- Painting about cateogry Section Start -->
<section class="ws-about-section" id="cateogry">
    <div class="container">
        <div class="row">

            <!-- Description -->
            <div class="ws-about-content clearfix">
                <div class="col-sm-8 col-sm-offset-2">
                    <h3>PAINTINGS BY Category</h3>
                    <div class="ws-separator"></div>
                    <p>Explore different mediums and techniques</p>
                </div>
            </div>


            <!-- Featured Collections -->
            <div class="ws-featured-collections clearfix">

                <!-- Item -->
                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                    <a onclick="cateogry('water')" style="cursor: pointer">
                        <div class="thumbnail">
                            <!--<img src="assets/img/backgrounds/water.jpg" alt="Masterpieces Buy Painting Online">-->
                            <div class="ws-overlay">
                                <div class="caption">
                                    <h3>WaterColor</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- Item -->
                <div class="col-sm-3 col-md-3 col-xs-6  featured-collections-item">
                    <a onclick="cateogry('acrylic')" style="cursor: pointer">
                        <div class="thumbnail">
                            <!-- <img src="assets/img/backgrounds/acrylic.jpg" alt="Masterpieces Buy Painting Online">-->
                            <div class="ws-overlay">
                                <div class="caption">
                                    <h3>Acrylic</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- Item -->
                <div class="col-sm-3 col-md-3 col-xs-6  featured-collections-item">
                    <a onclick="cateogry('oil')" style="cursor: pointer">
                        <div class="thumbnail">
                            <!--<img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                            <div class="ws-overlay">
                                <div class="caption">
                                    <h3>Oil</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- Item -->
                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                    <a onclick="cateogry('pencil')" style="cursor: pointer">
                        <div class="thumbnail">
                            <!--<img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                            <div class="ws-overlay">
                                <div class="caption">
                                    <h3 style="padding-bottom: 5px">Pencil</h3>
                                    <h3>& Charcoal</span></h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- Item -->
                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                    <a onclick="cateogry('graphite')" style="cursor: pointer">
                        <div class="thumbnail">
                            <!-- <img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                            <div class="ws-overlay">
                                <div class="caption">
                                    <h3>Graphite</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- Item -->
                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                    <a onclick="cateogry('top')" style="cursor: pointer">
                        <div class="thumbnail">
                            <!-- <img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                            <div class="ws-overlay">
                                <div class="caption">
                                    <h3>Top Paintings</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- Item -->
                <!--<div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                    <a onclick="cateogry('popular')" style="cursor: pointer">
                        <div class="thumbnail">
                            <div class="ws-overlay">
                                <div class="caption">
                                    <h3>Popular</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>-->

                <!-- Item -->
                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                    <a onclick="cateogry('2016')" style="cursor: pointer">
                        <div class="thumbnail">
                            <!-- <img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                            <div class="ws-overlay">
                                <div class="caption">
                                    <h3>2016 Paintings</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- Item -->
                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                    <a onclick="cateogry('2017')" style="cursor: pointer">
                        <div class="thumbnail">
                            <!--<img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                            <div class="ws-overlay">
                                <div class="caption">
                                    <h3>2017 Paintings</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- Item -->
                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                    <a onclick="cateogry('2018')" style="cursor: pointer">
                        <div class="thumbnail">
                            <!-- <img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                            <div class="ws-overlay">
                                <div class="caption">
                                    <h3>2018 Paintings</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>


            </div>

        </div>
    </div>
</section>
<!-- Painting Catteogry Section End -->


<!-- New Arrivals Section -->
<section class="ws-arrivals-section border_none">
    <div class="ws-works-title clearfix">
        <div class="col-sm-12">
            <h3>Latest Paintings</h3>
            <div class="ws-separator"></div>
        </div>
    </div>

    <div id="ws-items-carousel" style="">
        <?php
        $sql = "SELECT * FROM tasks WHERE status!=3 ORDER BY id DESC LIMIT 12"; //fetching 12 Result Only from Users Table
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $product_id = $row['id'];
                $art_image = $row['image_path'];
                $art_name = $row['task'];
                $art_description = $row['task_description'];
                $art_technique = $row['technique'];
                $art_price = $row['price'];
                ?>
                <!-- New Arrivals-->
                <div class="ws-works-item new_arts" data-sr='wait 0.1s, ease-in 20px'>
                    <a href="Paintings/painting_details.php/<?php echo friendlyURL($product_id, $art_name); ?>">
                        <div class="ws-item-offer ">
                            <!-- Image -->
                            <figure>
                                <img src="<?php echo $art_image; ?>" alt="Your Masterpieces<?php echo $art_name; ?>"
                                     class="new_arrival">
                            </figure>
                        </div>

                        <div class="ws-works-caption text-center">
                            <!-- Item Category -->
                            <div class="ws-item-category"><?php echo $art_technique; ?></div>
                            <!-- Title -->
                            <h3 class="ws-item-title"><?php echo $art_name; ?></h3>

                            <div class="ws-item-separator"></div>

                            <!-- Price -->
                            <div class="ws-item-price">
                                <ins>
                                    <?php get_painting_price($art_price); ?>
                                </ins>
                            </div>
                        </div>
                    </a>
                </div>
            <?php }
        } ?>

    </div>


    <div class="ws-more-btn-holder col-sm-12">
        <div id="remove_row">
            <a class="btn ws-more-btn" name="btn_more" id="latest_painting" onclick="cateogry('latest')">See All Latest
                Paintings
            </a>
        </div>
    </div>

    <script type="text/javascript">
        function cateogry(typeCat) {
            page_viewed_counter(typeCat);
        }
    </script>

    <script type="text/javascript">
        function page_viewed_counter(typeCat) {
            var n = localStorage.getItem('type');
            n = typeCat;
            localStorage.setItem("type", n);
            var page_viewed = localStorage.type;
            if (page_viewed == typeCat) {
                window.location = "Paintings/paintings.php";
            }
        }
    </script>


</section>
<!-- End New Arrivals Section -->




<!--login for like -->
<?php include './layout/login_modal.php' ?>
<!-- Login For Like End -->

<!-- Subscribe Section -->
<?php include 'layout/subscriber.php'; ?>
<!-- End Subscribe Section -->

<!-- Footer Start -->


<?php include './layout/footer.php'; ?>


<!-- Footer Bar End -->

<!-- Sricpts -->


<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/frontend/cart-fragments.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js'></script>
<script type='text/javascript'
        src="<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8"></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>
<script src="<?php echo $webroot ?>/Paintings/js/like.js" type="text/javascript"></script>

<script src="assets/js/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/js/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
<script src="assets/js/plugins/parallax.min.js"></script>
<script src="assets/js/plugins/scrollReveal.min.js"></script>
<script src="assets/js/plugins/bootstrap-dropdownhover.min.js"></script>
<script src="assets/js/main.js"></script>
<script src="Paintings/js/like.js"></script>

</body>
</html>