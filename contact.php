<?php session_start(); ?>
<?php require_once './layout/db.php'; ?>
<?php require_once './Artist/lat_long_conversion.php' ?>
<?php require_once './layout/functions.php'; ?>
<?php require_once './auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler(); ?>

<!DOCTYPE html>
<html lang="en-US">
<head>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php include  'google-analytics.php' ; ?>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">


    <link rel="shortcut icon" href="<?php echo $webroot?>/images/favicon.png"
          type="image/x-icon">


    <title>Your Masterpieces - Contact Us </title>


    <!-- This site is optimized with the Yoast SEO plugin v5.1 - https://yoast.com/wordpress/plugins/seo/ -->
    <meta charset="UTF-8"/>
    <meta name="description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings.">
    <meta name="keywords"
          content="Paintings,Your Masterpieces, Non Digital Art, Buy paintings, Sell paintings, Sell Art, Buy Art, Bulk paintings buying, Foreign paintings, Art collectors, Collect Art, Buy painting online, Sell painting online, Painter profile, Artist profile"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <meta property="og:title" content="Your Masterpieces"/>
    <meta property="og:type" content=""/>
    <meta property="og:url" content="<?php echo $webroot; ?>"/>
    <meta property="og:image" content="http://www.yourmasterpieces.com/images/favicon.png"/>
    <meta property="og:site_name" content="Your Masterpieces"/>
    <meta property="fb:admins" content="Your Masterpieces"/>
    <meta property="og:locale" content="en_us"/>
    <meta property="og:description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings."/>
    <link rel="canonical" href="<?php echo $webroot; ?>"/>

    <!-- / Yoast SEO plugin. -->



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>


    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }

        div.wpcf7 .ajax-loader.is-active {
            visibility: hidden !important;
        }
        .fbicon{

            background:#333333;
            color:white;
            text-align: center;
            padding: 7px 0 2px;
            border-radius: 50px;
            width:30px;
            height:30px;
        }
        .fbicon:hover{
            background: #3b5998;
        }

        .instaicon {
            background: #333333;
            color: white;
            text-align: center;
            padding: 7px 0 2px;
            border-radius: 50px;
            width: 30px;
            height: 30px;
            margin-left: 15px;
        }
        .instaicon:hover{
            background: #f09433;
            background: -moz-linear-gradient(45deg, #f09433 0%, #e6683c 25%, #dc2743 50%, #cc2366 75%, #bc1888 100%);
            background: -webkit-linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%);
            background: linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f09433', endColorstr='#bc1888',GradientType=1 );

        }

        .instaimg {
            width: 60px;
            height: 40px;
        }
    </style>


    <?php include './Artist/artist_page_external_style.php'; ?>


</head>
<body class="page-template page-template-template-page page-template-template-page-php page page-id-566 wpb-js-composer js-comp-ver-4.12.1 vc_responsive currency-usd">

<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

<!-- Loader Start -->
<div id="preloader">
    <div class="preloader-container">
        <div class="ws-spinner"></div>
    </div>
</div>
<!-- End Loader Start -->


<!-- Top Bar Start -->

<?php include './layout/header.php'; ?>
<!-- End Header -->

<!-- Page Parallax Header -->
<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="<?php echo $webroot?>/images/masterpiece_contact.jpg"
     data-img-alt="Your Masterpieces">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>Contact Us</h1>
            </div>
        </div>
    </div>
</div>
<!-- End Page Parallax Header -->


<!-- Container Start -->
<div class="container ws-page-container">
    <!-- Row Start -->
    <div class="row">

        <div class="col-sm-12">


            <article id="post-693" class="post-693 page type-page status-publish hentry">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <div class="ws-contact-info">

                                            <h2>Business Development Office</h2>
                                            <p style="color:#C2A476;">Mumbai, Vancouver, Singapore</p>
                                            <p>&nbsp;</p>

                                            <h2>Technology & Head Office</h2>
                                            <p style="color:#C2A476;">Mumbai, India</p>
                                            <p>&nbsp;</p>

                                            <h2>Email Us </h2>
                                            <p><a href="mailto:contact@yourmasterpieces.com">contact@yourmasterpieces.com</a></p>
                                            <p><a href="mailto:sales@yourmasterpieces.com">sales@yourmasterpieces.com</a></p>
                                            <p><a href="mailto:info@yourmasterpieces.com">info@yourmasterpieces.com</a></p>


                                            <p>&nbsp;</p>

                                            <h2>Career</h2>
                                            <p><a href="mailto:career@yourmasterpieces.com">career@yourmasterpieces.com</a></p>
                                            <p>&nbsp;</p>


                                            <h2>Connect with us</h2>
                                            <p>
                                                <a href="https://www.facebook.com/yourmasterpieces/"><i
                                                            class="fa fa-facebook fbicon"></i></a>

                                                <a href="" id="insta"><img src='images/insta.svg' class='instaimg' style="display: none"> <i class="fa fa-instagram instaicon"></i></a>
                                            </p>
                                            <p>&nbsp;</p>

                                            <script type="text/javascript">

                                               /* $('#insta').hover(

                                                    function () {
                                                       $('.instaimg').show();
                                                       $('.instaicon').hide();


                                                    },

                                                    function () {
                                                        $('.instaimg').hide();
                                                        $('.instaicon').show();
                                                    }
                                                );


*/

                                              /*  $('#insta').mouseenter(function () {
                                                    $('#insta').html("<i class='fa fa-instagram instaicon'></i>");

                                                });

                                                $('#insta').mouseout(function () {

                                                        $('#insta').find('.instaicon').toggle('hide');
                                                        $('#insta').html("<img src='images/insta.svg' class='instaimg'></img>");

                                                    }
                                                )
                                                ;

*/                                            </script>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div role="form" class="wpcf7" id="" lang="en-US" dir="ltr">
                                    <div class="screen-reader-response"></div>
                                    <form action="contact_submit.php" method="post"
                                          class="wpcf7-form form-horizontal ws-contact-form">

                                        <div class="row ws-contact-form">
                                            <div class="col-sm-6">
                                                <span class="wpcf7-form-control-wrap text-1"><input type="text"
                                                                                                    name="fname"
                                                                                                    value="" size="40"
                                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"
                                                                                                    aria-required="true"
                                                                                                    aria-invalid="false"
                                                                                                    placeholder="First Name"
                                                                                                    required="required"
                                                    /></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <span class="wpcf7-form-control-wrap text-2"><input type="text"
                                                                                                    name="lname"
                                                                                                    value="" size="40"
                                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"
                                                                                                    aria-required="true"
                                                                                                    aria-invalid="false"
                                                                                                    placeholder="Last Name"
                                                                                                    required="required"
                                                    /></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <span class="wpcf7-form-control-wrap email-1"><input type="email"
                                                                                                     name="email"
                                                                                                     value="" size="40"
                                                                                                     class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"
                                                                                                     aria-required="true"
                                                                                                     aria-invalid="false"
                                                                                                     placeholder="Email"
                                                                                                     required="required"
                                                    /></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <span class="wpcf7-form-control-wrap text-3"><input type="text"
                                                                                                    name="subject"
                                                                                                    value="" size="40"
                                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"
                                                                                                    aria-required="true"
                                                                                                    aria-invalid="false"
                                                                                                    placeholder="Subject"
                                                                                                    required="required"
                                                    /></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <span class="wpcf7-form-control-wrap textarea-1"><textarea
                                                            name="message" cols="40" rows="10"
                                                            class="wpcf7-form-control wpcf7-textarea form-control"
                                                            aria-invalid="false"
                                                            placeholder="Message"></textarea></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <input type="submit" value="Send"
                                                       class="wpcf7-form-control wpcf7-submit ws-big-btn"
                                                       id="contact"/>
                                            </div>
                                        </div>
                                        <div class="wpcf7-response-output" id="response">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>



        </div>


    </div><!-- Row End -->
</div><!-- Container End -->



<div id="woss_cf7_widget-3" class="widget follow-widget widget-space">        <!-- Subscribe Section -->

    <!-- SUBSCRIBE SECTION START -->
    <?php include './layout/subscriber.php'; ?>
    <!-- End Subscribe Section -->
    <!-- End Subscribe Section -->
</div>



<?php include './layout/footer.php'; ?>
<!-- Footer Bar End -->

<div id="fb-root"></div>
</body>
</html>


<script type = 'text/javascript' src = '<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8' ></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>