<?php session_start(); ?>
<?php require_once './layout/db.php'; ?>
<?php require_once './Artist/lat_long_conversion.php' ?>
<?php require_once './layout/functions.php'; ?>
<?php require_once './auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler(); ?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">


    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png"
          type="image/x-icon">


    <title>Policy - masterpiece</title>

    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }

        .link {
            color: #C2A476;
        }

        .link:hover {
            color: #353535;
        }
    </style>
    <?php include './Artist/artist_page_external_style.php'; ?>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/google-analytics.php'; ?>
</head>
<body class="page-template page-template-template-page page-template-template-page-php page page-id-635 wpb-js-composer js-comp-ver-4.12.1 vc_responsive currency-usd">

<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

<!-- Loader Start -->
<div id="preloader">
    <div class="preloader-container">
        <div class="ws-spinner"></div>
    </div>
</div>
<!-- End Loader Start -->


<!-- Top Bar Start -->

<?php include './layout/header.php'; ?>
<!-- End Header -->

<!-- Page Parallax Header -->
<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="<?php echo $webroot ?>/images/masterpiece_policy.jpg">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>Policy</h1>
            </div>
        </div>
    </div>
</div>
<!-- End Page Parallax Header -->

<!-- Container Start -->
<div class="container ws-page-container">

    <!-- Row Start -->
    <div class="row">

        <div class="col-sm-12">


            <article id="post-635" class="post-635 page type-page status-publish hentry">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-8">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p>
                                            At Masterpiece, we value the trust you place in us. That is why we insist
                                            upon the
                                            highest standards for security of transactions and privacy of your
                                            information. Please
                                            feel free to contact us at
                                            <a class="link"
                                               href="mailto:contact@yourmasterpieces.com">contact@yourmasterpieces.com</a>
                                            if you have any questions or
                                            suggestions regarding our <a
                                                    href="http://www.yourmasterpieces.com/policy.php" class="link">Privacy
                                                and Policy </a>.
                                        </p>
                                    </div>
                                </div>
                                <div id='1475937956766-f0548986-ce79'
                                     class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>1. GENERAL</h4><i class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>

                                        <ul>
                                            <li>
                                                Codoc Technologies(<b>“Masterpiece”</b>) is committed to the
                                                protection of personal information
                                                provided by Users to Masterpiece on its website <a
                                                        href="www.yourmasterpieces.com"
                                                        class="link">www.masterpiece.com</a>
                                                (“Website”) . Users agree that
                                                use of Website implies consent to the collection, retention and use of
                                                Users’ personal information in
                                                accordance with the terms of this <a
                                                        href="http://www.yourmasterpieces.com/policy.php" class="link">Privacy
                                                    and Policy </a> (“<a
                                                        href="http://www.yourmasterpieces.com/policy.php" class="link">Privacy
                                                    and Policy </a>”).
                                            </li>
                                            <li>
                                                No User information is rented or sold by Masterpiece to any third party.
                                                When a User uses the Services
                                                of masterpiece, Users’ IP address and other personal information may
                                                automatically get stored. A high
                                                standard of security is maintained by Masterpiece for Users. However,
                                                the transmission of information
                                                via the internet and web networks is not completely secure. While
                                                Masterpiece does its best to protect
                                                Users’ information, particularly with respect to protection of Users’
                                                personal data, Masterpiece cannot
                                                ensure the security of Users’ data transmitted via the internet, web
                                                network or any other networks.
                                            </li>
                                        </ul>
                                        </p>

                                    </div>
                                </div>
                                <div id='1475938373224-675eda39-4057'
                                     class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>2. INFORMATION COLLECTED </h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>

                                        <ul>
                                            <li>
                                                order to provide Services, Masterpiece automatically tracks and collects
                                                the following categories of
                                                information when a User uses its Services:
                                                (a) IP addresses
                                                (b) Domain servers, and,
                                                (c) Other information associated with the interaction of Users device
                                                and Services

                                            </li>
                                            <li>
                                                We use your Personal Information (in some cases, in conjunction with
                                                your Non-Identifying Information)
                                                primarily to provide and improve the Services, to complete your
                                                transactions and respond to your
                                                inquiries.
                                            </li>
                                            <li>
                                                We also use your Personal Information to contact you with Masterpiece
                                                newsletters, marketing or
                                                promotional materials and other information that may be of interest to
                                                you. If you decide at any time
                                                that you no longer wish to receive such communications from us, please
                                                follow the unsubscribe
                                                instructions provided in any of the communications or via the “Account”
                                                section of the website.

                                            </li>
                                        </ul>
                                        </p>
                                    </div>
                                </div>
                                <div id='1475939005760-ac3d531c-1d2c'
                                     class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>3. DISCLOSURE OF PERSONAL INFORMATION</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>
                                        <ul>
                                            <li>
                                                Masterpiece does not disclose User’s Personal Information to any third
                                                parties other than to Masterpiece’s officers, employees, agents,
                                                consultants
                                                and affiliates.
                                            </li>
                                            <li>
                                                Masterpiece reserves the right to disclose Personal Information if
                                                required
                                                to do so by law or if Masterpiece believes that it is necessary to do so
                                                to
                                                protect and defend the rights, property and/or personal safety of
                                                Masterpiece, the Services, or other Users.
                                            </li>
                                            <li>
                                                Masterpiece does not guarantee that there will be no unintended
                                                disclosures
                                                of Personal Information. If Masterpiece becomes aware that certain
                                                Personal
                                                Information has been disclosed in a manner not in accordance with this
                                                <a href="http://www.yourmasterpieces.com/policy.php" class="link">Privacy
                                                    and Policy </a>, Masterpiece will use reasonable efforts to notify
                                                the
                                                concerned User of the nature and extent of such disclosure (to the
                                                extent
                                                Masterpiece is aware of that information) as soon as reasonably possible
                                                and
                                                as permitted by law.
                                            </li>

                                        </ul>

                                        </p>
                                    </div>
                                </div>
                                <div id='1475939129262-82f50325-eb94'
                                     class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>4. CONFIDENTIALITY AND SECURITY</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>

                                            Except as otherwise provided in this <a
                                                    href="http://www.yourmasterpieces.com/policy.php" class="link">Privacy
                                                and Policy </a>, Masterpiece will keep
                                            all Personal Information private and will not share it with third parties,
                                            unless Masterpiece believes in good faith that disclosure of such Personal
                                            Information or any other information Masterpiece collects about Users is
                                            necessary for Permitted Use, or to:
                                            <br>(a) Comply with a court order or other legal process;
                                            <br>(b) Protect the rights, property or safety of Masterpiece or another
                                            party;
                                            <br>(c) Enforce the Agreement, including Terms; and/or;
                                            <br>(d) Respond to claims in relation to violation of rights of
                                            third-parties by Masterpiece


                                        </p>
                                    </div>
                                </div>
                                <div id='1475939403625-a71a8f17-ee40'
                                     class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>5. OTHER RIGHTS</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>
                                        <ul><br/>
                                            <li>
                                                Users have the right to correct any errors in User’s Personal
                                                Information
                                                available with Masterpiece. A User may request Masterpiece in writing
                                                that
                                                Masterpiece cease to use User’s Personal Information. Masterpiece may
                                                stop
                                                providing Services to such a User, if so required.
                                            </li>
                                        </ul>
                                        </p>
                                    </div>
                                </div>
                                <div id='1475989746125-5b56387b-df6a'
                                     class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>6. UPDATES AND CHANGES TO
                                            PRIVACY
                                            AND POLICY </h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>Masterpiece reserves the right, at any time, to
                                            add to, change, update, or modify this <a
                                                    href="http://www.yourmasterpieces.com/policy.php" class="link">Privacy
                                                and Policy </a>. Users are requested
                                            to review this <a href="http://www.yourmasterpieces.com/policy.php"
                                                              class="link">Privacy and Policy </a> frequently. If any
                                            changes are incorporated in
                                            this <a href="http://www.yourmasterpieces.com/policy.php" class="link">Privacy
                                                and Policy </a>, Masterpiece will post such changes on this page.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                </div>
            </article>


        </div>


    </div><!-- Row End -->
</div><!-- Container End -->


<!-- Footer Start -->

<?php include './layout/footer.php'; ?>
<!-- Footer Bar End -->


<div id="fb-root"></div>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>


<script type='text/javascript'>
    function vc_js() {
        vc_toggleBehaviour(), vc_tabsBehaviour(), vc_accordionBehaviour(), vc_teaserGrid(), vc_carouselBehaviour(), vc_slidersBehaviour(), vc_prettyPhoto(), vc_googleplus(), vc_pinterest(), vc_progress_bar(), vc_plugin_flexslider(), vc_google_fonts(), vc_gridBehaviour(), vc_rowBehaviour(), vc_googleMapsPointer(), vc_ttaActivation(), jQuery(document).trigger("vc_js"), window.setTimeout(vc_waypoints, 500)
    }
    function getSizeName() {
        var screen_w = jQuery(window).width();
        return screen_w > 1170 ? "desktop_wide" : screen_w > 960 && 1169 > screen_w ? "desktop" : screen_w > 768 && 959 > screen_w ? "tablet" : screen_w > 300 && 767 > screen_w ? "mobile" : 300 > screen_w ? "mobile_portrait" : ""
    }
    function loadScript(url, $obj, callback) {
        var script = document.createElement("script");
        script.type = "text/javascript", script.readyState && (script.onreadystatechange = function () {
            ("loaded" === script.readyState || "complete" === script.readyState) && (script.onreadystatechange = null, callback())
        }), script.src = url, $obj.get(0).appendChild(script)
    }
    function vc_ttaActivation() {
        jQuery("[data-vc-accordion]").on("show.vc.accordion", function (e) {
            var $ = window.jQuery, ui = {};
            ui.newPanel = $(this).data("vc.accordion").getTarget(), window.wpb_prepare_tab_content(e, ui)
        })
    }
    function vc_accordionActivate(event, ui) {
        if (ui.newPanel.length && ui.newHeader.length) {
            var $pie_charts = ui.newPanel.find(".vc_pie_chart:not(.vc_ready)"),
                $round_charts = ui.newPanel.find(".vc_round-chart"), $line_charts = ui.newPanel.find(".vc_line-chart"),
                $carousel = ui.newPanel.find('[data-ride="vc_carousel"]');
            "undefined" != typeof jQuery.fn.isotope && ui.newPanel.find(".isotope, .wpb_image_grid_ul").isotope("layout"), ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
                var grid = jQuery(this).data("vcGrid");
                grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
            }), vc_carouselBehaviour(ui.newPanel), vc_plugin_flexslider(ui.newPanel), $pie_charts.length && jQuery.fn.vcChat && $pie_charts.vcChat(), $round_charts.length && jQuery.fn.vcRoundChart && $round_charts.vcRoundChart({reload: !1}), $line_charts.length && jQuery.fn.vcLineChart && $line_charts.vcLineChart({reload: !1}), $carousel.length && jQuery.fn.carousel && $carousel.carousel("resizeAction"), ui.newPanel.parents(".isotope").length && ui.newPanel.parents(".isotope").each(function () {
                jQuery(this).isotope("layout")
            })
        }
    }
    function initVideoBackgrounds() {
        return window.console && window.console.warn && window.console.warn("this function is deprecated use vc_initVideoBackgrounds"), vc_initVideoBackgrounds()
    }
    function vc_initVideoBackgrounds() {
        jQuery(".vc_row").each(function () {
            var youtubeUrl, youtubeId, $row = jQuery(this);
            $row.data("vcVideoBg") ? (youtubeUrl = $row.data("vcVideoBg"), youtubeId = vcExtractYoutubeId(youtubeUrl), youtubeId && ($row.find(".vc_video-bg").remove(), insertYoutubeVideoAsBackground($row, youtubeId)), jQuery(window).on("grid:items:added", function (event, $grid) {
                $row.has($grid).length && vcResizeVideoBackground($row)
            })) : $row.find(".vc_video-bg").remove()
        })
    }
    function insertYoutubeVideoAsBackground($element, youtubeId, counter) {
        if ("undefined" == typeof YT.Player)return counter = "undefined" == typeof counter ? 0 : counter, counter > 100 ? void console.warn("Too many attempts to load YouTube api") : void setTimeout(function () {
            insertYoutubeVideoAsBackground($element, youtubeId, counter++)
        }, 100);
        var $container = $element.prepend('<div class="vc_video-bg vc_hidden-xs"><div class="inner"></div></div>').find(".inner");
        new YT.Player($container[0], {
            width: "100%",
            height: "100%",
            videoId: youtubeId,
            playerVars: {
                playlist: youtubeId,
                iv_load_policy: 3,
                enablejsapi: 1,
                disablekb: 1,
                autoplay: 1,
                controls: 0,
                showinfo: 0,
                rel: 0,
                loop: 1,
                wmode: "transparent"
            },
            events: {
                onReady: function (event) {
                    event.target.mute().setLoop(!0)
                }
            }
        }), vcResizeVideoBackground($element), jQuery(window).bind("resize", function () {
            vcResizeVideoBackground($element)
        })
    }
    function vcResizeVideoBackground($element) {
        var iframeW, iframeH, marginLeft, marginTop, containerW = $element.innerWidth(),
            containerH = $element.innerHeight(), ratio1 = 16, ratio2 = 9;
        ratio1 / ratio2 > containerW / containerH ? (iframeW = containerH * (ratio1 / ratio2), iframeH = containerH, marginLeft = -Math.round((iframeW - containerW) / 2) + "px", marginTop = -Math.round((iframeH - containerH) / 2) + "px", iframeW += "px", iframeH += "px") : (iframeW = containerW, iframeH = containerW * (ratio2 / ratio1), marginTop = -Math.round((iframeH - containerH) / 2) + "px", marginLeft = -Math.round((iframeW - containerW) / 2) + "px", iframeW += "px", iframeH += "px"), $element.find(".vc_video-bg iframe").css({
            maxWidth: "1000%",
            marginLeft: marginLeft,
            marginTop: marginTop,
            width: iframeW,
            height: iframeH
        })
    }
    function vcExtractYoutubeId(url) {
        if ("undefined" == typeof url)return !1;
        var id = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
        return null !== id ? id[1] : !1
    }
    function vc_googleMapsPointer() {
        var $ = window.jQuery, $wpbGmapsWidget = $(".wpb_gmaps_widget");
        $wpbGmapsWidget.click(function () {
            $("iframe", this).css("pointer-events", "auto")
        }), $wpbGmapsWidget.mouseleave(function () {
            $("iframe", this).css("pointer-events", "none")
        }), $(".wpb_gmaps_widget iframe").css("pointer-events", "none")
    }
    document.documentElement.className += " js_active ", document.documentElement.className += "ontouchstart" in document.documentElement ? " vc_mobile " : " vc_desktop ", function () {
        for (var prefix = ["-webkit-", "-moz-", "-ms-", "-o-", ""], i = 0; i < prefix.length; i++)prefix[i] + "transform" in document.documentElement.style && (document.documentElement.className += " vc_transform ")
    }(), "function" != typeof window.vc_plugin_flexslider && (window.vc_plugin_flexslider = function ($parent) {
        var $slider = $parent ? $parent.find(".wpb_flexslider") : jQuery(".wpb_flexslider");
        $slider.each(function () {
            var this_element = jQuery(this), sliderSpeed = 800,
                sliderTimeout = 1e3 * parseInt(this_element.attr("data-interval")),
                sliderFx = this_element.attr("data-flex_fx"), slideshow = !0;
            0 === sliderTimeout && (slideshow = !1), this_element.is(":visible") && this_element.flexslider({
                animation: sliderFx,
                slideshow: slideshow,
                slideshowSpeed: sliderTimeout,
                sliderSpeed: sliderSpeed,
                smoothHeight: !0
            })
        })
    }), "function" != typeof window.vc_googleplus && (window.vc_googleplus = function () {
        0 < jQuery(".wpb_googleplus").length && !function () {
            var po = document.createElement("script");
            po.type = "text/javascript", po.async = !0, po.src = "//apis.google.com/js/plusone.js";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(po, s)
        }()
    }), "function" != typeof window.vc_pinterest && (window.vc_pinterest = function () {
        0 < jQuery(".wpb_pinterest").length && !function () {
            var po = document.createElement("script");
            po.type = "text/javascript", po.async = !0, po.src = "//assets.pinterest.com/js/pinit.js";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(po, s)
        }()
    }), "function" != typeof window.vc_progress_bar && (window.vc_progress_bar = function () {
        "undefined" != typeof jQuery.fn.waypoint && jQuery(".vc_progress_bar").waypoint(function () {
            jQuery(this).find(".vc_single_bar").each(function (index) {
                var $this = jQuery(this), bar = $this.find(".vc_bar"), val = bar.data("percentage-value");
                setTimeout(function () {
                    bar.css({width: val + "%"})
                }, 200 * index)
            })
        }, {offset: "85%"})
    }), "function" != typeof window.vc_waypoints && (window.vc_waypoints = function () {
        "undefined" != typeof jQuery.fn.waypoint && jQuery(".wpb_animate_when_almost_visible:not(.wpb_start_animation)").waypoint(function () {
            jQuery(this).addClass("wpb_start_animation")
        }, {offset: "85%"})
    }), "function" != typeof window.vc_toggleBehaviour && (window.vc_toggleBehaviour = function ($el) {
        function event(e) {
            e && e.preventDefault && e.preventDefault();
            var title = jQuery(this), element = title.closest(".vc_toggle"),
                content = element.find(".vc_toggle_content");
            element.hasClass("vc_toggle_active") ? content.slideUp({
                duration: 300, complete: function () {
                    element.removeClass("vc_toggle_active")
                }
            }) : content.slideDown({
                duration: 300, complete: function () {
                    element.addClass("vc_toggle_active")
                }
            })
        }

        $el ? $el.hasClass("vc_toggle_title") ? $el.unbind("click").click(event) : $el.find(".vc_toggle_title").unbind("click").click(event) : jQuery(".vc_toggle_title").unbind("click").on("click", event)
    }), "function" != typeof window.vc_tabsBehaviour && (window.vc_tabsBehaviour = function ($tab) {
        if (jQuery.ui) {
            var $call = $tab || jQuery(".wpb_tabs, .wpb_tour"),
                ver = jQuery.ui && jQuery.ui.version ? jQuery.ui.version.split(".") : "1.10",
                old_version = 1 === parseInt(ver[0]) && 9 > parseInt(ver[1]);
            $call.each(function (index) {
                var $tabs, interval = jQuery(this).attr("data-interval"), tabs_array = [];
                if ($tabs = jQuery(this).find(".wpb_tour_tabs_wrapper").tabs({
                        show: function (event, ui) {
                            wpb_prepare_tab_content(event, ui)
                        }, beforeActivate: function (event, ui) {
                            1 !== ui.newPanel.index() && ui.newPanel.find(".vc_pie_chart:not(.vc_ready)")
                        }, activate: function (event, ui) {
                            wpb_prepare_tab_content(event, ui)
                        }
                    }), interval && interval > 0)try {
                    $tabs.tabs("rotate", 1e3 * interval)
                } catch (e) {
                    window.console && window.console.log && console.log(e)
                }
                jQuery(this).find(".wpb_tab").each(function () {
                    tabs_array.push(this.id)
                }), jQuery(this).find(".wpb_tabs_nav li").click(function (e) {
                    return e.preventDefault(), old_version ? $tabs.tabs("select", jQuery("a", this).attr("href")) : $tabs.tabs("option", "active", jQuery(this).index()), !1
                }), jQuery(this).find(".wpb_prev_slide a, .wpb_next_slide a").click(function (e) {
                    if (e.preventDefault(), old_version) {
                        var index = $tabs.tabs("option", "selected");
                        jQuery(this).parent().hasClass("wpb_next_slide") ? index++ : index--, 0 > index ? index = $tabs.tabs("length") - 1 : index >= $tabs.tabs("length") && (index = 0), $tabs.tabs("select", index)
                    } else {
                        var index = $tabs.tabs("option", "active"), length = $tabs.find(".wpb_tab").length;
                        index = jQuery(this).parent().hasClass("wpb_next_slide") ? index + 1 >= length ? 0 : index + 1 : 0 > index - 1 ? length - 1 : index - 1, $tabs.tabs("option", "active", index)
                    }
                })
            })
        }
    }), "function" != typeof window.vc_accordionBehaviour && (window.vc_accordionBehaviour = function () {
        jQuery(".wpb_accordion").each(function (index) {
            var $tabs, $this = jQuery(this),
                active_tab = ($this.attr("data-interval"), !isNaN(jQuery(this).data("active-tab")) && 0 < parseInt($this.data("active-tab")) ? parseInt($this.data("active-tab")) - 1 : !1),
                collapsible = !1 === active_tab || "yes" === $this.data("collapsible");
            $tabs = $this.find(".wpb_accordion_wrapper").accordion({
                header: "> div > h3",
                autoHeight: !1,
                heightStyle: "content",
                active: active_tab,
                collapsible: collapsible,
                navigation: !0,
                activate: vc_accordionActivate,
                change: function (event, ui) {
                    "undefined" != typeof jQuery.fn.isotope && ui.newContent.find(".isotope").isotope("layout"), vc_carouselBehaviour(ui.newPanel)
                }
            }), !0 === $this.data("vcDisableKeydown") && ($tabs.data("uiAccordion")._keydown = function () {
            })
        })
    }), "function" != typeof window.vc_teaserGrid && (window.vc_teaserGrid = function () {
        var layout_modes = {fitrows: "fitRows", masonry: "masonry"};
        jQuery(".wpb_grid .teaser_grid_container:not(.wpb_carousel), .wpb_filtered_grid .teaser_grid_container:not(.wpb_carousel)").each(function () {
            var $container = jQuery(this), $thumbs = $container.find(".wpb_thumbnails"),
                layout_mode = $thumbs.attr("data-layout-mode");
            $thumbs.isotope({
                itemSelector: ".isotope-item",
                layoutMode: "undefined" == typeof layout_modes[layout_mode] ? "fitRows" : layout_modes[layout_mode]
            }), $container.find(".categories_filter a").data("isotope", $thumbs).click(function (e) {
                e.preventDefault();
                var $thumbs = jQuery(this).data("isotope");
                jQuery(this).parent().parent().find(".active").removeClass("active"), jQuery(this).parent().addClass("active"), $thumbs.isotope({filter: jQuery(this).attr("data-filter")})
            }), jQuery(window).bind("load resize", function () {
                $thumbs.isotope("layout")
            })
        })
    }), "function" != typeof window.vc_carouselBehaviour && (window.vc_carouselBehaviour = function ($parent) {
        var $carousel = $parent ? $parent.find(".wpb_carousel") : jQuery(".wpb_carousel");
        $carousel.each(function () {
            var $this = jQuery(this);
            if (!0 !== $this.data("carousel_enabled") && $this.is(":visible")) {
                $this.data("carousel_enabled", !0);
                var carousel_speed = (getColumnsCount(jQuery(this)), 500);
                jQuery(this).hasClass("columns_count_1") && (carousel_speed = 900);
                var carousele_li = jQuery(this).find(".wpb_thumbnails-fluid li");
                carousele_li.css({"margin-right": carousele_li.css("margin-left"), "margin-left": 0});
                var fluid_ul = jQuery(this).find("ul.wpb_thumbnails-fluid");
                fluid_ul.width(fluid_ul.width() + 300), jQuery(window).resize(function () {
                    var before_resize = screen_size;
                    screen_size = getSizeName(), before_resize != screen_size && window.setTimeout("location.reload()", 20)
                })
            }
        })
    }), "function" != typeof window.vc_slidersBehaviour && (window.vc_slidersBehaviour = function () {
        jQuery(".wpb_gallery_slides").each(function (index) {
            var $imagesGrid, this_element = jQuery(this);
            if (this_element.hasClass("wpb_slider_nivo")) {
                var sliderSpeed = 800, sliderTimeout = 1e3 * this_element.attr("data-interval");
                0 === sliderTimeout && (sliderTimeout = 9999999999), this_element.find(".nivoSlider").nivoSlider({
                    effect: "boxRainGrow,boxRain,boxRainReverse,boxRainGrowReverse",
                    slices: 15,
                    boxCols: 8,
                    boxRows: 4,
                    animSpeed: sliderSpeed,
                    pauseTime: sliderTimeout,
                    startSlide: 0,
                    directionNav: !0,
                    directionNavHide: !0,
                    controlNav: !0,
                    keyboardNav: !1,
                    pauseOnHover: !0,
                    manualAdvance: !1,
                    prevText: "Prev",
                    nextText: "Next"
                })
            } else this_element.hasClass("wpb_image_grid") && (jQuery.fn.imagesLoaded ? $imagesGrid = this_element.find(".wpb_image_grid_ul").imagesLoaded(function () {
                $imagesGrid.isotope({itemSelector: ".isotope-item", layoutMode: "fitRows"})
            }) : this_element.find(".wpb_image_grid_ul").isotope({
                itemSelector: ".isotope-item",
                layoutMode: "fitRows"
            }))
        })
    }), "function" != typeof window.vc_prettyPhoto && (window.vc_prettyPhoto = function () {
        try {
            jQuery && jQuery.fn && jQuery.fn.prettyPhoto && jQuery('a.prettyphoto, .gallery-icon a[href*=".jpg"]').prettyPhoto({
                animationSpeed: "normal",
                hook: "data-rel",
                padding: 15,
                opacity: .7,
                showTitle: !0,
                allowresize: !0,
                counter_separator_label: "/",
                hideflash: !1,
                deeplinking: !1,
                modal: !1,
                callback: function () {
                    var url = location.href, hashtag = url.indexOf("#!prettyPhoto") ? !0 : !1;
                    hashtag && (location.hash = "")
                },
                social_tools: ""
            })
        } catch (err) {
            window.console && window.console.log && console.log(err)
        }
    }), "function" != typeof window.vc_google_fonts && (window.vc_google_fonts = function () {
        return !1
    }), window.vcParallaxSkroll = !1, "function" != typeof window.vc_rowBehaviour && (window.vc_rowBehaviour = function () {
        function fullWidthRow() {
            var $elements = $('[data-vc-full-width="true"]');
            $.each($elements, function (key, item) {
                var $el = $(this);
                $el.addClass("vc_hidden");
                var $el_full = $el.next(".vc_row-full-width");
                if ($el_full.length || ($el_full = $el.parent().next(".vc_row-full-width")), $el_full.length) {
                    var el_margin_left = parseInt($el.css("margin-left"), 10),
                        el_margin_right = parseInt($el.css("margin-right"), 10),
                        offset = 0 - $el_full.offset().left - el_margin_left, width = $(window).width();
                    if ($el.css({
                            position: "relative",
                            left: offset,
                            "box-sizing": "border-box",
                            width: $(window).width()
                        }), !$el.data("vcStretchContent")) {
                        var padding = -1 * offset;
                        0 > padding && (padding = 0);
                        var paddingRight = width - padding - $el_full.width() + el_margin_left + el_margin_right;
                        0 > paddingRight && (paddingRight = 0), $el.css({
                            "padding-left": padding + "px",
                            "padding-right": paddingRight + "px"
                        })
                    }
                    $el.attr("data-vc-full-width-init", "true"), $el.removeClass("vc_hidden")
                }
            }), $(document).trigger("vc-full-width-row", $elements)
        }

        function parallaxRow() {
            var vcSkrollrOptions, callSkrollInit = !1;
            return window.vcParallaxSkroll && window.vcParallaxSkroll.destroy(), $(".vc_parallax-inner").remove(), $("[data-5p-top-bottom]").removeAttr("data-5p-top-bottom data-30p-top-bottom"), $("[data-vc-parallax]").each(function () {
                var skrollrSpeed, skrollrSize, skrollrStart, skrollrEnd, $parallaxElement, parallaxImage, youtubeId;
                callSkrollInit = !0, "on" === $(this).data("vcParallaxOFade") && $(this).children().attr("data-5p-top-bottom", "opacity:0;").attr("data-30p-top-bottom", "opacity:1;"), skrollrSize = 100 * $(this).data("vcParallax"), $parallaxElement = $("<div />").addClass("vc_parallax-inner").appendTo($(this)), $parallaxElement.height(skrollrSize + "%"), parallaxImage = $(this).data("vcParallaxImage"), youtubeId = vcExtractYoutubeId(parallaxImage), youtubeId ? insertYoutubeVideoAsBackground($parallaxElement, youtubeId) : "undefined" != typeof parallaxImage && $parallaxElement.css("background-image", "url(" + parallaxImage + ")"), skrollrSpeed = skrollrSize - 100, skrollrStart = -skrollrSpeed, skrollrEnd = 0, $parallaxElement.attr("data-bottom-top", "top: " + skrollrStart + "%;").attr("data-top-bottom", "top: " + skrollrEnd + "%;")
            }), callSkrollInit && window.skrollr ? (vcSkrollrOptions = {
                forceHeight: !1,
                smoothScrolling: !1,
                mobileCheck: function () {
                    return !1
                }
            }, window.vcParallaxSkroll = skrollr.init(vcSkrollrOptions), window.vcParallaxSkroll) : !1
        }

        function fullHeightRow() {
            var $element = $(".vc_row-o-full-height:first");
            if ($element.length) {
                var $window, windowHeight, offsetTop, fullHeight;
                $window = $(window), windowHeight = $window.height(), offsetTop = $element.offset().top, windowHeight > offsetTop && (fullHeight = 100 - offsetTop / (windowHeight / 100), $element.css("min-height", fullHeight + "vh"))
            }
            $(document).trigger("vc-full-height-row", $element)
        }

        function fixIeFlexbox() {
            var ua = window.navigator.userAgent, msie = ua.indexOf("MSIE ");
            (msie > 0 || navigator.userAgent.match(/Trident.*rv\:11\./)) && $(".vc_row-o-full-height").each(function () {
                "flex" === $(this).css("display") && $(this).wrap('<div class="vc_ie-flexbox-fixer"></div>')
            })
        }

        var $ = window.jQuery;
        $(window).off("resize.vcRowBehaviour").on("resize.vcRowBehaviour", fullWidthRow).on("resize.vcRowBehaviour", fullHeightRow), fullWidthRow(), fullHeightRow(), fixIeFlexbox(), vc_initVideoBackgrounds(), parallaxRow()
    }), "function" != typeof window.vc_gridBehaviour && (window.vc_gridBehaviour = function () {
        jQuery.fn.vcGrid && jQuery("[data-vc-grid]").vcGrid()
    }), "function" != typeof window.getColumnsCount && (window.getColumnsCount = function (el) {
        for (var find = !1, i = 1; !1 === find;) {
            if (el.hasClass("columns_count_" + i))return find = !0, i;
            i++
        }
    });
    var screen_size = getSizeName();
    "function" != typeof window.wpb_prepare_tab_content && (window.wpb_prepare_tab_content = function (event, ui) {
        var $ui_panel, $google_maps, panel = ui.panel || ui.newPanel,
            $pie_charts = panel.find(".vc_pie_chart:not(.vc_ready)"), $round_charts = panel.find(".vc_round-chart"),
            $line_charts = panel.find(".vc_line-chart"), $carousel = panel.find('[data-ride="vc_carousel"]');
        if (vc_carouselBehaviour(), vc_plugin_flexslider(panel), ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
                var grid = jQuery(this).data("vcGrid");
                grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
            }), panel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && panel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
                var grid = jQuery(this).data("vcGrid");
                grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
            }), $pie_charts.length && jQuery.fn.vcChat && $pie_charts.vcChat(), $round_charts.length && jQuery.fn.vcRoundChart && $round_charts.vcRoundChart({reload: !1}), $line_charts.length && jQuery.fn.vcLineChart && $line_charts.vcLineChart({reload: !1}), $carousel.length && jQuery.fn.carousel && $carousel.carousel("resizeAction"), $ui_panel = panel.find(".isotope, .wpb_image_grid_ul"), $google_maps = panel.find(".wpb_gmaps_widget"), 0 < $ui_panel.length && $ui_panel.isotope("layout"), $google_maps.length && !$google_maps.is(".map_ready")) {
            var $frame = $google_maps.find("iframe");
            $frame.attr("src", $frame.attr("src")), $google_maps.addClass("map_ready")
        }
        panel.parents(".isotope").length && panel.parents(".isotope").each(function () {
            jQuery(this).isotope("layout")
        })
    }), "function" != typeof window.vc_googleMapsPointer, jQuery(document).ready(function ($) {
        window.vc_js()
    });


</script>

</body>
</html>

