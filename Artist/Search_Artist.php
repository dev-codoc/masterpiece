<?php session_start();?>
<?php include '../layout/db.php'; ?>
<?php include 'lat_long_conversion.php'; ?>
<?php include '../layout/functions.php'; ?>

<!-- LAYOUT FOR ARTIST -->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Masterpiece Artist</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../Paintings/search_design.css">
    <link rel="shortcut icon" href="<?php echo $webroot?>/images/favicon.png"
          type="image/x-icon">
    <?php include 'artist_page_external_style.php'; ?>
    <link rel="stylesheet" href="commoncss.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php include $_SERVER['DOCUMENT_ROOT'] .'/google-analytics.php' ; ?>

</head>
<body>

<!--Header File For MAsterpiece -->
<?php include '../layout/header.php'; ?>

<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="<?php echo $webroot?>/images/masterpiece_paint.jpg"
     style="background:rgba(0,0,0,0.4)">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>Artists</h1>
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <nav class="navbar navbar-default">
                                <div class="nav nav-justified navbar-nav">
                                    <form class="navbar-form navbar-search" role="search" method="GET"
                                          action="Search_Artist.php">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="query"
                                                   placeholder=" Search Artist Name">
                                            <div class="input-group-btn">
                                                <input type="submit" value="Search"
                                                       class="btn btn-search btn-info search-paint">
                                                </input>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </nav>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- END HEADING SCROLL ARTIST TEXT -->

<div class="container ws-page-container">
    <div class="row">
        <div class="col-sm-12">

            <article id="post-2478" class="post-2478 page type-page status-publish hentry">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="vc_empty_space" style="height: 35px">
                                        <span class="vc_empty_space_inner">

                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_row-fluid">
                    <?php
                    $query = $_GET['query'];
                    $sql = "SELECT * FROM `users` WHERE totUploaded>0  AND (users.firstname LIKE'" . $query . '%' . "' OR users.bio LIKE'" . '%' . $query . '%' . "' OR  users.name LIKE'" . $query . '%' . "' )"; //fetching 32 Result Only from Users Table
                    //echo $sql;
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                    $count_result = mysqli_num_rows($result); // Total number of Result showing
                    ?>


                    <div class="ws-contact-info" style="text-align: center;margin-top: -35px">
                        <h2>Search result for "<?php echo $query ?>" </h2>
                        <p><a href="javascript:void(0)" style="cursor: default">total <?php echo $count_result ?>
                                result(s) found</a>
                        </p>
                        <br>
                        <br>
                    </div>

                    <?php

                    // output data of each row
                    while ($row = $result->fetch_assoc()) {

                        $id = $row['id'];  // id for detail page of artist
                        $artist_name=$row['name'];

                        if (empty($artist_name)) {
                            if (!empty($row['firstname'])) {
                                $artist_name = $row['firstname'];
                            } else {
                                $artist_name = "artist";
                            }
                        }

                        $facebook_image_id = $row['fb_id'];
                        $city = $row['city'];
                        $country = $row['country'];
                        $location = $row['location']; //storing latlong value into a variable

                        ?>

                        <div class="wpb_column vc_column_container vc_col-sm-3">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                        <div class="wpb_wrapper">

                                            <a href="artist_details.php/<?php echo friendlyURL($id, $artist_name); ?>">
                                                <center>
                                                    <img src="<?php get_artist_profile_image($conn, $id, $facebook_image_id) ?>"
                                                         alt="<?php echo $row['firstname']; ?>"
                                                         class="img_circle img_artist">
                                                </center>
                                            </a>

                                            <h3 class="artist_name"> <?php echo $row['firstname']; ?> </h3>
                                            <div class="ws-separator-small"></div>
                                            <h5 class="location">
                                                <?php if ((empty($city)) && (empty($country))) { ?>
                                                    <?php get_artist_location($location) ?>
                                                <?php } else {
                                                    echo ucwords($city);
                                                    if (!empty($country) && !empty($city)) {
                                                        echo ",";
                                                    }
                                                    echo ucwords($country);
                                                } ?>
                                            </h5>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $last_totLikes_id = $row["totLikes"]; //saving last Artist Id in a varibale
                    }
                    ?>
                </div>


                <div id="load_data_table"></div>

                <div class="ws-more-btn-holder col-sm-12">

                    <!-- <div id="remove_row">
                        <button type="button" name="btn_more" data-vid="<?php /*echo $last_totLikes_id; */ ?>"
                                id="btn_more" class="btn ws-more-btn">LOAD MORE
                        </button>
                    </div>-->

                </div>
                <?php } else {
                    echo "<b style='text-align:center;font-size:25px'><div class='col-md-12' >Sorry No result Found</div></b>";
                }
                ?>
            </article>
        </div>
    </div>


    <br>
    <br>


</div>


<?php include '../layout/footer.php'; ?>

</body>
</html>


<script type = 'text/javascript' src = '<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8' ></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>
