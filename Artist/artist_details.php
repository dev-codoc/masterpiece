<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);
*/ ?>
<?php session_start(); ?>
<?php include '../layout/db.php'; ?>
<?php include '../Artist/lat_long_conversion.php'; ?>
<?php include '../layout/functions.php'; ?>
<?php include '../auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler(); ?>

<!DOCTYPE html>
<html lang="en">
<head>

    <?php
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $link_array = explode('-', $actual_link);
    $artist_detail_id = end($link_array);
    $sql = "SELECT * FROM users where id='$artist_detail_id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $fb_id = $row['fb_id'];
            ?>
            <title><?php echo $row['name'] ?></title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content=" Artist on Your Masterpieces || <?php echo $row['bio']; ?>">
            <meta property="og:image" content="<?php get_artist_profile_image($conn, $artist_detail_id, $fb_id); ?>">

        <?php }
    } ?>

    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png" type="image/x-icon">
    <?php include 'artist_page_external_style.php'; ?>
    <link rel="stylesheet" href="<?php echo $webroot ?>/Artist/assets/css/artist-detail-stylesheet.css">
    <link rel="stylesheet" href="<?php echo $webroot ?>/auth/user/css/profile.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php include '../google-analytics.php'; ?>
</head>
<body>
<!--Header File For Masterpiece -->
<?php include '../layout/header.php'; ?>


<?php
$url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
// URL of page for share PAGE FOR ARTIST
?>

<!-- Container Start -->

<div class="ws-footer" class="img-responsive">
    <?php
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $link_array = explode('-', $actual_link);

    //$link_array=explode ('/',$actual_link);

    $artist_detail_id = end($link_array);

    $sql = "SELECT * FROM users where id='$artist_detail_id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {

    $user_profile_id=$row['id'];
    $facebook = $row['fb_id'];
    $artist_name = $row['firstname']; //Storing name
    $location = $row['location']; //storing latlong value into a variable
    $city = $row['city'];
    $country = $row['country'];
    $tot_uploaded = $row['totUploaded'];
    $tot_created = $row['totCreated'];
    $tot_liked = $row['totLiked'];
    ?>

    <center>
        <img src="<?php get_artist_profile_image($conn, $artist_detail_id, $facebook) ?>"
             class="img-circle-artist-page">
    </center>
    <br/>

    <h2 style="text-align:center; font-family:Montserrat;font-size:22px;text-transform: capitalize"><?php echo $artist_name; ?></h2>
    <center>
        <div class="map-text">
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <b style="color:#C2A476"></b>

            <!-- ARTIST LOCATION START WITH COUNTRY AND CITY -->
            <?php if ((empty($city)) && (empty($country))) { ?>
                <?php get_artist_location($location) ?>
            <?php } else {
                echo ucwords($city);
                if (!empty($country) && !empty($city)) {
                    echo ",";
                }
                echo ucwords($country);
            } ?>
            <!-- ARTIST LOCATION END WITH COUNTRY AND CITY -->
            <br/>
        </div>
        <div class="col-sm-12 ws-footer-col">
            <div id="text-3" class="bar widget-space widget_text">
                <div class="textwidget"
                     style=" padding-right: 25px;padding-left:25px;">
                    <p class="artist_bio_details"><?php echo $row['bio']; ?> </p>

                </div>
            </div>
        </div>
        <br/>
        <?php }
        }; ?>

        <!-- ICON ADDED -->
        <div class="text-center icon-new">
                <span class="uplaod-icon">
                    <img src="<?php echo $webroot ?>/auth/user/images/upload-icon.png" class="icon-trend">
                    <span class="text-trend"><?php echo $db->getTotUploads($user_profile_id); ?></span>
                    <p class="grey-text">Uploaded</p>
                </span>
            <span class="uplaod-icon">
                    <img src="<?php echo $webroot ?>/auth/user/images/created-icon.png" class="icon-trend">
                        <span class="text-trend"><?php echo $db->getTotCreated($user_profile_id); ?></span>
                        <p class="grey-text">Created</p>
                </span>

            <span class="uplaod-icon">
                    <img src="<?php echo $webroot ?>/auth/user/images/like-icon.png" class="icon-trend">
                        <span class="text-trend"><?php echo $db->getTotLikes($user_profile_id); ?></span>
                        <p class="grey-text">Likes</p>
                </span>
        </div>
        <!-- ICONS END -->


        <!-- SHARE LINKS FOR ARTIST PROFILE -->
        <div style="padding-top: 15px">
            <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-icon-color="#333333"
                 style="display: inline-block;margin-top: 15px;"
                 data-a2a-url="<?php echo $url ?>"
                 data-a2a-title="<?php echo $artist_name ?>">
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_whatsapp"></a>
                <a class="a2a_button_twitter"></a>
                <a class="a2a_button_google_plus"></a>
                <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
            </div>
            <p class="share_profile_links">SHARE PROFILE</p>
        </div>

        <script async src="https://static.addtoany.com/menu/page.js"></script>
        <script type="text/javascript">
            var a2a_config = a2a_config || {};
            a2a_config.exclude_services = ["facebook", "twitter", "whatsapp","google_plus"];
        </script>

        <!-- SHARE LINKS FOR ARTIST PROFILE END-->
</div>


<div class="container ws-page-container" style="padding-top:unset !important">
    <div class="row">
        <div class="col-sm-12">
            <article id="post-1786" class="post-1786 page type-page status-publish hentry">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                    <div class="wpb_wrapper">

                                        <div class="vc_row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="woocommerce columns-4">
                                                            <div class="row">
                                                                <ul class="products">

                                                                    <?php
                                                                    //$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; // Fetching id of artist
                                                                    //$artist_detail_id = preg_replace('/[^0-9]/', '', $actual_link);

                                                                    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                                                    $link_array = explode('-', $actual_link);
                                                                    $artist_detail_id = end($link_array);

                                                                    // Storing Artist ID DOn't Confuse
                                                                    $sql = " SELECT t.* FROM tasks t, user_tasks ut WHERE ut.user_id = $artist_detail_id AND t.id = ut.task_id AND status=0";
                                                                    $result = $conn->query($sql);
                                                                    if ($result->num_rows > 0) {
                                                                        // output data of each row
                                                                        while ($row = $result->fetch_assoc()) {
                                                                            $task_id = $row['id'];
                                                                            $art_image = $row['image_path'];
                                                                            $art_name = $row['task'];
                                                                            $art_price = $row['price'];
                                                                            $art_new_price=$row['price_new'];
                                                                            $art_technique = $row['technique'];
                                                                            $tot_likes = $row['tot_likes'];
                                                                            $curr_code = $row['curr_code'];
                                                                            ?>

                                                                            <li class="first  post-<?php echo $task_id; ?> product type-product status-publish has-post-thumbnail product_cat-water-color-painting instock shipping-taxable product-type-simple">

                                                                                <a href="<?php echo $webroot ?>/Paintings/painting_details.php/<?php echo friendlyURL($task_id, $art_name); ?>"
                                                                                   class="woocommerce-LoopProduct-link">
                                                                                    <a href="<?php echo $webroot ?>/Paintings/painting_details.php/<?php echo friendlyURL($task_id, $art_name); ?>"
                                                                                       class="woocommerce-LoopProduct-link">
                                                                                        <figure class="ws-product-bg">
                                                                                            <img width="300"
                                                                                                 height="300"
                                                                                                 src="<?php echo $art_image; ?>"
                                                                                                 class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                                                                 alt=""/>
                                                                                        </figure>
                                                                                        <div class="col-md-9 col-sm-9 col-xs-9 text-left">
                                                                                            <h3><?php echo $art_name; ?></h3>
                                                                                            <span class="ws-item-subtitle"><?php echo $art_technique; ?></span>
                                                                                            <span class="price">
                                                        <span class="woocs_price_code" data-product-id="1318">
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span class="woocommerce-Price-currencySymbol"></span>
                                                                <!-- PRICE IF CURR CODE AVAILBALE -->
                                                                <?php if ((!empty($curr_code)) && (!empty($art_new_price)))
                                                                    echo "$curr_code " . $art_new_price;
                                                                else get_painting_price($art_price);
                                                                ?>
                                                                <!-- END PRICE CODE -->
                                                            </span>
                                                        </span>
                                                    </span>
                                                                                        </div>
                                                                                    </a>

                                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                <span class="countlike<?php echo $task_id ?>" id="countlike">
                                                    <?php $db->countlikesArt($task_id); ?>
                                                </span>

                                                                                        <!-- LIKE SECTION START -->
                                                                                        <div class="pull-right for_like">
                                                                                            <?php if (isset($_SESSION['login_user'])) { // Checking Session user
                                                                                                $user_id = $_SESSION['login_user'];
                                                                                                $db->hasUserLiked($user_id, $task_id); // Checking that user has liked painting or not
                                                                                                if (($db->hasUserLiked($user_id, $task_id)) == true) {
                                                                                                    echo "<a class='go_dislike' href='javascript:void(0)' data-id='$task_id'><i class=\"fa fa-heart heart\" ></i></a>";
                                                                                                } else {
                                                                                                    echo "<a class='go_like' href='javascript:void(0)' data-id='$task_id'><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                                                                }
                                                                                            } else { //When user is not logined
                                                                                                echo " <a class='login-required' href='#login-modal-shortlist' data-toggle='modal' ><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                                                            }
                                                                                            ?>
                                                                                        </div>
                                                                                        <!-- LIKE SECTION END -->
                                                                                    </div>
                                                                                    <!-- LIKE SECTION END -->
                                                                            </li>
                                                                        <?php }
                                                                    }; ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>


<?php include '../layout/footer.php'; ?>


<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/frontend/cart-fragments.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js'></script>
<script type='text/javascript'
        src="<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8"></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>
<script src="<?php echo $webroot ?>/Paintings/js/like.js" type="text/javascript"></script>

<?php include '../layout/login_modal.php' ?>
</body>
</html>
