<?php include '../layout/db.php'; ?>
<?php include '../layout/functions.php'; ?>
<?php include '../Artist/lat_long_conversion.php'; ?>

<?php
$artist_id = '';
$lat = '';
$lng = '';
$address = '';

$sql = "select DISTINCT users.*, user_tasks.user_id from users INNER join user_tasks on user_tasks.user_id = users.id INNER join tasks on tasks.id = user_tasks.task_id WHERE tasks.status!=3 AND
users.totLikes < " . $_POST['last_totLikes_id'] . " AND totLikes!=0 ORDER BY totLikes DESC LIMIT 16";


$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $artist_Likes_id = $row["totLikes"];
        $artist_id = $row["id"];
        $artist_name=$row['name'];


        //LATLONG Conversion
        $location = $row['location'];
        if (empty($location)) {
            $address = "Location Unavailable";
        } else
            if ($location == '0,0') {
                $address = "Location Unavailable";
            } else {
                $arr = explode(',', $location);
                $lat = $arr[0];
                $lng = $arr[1];
                $address = getaddress($lat, $lng);
            } ?>


        <div class="wpb_column vc_column_container vc_col-sm-3">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                        <div class="wpb_wrapper">
                            <a href="artist_details.php/<?php echo friendlyURL($artist_id, $artist_name); ?>">
                                <center>
                                    <img src="<?php if (!empty($row['fb_id'])) {
                                        if ($row['fb_id'] == 'nil') {

                                            #Painting image as a profile image

                                            $img_sql = " SELECT t.* FROM tasks t, user_tasks ut WHERE ut.user_id = $artist_id AND t.id = ut.task_id AND status!=3 ORDER BY tot_likes DESC LIMIT 1" ;
                                            $result_img = $conn->query($img_sql);

                                            if ($result_img->num_rows > 0) {
                                                while ($result_painting_img = $result_img->fetch_assoc()) {


                                                    echo $result_painting_img['image_path'];
                                                    // return $row['image_path'];
                                                }
                                            }


                                        } else
                                            echo "https://graph.facebook.com/{$row['fb_id']}/picture?type=large";
                                    } else

                                        if (empty($row['profilepic_path'])) {


                                            #Painting image as a profile image

                                            $img_sql = " SELECT t.* FROM tasks t, user_tasks ut WHERE ut.user_id = $id AND t.id = ut.task_id AND status!=3 ORDER BY tot_likes DESC LIMIT 1" ;
                                            $result_img = $conn->query($img_sql);

                                            if ($result_img->num_rows > 0) {
                                                while ($result_painting_img = $result_img->fetch_assoc()) {


                                                    echo $result_painting_img['image_path'];
                                                    // return $row['image_path'];
                                                }
                                            }


                                        } else
                                            echo $row['profilepic_path']; ?>"
                                         alt="<?php echo $row['name']; ?>"
                                         class="img_artist"
                                         class="img_circle">
                                </center>
                            </a>

                            <h3 class="artist_name text-capitalize"> <?php echo $row['firstname']; ?> </h3>
                            <div class="ws-separator-small"></div>
                            <h5 class="location">
                                <?php
                                $new_address = preg_replace('/[0-9]+/', '', $address);
                                $country = explode(',', $new_address);
                                echo "<i style='display:none;'>" . end($country) . "</i>"; // Display None Set Dont Delete This line
                                $city = prev($country); // storing City Name
                                echo $city;
                                if (!empty(trim($city))) {
                                    echo ",";
                                }
                                echo end($country);
                                ?>

                            </h5>
                            <br/>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    <?php } ?>

    <div class="ws-more-btn-holder col-sm-12">
        <div id="remove_row">
            <button type="button" name="btn_more" data-vid="<?php echo $artist_Likes_id; ?>"
                    id="btn_more" class="btn ws-more-btn">LOAD MORE
            </button>
        </div>
    </div>


<?php } else {
    echo "Sorry No result Found";
}
?>
