
<link rel='stylesheet' id='bootstrap-css'  href='<?php echo $webroot?>/layout/assets/plugins/bootstrap/css/bootstrap.min.css?ver=4.7.8' type='text/css' media='all' />
<link rel='stylesheet' id='owl-css'  href='<?php echo $webroot?>/layout/assets/js/plugins/owl-carousel/owl.carousel.css?ver=4.7.8' type='text/css' media='all' />
<link rel='stylesheet' id='artday-style-css'  href='<?php echo $webroot?>/layout/assets/css/style.css?ver=4.7.8' type='text/css' media='all' />
<link rel='stylesheet' id='artday-dynamic-css'  href='<?php echo $webroot?>/layout/assets/css/dynamic.css?ver=4.7.8' type='text/css' media='all' />
<link rel='stylesheet' id='artday-fonts-css'  href='https://fonts.googleapis.com/css?family=PT+Serif%7CMontserrat&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='<?php echo $webroot?>/layout/assets/plugins/js_composer/js_composer.min.css?ver=4.12.1' type='text/css' media='all' />
<link rel='stylesheet' id='bfa-font-awesome-css'  href='//cdn.jsdelivr.net/fontawesome/4.7.0/css/font-awesome.min.css?ver=4.7.0' type='text/css' media='all' />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
