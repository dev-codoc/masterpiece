<?php session_start();?>
<?php include '../layout/db.php'; ?>
<?php include 'lat_long_conversion.php'; ?>
<?php include '../layout/functions.php'; ?>

<!-- LAYOUT FOR ARTIST -->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Your Masterpieces - Artists</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings.">
    <meta property="og:image" content="http://www.yourmasterpieces.com/images/favicon.png"/>
    <link rel="stylesheet" href="../Paintings/search_design.css">

    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png"
          type="image/x-icon">
    <?php include 'artist_page_external_style.php'; ?>
    <link rel="stylesheet" href="commoncss.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php include '../google-analytics.php' ; ?>

</head>
<body>

<!--Header File For MAsterpiece -->
<?php include '../layout/header.php'; ?>

<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="<?php echo $webroot ?>/images/your_masterpieces_painting_background.jpg"
     style="background:rgba(0,0,0,0.4)">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>Artists</h1>
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <nav class="navbar navbar-default">
                                <div class="nav nav-justified navbar-nav">
                                    <form class="navbar-form navbar-search" role="search" method="GET"
                                          action="Search_Artist.php">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="query"
                                                   placeholder="Search All Artists">
                                            <div class="input-group-btn">
                                                <input type="submit" value="Search"
                                                       class="btn btn-search btn-info search-paint">
                                                </input>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </nav>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- END HEADING SCROLL ARTIST TEXT -->

<div class="container ws-page-container">
    <div class="row">
        <div class="col-sm-12">

            <article id="post-2478" class="post-2478 page type-page status-publish hentry">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="vc_empty_space" style="height: 35px">
                                        <span class="vc_empty_space_inner">

                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_row-fluid">
                    <?php
                    $sql = "select DISTINCT users.*, user_tasks.user_id from users INNER join user_tasks on user_tasks.user_id = users.id INNER join tasks on tasks.id = user_tasks.task_id WHERE tasks.status!=3 ORDER BY totLikes DESC LIMIT 32"; //fetching 32 Result Only from Users Table
                    $result = $conn->query($sql);
                    $artist_id = '';  // varibale is used for load more button
                    $lat = '';
                    $lng = '';
                    $address = '';

                    if ($result->num_rows > 0) {
                    // output data of each row
                    while ($row = $result->fetch_assoc()) {

                        $artist_name=$row['name'];
                        if (empty($artist_name)) {
                            if (!empty($row['firstname'])) {
                                $artist_name = $row['firstname'];
                            } else {
                                $artist_name = "artist";
                            }
                        }
                        $id = $row['id'];  // id for detail page of artist
                        $facebook_image_id = $row['fb_id']; //Facebook Image id
                        $location = $row['location']; //storing latlong value into a variable
                        $city = $row['city'];
                        $country = $row['country'];

                        ?>

                        <div class="wpb_column vc_column_container vc_col-sm-3">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                        <div class="wpb_wrapper">

                                            <a href="artist_details.php/<?php echo friendlyURL($id, $artist_name); ?>">
                                                <center>

                                                    <img src="<?php get_artist_profile_image($conn, $id, $facebook_image_id) ?>"

                                                         alt="<?php echo $row['firstname']; ?>"
                                                         class="img_circle img_artist">
                                                </center>
                                            </a>

                                            <h3 class="artist_name text-capitalize"> <?php echo $row['firstname'] ; ?> </h3>
                                            <div class="ws-separator-small"></div>
                                            <h5 class="location">
                                                <?php if ((empty($city)) && (empty($country))) { ?>
                                                    <?php get_artist_location($location) ?>
                                                <?php } else {
                                                    echo ucwords($city);
                                                    if (!empty($country) && !empty($city)) {
                                                        echo ",";
                                                    }
                                                    echo ucwords($country);
                                                } ?>
                                            </h5>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <?php
                        $last_totLikes_id = $row["totLikes"]; //saving last Artist Id in a varibale
                    }
                    ?>
                </div>


                <div id="load_data_table"></div>

                <div class="ws-more-btn-holder col-sm-12">

                    <div id="remove_row">
                        <button type="button" name="btn_more" data-vid="<?php echo $last_totLikes_id; ?>"
                                id="btn_more" class="btn ws-more-btn">LOAD MORE
                        </button>
                    </div>

                </div>
            </article>
        </div>
    </div>
</div>
<br>
<br>
<?php } else {
    echo "Sorry No result Found";
}
?>


</div>


<?php include '../layout/footer.php'; ?>

</body>
</html>

<script>
    $(document).ready(function () {
        $(document).on('click', '#btn_more', function () {
            var last_totLikes_id = $(this).data("vid");
            $('#btn_more').html("Loading...");
            $.ajax({
                url: "../Artist/load_more_artist.php",
                method: "POST",
                data: {last_totLikes_id: last_totLikes_id},
                dataType: "text",
                success: function (data) {
                    if (data != '') {
                        $('#remove_row').remove();
                        $('#load_data_table').append(data);
                        $('#btn_more').html("LOAD MORE");
                    }
                    else {
                        $('#btn_more').html("No Data");
                    }
                }
            });
        });
    });

</script>

<script type="text/javascript">

     $('#menushow').click(function() {
     if ($('.ws-parallax-header').hasClass('paddingadded')){
     $('.ws-parallax-header').removeClass('paddingadded');
     } else {
     $('.ws-parallax-header').addClass('paddingadded');
     }
     });

</script>


<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/frontend/cart-fragments.min.js?ver=2.6.9'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src="<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8"></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>
