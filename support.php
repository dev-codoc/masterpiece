<?php session_start(); ?>
<?php require_once './layout/db.php'; ?>
<?php require_once './Artist/lat_long_conversion.php' ?>
<?php require_once './layout/functions.php'; ?>
<?php require_once './auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler(); ?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">


    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png"
          type="image/x-icon">


    <title> Masterpieces - Support </title>


    <meta charset="UTF-8"/>
    <meta name="description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings.">
    <meta name="keywords"
          content="Paintings,Your Masterpieces, Non Digital Art, Buy paintings, Sell paintings, Sell Art, Buy Art, Bulk paintings buying, Foreign paintings, Art collectors, Collect Art, Buy painting online, Sell painting online, Painter profile, Artist profile"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <meta property="og:title" content="Your Masterpieces"/>
    <meta property="og:type" content=""/>
    <meta property="og:url" content="<?php echo $webroot; ?>"/>
    <meta property="og:image" content="http://www.yourmasterpieces.com/images/favicon.jpeg"/>
    <meta property="og:site_name" content="Your Masterpieces"/>
    <meta property="fb:admins" content="Your Masterpieces"/>
    <meta property="og:locale" content="en_us"/>
    <meta property="og:description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings."/>
    <link rel="canonical" href="<?php echo $webroot; ?>"/>


    <script>
        /* You can add more configuration options to webfontloader by previously defining the WebFontConfig with your options */
        if (typeof WebFontConfig === "undefined") {
            WebFontConfig = new Object();
        }
        WebFontConfig['google'] = {families: ['PT+Serif', 'Montserrat']};

        (function () {
            var wf = document.createElement('script');
            wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.5.3/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- This site is optimized with the Yoast SEO plugin v5.1 - https://yoast.com/wordpress/plugins/seo/ -->
    <?php include './Artist/artist_page_external_style.php'; ?>
    <?php include './google-analytics.php'; ?>
</head>
<body class="page-template page-template-template-page page-template-template-page-php page page-id-637 wpb-js-composer js-comp-ver-4.12.1 vc_responsive currency-usd">

<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

<!-- Loader Start -->
<div id="preloader">
    <div class="preloader-container">
        <div class="ws-spinner"></div>
    </div>
</div>
<!-- End Loader Start -->


<!-- Top Bar Start -->

<?php include './layout/header.php'; ?>
<!-- End Header -->

<style type="text/css">

    .link{
        color:#C2A476;
    }

</style>

<!-- Page Parallax Header -->
<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="<?php echo $webroot ?>/images/index_bg.jpg">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>Support</h1>
            </div>
        </div>
    </div>
</div>
<!-- End Page Parallax Header -->

<!-- Container Start -->
<div class="container ws-page-container">

    <!-- Row Start -->
    <div class="row">

        <div class="col-sm-12">


            <article id="post-637" class="post-637 page type-page status-publish hentry">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-3">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                    <div class="wpb_wrapper">
                                        <hr>
                                    </div>
                                </div>

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p>

                                            Welcome to our Artist and Buyer Support Centre. Partner
                                            artists are the backbone of Masterpieces and recognizing this,
                                            we have built this space to resolve all your queries. This
                                            section will also help you understand how to use Masterpieces
                                            to your advantage and showcase your art better. Get browsing
                                            now!
                                        </p>
                                        <p>&nbsp;</p>

                                    </div>
                                </div>

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-3">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-3">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div id='1481624867674-a6af00c5-b0ea'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4> Why choose Masterpieces for selling or buying
                                            art?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p class="paratext mt15">We are an open platform, encouraging artists all over
                                            the globe
                                            irrespective of their experience or age to be able to share their stories
                                            and handmade art. We at ‘<b>Masterpieces</b>’ believe the artist
                                            deserves the full price for their handwork and hence we charge no
                                            commission.
                                            <br>
                                            Artists over 65 countries shared their stories with us, what do you
                                            have to tell? Write to us at
                                            <a href="mailto:contact@yourmasterpieces.com" class="link">
                                                contact@yourmasterpieces.com</a>
                                            or you
                                            may upload your artwork here.
                                            <br>
                                            Masterpieces connects Artists and Art Collectors. As a platform, we
                                            take minimal handling charges and other taxes which are borne by
                                            the buyers. Buyers get access to unlimited unrestricted handmade
                                            arts from all over the globe from Artists of all expertise. The featured
                                            Artists and paintings are a curated list of handpicked paintings that
                                            are sent via emails and newsletters directly to your inbox which make
                                            the buying experience smooth.</p>

                                    </div>
                                </div>


                                <div id='1481624867674-a6af00c5-b0ea'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4> How do I photograph my art?</h4>
                                        <i class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p class="paratext mt15">

                                            The photograph of your artwork is the only reference to the
                                            buyer as a final product, so make sure the image you upload
                                            captures the essence and the beauty of the artwork you made.
                                            Photographing your artwork is a simple 3 step process. You
                                            can use any basic digital camera or your Mobile device to
                                            photograph your work.
                                            <br>
                                               <b>Click</b>: Set up your artwork completely upright – stretch out
                                            a canvas or a painting against a wall; place sculptures on an
                                            even, clean, light surface. Shoot in bright, soft light and make
                                            sure your images aren’t too dark or too light. When you take a
                                            photo of your paintings or your other art, make sure that your
                                            camera is parallel to the artwork, and keep minimal space
                                            around the object you’re shooting to get a large, clear image.
                                            Remember to ensure that the camera flash isn’t visible in the
                                            image.
                                            <br>
                                               <b>Edit</b>: We know editing enhances the images, however, to
                                            get the best knowledge on your artwork we strongly
                                            recommend to keep it as original as possible. The image
                                            should not exceed than 10mb, to ensure a good visual
                                            experience, please capture the image at least 200 dpi to enable
                                            open edition prints. Make sure you crop all edges from the
                                            image and do not include any frames or mounts on a painting.
                                            Remember, your image need to conform to these specifications to be accepted,
                                            after all, a
                                            beautiful artwork needs less editing.
                                            <br>
                                            <b>
                                                   Upload</b>: To Upload, simply visit your profile in My
                                            Account section and click on the ‘Upload Art’ button


                                        </p>

                                    </div>
                                </div>


                                <div id='1481624995837-b46c701e-33f6'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4> How do I pack my art for couriering?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p class="paratext mt15">Packing your artwork professionally not only
                                                    lives a good impression
                                                    on the buyer but also ensures the safety of your artwork being
                                                    damaged during transit.
                                                    Below are few guidelines to help you with packing the art:
                                                    To pack your artwork, you’ll need the following material
                                                </p>
                                                <ul class="artwork-desc-list mt15">
                                                    <ul class="artwork-desc-list mt15">
                                                        <li>Glassine Paper/Butter Paper</li>
                                                        <li>Bubble Wrap/Thermocol</li>
                                                        <li>Sealant like sellotape, brown packing tape</li>
                                                        <li>Masking tape</li>
                                                        <li>A container for the painting – a tube for a roll, and a
                                                            crate for framed/stretched artwork.
                                                        </li>
                                                    </ul>
                                                </ul>
                                                <p class="paratext mt15">Here’s how you should pack your art.</p>
                                                <ul class="artwork-desc-list mt15">
                                                    <li>
                                                        Ensure paintings are completely dry before packing
                                                        them. Research the correct amount of drying time before
                                                        packing and keep a few extra days as a buffer.
                                                    </li>
                                                    <li>
                                                        Protect the painting with a layer of butter paper on the
                                                        painted surface. Any material that comes in contact with
                                                        the surface of the painting could potentially damage it.
                                                    </li>
                                                    <li>
                                                        Use the correct types of packing paper like glassine/
                                                        butter paper as the first layer between the painting and
                                                        any packing material like bubble wrap or newspaper. DO
                                                        NOT use printed paper, newspaper, or anything printed
                                                        with ink to create the first layer of packing.
                                                    </li>
                                                    <li>
                                                        Now roll the canvas/ paper and cover it in a layer of
                                                        bubble wrap to protect from moisture. Insert this roll into
                                                        a PVC or Cardboard tube and seal it from both ends. If
                                                        you are sending the artwork as stretched or framed,
                                                        secure the glass by sticking masking tape in a cross
                                                        across the glass. This will ensure that even in the event of
                                                        breakage, the broken glass does not damage the work.
                                                        Wrap the artwork in a plastic sheet and tape it into place.

                                                    </li>
                                                    <li>Now cover it in a layer of bubble wrap, giving special
                                                        attention to the edges. Finally, place this work in a
                                                        wooden crate with a thermocol layer on all sides, seal,
                                                        and courier it.
                                                    </li>
                                                </ul>
                                                <p>&nbsp;</p>
                                            </div>
                                        </div>
                                        <div class="ws-heading text-left"></div>
                                        <div class="wpb_text_column wpb_content_element "></div>
                                    </div>
                                </div>
                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>What should be the size of the images to be
                                            uploaded?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>Your image resolution needs to be a minimum of
                                            1400 x 1400 pixel
                                            for good viewing experience of the buyer. You can upload images up
                                            to 10mb of size and of any image formats.</p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>


                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>Who will courier the artwork to the buyer?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            As an artist, you have to pack the artwork and our courier partners
                                            will get in touch with you to receive the artwork and deliver the same
                                            to the buyer.
                                            <br>
                                            <b>Note</b> : Please pack your artwork professionally to ensure it’s safety.
                                            Please insert a color printout of your artwork along with your
                                            signature and date as an authenticity certificate. Please click here to
                                            download the authenticity certificate.</p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>How will I receive my payment for my artwork
                                            sold?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            ‘Masterpieces’ believes in quick payment and transparency. You will
                                            receive your payment within 10 working days of the buyer confirming
                                            that she/he has received the artwork.<br>
                                            <b>Please note</b>:
                                            Payments are processed only after receiving an email confirmation
                                            from the buyer that they have received the artwork and that they are
                                            satisfied with the artwork.
                                            These payments are made directly to your registered bank account,
                                            please keep your bank details updated with us all the time.</p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>Do you take any commissions on the paintings being
                                            sold on
                                            your platform?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            We at ‘Masterpieces’ believe the artist deserves the full price for
                                            their handwork and hence no commission, Taxes, Shipping and
                                            handling costs are charged to the artists.</p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>
                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>What happens to the copyright of my artwork?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            Our Artists' safety and privacy are our utmost priority at Your
                                            Masterpieces. Please visit our <a href="policy.php" class="link">Privacy and
                                                Policy </a> page for more
                                            details</p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4> What happens to the artwork is damaged in
                                            transit?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            Sometimes, buyers claim that an artwork was damaged in transit. In
                                            such scenarios, the buyer is expected to contact us at <a href="mailto:contact@yourmasterpiecees.com" class="link">contact@yourmasterpieces.com</a>, and we’ll
                                            arrange for the return of the artwork to you, as well as a full refund for
                                            the buyer. However, we must point out that since you will be packing
                                            your artwork yourself, it is your responsibility to ensure that the
                                            artwork is packed professionally and safely.

                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4> Do you provide help in installing the art?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            Unfortunately, we do not provide the service to install the artwork.
                                            The paintings will be couriered without frames to minimize the
                                            handling charges. Buyers can frame the canvases as per their choice
                                            to suit the interiors.

                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4> How do I know if the art purchased is
                                            original? </h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            Masterpieces procures every item directly and exclusively from
                                            Artists and communities. We guarantee the authenticity of each and
                                            every product we have on our platform.

                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4> Can I re-sell the artwork I already own, which is
                                            not made by
                                            me? </h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            Yes, you can resell the pre-owned art. However, to so do please
                                            ensure to insert an authenticity certificate along with the artwork
                                            image signed by you to ensure it’s reselling safety. Please also read
                                            our <a href="terms&conditions.php" class="link"> Terms and Conditions</a>
                                            page.

                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>
                                            As an Artist, do I have to pay for the shipping
                                            and handling
                                            cost?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            We promote Art and value your efforts and hence you will NOT be
                                            charged for any shipping and/or handling costs.

                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>
                                            How do I place an order?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            Liked an artwork and wish to buy? It’s simple.
                                            Select the artwork you like and click on the ‘Request to Buy’ button,
                                            fill in the form with your details and the ‘Masterpieces’ team will get
                                            back to you with the further process to complete your purchase.

                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>
                                            As a Buyer, do I have to pay for the shipping and handling
                                            cost?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            Yes, depending on where the order is being shipped, sales tax will be
                                            charged on the order subtotal, shipping, and/or handling at the
                                            applicable county rate. Tax rates within counties vary.
                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>
                                            How do I check the status of my order?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            Once you have successfully placed an order with ‘Masterpieces’, we
                                            will keep you notified on the status of the order and shall share the
                                            tracking link as soon as the order is dispatched with our courier
                                            partners.
                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>
                                            I have received my order, is that all?</h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content"><p>
                                            Customer satisfaction is our priority. Once you have received the
                                            artwork and are satisfied with the artwork, you need to confirm the
                                            receipt of your order to us at
                                            <a href="mailto:support@yourmasterpieces.com"
                                               class="link">support@yourmasterpiece.com
                                            </a>
                                            We would love to hear from you, Please share your experience and
                                            also send us pictures of the art in its new home. Please share your
                                            experience with us at <a href="mailto:contact@yourmasterpieces.com"
                                                                     class="link">contact@yourmasterpieces.com</a>
                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>

                                            I received a damaged artwork. What should I do?
                                        </h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>
                                            Your satisfaction is our top priority, and we want your shopping
                                            experience to be perfect. You can refuse the item if you see any
                                            damage or quality issues. We ’ll replace the art if it’s still available or
                                            issue a full refund, excluding shipping charges. Make sure to
                                            note the damage or quality issues along with a photograph of the
                                            damaged artwork and email it to us at
                                            <a href="mailto:support@yourmasterpieces.com" class="link">support@yourmasterpieces.com</a>
                                            immediately. Please include your
                                            order number and the name of the item that was damaged.

                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>

                                            Do you have Return and Exchange policy?
                                        </h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>
                                            All items on our site will be final sale and are not eligible for a
                                            return or exchange.<br>
                                            However, below are the cases considered for return or exchange:
                                            If the artwork received is damaged.<br>
                                            Your satisfaction is our top priority, and we want your shopping
                                            experience to be perfect. You can refuse the item if you see any
                                            damage or quality issues. We will replace the art if it’s still available
                                            or issue a full refund, excluding shipping charges.<br>
                                            Make sure to note the damage or quality issues along with a
                                            photograph of the damaged artwork and email it to us at
                                            support@yourmasterpieces.com immediately. Please include your
                                            order number and the name of the item that was damaged.<br>
                                            If the artwork is different from what you have ordered.<br>
                                            We apologize for the inconvenience, please write to us immediately
                                            at support@yourmasterpieces.com. Our Support team will get back
                                            to you with the process to get the artwork you wished to buy.<br>
                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>

                                            What Payment methods you accept?
                                        </h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>
                                            Your Masterpieces platform accepts payments from Visa,
                                            MasterCard, and Paypal.
                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>

                                            How do I make payment for the purchase?
                                        </h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>
                                            When you place an order, we authorize your card to confirm it is
                                            valid and that there are sufficient funds available for your purchase.
                                            We charge your card only on successful payment.
                                            Please review your shipment notification email for details on the
                                            amount actually charged to your card.

                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>

                                            How do I cancel my order?
                                        </h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>
                                            An order may be canceled as long as the cancel request is made
                                            within 24 hours of your purchase. For any other queries, please
                                            contact our Customer service at or you may also write to us at
                                            <a href="mailto:support@yourmasterpieces.com" class="link">support@yourmasterpieces.com</a>

                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                                <div id='1484658475187-442569af-85b8'
                                     class="vc_toggle vc_toggle_default vc_toggle_color_default  vc_toggle_size_md">
                                    <div class="vc_toggle_title"><h4>

                                            Can I have COD order?
                                        </h4><i
                                                class="vc_toggle_icon"></i></div>
                                    <div class="vc_toggle_content">
                                        <p>
                                            Unfortunately, we are unable to fulfil Cash On Delivery orders at this
                                            time. However Masterpieces provide many payments services. If you
                                            are unable to use any of the payment service, please feel free to
                                            write us at
                                            <a href="mailto:support@yourmasterpieces.com" class="link">support@yourmasterpieces.com</a>

                                        </p>
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-3">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                </div>
            </article>


        </div>


    </div><!-- Row End -->
</div><!-- Container End -->


<!-- Footer Start -->

<?php include './layout/footer.php'; ?>
<!-- Footer Bar End -->


<div id="fb-root"></div>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>


<script type='text/javascript'>
    function vc_js() {
        vc_toggleBehaviour(), vc_tabsBehaviour(), vc_accordionBehaviour(), vc_teaserGrid(), vc_carouselBehaviour(), vc_slidersBehaviour(), vc_prettyPhoto(), vc_googleplus(), vc_pinterest(), vc_progress_bar(), vc_plugin_flexslider(), vc_google_fonts(), vc_gridBehaviour(), vc_rowBehaviour(), vc_googleMapsPointer(), vc_ttaActivation(), jQuery(document).trigger("vc_js"), window.setTimeout(vc_waypoints, 500)
    }
    function getSizeName() {
        var screen_w = jQuery(window).width();
        return screen_w > 1170 ? "desktop_wide" : screen_w > 960 && 1169 > screen_w ? "desktop" : screen_w > 768 && 959 > screen_w ? "tablet" : screen_w > 300 && 767 > screen_w ? "mobile" : 300 > screen_w ? "mobile_portrait" : ""
    }
    function loadScript(url, $obj, callback) {
        var script = document.createElement("script");
        script.type = "text/javascript", script.readyState && (script.onreadystatechange = function () {
            ("loaded" === script.readyState || "complete" === script.readyState) && (script.onreadystatechange = null, callback())
        }), script.src = url, $obj.get(0).appendChild(script)
    }
    function vc_ttaActivation() {
        jQuery("[data-vc-accordion]").on("show.vc.accordion", function (e) {
            var $ = window.jQuery, ui = {};
            ui.newPanel = $(this).data("vc.accordion").getTarget(), window.wpb_prepare_tab_content(e, ui)
        })
    }
    function vc_accordionActivate(event, ui) {
        if (ui.newPanel.length && ui.newHeader.length) {
            var $pie_charts = ui.newPanel.find(".vc_pie_chart:not(.vc_ready)"),
                $round_charts = ui.newPanel.find(".vc_round-chart"), $line_charts = ui.newPanel.find(".vc_line-chart"),
                $carousel = ui.newPanel.find('[data-ride="vc_carousel"]');
            "undefined" != typeof jQuery.fn.isotope && ui.newPanel.find(".isotope, .wpb_image_grid_ul").isotope("layout"), ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
                var grid = jQuery(this).data("vcGrid");
                grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
            }), vc_carouselBehaviour(ui.newPanel), vc_plugin_flexslider(ui.newPanel), $pie_charts.length && jQuery.fn.vcChat && $pie_charts.vcChat(), $round_charts.length && jQuery.fn.vcRoundChart && $round_charts.vcRoundChart({reload: !1}), $line_charts.length && jQuery.fn.vcLineChart && $line_charts.vcLineChart({reload: !1}), $carousel.length && jQuery.fn.carousel && $carousel.carousel("resizeAction"), ui.newPanel.parents(".isotope").length && ui.newPanel.parents(".isotope").each(function () {
                jQuery(this).isotope("layout")
            })
        }
    }
    function initVideoBackgrounds() {
        return window.console && window.console.warn && window.console.warn("this function is deprecated use vc_initVideoBackgrounds"), vc_initVideoBackgrounds()
    }
    function vc_initVideoBackgrounds() {
        jQuery(".vc_row").each(function () {
            var youtubeUrl, youtubeId, $row = jQuery(this);
            $row.data("vcVideoBg") ? (youtubeUrl = $row.data("vcVideoBg"), youtubeId = vcExtractYoutubeId(youtubeUrl), youtubeId && ($row.find(".vc_video-bg").remove(), insertYoutubeVideoAsBackground($row, youtubeId)), jQuery(window).on("grid:items:added", function (event, $grid) {
                $row.has($grid).length && vcResizeVideoBackground($row)
            })) : $row.find(".vc_video-bg").remove()
        })
    }
    function insertYoutubeVideoAsBackground($element, youtubeId, counter) {
        if ("undefined" == typeof YT.Player)return counter = "undefined" == typeof counter ? 0 : counter, counter > 100 ? void console.warn("Too many attempts to load YouTube api") : void setTimeout(function () {
            insertYoutubeVideoAsBackground($element, youtubeId, counter++)
        }, 100);
        var $container = $element.prepend('<div class="vc_video-bg vc_hidden-xs"><div class="inner"></div></div>').find(".inner");
        new YT.Player($container[0], {
            width: "100%",
            height: "100%",
            videoId: youtubeId,
            playerVars: {
                playlist: youtubeId,
                iv_load_policy: 3,
                enablejsapi: 1,
                disablekb: 1,
                autoplay: 1,
                controls: 0,
                showinfo: 0,
                rel: 0,
                loop: 1,
                wmode: "transparent"
            },
            events: {
                onReady: function (event) {
                    event.target.mute().setLoop(!0)
                }
            }
        }), vcResizeVideoBackground($element), jQuery(window).bind("resize", function () {
            vcResizeVideoBackground($element)
        })
    }
    function vcResizeVideoBackground($element) {
        var iframeW, iframeH, marginLeft, marginTop, containerW = $element.innerWidth(),
            containerH = $element.innerHeight(), ratio1 = 16, ratio2 = 9;
        ratio1 / ratio2 > containerW / containerH ? (iframeW = containerH * (ratio1 / ratio2), iframeH = containerH, marginLeft = -Math.round((iframeW - containerW) / 2) + "px", marginTop = -Math.round((iframeH - containerH) / 2) + "px", iframeW += "px", iframeH += "px") : (iframeW = containerW, iframeH = containerW * (ratio2 / ratio1), marginTop = -Math.round((iframeH - containerH) / 2) + "px", marginLeft = -Math.round((iframeW - containerW) / 2) + "px", iframeW += "px", iframeH += "px"), $element.find(".vc_video-bg iframe").css({
            maxWidth: "1000%",
            marginLeft: marginLeft,
            marginTop: marginTop,
            width: iframeW,
            height: iframeH
        })
    }
    function vcExtractYoutubeId(url) {
        if ("undefined" == typeof url)return !1;
        var id = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
        return null !== id ? id[1] : !1
    }
    function vc_googleMapsPointer() {
        var $ = window.jQuery, $wpbGmapsWidget = $(".wpb_gmaps_widget");
        $wpbGmapsWidget.click(function () {
            $("iframe", this).css("pointer-events", "auto")
        }), $wpbGmapsWidget.mouseleave(function () {
            $("iframe", this).css("pointer-events", "none")
        }), $(".wpb_gmaps_widget iframe").css("pointer-events", "none")
    }
    document.documentElement.className += " js_active ", document.documentElement.className += "ontouchstart" in document.documentElement ? " vc_mobile " : " vc_desktop ", function () {
        for (var prefix = ["-webkit-", "-moz-", "-ms-", "-o-", ""], i = 0; i < prefix.length; i++)prefix[i] + "transform" in document.documentElement.style && (document.documentElement.className += " vc_transform ")
    }(), "function" != typeof window.vc_plugin_flexslider && (window.vc_plugin_flexslider = function ($parent) {
        var $slider = $parent ? $parent.find(".wpb_flexslider") : jQuery(".wpb_flexslider");
        $slider.each(function () {
            var this_element = jQuery(this), sliderSpeed = 800,
                sliderTimeout = 1e3 * parseInt(this_element.attr("data-interval")),
                sliderFx = this_element.attr("data-flex_fx"), slideshow = !0;
            0 === sliderTimeout && (slideshow = !1), this_element.is(":visible") && this_element.flexslider({
                animation: sliderFx,
                slideshow: slideshow,
                slideshowSpeed: sliderTimeout,
                sliderSpeed: sliderSpeed,
                smoothHeight: !0
            })
        })
    }), "function" != typeof window.vc_googleplus && (window.vc_googleplus = function () {
        0 < jQuery(".wpb_googleplus").length && !function () {
            var po = document.createElement("script");
            po.type = "text/javascript", po.async = !0, po.src = "//apis.google.com/js/plusone.js";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(po, s)
        }()
    }), "function" != typeof window.vc_pinterest && (window.vc_pinterest = function () {
        0 < jQuery(".wpb_pinterest").length && !function () {
            var po = document.createElement("script");
            po.type = "text/javascript", po.async = !0, po.src = "//assets.pinterest.com/js/pinit.js";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(po, s)
        }()
    }), "function" != typeof window.vc_progress_bar && (window.vc_progress_bar = function () {
        "undefined" != typeof jQuery.fn.waypoint && jQuery(".vc_progress_bar").waypoint(function () {
            jQuery(this).find(".vc_single_bar").each(function (index) {
                var $this = jQuery(this), bar = $this.find(".vc_bar"), val = bar.data("percentage-value");
                setTimeout(function () {
                    bar.css({width: val + "%"})
                }, 200 * index)
            })
        }, {offset: "85%"})
    }), "function" != typeof window.vc_waypoints && (window.vc_waypoints = function () {
        "undefined" != typeof jQuery.fn.waypoint && jQuery(".wpb_animate_when_almost_visible:not(.wpb_start_animation)").waypoint(function () {
            jQuery(this).addClass("wpb_start_animation")
        }, {offset: "85%"})
    }), "function" != typeof window.vc_toggleBehaviour && (window.vc_toggleBehaviour = function ($el) {
        function event(e) {
            e && e.preventDefault && e.preventDefault();
            var title = jQuery(this), element = title.closest(".vc_toggle"),
                content = element.find(".vc_toggle_content");
            element.hasClass("vc_toggle_active") ? content.slideUp({
                duration: 300, complete: function () {
                    element.removeClass("vc_toggle_active")
                }
            }) : content.slideDown({
                duration: 300, complete: function () {
                    element.addClass("vc_toggle_active")
                }
            })
        }

        $el ? $el.hasClass("vc_toggle_title") ? $el.unbind("click").click(event) : $el.find(".vc_toggle_title").unbind("click").click(event) : jQuery(".vc_toggle_title").unbind("click").on("click", event)
    }), "function" != typeof window.vc_tabsBehaviour && (window.vc_tabsBehaviour = function ($tab) {
        if (jQuery.ui) {
            var $call = $tab || jQuery(".wpb_tabs, .wpb_tour"),
                ver = jQuery.ui && jQuery.ui.version ? jQuery.ui.version.split(".") : "1.10",
                old_version = 1 === parseInt(ver[0]) && 9 > parseInt(ver[1]);
            $call.each(function (index) {
                var $tabs, interval = jQuery(this).attr("data-interval"), tabs_array = [];
                if ($tabs = jQuery(this).find(".wpb_tour_tabs_wrapper").tabs({
                        show: function (event, ui) {
                            wpb_prepare_tab_content(event, ui)
                        }, beforeActivate: function (event, ui) {
                            1 !== ui.newPanel.index() && ui.newPanel.find(".vc_pie_chart:not(.vc_ready)")
                        }, activate: function (event, ui) {
                            wpb_prepare_tab_content(event, ui)
                        }
                    }), interval && interval > 0)try {
                    $tabs.tabs("rotate", 1e3 * interval)
                } catch (e) {
                    window.console && window.console.log && console.log(e)
                }
                jQuery(this).find(".wpb_tab").each(function () {
                    tabs_array.push(this.id)
                }), jQuery(this).find(".wpb_tabs_nav li").click(function (e) {
                    return e.preventDefault(), old_version ? $tabs.tabs("select", jQuery("a", this).attr("href")) : $tabs.tabs("option", "active", jQuery(this).index()), !1
                }), jQuery(this).find(".wpb_prev_slide a, .wpb_next_slide a").click(function (e) {
                    if (e.preventDefault(), old_version) {
                        var index = $tabs.tabs("option", "selected");
                        jQuery(this).parent().hasClass("wpb_next_slide") ? index++ : index--, 0 > index ? index = $tabs.tabs("length") - 1 : index >= $tabs.tabs("length") && (index = 0), $tabs.tabs("select", index)
                    } else {
                        var index = $tabs.tabs("option", "active"), length = $tabs.find(".wpb_tab").length;
                        index = jQuery(this).parent().hasClass("wpb_next_slide") ? index + 1 >= length ? 0 : index + 1 : 0 > index - 1 ? length - 1 : index - 1, $tabs.tabs("option", "active", index)
                    }
                })
            })
        }
    }), "function" != typeof window.vc_accordionBehaviour && (window.vc_accordionBehaviour = function () {
        jQuery(".wpb_accordion").each(function (index) {
            var $tabs, $this = jQuery(this),
                active_tab = ($this.attr("data-interval"), !isNaN(jQuery(this).data("active-tab")) && 0 < parseInt($this.data("active-tab")) ? parseInt($this.data("active-tab")) - 1 : !1),
                collapsible = !1 === active_tab || "yes" === $this.data("collapsible");
            $tabs = $this.find(".wpb_accordion_wrapper").accordion({
                header: "> div > h3",
                autoHeight: !1,
                heightStyle: "content",
                active: active_tab,
                collapsible: collapsible,
                navigation: !0,
                activate: vc_accordionActivate,
                change: function (event, ui) {
                    "undefined" != typeof jQuery.fn.isotope && ui.newContent.find(".isotope").isotope("layout"), vc_carouselBehaviour(ui.newPanel)
                }
            }), !0 === $this.data("vcDisableKeydown") && ($tabs.data("uiAccordion")._keydown = function () {
            })
        })
    }), "function" != typeof window.vc_teaserGrid && (window.vc_teaserGrid = function () {
        var layout_modes = {fitrows: "fitRows", masonry: "masonry"};
        jQuery(".wpb_grid .teaser_grid_container:not(.wpb_carousel), .wpb_filtered_grid .teaser_grid_container:not(.wpb_carousel)").each(function () {
            var $container = jQuery(this), $thumbs = $container.find(".wpb_thumbnails"),
                layout_mode = $thumbs.attr("data-layout-mode");
            $thumbs.isotope({
                itemSelector: ".isotope-item",
                layoutMode: "undefined" == typeof layout_modes[layout_mode] ? "fitRows" : layout_modes[layout_mode]
            }), $container.find(".categories_filter a").data("isotope", $thumbs).click(function (e) {
                e.preventDefault();
                var $thumbs = jQuery(this).data("isotope");
                jQuery(this).parent().parent().find(".active").removeClass("active"), jQuery(this).parent().addClass("active"), $thumbs.isotope({filter: jQuery(this).attr("data-filter")})
            }), jQuery(window).bind("load resize", function () {
                $thumbs.isotope("layout")
            })
        })
    }), "function" != typeof window.vc_carouselBehaviour && (window.vc_carouselBehaviour = function ($parent) {
        var $carousel = $parent ? $parent.find(".wpb_carousel") : jQuery(".wpb_carousel");
        $carousel.each(function () {
            var $this = jQuery(this);
            if (!0 !== $this.data("carousel_enabled") && $this.is(":visible")) {
                $this.data("carousel_enabled", !0);
                var carousel_speed = (getColumnsCount(jQuery(this)), 500);
                jQuery(this).hasClass("columns_count_1") && (carousel_speed = 900);
                var carousele_li = jQuery(this).find(".wpb_thumbnails-fluid li");
                carousele_li.css({"margin-right": carousele_li.css("margin-left"), "margin-left": 0});
                var fluid_ul = jQuery(this).find("ul.wpb_thumbnails-fluid");
                fluid_ul.width(fluid_ul.width() + 300), jQuery(window).resize(function () {
                    var before_resize = screen_size;
                    screen_size = getSizeName(), before_resize != screen_size && window.setTimeout("location.reload()", 20)
                })
            }
        })
    }), "function" != typeof window.vc_slidersBehaviour && (window.vc_slidersBehaviour = function () {
        jQuery(".wpb_gallery_slides").each(function (index) {
            var $imagesGrid, this_element = jQuery(this);
            if (this_element.hasClass("wpb_slider_nivo")) {
                var sliderSpeed = 800, sliderTimeout = 1e3 * this_element.attr("data-interval");
                0 === sliderTimeout && (sliderTimeout = 9999999999), this_element.find(".nivoSlider").nivoSlider({
                    effect: "boxRainGrow,boxRain,boxRainReverse,boxRainGrowReverse",
                    slices: 15,
                    boxCols: 8,
                    boxRows: 4,
                    animSpeed: sliderSpeed,
                    pauseTime: sliderTimeout,
                    startSlide: 0,
                    directionNav: !0,
                    directionNavHide: !0,
                    controlNav: !0,
                    keyboardNav: !1,
                    pauseOnHover: !0,
                    manualAdvance: !1,
                    prevText: "Prev",
                    nextText: "Next"
                })
            } else this_element.hasClass("wpb_image_grid") && (jQuery.fn.imagesLoaded ? $imagesGrid = this_element.find(".wpb_image_grid_ul").imagesLoaded(function () {
                $imagesGrid.isotope({itemSelector: ".isotope-item", layoutMode: "fitRows"})
            }) : this_element.find(".wpb_image_grid_ul").isotope({
                itemSelector: ".isotope-item",
                layoutMode: "fitRows"
            }))
        })
    }), "function" != typeof window.vc_prettyPhoto && (window.vc_prettyPhoto = function () {
        try {
            jQuery && jQuery.fn && jQuery.fn.prettyPhoto && jQuery('a.prettyphoto, .gallery-icon a[href*=".jpg"]').prettyPhoto({
                animationSpeed: "normal",
                hook: "data-rel",
                padding: 15,
                opacity: .7,
                showTitle: !0,
                allowresize: !0,
                counter_separator_label: "/",
                hideflash: !1,
                deeplinking: !1,
                modal: !1,
                callback: function () {
                    var url = location.href, hashtag = url.indexOf("#!prettyPhoto") ? !0 : !1;
                    hashtag && (location.hash = "")
                },
                social_tools: ""
            })
        } catch (err) {
            window.console && window.console.log && console.log(err)
        }
    }), "function" != typeof window.vc_google_fonts && (window.vc_google_fonts = function () {
        return !1
    }), window.vcParallaxSkroll = !1, "function" != typeof window.vc_rowBehaviour && (window.vc_rowBehaviour = function () {
        function fullWidthRow() {
            var $elements = $('[data-vc-full-width="true"]');
            $.each($elements, function (key, item) {
                var $el = $(this);
                $el.addClass("vc_hidden");
                var $el_full = $el.next(".vc_row-full-width");
                if ($el_full.length || ($el_full = $el.parent().next(".vc_row-full-width")), $el_full.length) {
                    var el_margin_left = parseInt($el.css("margin-left"), 10),
                        el_margin_right = parseInt($el.css("margin-right"), 10),
                        offset = 0 - $el_full.offset().left - el_margin_left, width = $(window).width();
                    if ($el.css({
                            position: "relative",
                            left: offset,
                            "box-sizing": "border-box",
                            width: $(window).width()
                        }), !$el.data("vcStretchContent")) {
                        var padding = -1 * offset;
                        0 > padding && (padding = 0);
                        var paddingRight = width - padding - $el_full.width() + el_margin_left + el_margin_right;
                        0 > paddingRight && (paddingRight = 0), $el.css({
                            "padding-left": padding + "px",
                            "padding-right": paddingRight + "px"
                        })
                    }
                    $el.attr("data-vc-full-width-init", "true"), $el.removeClass("vc_hidden")
                }
            }), $(document).trigger("vc-full-width-row", $elements)
        }

        function parallaxRow() {
            var vcSkrollrOptions, callSkrollInit = !1;
            return window.vcParallaxSkroll && window.vcParallaxSkroll.destroy(), $(".vc_parallax-inner").remove(), $("[data-5p-top-bottom]").removeAttr("data-5p-top-bottom data-30p-top-bottom"), $("[data-vc-parallax]").each(function () {
                var skrollrSpeed, skrollrSize, skrollrStart, skrollrEnd, $parallaxElement, parallaxImage, youtubeId;
                callSkrollInit = !0, "on" === $(this).data("vcParallaxOFade") && $(this).children().attr("data-5p-top-bottom", "opacity:0;").attr("data-30p-top-bottom", "opacity:1;"), skrollrSize = 100 * $(this).data("vcParallax"), $parallaxElement = $("<div />").addClass("vc_parallax-inner").appendTo($(this)), $parallaxElement.height(skrollrSize + "%"), parallaxImage = $(this).data("vcParallaxImage"), youtubeId = vcExtractYoutubeId(parallaxImage), youtubeId ? insertYoutubeVideoAsBackground($parallaxElement, youtubeId) : "undefined" != typeof parallaxImage && $parallaxElement.css("background-image", "url(" + parallaxImage + ")"), skrollrSpeed = skrollrSize - 100, skrollrStart = -skrollrSpeed, skrollrEnd = 0, $parallaxElement.attr("data-bottom-top", "top: " + skrollrStart + "%;").attr("data-top-bottom", "top: " + skrollrEnd + "%;")
            }), callSkrollInit && window.skrollr ? (vcSkrollrOptions = {
                forceHeight: !1,
                smoothScrolling: !1,
                mobileCheck: function () {
                    return !1
                }
            }, window.vcParallaxSkroll = skrollr.init(vcSkrollrOptions), window.vcParallaxSkroll) : !1
        }

        function fullHeightRow() {
            var $element = $(".vc_row-o-full-height:first");
            if ($element.length) {
                var $window, windowHeight, offsetTop, fullHeight;
                $window = $(window), windowHeight = $window.height(), offsetTop = $element.offset().top, windowHeight > offsetTop && (fullHeight = 100 - offsetTop / (windowHeight / 100), $element.css("min-height", fullHeight + "vh"))
            }
            $(document).trigger("vc-full-height-row", $element)
        }

        function fixIeFlexbox() {
            var ua = window.navigator.userAgent, msie = ua.indexOf("MSIE ");
            (msie > 0 || navigator.userAgent.match(/Trident.*rv\:11\./)) && $(".vc_row-o-full-height").each(function () {
                "flex" === $(this).css("display") && $(this).wrap('<div class="vc_ie-flexbox-fixer"></div>')
            })
        }

        var $ = window.jQuery;
        $(window).off("resize.vcRowBehaviour").on("resize.vcRowBehaviour", fullWidthRow).on("resize.vcRowBehaviour", fullHeightRow), fullWidthRow(), fullHeightRow(), fixIeFlexbox(), vc_initVideoBackgrounds(), parallaxRow()
    }), "function" != typeof window.vc_gridBehaviour && (window.vc_gridBehaviour = function () {
        jQuery.fn.vcGrid && jQuery("[data-vc-grid]").vcGrid()
    }), "function" != typeof window.getColumnsCount && (window.getColumnsCount = function (el) {
        for (var find = !1, i = 1; !1 === find;) {
            if (el.hasClass("columns_count_" + i))return find = !0, i;
            i++
        }
    });
    var screen_size = getSizeName();
    "function" != typeof window.wpb_prepare_tab_content && (window.wpb_prepare_tab_content = function (event, ui) {
        var $ui_panel, $google_maps, panel = ui.panel || ui.newPanel,
            $pie_charts = panel.find(".vc_pie_chart:not(.vc_ready)"), $round_charts = panel.find(".vc_round-chart"),
            $line_charts = panel.find(".vc_line-chart"), $carousel = panel.find('[data-ride="vc_carousel"]');
        if (vc_carouselBehaviour(), vc_plugin_flexslider(panel), ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
                var grid = jQuery(this).data("vcGrid");
                grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
            }), panel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && panel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
                var grid = jQuery(this).data("vcGrid");
                grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
            }), $pie_charts.length && jQuery.fn.vcChat && $pie_charts.vcChat(), $round_charts.length && jQuery.fn.vcRoundChart && $round_charts.vcRoundChart({reload: !1}), $line_charts.length && jQuery.fn.vcLineChart && $line_charts.vcLineChart({reload: !1}), $carousel.length && jQuery.fn.carousel && $carousel.carousel("resizeAction"), $ui_panel = panel.find(".isotope, .wpb_image_grid_ul"), $google_maps = panel.find(".wpb_gmaps_widget"), 0 < $ui_panel.length && $ui_panel.isotope("layout"), $google_maps.length && !$google_maps.is(".map_ready")) {
            var $frame = $google_maps.find("iframe");
            $frame.attr("src", $frame.attr("src")), $google_maps.addClass("map_ready")
        }
        panel.parents(".isotope").length && panel.parents(".isotope").each(function () {
            jQuery(this).isotope("layout")
        })
    }), "function" != typeof window.vc_googleMapsPointer, jQuery(document).ready(function ($) {
        window.vc_js()
    });


</script>


</body>
</html>
