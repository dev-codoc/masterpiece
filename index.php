﻿<?php session_start(); ?>
<?php require_once './layout/db.php'; ?>
<?php require_once './Artist/lat_long_conversion.php' ?>
<?php require_once './layout/functions.php'; ?>
<?php require_once './auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler(); ?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Masterpieces</title>
    <meta charset="UTF-8"/>
    <meta name="description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings.">
    <meta name="keywords"
          content="Paintings,Your Masterpieces, Non Digital Art, Buy paintings, Sell paintings, Sell Art, Buy Art, Bulk paintings buying, Foreign paintings, Art collectors, Collect Art, Buy painting online, Sell painting online, Painter profile, Artist profile"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta property="og:title" content="Masterpieces"/>
    <meta property="og:type" content="Website"/>
    <meta property="og:url" content="<?php echo $webroot; ?>"/>
    <meta property="og:image:type" content="image/png">  
    <meta property="og:image" content="<?php echo $webroot; ?>/images/masterpiece_share.png"/>
    <meta property="og:site_name" content="Masterpieces"/>
    <meta property="fb:admins" content="Masterpieces"/>
    <meta property="og:locale" content="en_us"/>
    <meta property="og:description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings."/>
    <link rel="canonical" href="<?php echo $webroot; ?>"/>
    <link rel="shortcut icon" href="<?php echo $webroot; ?>/images/favicon.png" type="image/png">


    <link rel='stylesheet' id='contact-form-7-css'
          href='<?php echo $webroot ?>/assets/homenew/css/styles.css?ver=4.7'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='rs-plugin-settings-css'
          href='<?php echo $webroot ?>/assets/homenew/css/settings.css?ver=5.2.6'
          type='text/css' media='all'/>

    <link rel='stylesheet' id='woocommerce-layout-css'
          href='<?php echo $webroot ?>/assets/homenew/css/woocommerce-layout.css?ver=2.6.4'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='woocommerce-smallscreen-css'
          href='<?php echo $webroot ?>/assets/homenew/css/woocommerce-smallscreen.css?ver=2.6.4'
          type='text/css' media='only screen and (max-width: 768px)'/>
    <link rel='stylesheet' id='woocommerce-general-css'
          href='<?php echo $webroot ?>/assets/homenew/css/woocommerce.css?ver=2.6.4'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='bootstrap-css'
          href='<?php echo $webroot ?>/assets/homenew/css/bootstrap.min.css?ver=4.6.11'
          type='text/css' media='all'/>

    <link rel='stylesheet' id='owl-css'
          href='<?php echo $webroot ?>/assets/homenew/css/owl.carousel.css?ver=4.6.11'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='artday-style-css'
          href='<?php echo $webroot ?>/assets/homenew/css/style.css?ver=4.6.11' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='artday-dynamic-css'
          href='<?php echo $webroot ?>/assets/homenew/css/dynamic.css?ver=4.6.11' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='artday-fonts-css'
          href='https://fonts.googleapis.com/css?family=PT+Serif%7CMontserrat&#038;subset=latin%2Clatin-ext'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='js_composer_front-css'
          href='<?php echo $webroot ?>/assets/homenew/css/js_composer.min.css?ver=4.12.1'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='ulp-css'
          href='<?php echo $webroot ?>/assets/homenew/css/style.min.css?ver=5.83'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='ulp-link-buttons-css'
          href='<?php echo $webroot ?>/assets/homenew/css/link-buttons.min.css?ver=5.83'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='perfect-scrollbar-css'
          href='<?php echo $webroot ?>/assets/homenew/css/perfect-scrollbar-0.4.6.min.css?ver=5.83'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='animate.css-css'
          href='<?php echo $webroot ?>/assets/homenew/css/animate.min.css?ver=5.83'
          type='text/css' media='all'/>

    <script type='text/javascript'
            src='<?php echo $webroot ?>/assets/homenew/js/jquery.js?ver=1.12.4'></script>


    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>

    <script type='text/javascript'
            src='<?php echo $webroot ?>/assets/homenew/js/jquery-migrate.min.js?ver=1.4.1'></script>
    <script type='text/javascript'
            src='<?php echo $webroot ?>/assets/homenew/js/jquery.themepunch.tools.min.js?ver=5.2.6'></script>
    <script type='text/javascript'
            src='<?php echo $webroot ?>/assets/homenew/js/jquery.themepunch.revolution.min.js?ver=5.2.6'></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="Artist/commoncss.css">
    <link rel="stylesheet" href="assets/css/homepage_stylesheet.css">
</head>
<body class="page page-id-187 page-template page-template-template-home page-template-template-home-php wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

<!-- Loader Start -->
<div id="preloader">
    <div class="preloader-container">
        <div class="ws-spinner"></div>
    </div>
</div>
<!-- End Loader Start -->


<!-- Header Start -->
<?php require_once './layout/header.php'; ?>
<!-- End Header -->


<!-- Container Start -->
<div class="container ws-page-container">
    <!-- Row Start -->
    <div class="row">
        <div class="col-sm-12">

            <article id="post-187" class="post-187 page type-page status-publish hentry">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_revslider_element wpb_content_element">
                                    <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullscreen-container"
                                         style="background-color:#f5f5f5;padding:0px;">
                                        <!-- START REVOLUTION SLIDER 5.2.6 fullscreen mode -->
                                        <div id="rev_slider_4_1" class="rev_slider fullscreenbanner"
                                             style="display:none;" data-version="5.2.6">
                                            <ul>    <!-- SLIDE  -->
                                                <li data-index="rs-8" data-transition="fade" data-slotamount="default"
                                                    data-hideafterloop="0" data-hideslideonmobile="off"
                                                    data-easein="default" data-easeout="default" data-masterspeed="1500"
                                                    data-rotate="0" data-saveperformance="off" data-title="Intro"
                                                    data-param1="" data-param2="" data-param3="" data-param4=""
                                                    data-param5="" data-param6="" data-param7="" data-param8=""
                                                    data-param9="" data-param10="" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="" alt="" title="3D Parallax Image"
                                                         data-bgposition="center center" data-bgfit="cover"
                                                         data-bgrepeat="no-repeat" data-bgparallax="5"
                                                         class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->

                                                    <!-- LAYER NR. 1 -->
                                                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-6"
                                                         id="slide-8-layer-1"
                                                         data-x="['right','right','center','center']"
                                                         data-hoffset="['-134','-453','0','0']"
                                                         data-y="['middle','middle','middle','bottom']"
                                                         data-voffset="['20','50','71','105']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"
                                                         data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                                         data-transform_out="opacity:0;s:1500;e:Power4.easeIn;"
                                                         data-start="2500"
                                                         data-responsive_offset="on"


                                                         style="z-index: 5;"><img
                                                                src="<?php echo $webroot ?>/assets/img/masterpiece1.png"
                                                                alt="" width="800" height="571"
                                                                data-ww="['1000px','1000px','500px','350px']"
                                                                data-hh="['600px','600px','300px','210px']"
                                                                data-no-retina></div>

                                                    <!-- LAYER NR. 2 -->
                                                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                                                         id="slide-8-layer-3"
                                                         data-x="['left','left','center','center']"
                                                         data-hoffset="['593','633','-110','-60']"
                                                         data-y="['top','top','top','bottom']"
                                                         data-voffset="['123','203','450','90']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"

                                                         data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                                         data-transform_out="opacity:0;s:1500;e:Power4.easeIn;"
                                                         data-start="2750"
                                                         data-responsive_offset="on"


                                                         style="z-index: 6;"><img
                                                                src="<?php echo $webroot ?>/assets/img/masterpiece2.png"
                                                                alt="" width="800" height="926"
                                                                data-ww="['518px','518px','200px','170px']"
                                                                data-hh="['600px','600px','251px','213px']"
                                                                data-no-retina></div>

                                                    <!-- LAYER NR. 3 -->
                                                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                                                         id="slide-8-layer-5"
                                                         data-x="['left','left','left','left']"
                                                         data-hoffset="['590','553','357','258']"
                                                         data-y="['top','top','top','top']"
                                                         data-voffset="['248','297','522','489']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"

                                                         data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                                         data-transform_out="opacity:0;s:1500;e:Power4.easeIn;"
                                                         data-start="3000"
                                                         data-responsive_offset="on"


                                                         style="z-index: 7;"><img
                                                                src="<?php echo $webroot ?>/assets/img/masterpiece3.png"
                                                                alt="" width="800" height="926"
                                                                data-ww="['400px','400px','130px','100px']"
                                                                data-hh="['463px','463px','225px','173px']"
                                                                data-no-retina></div>

                                                    <!-- LAYER NR. 4 -->
                                                    <div class="tp-caption WebProduct-Title   tp-resizeme rs-parallaxlevel-7"
                                                         id="slide-8-layer-7"
                                                         data-x="['left','left','center','center']"
                                                         data-hoffset="['30','30','0','0']"
                                                         data-y="['middle','middle','top','top']"
                                                         data-voffset="['-50','-50','47','80']"
                                                         data-fontsize="['90','90','75','60']"
                                                         data-lineheight="['90','90','75','60']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"
                                                         data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;"
                                                         data-transform_out="opacity:0;s:1500;e:Power4.easeIn;"
                                                         data-start="1000"
                                                         data-splitin="none"
                                                         data-splitout="none"
                                                         data-responsive_offset="on"


                                                         style="z-index: 8; white-space: nowrap;font-size:20px !important;">
                                                        Enhance your walls<br/>with handmade artworks<br/>from artists
                                                        around <br/>the world
                                                    </div>

                                                    <!-- LAYER NR. 5 -->
                                                    <a class="tp-caption ws-slider-btn rev-btn  rs-parallaxlevel-8"
                                                       href="<?php echo $webroot ?>/Paintings/paintings.php"
                                                       target="_self"
                                                       id="slide-8-layer-8"
                                                       data-x="['left','left','center','center']"
                                                       data-hoffset="['30','30','0','0']"
                                                       data-y="['middle','middle','top','top']"
                                                       data-voffset="['178','178','305','320']"
                                                       data-fontsize="['13','13','16','16']"
                                                       data-lineheight="['13','13','48','48']"
                                                       data-fontweight="['700','700','100','700']"
                                                       data-width="none"
                                                       data-height="none"
                                                       data-whitespace="nowrap"
                                                       data-transform_idle="o:1;"
                                                       data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;"
                                                       data-style_hover="c:rgba(255, 255, 255, 1.00);"

                                                       data-transform_in="y:-50px;opacity:0;s:500;e:Power3.easeInOut;"
                                                       data-transform_out="s:1500;"
                                                       data-start="1750"
                                                       data-splitin="none"
                                                       data-splitout="none"
                                                       data-actions=''
                                                       data-responsive_offset="on"
                                                       data-responsive="off"

                                                       style="z-index: 9; white-space: nowrap;font-family:Montserrat;text-align:center;background-color:rgba(0, 0, 0, 1.00);padding:18px 46px 18px 46px;letter-spacing:1px;">Browse
                                                        Artworks</a>
                                                </li>
                                            </ul>
                                            <div style="" class="tp-static-layers">

                                                <!-- LAYER NR. 1 -->
                                                <div class="tp-caption -   tp-static-layer"
                                                     id="slider-4-layer-1"
                                                     data-x="['right','right','right','right']"
                                                     data-hoffset="['30','30','30','30']"
                                                     data-y="['top','top','top','top']"
                                                     data-voffset="['30','30','30','30']"
                                                     data-width="none"
                                                     data-height="none"
                                                     data-whitespace="nowrap"
                                                     data-transform_idle="o:1;"

                                                     data-transform_in="opacity:0;s:1000;e:Power3.easeInOut;"
                                                     data-transform_out="auto:auto;s:1000;"
                                                     data-start="500"
                                                     data-splitin="none"
                                                     data-splitout="none"
                                                     data-actions='[{"event":"click","action":"toggleclass","layer":"slider-4-layer-1","delay":"0","classname":"open"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slider-4-layer-3","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slider-4-layer-4","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slider-4-layer-5","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slider-4-layer-6","delay":"0"}]'
                                                     data-basealign="slide"
                                                     data-responsive_offset="off"
                                                     data-responsive="off"
                                                     data-startslide="-1"
                                                     data-endslide="-1"

                                                     style="z-index: 5; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);text-transform:left;">
                                                    <div id="rev-burger">
                                                        <span></span>
                                                        <span></span>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tp-bannertimer tp-bottom"
                                                 style="visibility: hidden !important;"></div>
                                        </div>
                                        <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                            var htmlDivCss = ".tp-caption.WebProduct-Title,.WebProduct-Title{color:rgba(51,51,51,1.00);font-size:90px;line-height:90px;font-weight:100;font-style:normal;font-family:Montserrat;padding:0 0 0 0px;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px}.tp-caption.ws-slider-btn,.ws-slider-btn{color:rgba(255,255,255,1.00);font-size:13px;line-height:13px;font-weight:500;font-style:normal;font-family:Montserrat;padding:0px 40px 0px 40px;text-decoration:none;background-color:rgba(51,51,51,1.00);border-color:rgba(0,0,0,1.00);border-style:none;border-width:2px;border-radius:0 0 0 0px;text-align:left}.tp-caption.ws-slider-btn:hover,.ws-slider-btn:hover{color:rgba(51,51,51,1.00);text-decoration:none;background-color:rgba(255,255,255,1.00);border-color:rgba(0,0,0,1.00);border-style:none;border-width:2px;border-radius:0 0 0 0px}";
                                            if (htmlDiv) {
                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                            } else {
                                                var htmlDiv = document.createElement("div");
                                                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                            }
                                        </script>
                                        <script type="text/javascript">
                                            /******************************************
                                             -    PREPARE PLACEHOLDER FOR SLIDER    -
                                             ******************************************/

                                            var setREVStartSize = function () {
                                                try {
                                                    var e = new Object, i = jQuery(window).width(), t = 9999, r = 0,
                                                        n = 0, l = 0, f = 0, s = 0, h = 0;
                                                    e.c = jQuery('#rev_slider_4_1');
                                                    e.responsiveLevels = [1240, 1024, 778, 480];
                                                    e.gridwidth = [1400, 1240, 778, 480];
                                                    e.gridheight = [768, 768, 960, 720];

                                                    e.sliderLayout = "fullscreen";
                                                    e.fullScreenAutoWidth = 'off';
                                                    e.fullScreenAlignForce = 'off';
                                                    e.fullScreenOffsetContainer = '.ws-topbar, header';
                                                    e.fullScreenOffset = '';
                                                    if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                                                            f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                                                        }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                                                        var u = (e.c.width(), jQuery(window).height());
                                                        if (void 0 != e.fullScreenOffsetContainer) {
                                                            var c = e.fullScreenOffsetContainer.split(",");
                                                            if (c) jQuery.each(c, function (e, i) {
                                                                u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                                                            }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                                                        }
                                                        f = u
                                                    } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                                                    e.c.closest(".rev_slider_wrapper").css({height: f})

                                                } catch (d) {
                                                    console.log("Failure at Presize of Slider:" + d)
                                                }
                                            };

                                            setREVStartSize();

                                            var tpj = jQuery;

                                            var revapi4;
                                            tpj(document).ready(function () {
                                                if (tpj("#rev_slider_4_1").revolution == undefined) {
                                                    revslider_showDoubleJqueryError("#rev_slider_4_1");
                                                } else {
                                                    revapi4 = tpj("#rev_slider_4_1").show().revolution({
                                                        sliderType: "hero",
                                                        jsFileLocation: "//wossthemes.com/artday/wp-content/plugins/revslider/public/assets/js/",
                                                        sliderLayout: "fullscreen",
                                                        dottedOverlay: "none",
                                                        delay: 9000,
                                                        navigation: {},
                                                        responsiveLevels: [1240, 1024, 778, 480],
                                                        visibilityLevels: [1240, 1024, 778, 480],
                                                        gridwidth: [1400, 1240, 778, 480],
                                                        gridheight: [768, 768, 960, 720],
                                                        lazyType: "none",
                                                        parallax: {
                                                            type: "mouse+scroll",
                                                            origo: "slidercenter",
                                                            speed: 1000,
                                                            levels: [5, 10, 15, 20, 25, 30, 5, 0, 45, 50, 47, 48, 49, 50, 51, 55],
                                                            type: "mouse+scroll",
                                                        },
                                                        shadow: 0,
                                                        spinner: "off",
                                                        autoHeight: "off",
                                                        fullScreenAutoWidth: "off",
                                                        fullScreenAlignForce: "off",
                                                        fullScreenOffsetContainer: ".ws-topbar, header",
                                                        fullScreenOffset: "",
                                                        disableProgressBar: "on",
                                                        hideThumbsOnMobile: "off",
                                                        hideSliderAtLimit: 0,
                                                        hideCaptionAtLimit: 0,
                                                        hideAllCaptionAtLilmit: 0,
                                                        debugMode: false,
                                                        fallbacks: {
                                                            simplifyAll: "off",
                                                            disableFocusListener: false,
                                                        }
                                                    });
                                                }
                                            });
                                            /*ready*/
                                        </script>
                                    </div><!-- END REVOLUTION SLIDER --></div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- WHO WE ARE SECTION START-->
                <div class="padding-top-x50"></div>
                <div class="container">
                    <div class="row vertical-align">
                        <div class="col-sm-6" style="padding-bottom: 25px ">
                            <h3 class="who_we">WHO WE ARE</h3>
                            <div class="ws-footer-separator"></div>
                            <p class="inner_Who">We are a dedicated team of developers, artists, designers,
                                and marketeers with a vision to help non-digital artists
                                reach a global audience, both digitally and tangibly.</p>

                            <p class="inner_Who">Started in 2016, the Masterpiece community now has
                                over 3000 artists across 65 countries and grows stronger
                                every day.</p>

                            <p class="inner_Who">We aspire to promote artist’s stories and emotions
                                expressed through their handmade art. As technologists
                                our mission is to help these artists breach all barriers in
                                today’s digital world that prevents them from reaching
                                the right audience on a global scale.</p>
                        </div>

                        <div class="col-sm-6" style="padding-bottom: 25px">
                            <img src="<?php echo $webroot ?>/assets/img/masterpiecelogo.png" alt="your masterpiece"
                                 class="img-responsive" style="width:375px">
                        </div>
                    </div>
                </div>

                <!-- WHO WE ARE SECTION END -->


                <!-- FOR ARTIST AND PAINTING START-->

                <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true"
                     class="vc_row wpb_row vc_row-fluid ws-category-box vc_row-no-padding vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                    <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
                        <div class="vc_column-inner vc_custom_1454576392245">
                            <div class="wpb_wrapper">
                                <h4 class="vc_custom_heading for_artist">FOR ARTISTS</h4>
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">
                                            Post your handmade works by <br/>clicking on 'upload art' and setting <br/>
                                            your price
                                        </p>
                                    </div>
                                </div>
                                <div class="vc_empty_space" style="height: 30px"><span
                                            class="vc_empty_space_inner"></span></div>
                                <div class="vc_btn3-container vc_btn3-center">
                                    <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-classic vc_btn3-color-default"
                                       href="<?php echo $webroot ?>/auth/login.php"
                                       title="">publish now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_single_image wpb_content_element vc_align_center">

                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                            <img height="600"
                                                 src="<?php echo $webroot ?>/assets/img/art-sell.jpg"
                                                 class="vc_single_image-img attachment-full"
                                                 alt="category_image_01"
                                                 sizes="(max-width: 1000px) 100vw, 1000px"/>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="vc_row-full-width vc_clearfix"></div>
                <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true"
                     class="vc_row wpb_row vc_row-fluid ws-category-box vc_row-no-padding vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_single_image wpb_content_element vc_align_center">

                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                            <img width="1000"
                                                 height="600"
                                                 src="<?php echo $webroot ?>/assets/img/buy-art.jpg"
                                                 class="vc_single_image-img attachment-full buy-artimage"
                                                 alt="category_image_02"
                                                 sizes="(max-width: 1000px) 100vw, 1000px"/>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
                        <div class="vc_column-inner vc_custom_1454576392245">
                            <div class="wpb_wrapper">
                                <h4 class="vc_custom_heading for_artist">FOR ART LOVERS <br> & BUYERS</h4>
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">
                                            Browse through the artworks and <br/> click on 'request to buy' for
                                            your<br/> desired artwork
                                        </p>
                                    </div>
                                </div>
                                <div class="vc_empty_space" style="height: 30px"><span
                                            class="vc_empty_space_inner"></span></div>
                                <div class="vc_btn3-container vc_btn3-center">
                                    <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-classic vc_btn3-color-default"
                                       href="<?php echo $webroot ?>/Paintings/paintings.php"
                                       title="">BROWSE ARTWORKS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FOR ARTIST -->



                <!-- JOIN PLATFORM SECTION -->
                <div class="vc_row-full-width vc_clearfix"></div>
                <div class="vc_row wpb_row vc_row-fluid" style="padding-bottom: 25px">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="vc_empty_space" style="height: 50px">
                                    <span class="vc_empty_space_inner"></span>
                                </div>
                                <div class="ws-separator"></div>
                                <h3 class="ws-heading text-center padding-bottom-x15">TO JOIN THE MASTERPIECE
                                    PLATFORM</h3>

                                <div class="vc_btn3-container vc_btn3-center">
                                    <a class="create_profile btn ws-btn-subscribe"
                                       href="<?php echo $webroot ?>/auth/register.php"
                                       title="">CREATE PROFILE
                                    </a>

                                    <p class="profile_text">And tell us about yourself in the bio</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END JOIN PLATFORM -->


                <!-- BUTTON FOR DOWNLOAD START ANDROID OR IOS -->
                <section class="ws-call-section appblock"
                         style="margin-bottom:50px;background: #353535 !important">
                    <div class="ws-overlay">
                        <div class="ws-parallax-caption">
                            <div class="ws-parallax-holder">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <h2 class="app_heading">Download Your Masterpieces mobile app</h2>
                                    <div class="ws-separator"></div>
                                    <!--<p>Our blog covering interesting stories around culture and design.<br>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
                                    -->
                                    <div class="col-md-12" style="margin-top: 15px">

                                        <div class="col-md-6" style="margin-bottom: 15px">

                                                <a href="https://play.google.com/store/apps/details?id=com.codon.masterpiece&amp;hl=en">

                                                    <img src="<?php echo $webroot?>/assets/img/android.png" style="height: 60px;border: none;">

                                                </a>

                                        </div>

                                        <div class="col-md-6" style="margin-bottom: 15px">

                                                <a href="https://itunes.apple.com/in/app/masterpiece-platform-for-non/id1089890769?mt=8">
                                                    <img src="<?php echo $webroot?>/assets/img/ios.svg" style="height: 60px;border: none;">
                                                </a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>



                <!-- FEATURED ARTIST Collection Start  -->
                <div class="vc_row-full-width vc_clearfix"></div>
                <div class="padding-top-x70"></div>
                <div class="container">
                    <div class="row">

                        <div class="ws-works-title artist_heading">
                            <div class="col-sm-12">
                                <h3 class="who_we">Featured Artists</h3>
                                <div class="ws-separator"></div>
                            </div>
                        </div>

                        <!-- Item -->


                        <?php
                        $sql = "SELECT * FROM `users` WHERE id=2382 OR id=434 OR id=687"; //fetching 32 Result Only from Users Table

                       // $sql = "SELECT * FROM `users` Where created_at BETWEEN '2018-02-01' AND '2018-05-01' ORDER BY totLikes DESC LIMIT 3";
                        $result = $conn->query($sql);
                        $artist_id = '';  // varibale is used for load more button
                        $lat = '';
                        $lng = '';
                        $address = '';

                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {

                                $id = $row['id'];  // id for detail page of artist
                                $artist_name = $row['firstname'];
                                $fb_id = $row['fb_id'];
                                $location = $row['location']; //storing latlong value into a variable
                                ?>


                                <!-- FIRST ARTIST HARDCODED -->
                                <div class="col-sm-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                                <div class="wpb_wrapper">

                                                    <a href="Artist/artist_details.php/<?php echo friendlyURL($id, $artist_name); ?>">

                                                        <img src="<?php get_artist_profile_image($conn, $id, $fb_id); ?>"
                                                             alt="Your Masterpieces || <?php echo $row['firstname']; ?>"
                                                             class="img_circle img_artist">
                                                    </a>

                                                    <h3 class="artist_name"> <?php echo $row['firstname']; ?> </h3>
                                                    <div class="ws-separator-small"></div>
                                                    <h5 class="location">

                                                        <?php
                                                        get_artist_location($location);
                                                        ?>
                                                    </h5>
                                                    <br/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php }
                        } ?>
                        <!--FIRST ARTIST HARDCODED END -->
                    </div>
                </div>
                <!-- FEATURED ARTIST Collection End  -->


                <!-- FEATURED PAINTING Collection Start  -->
                <div class="vc_row-full-width vc_clearfix"></div>
                <div class="padding-top-x70"></div>
                <section class="ws-works-section">
                    <div class="container">
                        <div class="row">
                            <div class="ws-works-title">
                                <div class="col-sm-12">
                                    <h3 class="who_we">Featured ARTWORKS</h3>
                                    <div class="ws-separator" style="margin-bottom: 85px"></div>
                                </div>
                            </div>


                            <!-- Item -->
                            <?php
                            $sql = "SELECT * FROM tasks WHERE id='236' OR id='1424' OR id='2326' ORDER BY id DESC"; //fetching 12 Result Only from Users Table

                            //$sql = "SELECT * FROM `tasks` Where created_at BETWEEN '2018-02-01' AND '2018-04-01' ORDER BY tot_likes DESC LIMIT 3";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while ($row = $result->fetch_assoc()) {
                                    $product_id = $row['id'];
                                    $art_image = $row['image_path'];
                                    $art_name = $row['task'];
                                    $art_description = $row['task_description'];
                                    $art_technique = $row['technique'];
                                    $art_price = $row['price'];
                                    $art_new_price=$row['price_new'];
                                    ?>

                                    <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.1s, ease-in 20px'>

                                        <a href="Paintings/painting_details.php/<?php echo friendlyURL($product_id, $art_name); ?>">

                                            <div class="ws-item-offer product_background">
                                                <!-- Image -->
                                                <figure>
                                                    <img src="<?php echo $art_image; ?>"
                                                         alt="Your Masterpieces || <?php echo $art_name; ?>"
                                                         class="img-responsive margin_set">
                                                </figure>
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
                                                <div class="ws-works-caption text-left">
                                                    <!-- Title -->
                                                    <h3 class="ws-item-title font_17"><?php echo $art_name; ?></h3>
                                                    <!-- Item Category -->
                                                    <div class="ws-item-category one_line"><?php echo $art_technique; ?></div>

                                                    <div class="ws-footer-separator ptzero"></div>
                                                    <!-- Price -->
                                                    <div class="ws-item-price">
                                                        <?php if ((!empty($curr_code)) && (!empty($art_new_price)))
                                                            echo "$curr_code " . $art_new_price;
                                                        else get_painting_price($art_price);
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-3 col-xs-3">

                                                <!-- LIKE SECTION START -->
                                                <div class="pull-right for_like">

                                                <span class="countlike<?php echo $product_id ?>"
                                                      id="countlike"><?php $db->countlikesArt($product_id); ?>
                                                </span>


                                                    <?php if (isset($_SESSION['login_user'])) { // Checking Session user
                                                        $user_id = $_SESSION['login_user'];
                                                        $db->hasUserLiked($user_id, $product_id); // Checking that user has liked painting or not
                                                        if (($db->hasUserLiked($user_id, $product_id)) == true) {
                                                            echo "<a class='go_dislike' href='javascript:void(0)' data-id='$product_id'><i class=\"fa fa-heart heart\" ></i></a>";
                                                        } else {
                                                            echo "<a class='go_like' href='javascript:void(0)' data-id='$product_id'><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                        }
                                                    } else { //When user is not logined
                                                        echo " <a class='login-required' href='#login-modal-shortlist' data-toggle='modal' ><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                    }
                                                    ?>
                                                </div>
                                                <!-- LIKE SECTION END -->
                                            </div>
                                        </a>
                                    </div>
                                    <br>
                                <?php }
                            } ?>
                        </div>
                    </div>
                </section>
                <!-- FEATURED PAINTING Collection End  -->

                <!-- NEW ARRIVALS-->
                <div class="vc_row-full-width vc_clearfix"></div>
                <div class="padding-top-x70"></div>
                <div class="row">
                    <div class="ws-works-title">
                        <div class="col-sm-12">
                            <h3 class="who_we">LATEST ARTWORKS</h3>
                            <div class="ws-separator" style="margin-bottom: 65px"></div>
                        </div>
                    </div>
                </div>
                <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true"
                     class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
                    <div id="ws-items-carousel">

                        <?php
                        $sql = "SELECT * FROM tasks WHERE status=0 ORDER BY id DESC LIMIT 12"; //fetching 12 Result Only from Users Table
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {
                                $product_id = $row['id'];
                                $art_image = $row['image_path'];
                                $art_name = $row['task'];
                                $art_description = $row['task_description'];
                                $art_technique = $row['technique'];
                                $art_price = $row['price'];
                                $art_new_price=$row['price_new'];
                                $tot_likes = $row['tot_likes'];
                                $curr_code = $row['curr_code'];
                                ?>


                                <!-- Item -->
                                <div class="ws-works-item">
                                    <a id=""
                                       href="Paintings/painting_details.php/<?php echo friendlyURL($product_id, $art_name); ?>">
                                        <div class="ws-item-offer latest_background">
                                            <!-- Image -->
                                            <figure class="ws-product-bg">
                                                <img width="475" height="530"
                                                     src="<?php echo $art_image; ?>"
                                                     alt="Your Masterpieces <?php echo $art_name; ?>"
                                                     style="max-height:485px"
                                                     class="attachment-arrivals-thumb size-arrivals-thumb wp-post-image"
                                                />
                                            </figure>
                                        </div>

                                        <div class="ws-works-caption text-center">
                                            <!-- Item Category -->
                                            <div class="ws-item-category"><?php echo $art_technique; ?></div>

                                            <!-- Title -->
                                            <h3 class="ws-item-title"><?php echo $art_name; ?></h3>

                                            <div class="ws-item-separator"></div>

                                            <!-- Price -->
                                            <div class="ws-item-price">
                                        <span class="woocommerce-Price-amount amount">
                                            <span class="woocommerce-Price-currencySymbol"></span>
                                            <!-- PRICE IF CURR CODE AVAILBALE -->
                                            <?php if ((!empty($curr_code)) && (!empty($art_new_price)))
                                                echo "$curr_code " . $art_new_price;
                                            else get_painting_price($art_price);
                                            ?>
                                            <!-- END PRICE CODE -->
                                        </span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php }
                        } ?>
                        <!-- END ITEM -->


                    </div>


                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="vc_empty_space" style="height: 70px">
                                    <span class="vc_empty_space_inner"></span>
                                </div>
                                <div class="vc_btn3-container vc_btn3-center">
                                    <a class="create_profile btn ws-btn-subscribe"
                                       onclick="cateogry('latest')">
                                       See More Latest Artworks
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row-full-width vc_clearfix"></div>
                <!-- NEW ARRIAVLS END -->


                <!-- Painting about cateogry Section Start -->
                <section class="ws-about-section" id="cateogry">
                    <div class="container">
                        <div class="row">

                            <!-- Description -->
                            <div class="ws-about-content clearfix">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h3 class="who_we">CATEGORIES</h3>
                                    <div class="ws-separator" style="margin-bottom: 65px"></div>

                                </div>
                            </div>


                            <!-- Featured Collections -->
                            <div class="ws-featured-collections clearfix">

                                <!-- Item -->
                                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                                    <a onclick="cateogry('water')" style="cursor: pointer">
                                        <div class="thumbnail">
                                            <!--<img src="assets/img/backgrounds/water.jpg" alt="Masterpieces Buy Painting Online">-->
                                            <div class="ws-overlay">
                                                <div class="caption">
                                                    <h3>WaterColor</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <!-- Item -->
                                <div class="col-sm-3 col-md-3 col-xs-6  featured-collections-item">
                                    <a onclick="cateogry('acrylic')" style="cursor: pointer">
                                        <div class="thumbnail">
                                            <!-- <img src="assets/img/backgrounds/acrylic.jpg" alt="Masterpieces Buy Painting Online">-->
                                            <div class="ws-overlay">
                                                <div class="caption">
                                                    <h3>Acrylic</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <!-- Item -->
                                <div class="col-sm-3 col-md-3 col-xs-6  featured-collections-item">
                                    <a onclick="cateogry('oil')" style="cursor: pointer">
                                        <div class="thumbnail">
                                            <!--<img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                                            <div class="ws-overlay">
                                                <div class="caption">
                                                    <h3>Oil Paint</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <!-- Item -->
                                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                                    <a onclick="cateogry('pencil')" style="cursor: pointer">
                                        <div class="thumbnail">
                                            <!--<img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                                            <div class="ws-overlay">
                                                <div class="caption">
                                                    <h3 style="padding-bottom: 5px">Pencil</h3>
                                                    <h3>& Charcoal</span></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!-- Item -->
                                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                                    <a onclick="cateogry('graphite')" style="cursor: pointer">
                                        <div class="thumbnail">
                                            <!-- <img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                                            <div class="ws-overlay">
                                                <div class="caption">
                                                    <h3>Graphite</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!-- Item -->
                                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                                    <a onclick="cateogry('top')" style="cursor: pointer">
                                        <div class="thumbnail">
                                            <!-- <img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                                            <div class="ws-overlay">
                                                <div class="caption">
                                                    <h3 style="padding-bottom: 5px">Top</h3>
                                                    <h3>Artworks</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>


                                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                                    <a onclick="cateogry('2016')" style="cursor: pointer">
                                        <div class="thumbnail">
                                            <!-- <img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                                            <div class="ws-overlay">
                                                <div class="caption">
                                                    <h3 style="padding-bottom: 5px">2016 </h3>
                                                    <h3>Artworks</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <!-- Item -->
                                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                                    <a onclick="cateogry('2017')" style="cursor: pointer">
                                        <div class="thumbnail">
                                            <!--<img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                                            <div class="ws-overlay">
                                                <div class="caption">
                                                    <h3 style="padding-bottom: 5px">2017 </h3>
                                                    <h3>Artworks</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <!-- Item -->
                                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                                    <a onclick="cateogry('2018')" style="cursor: pointer">
                                        <div class="thumbnail">
                                            <!-- <img src="assets/img/backgrounds/oil.jpg" alt="Masterpieces Buy Painting Online ">-->
                                            <div class="ws-overlay">
                                                <div class="caption">
                                                    <h3 style="padding-bottom: 5px">2018 </h3>
                                                    <h3>Artworks</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <!-- Item -->
                                <div class="col-sm-3 col-md-3 col-xs-6 featured-collections-item">
                                    <a onclick="cateogry('latest')" style="cursor: pointer">
                                        <div class="thumbnail">
                                            <div class="ws-overlay">
                                                <div class="caption">
                                                    <h3 style="padding-bottom: 5px">Latest</h3>
                                                    <h3>Artworks</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!-- Item -->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Painting Catteogry Section End -->


            </article>

        </div>

    </div><!-- Row End -->
</div><!-- Container End -->


<script type="text/javascript">
    function cateogry(typeCat) {
        page_viewed_counter(typeCat);
    }
</script>

<script type="text/javascript">
    function page_viewed_counter(typeCat) {
        var n = localStorage.getItem('type');
        n = typeCat;
        localStorage.setItem("type", n);
        var page_viewed = localStorage.type;
        if (page_viewed == typeCat) {
            window.location = "Paintings/paintings.php";
        }
    }
</script>


<?php include './layout/login_modal.php' ?>

<!-- SUBSCRIBE SECTION START -->
<?php include './layout/subscriber.php'; ?>
<!-- End Subscribe Section -->


<!-- Footer Start -->

<?php require_once './layout/footer.php'; ?>
<!-- Footer End -->

<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/jquery.blockUI.min.js?ver=2.70'></script>

<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/cart-fragments.min.js?ver=2.6.4'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/bootstrap.min.js?ver=4.6.11'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/parallax.min.js?ver=4.6.11'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/scrollReveal.min.js?ver=4.6.11'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/owl.carousel.min.js?ver=4.6.11'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/bootstrap-dropdownhover.min.js?ver=4.6.11'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/jquery.sticky.js?ver=4.6.11'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/main.js?ver=4.6.11'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var options_isw = {
        "error": "There was a problem.Please try again.",
        "ajax_method": "method_load_more_button",
        "number_of_products": "8",
        "wrapper_result_count": ".woocommerce-result-count",
        "wrapper_breadcrumb": ".woocommerce-breadcrumb",
        "wrapper_products": "ul.products",
        "wrapper_pagination": ".pagination, .woo-pagination, .woocommerce-pagination, .emm-paginate, .wp-pagenavi, .pagination-wrapper",
        "selector_next": ".next",
        "icon": "http:\/\/wossthemes.com\/artday\/wp-content\/plugins\/woss-shortcodes\/include\/icons\/ajax-loader.gif",
        "load_more_button_text": "Load More",
        "load_more_button_animate": "",
        "load_more_transition": "",
        "animate_to_top": "",
        "pixels_from_top": "0",
        "start_loading_x_from_end": "0",
        "paged": "1"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/custom.js?ver=4.6.11'></script>
<script type='text/javascript' src='<?php echo $webroot ?>/assets/homenew/js/plusone.js?ver=4.6.11'></script>
<script type='text/javascript' src='<?php echo $webroot ?>/assets/homenew/js/wp-embed.min.js'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/assets/homenew/js/js_composer_front.min.js'></script>
<script src="<?php echo $webroot ?>/Paintings/js/like.js" type="text/javascript"></script>


</body>
</html>
