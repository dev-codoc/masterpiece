<style type="text/csss">
    .ws-footer-about p {
        color: #999;
        font-size: 12px;
        letter-spacing: 1px;
        font-family: Montserrat !important;
    }

    @media screen and (max-width: 479px) {
        .height_less {
            margin-top: -95px;
        }
    }

</style>


<footer class="ws-footer">
    <div class="container">
        <div class="row">
            <!-- About -->
            <div class="col-sm-6 ws-footer-col">
                <div id="text-3" class="bar widget-space widget_text">
                    <div class="sidebar-title"><h3>About Us</h3></div>
                    <div class="ws-footer-separator"></div>
                    <div class="ws-footer-about">
                        <p>We are a dedicated team of developers, artists, designers, and marketers with a vision to
                            help non-digital artists reach a global audience, both digitally and tangibility.We aspire
                            to promote artists’ stories and emotions expressed through their handmade art. As
                            technologists, our mission is to help these artists breach all barriers in today’s digital
                            world helping them reach the right audience on a global scale.
                        </p>
                    </div>
                </div>
            </div>

            <!-- Support Links -->
            <div class="col-sm-2 ws-footer-col">
            </div>

            <div class="col-sm-2 ws-footer-col">
                <div id="nav_menu-4" class="bar widget-space widget_nav_menu">
                    <div class="sidebar-title"><h3>Links</h3></div>
                        <div class="ws-footer-separator"></div>
                    <div class="menu-company-container">
                        <ul id="menu-company" class="menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-668"><a
                                        href="<?php echo $webroot ?>">Home</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-664">
                                <a href="<?php echo $webroot ?>/support.php">Support</a>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-665">
                                <a href="<?php echo $webroot ?>/policy.php">Policy</a>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-666">
                                <a href="<?php echo $webroot ?>/terms&conditions.php">Terms &amp;
                                    Conditions</a></li>

                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-666">
                                <a href="<?php echo $webroot ?>/faq.php">FAQ's</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</footer>

<div class="ws-footer-bar">
    <div class="container">

        <!-- Copyright -->
        <div class="pull-left">
            <p>Masterpieces © 2018 All rights reserved.</p></div>

        <!-- Payments -->
        <div class="pull-right">
            <ul class="ws-footer-payments">
                <li><i class="fa fa-cc-visa fa-lg"></i></li>
                <li><i class="fa fa-cc-paypal fa-lg"></i></li>
                <li><i class="fa fa-cc-mastercard fa-lg"></i></li>
            </ul>
        </div>
    </div>
</div>
