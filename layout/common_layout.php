<link rel='stylesheet' id='bootstrap-css'  href='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/css/bootstrap.min.css?ver=4.7.8' type='text/css' media='all' />
<link rel='stylesheet' id='artday-style-css'  href='<?php echo $webroot ?>/layout/assets/css/style.css?ver=4.7.8' type='text/css' media='all' />
<link rel='stylesheet' id='artday-fonts-css'  href='https://fonts.googleapis.com/css?family=PT+Serif%7CMontserrat&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link href="http://fonts.googleapis.com/css?family=PT+Serif%3A400" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<link href="http://fonts.googleapis.com/css?family=Roboto%3A500" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<link href="http://fonts.googleapis.com/css?family=Crimson+Text%3A400" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<link rel='stylesheet' id='artday-dynamic-css'  href='<?php echo $webroot ?>/layout/assets/css/dynamic.css' type='text/css' media='all' />
