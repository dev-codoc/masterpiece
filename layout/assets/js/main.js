(function ($, window, document, undefined) {
    'use strict';

	jQuery(document).ready(function() {

    //Sticky Navbar
    jQuery(document).ready(function(){
      jQuery(".ws-header-fourth").sticky({topSpacing:0});
    });
	
    // Parallax Background
    jQuery('.parallax-window').parallax;

    // Owl Carousel
    jQuery("#ws-items-carousel").owlCarousel({
      items :4,
      navigation : true
    });

    // Page Loader
    jQuery("#preloader").delay(2000).fadeOut(500);

    // Scroll Reveal
    window.sr = new scrollReveal();

  });

})(jQuery, window, document);
