<!--suppress ALL -->
<style type="text/css">
    /*--------------------------------------
        SUBSCRIBE
    ---------------------------------------*/
    .ws-subscribe-section{
        border-top: 1px solid #f2f2f2;
        padding-top: 70px;
        padding-bottom: 70px;
    }
    .ws-subscribe-content h3{
        font-size: 22px;
        font-weight: 600;
        letter-spacing: 1px;
        color: #353535;
    }
    .ws-subscribe-content p{
        margin: 0;
    }
    .ws-subscribe-content input.ws-input-subscribe:focus{
        background-color: #EBEBEB;
    }
    .ws-subscribe-content input.ws-input-subscribe{
        text-transform: uppercase;
        font-size: 12px;
        line-height: 55px;
        height: 55px;
        padding: 0 25px;
        font-weight: 600;
        letter-spacing: 1px;
        border-radius: 0;
        border: none;
        background-color: #f5f5f5;
        -webkit-box-shadow: none;
        box-shadow: none;
        -ms-transition: .3s ease-in-out;
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
        width: 360px;
    }
    /*.ws-subscribe-content input.ws-btn-subscribe{
        color: #fff;
        text-decoration: none;
        text-transform: uppercase;
        background-color: #353535;
        line-height: 55px;
        padding: 0 45px;
        margin-top: -1px;
        width: 192px;
        font-size: 12px;
        letter-spacing: 1px;
        font-weight: 600;
        border: none;
        border-radius: 0;
        box-shadow: none;
        text-shadow: none;

    }*/

</style>



<section class="ws-subscribe-section">
    <div class="container">
        <div class="row">
            <!-- Subscribe Content -->
            <div class="ws-subscribe-content text-center clearfix">
                <div class="col-sm-8 col-sm-offset-2">
                    <h3>Subscribe to our Newsletter</h3>
                    <div class="ws-separator"></div>
                    <!-- Form -->

                    <form action="<?php echo $webroot?>/subscriber_submit.php" class="form-inline" method="post" id="subscriber">
                        <div class="form-group">
                            <input type="email" class="form-control ws-input-subscribe" placeholder="Enter your email"
                                   name="subscriber_email" required>
                            <input type="submit" class="btn ws-btn-subscribe" name="subscribe"></input>
                        </div>
                        <!-- Button -->
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
<!--
<script type="text/javascript">
    $(function () {
        //forget passwrd Button Clicked Google Analytics
        $('#subscribe').on('click', function (e) {
            console.log('Subscribe Button Clicked');
            e.preventDefault();
            ga('send', 'event', {
                eventCategory: 'Subscribe',
                eventAction: 'Subscribe Button Clicked',
                eventLabel: 'Subscribe Button Clicked'
            });
            setTimeout(() => {
                window.location = "subscriber_submit.php";
        }, 1000)
        });
    });
</script>-->