<script src="<?php echo $webroot?>/layout/config.js" type="text/javascript"></script>

<style type="text/css">
    .login-header{
        border:none !important;
    }

    @media screen and (max-width:479px){

        .login-modal{
            padding-left: 10px !important;
            padding-right: 10px !important;
        }
        .ws-register-modal-content {
            width: 100%;
            padding: 0 15px;
        }

    }
</style>

<!--Login Modal -->
<div class="modal fade" id="login-modal-shortlist" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content login-modal">
            <div class="modal-header login-header">
                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
            </div>

            <div class="modal-body">

                <div class="row" style="padding-bottom: 35px">
                    <div class="ws-register-modal-content">
                        <!-- Register Form -->
                        <form method="post" class="ws-register-form" id="login_ajax">

                            <h3>Login</h3>
                            <div class="ws-separator"></div>


                            <!--<div id="error_login"><?php
                            /*                                if (isset($_SESSION['login_user'])) {
                                                                echoRespnse(200, $response);
                                                            }
                                                            */ ?></div>-->

                            <!-- EMAIL START -->
                            <div class="form-group">
                                <label for="username" class="control-label">email address<span
                                            class="required">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" value=""/>
                            </div>
                            <!-- EMAIL END -->

                            <!-- PASSWORD START -->
                            <div class="form-group">
                                <label for="password" class="control-label">Password<span
                                            class="required">*</span></label>
                                <input class="form-control" type="password" name="password" id="password"/>
                            </div>
                            <!-- PASSWORD END -->

                            <!-- STAY LOGINED -->
                            <div class="pull-left">
                                <div class="checkbox">
                                    <label for="rememberme" class="inline">
                                        <input name="rememberme" type="checkbox" id="rememberme" value="forever"> Stay signed in				</label>
                                </div>
                            </div>
                            <!-- STAY LOGINED -->

                            <!-- FORGOT PAASWORD LINK START -->
                            <div class="pull-right">
                                <div class="ws-forgot-pass">
                                    <a href="<?php echo $webroot ?>/auth/forgot_password/index.php">Forgot password?</a>
                                </div>
                            </div>
                            <!-- FORGOT PASSWORD LINK EDN-->

                            <div class="clearfix"></div>


                            <!-- Button -->
                            <input type="button"
                                   class="btn ws-btn-fullwidth login_btn"
                                   name="login"
                                   value="Login"/>
                            <div class="padding-top-x20"></div>
                            <!-- Facebook Button -->


                        </form>
                        <!-- LOGIN Form-->
                        <div class="ws-register-form">
                            <!-- Link -->
                            <div class="ws-register-link">
                                <a href="<?php echo $webroot?>/auth/register.php">Not Registered? Click here to
                                    Register
                                </a>
                            </div>
                        </div>
                        <!-- End Register -->
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- Login Modal -->

<?php
$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>
<script type="text/javascript">
    var url = "<?php echo $url ?>";
    $(".login_btn").click(function () {
        console.log("login for shortlist required");
        $.ajax({
            type: "POST",
            url: HOST + "/auth/login_function_ajax.php",
            data: $("#login_ajax").serialize(),
            success: function (response) {
                //console.log(response);
                $('#login-modal-shortlist').modal('hide');
                window.location.href = url;
                return true;
            }
        });
    });

</script>