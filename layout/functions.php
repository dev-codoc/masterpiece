<?php

//$webroot = 'http://yourmasterpieces.com'; // Webroot Defined

$webroot="http://localhost/Masterpiecelive";

##FUNCTION FOR GETTING PRICE OF PAINTING WITH 10% EXTRA
function get_painting_price($art_price)
{

    if (empty($art_price)) {

        echo "Price Unavailable";
    } else {


        if ($art_price == "0") //When Price is Zero
        {
            echo "Price Unavailable";
        } else {
            // When Price is something Like 1250$,1200Usd,1200PKR
            $Price_number = preg_replace('/[^0-9]/', '', $art_price); //fetching only numeric value
            $Price_symbol = preg_replace('/[^a-zA-Z $ ₹]/', '', $art_price); // fetching only symbol or text

            $profit = ($Price_number * 100) / 100; //Advantage Price After 10%
            //$art_new_price = $Price_number + $profit; //Adding profit in original Price
            if (!empty ($Price_symbol)) {
                echo $Price_symbol;
            } else {
                echo "$";
            }
            echo " ";
            echo $Price_number;

        }
    }
}
## FUNCTION GET PRICE END


## PROFILE IMAGE FOR ARTIST START

function get_artist_profile_image($conn, $id, $facebook_image_id)
{
    $sql = "select * from users where id=$id";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {

            if (!empty($facebook_image_id)) {

                if ($facebook_image_id == 'nil') {

                    if (empty($row['profilepic_path'])) {

                        # Painting image is showing when no image found

                        $img_sql = " SELECT t.* FROM tasks t, user_tasks ut WHERE ut.user_id = $id AND t.id = ut.task_id AND status=0 ORDER BY tot_likes DESC LIMIT 1";
                        $result_img = $conn->query($img_sql);

                        if ($result_img->num_rows > 0) {
                            while ($result_painting_img = $result_img->fetch_assoc()) {


                                echo $result_painting_img['image_path'];
                                // return $row['image_path'];
                            }
                        } else

                            echo "http://yourmasterpieces.com/auth/user/images/edit_profile.png";


                    } else
                        echo $row['profilepic_path'];
                } else
                    echo "https://graph.facebook.com/{$facebook_image_id}/picture?type=large";
            } else
                if (empty($row['profilepic_path'])) {

                    # Painting image is showing when no image found

                    $img_sql = " SELECT t.* FROM tasks t, user_tasks ut WHERE ut.user_id = $id AND t.id = ut.task_id AND status=0 ORDER BY tot_likes DESC LIMIT 1";
                    $result_img = $conn->query($img_sql);
                    //var_dump($img_sql);

                    if ($result_img->num_rows > 0) {
                        while ($result_painting_img = $result_img->fetch_assoc()) {


                            echo $result_painting_img['image_path'];
                            // return $row['image_path'];
                        }
                    } else {
                        echo "http://yourmasterpieces.com/auth/user/images/edit_profile.png";
                    }


                } else
                    echo $row['profilepic_path'];
        }
    }
}

##PROFILE IMAGE FOR ARTSIT END


function friendlyURL($id, $title)
{

    $string = $title;
    $paramcount = func_num_args();
    for ($i = 2; $i < $paramcount; $i++) {
        $string .= "-" . func_get_arg($i);
    }
    $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, "utf-8");
    $string = preg_replace("`&([a-z]+);`i", "", $string);
    $string = preg_replace("`['\[\]]`", "", $string);

    $string = preg_replace(array("/[^A-Za-z0-9]/", "`[-]+`"), "-", $string);

    $string = trim($string, '-');
    return trim($string . "-" . $id, '-');
}


function get_artist_location($location)
{

    if (empty($location)) {
        $address = "Location Unavailable";  // for empty lat long
    } else if ($location == '0,0') {
        $address = "Location Unavailable"; // For 0,0 Lat long
    } else             // when lat long is not zero
    {
        $arr = explode(',', $location);
        $lat = $arr[0];
        $lng = $arr[1];
        $address = getaddress($lat, $lng);
    }

    $new_address = preg_replace('/[0-9]+/', '', $address); // Replacing Numeric Value to Space
    $country = explode(',', $new_address);
    echo "<i style='display:none;'>" . end($country) . "</i>"; // Display None Set Dont Delete This line
    $city = prev($country); // storing City Name
    echo $city;
    if (!empty(trim($city))) {
        echo ",";
    }
    echo end($country);
}

