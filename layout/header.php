<style type="text/css">
    .text_link {
        color: white;
    }

    .text_link:hover {
        color: #C2A476;
    }

    .paddingadded {
        position: relative;
        min-height: 270px !important;
        background: transparent;
        margin-top: 129px !important;
    }

    .new-nav > li > a, .new-nav > li > a:hover {
        padding: 8.5px 30px 8.5px 30px;
        background: #C2A476 !important;
        color: white;
        font-family: Montserrat;

    }



    .new-nav {
        margin: 0px !important;
    }




    @media screen and (max-width: 768px) {
        .new-nav .open .dropdown-menu {
            position: absolute;
            float: none;
            left: 0% !important;
            width: auto;
            margin-top: 0;
            background-color: white;
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
        }
    }

    @media screen and (max-width: 479px) {
        .height_less {
            margin-top: -95px;
        }
        .dropdown-menu {
            min-width: 0px !important;
        }
        .new-nav .open .dropdown-menu{
            right:0px !important;
        }
    }

    @media screen and (max-width: 376px) {
        .ws-logo img {
            width: 130px !important;
        }

    }

</style>

<div class="ws-topbar">
    <div class="pull-left">
        <div class="ws-topbar-message hidden-xs">
            <!--<p><a href="https://play.google.com/store/apps/details?id=com.codon.masterpiece&amp;hl=en"
                  class="text_link">Android App </a> <i style="color:#333">......</i> <a
                        href="https://itunes.apple.com/in/app/masterpiece-platform-for-non/id1089890769?mt=8"
                        class="text_link"> Ios App</a></p>-->
        </div>
    </div>

    <div class="pull-right">
        
        <!-- Account -->

        <?php if (isset($_SESSION['login_user'])) { ?>
            <ul id="menu-nav-style-2" class="nav navbar-nav new-nav">
                <li id="menu-item-474"
                    class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-474 dropdown">

                    <?php

                    function get_user_name($id, $conn)
                    {
                        $sql = "select firstname from users where id=$id OR fb_id=$id ";
                        $result = $conn->query($sql);

                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                                $username = $row['firstname'];
                            }
                            echo ucwords($username);
                        }
                    }

                    ?>


                    <!-- --><?php /* echo $_SESSION['login_user'] */ ?>
                    <a data-hover="dropdown" data-animations="fadeIn" title="Home" href="#" data-toggle="dropdown"
                       class="dropdown-toggle" aria-haspopup="true"><?php get_user_name($_SESSION['login_user'], $conn); ?>
                        <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu dropdownhover-bottom">

                        <li id="menu-item-543"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-543">
                            <a data-hover="dropdown" data-animations="fadeIn"
                               href="<?php echo $webroot ?>/auth/user/profile.php">Profile</a>
                        </li>
                        <li id="menu-item-632"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-632"><a
                                    data-hover="dropdown" data-animations="fadeIn"
                                    href="<?php echo $webroot ?>/auth/user/logout.php">Logout </a></li>
                    </ul>
                </li>
            </ul>
        <?php } ?>
        <!--NEW DROPDOWN MENU END -->


        <?php if (!isset($_SESSION['login_user'])) { ?>
            <ul id="menu-nav-style-2" class="nav navbar-nav new-nav">
                <li class="ws-shop-cart">
                    <a href='<?php echo $webroot ?>/auth/login.php' class=\"btn cart-top-btn btn-sm\">My Account</a>
                </li>
            </ul>
        <?php } ?>

    </div>

</div>

<header class="ws-header-static">

    <!-- Navbar -->
    <nav class="navbar ws-navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false" id="menushow">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Logo -->
            <div class="ws-logo ws-center">
                <a href="http://www.yourmasterpieces.com">
                    <img src="<?php echo $webroot ?>/images/masterpiece_logo.png" alt="masterpiece"
                         style="width:165px"> </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <div class="menu-left-container">
                    <ul id="menu-left" class="nav navbar-nav navbar-left">
                        <li id="menu-item-653"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-653 active">
                            <a data-hover="dropdown" data-animations="fadeIn" title="Home" href="<?php echo $webroot ?>"
                               style="background: none">Home</a></li>
                        <li id="menu-item-654"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-654"><a
                                    data-hover="dropdown" data-animations="fadeIn" title="Shop"
                                    href="<?php echo $webroot ?>/Paintings/paintings.php">Paintings</a></li>
                    </ul>
                </div>
                <div class="menu-right-container">
                    <ul id="menu-right" class="nav navbar-nav navbar-right">
                        <li id="menu-item-2484"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2484"><a
                                    data-hover="dropdown" data-animations="fadeIn" title="Artist"
                                    href="<?php echo $webroot ?>/Artist/index.php">Artists</a></li>
                        <li id="menu-item-728"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-728"><a
                                    data-hover="dropdown" data-animations="fadeIn" title="Contact Us"
                                    href="<?php echo $webroot ?>/contact.php">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>

<script type="text/javascript">

    /*
        $('#menushow').click(function() {
            if ($('.ws-parallax-header').hasClass('paddingadded')){
                $('.ws-parallax-header').removeClass('paddingadded');
            } else {
                $('.ws-parallax-header').addClass('paddingadded');
            }
        });
    */

</script>

