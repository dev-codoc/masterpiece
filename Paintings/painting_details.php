<?php session_start(); ?>
<?php include '../layout/db.php'; ?>
<?php include '../Artist/lat_long_conversion.php'; ?>
<?php include '../layout/functions.php'; ?>
<?php require_once '../auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler(); ?>
<!-- LAYOUT FOR ARTIST -->
<!DOCTYPE html>
<html lang="en">
<head>

    <?php
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $link_array = explode('-', $actual_link);
    $product_id = end($link_array);
    // $product_id = $_GET["id"];
    $sql = "SELECT * FROM tasks where id='$product_id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $product_id = $row['id'];
            $art_image = $row['image_path'];
            $art_name = $row['task'];
            $art_description = $row['task_description'];
            $art_technique = $row['technique'];
            $art_price = $row['price'];
            $art_year = $row['year'];
            $art_dimension = $row['dimension'];
            ?>

            <title> <?php echo $art_name ?> </title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description"
                  content="<?php echo $art_name ?> by Artist  <?php $sql_artist = "SELECT u.* FROM users u, user_tasks ut WHERE ut.task_id = $product_id AND u.id = ut.user_id";
                  $result_name = $conn->query($sql_artist);
                  if ($result_name->num_rows > 0) {
                      while ($row_artist_name = $result_name->fetch_assoc()) { ?>


                <?php echo $row_artist_name['name'];
                      }
                  } ?> | <?php echo $art_technique . " |  " . $art_dimension . " |  " . $art_year ?> ">
            <meta property="og:image" content="<?php echo $art_image; ?>"/>

        <?php }
    } ?>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php include '../google-analytics.php'; ?>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <!-- ALL CSS START-->
    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png"
          type="image/x-icon">
    <link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'
          href='<?php echo $webroot ?>/layout/assets/css/prettyPhoto.css?ver=2.6.9'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='woocommerce-layout-css'
          href='<?php echo $webroot ?>/layout/assets/css/woocommerce-layout.css?ver=2.6.9'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='woocommerce-smallscreen-css'
          href='<?php echo $webroot ?>/layout/assets/css/woocommerce-smallscreen.css?ver=2.6.9'
          type='text/css' media='only screen and (max-width: 768px)'/>
    <link rel='stylesheet' id='woocommerce-general-css'
          href='<?php echo $webroot ?>/layout/assets/css/woocommerce.css?ver=2.6.9'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='bootstrap-css'
          href='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/css/bootstrap.min.css?ver=4.7.8'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='artday-style-css'
          href='<?php echo $webroot ?>/layout/assets/css/style.css?ver=4.7.8' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='artday-dynamic-css'
          href='<?php echo $webroot ?>/layout/assets/css/dynamic.css?ver=4.7.8'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='artday-fonts-css'
          href='https://fonts.googleapis.com/css?family=PT+Serif%7CMontserrat&#038;subset=latin%2Clatin-ext'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='bfa-font-awesome-css'
          href='//cdn.jsdelivr.net/fontawesome/4.7.0/css/font-awesome.min.css?ver=4.7.0' type='text/css' media='all'/>

    <!--ALL CSS END -->
    <link rel="stylesheet" href="<?php echo $webroot ?>/Paintings/painting_details_stylesheet.css">

</head>


<!--Header File For MAsterpiece -->
<?php include '../layout/header.php'; ?>

<body class="product-template-default single single-product postid-1273 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive currency-usd">

<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

<!-- Loader Start -->
<div id="preloader">
    <div class="preloader-container">
        <div class="ws-spinner"></div>
    </div>
</div>
<!-- End Loader Start -->


<!-- Page Content -->
<?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$link_array = explode('-', $actual_link);
$product_id = end($link_array);

$sql = "SELECT * FROM tasks where id='$product_id'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while ($row = $result->fetch_assoc()) {
$product_id = $row['id'];
$art_image = $row['image_path'];
$art_name = $row['task'];
$art_description = $row['task_description'];
$art_technique = $row['technique'];
$art_price = $row['price'];
$art_new_price=$row['price_new'];
$created_by_me = $row['createdbyme'];
$tot_likes = $row['tot_likes'];
$curr_code = $row['curr_code'];
?>

<div class="container ws-page-container">
    <div class="row">
        <div class="ws-breadcrumb col-sm-12">
            <div class="container">
                <ol class="breadcrumb">
                    <li>
                        <a href="http://www.yourmasterpieces.com">Home</a></li>
                    <li>
                        <a href="#" style="cursor: default"><?php echo $art_technique; ?></a>
                    </li>
                    <li><?php echo $art_name; ?></li>
                </ol>
            </div>
        </div>

        <div class="ws-journal-container">
            <div class="col-sm-12">
                <div itemscope itemtype="http://schema.org/Product"
                     id="product-<?php echo $product_id; ?>"
                     class="post-<?php echo $product_id; ?> product type-product status-publish has-post-thumbnail product_cat-oil-painting first instock shipping-taxable purchasable product-type-simple">
                    <div class="images ws-product-bg fixed_detail_img">

                        <a href="<?php echo $art_image; ?>"
                           itemprop="image"
                           class="woocommerce-main-image zoom"
                           title=""
                           data-rel="prettyPhoto">
                            <img src="<?php echo $art_image; ?>"
                                 class="attachment-shop_single size-shop_single wp-post-image"
                                 alt="<?php echo $art_name; ?>" title="<?php echo $art_name; ?>"/>

                        </a>


                        <!-- LIKE SECTION START -->
                        <?php if ($created_by_me == 1) { ?>
                            <div class="col-md-12 name-create">
                                <div class="pull-right created-name">
                                    Created By <?php echo $db->getUserByTaskId($product_id) ?>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-12 like-container">
                            <div class="pull-right for_like">
                            <span class="countlike<?php echo $product_id ?>"
                                  id="countlike"><?php $db->countlikesArt($product_id); ?>
                            </span>

                                <?php if (isset($_SESSION['login_user'])) { // Checking Session user
                                    $user_id = $_SESSION['login_user'];
                                    $db->hasUserLiked($user_id, $product_id); // Checking that user has liked painting or not
                                    if (($db->hasUserLiked($user_id, $product_id)) == true) {
                                        echo "<a class='go_dislike' href='javascript:void(0)' data-id='$product_id'><i class=\"fa fa-heart heart\" ></i></a>";
                                    } else {
                                        echo "<a class='go_like' href='javascript:void(0)' data-id='$product_id'><i class=\"fa fa-heart-o heart\" ></i></a>";
                                    }
                                } else { //When user is not logined
                                    echo " <a class='login-required' href='#login-modal-shortlist' data-toggle='modal' ><i class=\"fa fa-heart-o heart\" ></i></a>";
                                }
                                ?>

                            </div>
                        </div>
                        <!-- LIKE SECTION END -->
                    </div>

                    <div class="summary entry-summary text-left">

                        <h1 itemprop="name" class="product_title entry-title art-name">
                            <span><?php echo $art_name; ?></span>
                        </h1>

                        <div class="ws-separator seprate-name"></div>

                        <?php if (!empty ($art_description)) { ?>
                            <p class="description">"<?php echo $art_description; ?>"</p>
                        <?php } ?>


                        <!-- PAINTING INFO BLOCK-->
                        <div class="woocommerce-tabs wc-tabs-wrapper col-sm-12">
                            <ul class="tabs wc-tabs">
                                <li class="additional_information_tab">
                                    <a href="#tab-additional_information">Painting Information</a>
                                </li>
                            </ul>
                            <div class="panel entry-content wc-tab" id="tab-additional_information">

                                <table class="shop_attributes">
                                    <tr class="">
                                        <th>Technique :</th>
                                        <td><p><?php echo $art_technique; ?></p>
                                        </td>
                                    </tr>
                                    <tr class="alt">
                                        <th>Dimension :</th>
                                        <td><p><?php echo $row['dimension']; ?></p>
                                        </td>
                                    </tr>
                                    <tr class="alt">
                                        <th>Year Created:</th>
                                        <td><p><?php echo $row['year']; ?></p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>


                        <!-- END PAINTING INFO BLOCK-->

                        <div class="col-md-12 price-block" style="padding-bottom:15px">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                    <p class="price" id="priceBlock">
                                <span class="woocs_price_code" data-product-id="<?php echo $product_id; ?>">
                                    <span class="woocommerce-Price-amount amount price-amount">
                                        <span class="woocommerce-Price-currencySymbol"></span>

                                        <span id="price">
                                        <!-- PRICE IF CURR CODE AVAILBALE -->
                                            <?php if ((!empty($curr_code)) && (!empty($art_new_price)))
                                                echo "$curr_code " . $art_new_price;
                                            else get_painting_price($art_price);
                                            ?>
                                        </span>

                                        <span class="handlingText">
                                            *shipping and handling charges extra
                                        </span>

                                        <!-- END PRICE CODE -->
                                    </span>
                                </span>
                                    </p>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <button class="btn request_buy_btn"
                                        data-toggle="modal"
                                        data-target="#ws-register-modal">Request to Buy
                                </button>
                            </div>
                        </div>


                        <div class="col-md-12 border-artist">

                        </div>

                        <!--REQUEST BUY MODEL START -->

                        <?php include 'request_form.php'; ?>
                        <!-- REQUEST BUY MODEL ENDED -->


                        <div itemprop="description">
                            <?php }
                            }; ?>


                            <?php
                            $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            $link_array = explode('-', $actual_link);
                            $product_id = end($link_array);

                            $lat = '';
                            $lng = '';
                            $address = '';
                            $sql = "SELECT u.* FROM users u, user_tasks ut WHERE ut.task_id = $product_id AND u.id = ut.user_id";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {
                            $user_id = $row['id'];
                            $user_image = $row['profilepic_path'];
                            $artist_name=$row['name'];
                            if (empty($artist_name)) {
                                if (!empty($row['firstname'])) {
                                    $artist_name = $row['firstname'];
                                } else {
                                    $artist_name = "artist";
                                }
                            }
                            $user_name = $row['firstname'];
                            $user_bio = $row['bio'];
                            $fb_id = $row['fb_id'];
                            $location = $row['location'];
                            $city = $row['city'];
                            $country = $row['country'];
                            ?>

                            <p>

                                <a href="<?php echo $webroot ?>/Artist/artist_details.php/<?php echo friendlyURL($user_id, $artist_name); ?>">
                                    <img class="wp-image-885 alignleft profile_img"
                                         src="<?php get_artist_profile_image($conn, $user_id, $fb_id); ?>"
                                         alt=""/>
                                </a>
                                <b style="font-family: &quot;lucida;"></b>
                            </p>
                            <p class="small">
                                <b><a class="name-artist"
                                      href="<?php echo $webroot ?>/Artist/artist_details.php/<?php echo friendlyURL($user_id, $artist_name); ?>"><?php echo $user_name; ?></a></b>
                                <br/>
                                <a class="artist-locate">
                                    <!-- ARTIST LOCATION START WITH COUNTRY AND CITY -->
                                    <?php if ((empty($city)) && (empty($country))) { ?>
                                        <?php get_artist_location($location) ?>
                                    <?php } else {
                                        echo ucwords($city);
                                        if (!empty($country) && !empty($city)) {
                                            echo ",";
                                        }
                                        echo ucwords($country);
                                    } ?>
                                    <!-- ARTIST LOCATION END WITH COUNTRY AND CITY -->
                                </a>
                            </p>
                            <p class="big" style="font-family: &quot;lucida;"><?php echo $user_bio; ?></p>
                        </div>
                        <div class="product_meta">

                            <?php }
                            } ?>

                            <?php

                            $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            $link_array = explode('-', $actual_link);
                            $product_id = end($link_array);

                            $sql = "SELECT * FROM tasks where id='$product_id'";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {
                            $product_id = $row['id'];
                            $art_image = $row['image_path'];
                            $art_name = $row['task'];
                            $art_description = $row['task_description'];
                            $art_technique = $row['technique'];
                            $art_price = $row['price'];

                            ?>


                            <span class="posted_in">Tags:
                                <a href="#" rel="tag" style="cursor:default">
                                    <?php echo $art_technique; ?>
                                </a>
                            </span>

                            <span class="posted_in">
                                <a href="#" rel="tag" style="cursor:default">
                                    SHARE ARTWORK
                                </a>
                            </span>

                            <!-- Social Links -->

                            <?php
                            $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                            // URL of page for share PAGE FOR Details ARt
                            ?>

                            <div class="ws-product-description">
                                <div class="ws-product-social-icon">

                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-icon-color="#333333"
                                         style="display: inline-block;"
                                         data-a2a-url="<?php echo $url ?>"
                                         data-a2a-title="<?php echo $art_name ?>">
                                        <a class="a2a_button_facebook"></a>
                                        <a class="a2a_button_whatsapp"></a>
                                        <a class="a2a_button_twitter"></a>
                                        <a class="a2a_button_google_plus"></a>
                                        <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                                    </div>

                                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                                    <script type="text/javascript">
                                        var a2a_config = a2a_config || {};
                                        a2a_config.exclude_services = ["facebook", "twitter", "whatsapp","google_plus"];
                                    </script>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
} ?>
<div class="container ws-page-container suggest-paint">
    <div class="row">
        <div class="related products col-sm-12">
            <h2>Suggested Paintings</h2>
            <div class="ws-separator-related"></div>

            <div class="row">
                <ul class="products">

                    <?php
                    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $link_array = explode('-', $actual_link);
                    $product_id = end($link_array);

                    $sql = "SELECT * FROM tasks where id='$product_id'";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        // output data of each row
                        while ($row = $result->fetch_assoc()) {

                            $query = $row['technique'];
                            $related_items = "SELECT * FROM tasks WHERE (`technique` LIKE '%" . $query . "%') OR (`technique` LIKE '%" . $query . "%') LIMIT 3";
                            $sql = "$related_items";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while ($row = $result->fetch_assoc()) {

                                    $art_name = $row['task'];
                                    $new_product_id = $row['id'];
                                    $art_price = $row['price'];
                                    $art_new_price=$row['price_new'];
                                    $tot_likes = $row['tot_likes'];
                                    $curr_code = $row['curr_code'];

                                    ?>


                                    <li class="first  post-788 product type-product status-publish has-post-thumbnail product_cat-oil-painting instock virtual shipping-taxable product-type-simple"
                                        data-sr='wait 0.1s, ease-in 20px'>
                                        <a href="<?php echo $webroot ?>/Paintings/painting_details.php/<?php echo friendlyURL($new_product_id, $art_name); ?>"
                                           class="woocommerce-LoopProduct-link">
                                            <a href="<?php echo $webroot ?>/Paintings/painting_details.php/<?php echo friendlyURL($new_product_id, $art_name); ?>"
                                               class="woocommerce-LoopProduct-link">
                                                <figure class="ws-product-bg fixed_height">
                                                    <img width="300" height="300"
                                                         src="<?php echo $row['image_path']; ?>"
                                                         class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                         alt=""/>
                                                </figure>
                                                <div class="col-md-9 col-sm-9 col-xs-9 text-left">
                                                    <h3><?php echo $art_name; ?></h3>
                                                    <span class="ws-item-subtitle"><?php echo $art_technique; ?></span>
                                                    <span class="price">
                                                        <span class="woocs_price_code" data-product-id="1318">
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span class="woocommerce-Price-currencySymbol"></span>

                                                                <!-- PRICE IF CURR CODE AVAILBALE -->
                                                                <?php if ((!empty($curr_code)) && (!empty($art_new_price)))
                                                                    echo "$curr_code " . $$art_new_price;
                                                                else get_painting_price($art_price);
                                                                ?>
                                                                <!-- END PRICE CODE -->

                                                            </span>
                                                        </span>
                                                    </span>
                                                </div>
                                            </a>

                                            <div class="col-md-3 col-sm-3 col-xs-3">

                                                <!-- LIKE SECTION START -->
                                                <div class="pull-right for_like">

                                                <span class="countlike<?php echo $new_product_id ?>"
                                                      id="countlike"><?php $db->countlikesArt($new_product_id); ?>
                                                </span>

                                                    <?php if (isset($_SESSION['login_user'])) { // Checking Session user
                                                        $user_id = $_SESSION['login_user'];
                                                        $db->hasUserLiked($user_id, $new_product_id); // Checking that user has liked painting or not
                                                        if (($db->hasUserLiked($user_id, $new_product_id)) == true) {
                                                            echo "<a class='go_dislike' href='javascript:void(0)' data-id='$new_product_id'><i class=\"fa fa-heart heart\" ></i></a>";
                                                        } else {
                                                            echo "<a class='go_like' href='javascript:void(0)' data-id='$new_product_id'><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                        }
                                                    } else { //When user is not logined
                                                        echo " <a class='login-required' href='#login-modal-shortlist' data-toggle='modal' ><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                    }
                                                    ?>
                                                </div>
                                                <!-- LIKE SECTION END -->

                                            </div>


                                    </li>

                                <?php }
                            }
                        }
                    } ?>


                </ul>
            </div>
        </div>

    </div>
</div>

<?php include '../layout/login_modal.php' ?>




<!-- Footer Start -->
<?php include '../layout/footer.php'; ?>


<script type="text/javascript">
    if ($('#price').text().trim() == "Price Unavailable") {
        //console.log('hello');
        $('.handlingText').hide();
    }
</script>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/frontend/single-product.min.js?ver=2.6.9'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/frontend/woocommerce.min.js?ver=2.6.9'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/frontend/cart-fragments.min.js?ver=2.6.9'></script>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/postshare.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8'></script>

<script src="<?php echo $webroot ?>/Paintings/js/like.js" type="text/javascript"></script>

</body>
</html>