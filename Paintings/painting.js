<!-- CATEOGRY BUTTON CLICK ACTIVE CLASS START -->
$(".ws-shop-nav li a").click(function () {
    $(this).parent().addClass('active').siblings().removeClass('active');
});
<!-- CATEOGRY BUTTON CLICK ACTIVE CLASS END -->

/*

$('.price_range').jRange({
    from: 0,
    to: 5500,
    step: 500,
    format: '%s USD',
    width: 250,
    showLabels: true,
    isRange: true
});


function filterProducts() {
    var price_range = $('.price_range').val();
    $.ajax({
        type: 'POST',
        url: '../Paintings/getProducts.php',
        data: 'price_range=' + price_range,
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
        }
    });
}

*/

<!-- PRICE FILTER JS ENDED -->

<!-- LOAD MORE FUNCTIONALITY start -->

$(document).ready(function () {
    $(document).on('click', '#btn_more', function () {

        var cat = localStorage.getItem('last_title_name');
        var last_painting_id = localStorage.getItem('last_id');
        $('#load_product').css('display', 'block');
        $('#btn_more').html("Loading...");
        $.ajax({
            url: "../Paintings/load_more_painting.php",
            method: "POST",
            data: {last_painting_id: last_painting_id, cateogry: cat},
            dataType: "text",
            success: function (data) {
                if (data != '') {
                    $('#remove_row').remove();
                    $('#load_data_table').append(data);


                    /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
                    if ($('#not-found').length == 1) {
                        $('#btn_more').html('')
                    }
                    else{
                        $('#btn_more').html("LOAD MORE");
                    }
                    /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/

                }
                else {
                    $('#btn_more').html("No Data");
                }
            }
        });
    });
});


function OilPainting() {
    $('.load_product').remove(); // if product previously loaded then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/OilPainting.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            //$('#btn_more').css('display', 'none');

            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/
        }
    });
}


function GraphitePainting() {
    $('.load_product').remove(); // if product previously loaded then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/GraphitePainting.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            //$('#btn_more').css('display', 'none');
            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/
        }
    });
}


function AcrylicPainting() {
    $('.load_product').remove(); // if product previously loaded then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/AcrylicPainting.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            //$('#btn_more').css('display', 'none');
            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/
        }
    });
}


function WaterColor() {
    $('.load_product').remove(); // if product previously loaded then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/WaterColor.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            // $('#btn_more').css('display', 'none');
            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/
        }
    });
}


function pencilPainting() {
    $('.load_product').remove(); // if product previously loaded then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/PencilPainting.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            //$('#btn_more').css('display', 'none');
            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/
        }
    });
}


<!-- POPULAR FILTER JS END -->

function popularPainting() {
    $('.load_product').remove(); // if product previously loaded then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/popular.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/

        }
    });
}

<!-- POPULAR FILTER JS END -->

<!-- LATEST PAINTING FILTER JS END -->

function LatestPainting() {
    $('.load_product').remove(); // If product is loaded dut to li clcik then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/latest_paintings.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/
        }
    });
}

<!-- LATEST PAINTINGS FILTER JS END -->


<!-- TOP PAINTINGS FILTER JS START -->

function TopPainting() {
    $('.load_product').remove(); // If product is loaded dut to li clcik then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/top_paintings.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/
        }
    });
}

/* TOP PAINITNG CODE END */

<!-- YEAR 2018 PAINTING FILTER JS END -->

function year2018Paintings() {
    $('.load_product').remove(); // If product is loaded dut to li clcik then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/year2018_paintings.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/
        }
    });
}

<!-- YEAR 2018 PAINTINGS FILTER JS END -->

<!-- YEAR 2018 PAINTING FILTER JS END -->

function year2017Paintings() {
    $('.load_product').remove(); // If product is loaded dut to li clcik then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/year2017_paintings.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/
        }
    });
}

<!-- YEAR 2018 PAINTINGS FILTER JS END -->

<!-- YEAR 2018 PAINTING FILTER JS END -->

function year2016Paintings() {
    $('.load_product').remove(); // If product is loaded dut to li clcik then remove
    $.ajax({
        type: 'POST',
        url: '../Paintings/year2016_paintings.php',
        beforeSend: function () {
            $('.container').css("opacity", ".5");
        },
        success: function (html) {
            $('#productContainer').html(html);
            $('.container').css("opacity", "");
            /*REMOVE LOAD MORE BUTTON WHEN NO RESULT FOUND */
            if ($('#not-found').length == 1) {
                $('#btn_more').html('')
            }
            else{
                $('#btn_more').html("LOAD MORE");
            }
            /*END LOAD MORE BUTTON WHEN NO RESULT FOUND*/
        }
    });
}

<!-- YEAR 2018 PAINTINGS FILTER JS END -->


<!-- LATEST PAINTING VIA LOCXAL STORAGE -->


$('document').ready(function () {

    var CatType = localStorage.getItem('type');

    if (CatType == 'top') {

        $('#all').removeClass('active');
        $('#top').addClass('active');
        TopPainting();
        localStorage.removeItem('type');
    }

    if (CatType == 'latest') {

        $('#all').removeClass('active');
        $('#latest').addClass('active');
        LatestPainting();
        localStorage.removeItem('type');
    }
    if (CatType == 'oil') {

        $('#all').removeClass('active');
        $('#oil').addClass('active');
        OilPainting();
        localStorage.removeItem('type');
    }
    if (CatType == 'water') {

        $('#all').removeClass('active');
        $('#water').addClass('active');
        WaterColor();
        localStorage.removeItem('type');
    }
    if (CatType == 'acrylic') {

        $('#all').removeClass('active');
        $('#acrylic').addClass('active');
        AcrylicPainting();
        localStorage.removeItem('type');
    }

    if (CatType == 'pencil') {

        $('#all').removeClass('active');
        $('#pencil').addClass('active');
        pencilPainting();
        localStorage.removeItem('type');
    }

    if (CatType == 'graphite') {

        $('#all').removeClass('active');
        $('#graphite').addClass('active');
        GraphitePainting();
        localStorage.removeItem('type');
    }
    if (CatType == 'popular') {

        $('#all').removeClass('active');
        $('#popular').addClass('active');
        popularPainting();
        localStorage.removeItem('type');
    }
    if (CatType == '2018') {

        $('#all').removeClass('active');
        $('#2018').addClass('active');
        year2018Paintings();
        localStorage.removeItem('type');
    }
    if (CatType == '2017') {

        $('#all').removeClass('active');
        $('#2017').addClass('active');
        year2017Paintings();
        localStorage.removeItem('type');
    }

    if (CatType == '2016') {

        $('#all').removeClass('active');
        $('#2016').addClass('active');
        year2016Paintings();
        localStorage.removeItem('type');
    }
});




