<?php session_start(); // Starting Session ?>
<?php include '../layout/db.php'; ?>
<?php include '../layout/functions.php'; ?>
<?php include '../auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler(); ?>

<!-- LAYOUT FOR ARTIST -->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Your Masterpieces - Buy painting Online ,Sell Painting Online </title>
    <meta charset="UTF-8"/>
    <meta name="description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings.">
    <meta name="keywords"
          content="Paintings,Your Masterpieces, Non Digital Art, Buy paintings, Sell paintings, Sell Art, Buy Art, Bulk paintings buying, Foreign paintings, Art collectors, Collect Art, Buy painting online, Sell painting online, Painter profile, Artist profile"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta property="og:title" content="Your Masterpieces"/>
    <meta property="og:type" content=""/>
    <meta property="og:url" content="<?php echo $webroot; ?>"/>
    <meta property="og:image" content="http://www.yourmasterpieces.com/images/favicon.jpeg"/>
    <meta property="og:site_name" content="Your Masterpieces"/>
    <meta property="fb:admins" content="Your Masterpieces"/>
    <meta property="og:locale" content="en_us"/>
    <meta property="og:description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings."/>
    <link rel="canonical" href="<?php echo $webroot; ?>"/>

    <?php include '../layout/common_layout.php'; ?>
    <link rel="stylesheet" href="search_design.css">

    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png"
          type="image/x-icon">
    <link href="css/slidercss.css" rel="stylesheet">


    <link rel='stylesheet' id='artday-dynamic-css'
          href='<?php echo $webroot ?>/layout/assets/css/dynamic.css?ver=4.7.8'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='artday-fonts-css'
          href='https://fonts.googleapis.com/css?family=PT+Serif%7CMontserrat&#038;subset=latin%2Clatin-ext'
          type='text/css' media='all'/>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php include '../google-analytics.php'; ?>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://demos.codexworld.com/includes/js/bootstrap.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="http://demos.codexworld.com/price-range-slider-jquery-ajax-php-mysql/jquery.range.js"></script>
    <link rel="stylesheet" href="painting_stylesheet.css">
    <link rel='stylesheet' id='bfa-font-awesome-css'
          href='//cdn.jsdelivr.net/fontawesome/4.7.0/css/font-awesome.min.css?ver=4.7.0' type='text/css' media='all'/>
    <script type='text/javascript' src='<?php echo $webroot ?>/layout/assets/js/jquery/jquery.js?ver=1.12.4'></script>


</head>


<!--Header File For MAsterpiece -->
<?php include '../layout/header.php'; ?>

<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="<?php echo $webroot ?>/images/your_masterpieces_painting_background.jpg"
     style="background:rgba(0,0,0,0.4)">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>Paintings</h1>
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <nav class="navbar navbar-default">
                                <div class="nav nav-justified navbar-nav">
                                    <form class="navbar-form navbar-search" role="search" method="GET"
                                          action="Search_Art.php">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="query"
                                                   placeholder="Search All Paintings">
                                            <div class="input-group-btn">
                                                <input type="submit" value="Search"
                                                       class="btn btn-search btn-info search-paint">
                                                </input>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </nav>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END HEADING SCROLL ARTIST TEXT -->
<div class="container ws-page-container">
    <div class="row">
        <div class="col-sm-12">
            <ul class="ws-shop-nav">
                <li class="cat_item active" id="all">
                    <a href="paintings.php">All</a>
                </li>
                <li class="cat_item" id="oil">
                    <a href="javascript:void(0)" onclick="OilPainting()">Oil paintings</a>
                </li>
                <li class="cat_item" id="acrylic">
                    <a href="javascript:void(0)" onclick="AcrylicPainting()">Acrylic paintings</a>
                </li>
                <li class="cat_item" id="water">
                    <a href="javascript:void(0)" onclick="WaterColor()">WaterColor paintings</a>
                </li>
                <li class="cat_item" id="pencil">
                    <a href="javascript:void(0)" onclick="pencilPainting()">Pencil &amp; Charcoal Sketch</a>
                </li>
                <li class="cat_item" id="graphite">
                    <a href="javascript:void(0)" onclick="GraphitePainting()">Graphite paintings</a>
                </li>

                <li class="cat_item" id="top">
                    <a href="javascript:void(0)" onclick="TopPainting()">Top paintings</a>
                </li>

                <li class="cat_item" id="latest">
                    <a href="javascript:void(0)" onclick="LatestPainting()">Latest Paintings</a>
                </li>

                <!--<li class="cat_item" id="popular">
                    <a href="javascript:void(0)" onclick="popularPainting()">Popular Paintings</a>
                </li>-->

                <li class="cat_item" id="2016">
                    <a href="javascript:void(0)" onclick="year2016Paintings()">2016 Paintings</a>
                </li>

                <li class="cat_item" id="2017">
                    <a href="javascript:void(0)" onclick="year2017Paintings()">2017 Paintings</a>
                </li>

                <li class="cat_item" id="2018">
                    <a href="javascript:void(0)" onclick="year2018Paintings()">2018 Paintings</a>
                </li>


            </ul>
        </div>

        <div class="ws-journal-container">
            <div class="col-sm-12">
                <div class="row">
                    <ul class="products">
                        <div id="productContainer">
                            <?php
                            $painting_id = '';
                            $sql = "SELECT * FROM tasks WHERE status=0 ORDER BY tot_likes Desc LIMIT 12 "; //fetching 12 Result Only from Users Table
                            $result = $conn->query($sql);

                            if ($result->num_rows > 0) {
                                // output data of each row
                            while ($row = $result->fetch_assoc()) {
                                $last_likes_id = $row['id']; // Last like Id for ajax Call
                                $product_id = $row['id'];
                                $art_image = $row['image_path'];
                                $art_name = $row['task'];
                                $art_description = $row['task_description'];
                                $art_technique = $row['technique'];
                                $art_price = $row['price'];
                                $art_new_price=$row['price_new'];
                                $tot_likes = $row['tot_likes'];
                                $curr_code = $row['curr_code'];
                                ?>
                                <li class="post-1250 product type-product status-publish has-post-thumbnail product_cat-acryclic last instock shipping-taxable purchasable product-type-simple">


                                    <a href="painting_details.php/<?php echo friendlyURL($product_id, $art_name); ?>"
                                       class="woocommerce-LoopProduct-link">

                                        <figure class="ws-product-bg fixed_image">
                                            <img width="300"
                                                 height="300"
                                                 src="<?php echo $art_image; ?>"
                                                 class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                 alt="<?php echo $art_name; ?>">

                                        </figure>

                                        <div class="col-md-9 col-sm-9 col-xs-9 text-left">
                                            <h3><?php echo $art_name; ?></h3>
                                            <span class="ws-item-subtitle"><?php echo $art_technique; ?></span>
                                            <span class="price">
                                                        <span class="woocs_price_code" data-product-id="1318">
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span class="woocommerce-Price-currencySymbol"></span>
                                                                <!-- PRICE IF CURR CODE AVAILBALE -->
                                                                <?php if ((!empty($curr_code)) && (!empty($art_new_price)))
                                                                    echo "$curr_code " . $art_new_price;
                                                                else get_painting_price($art_price);
                                                                ?>
                                                            </span>
                                                        </span>
                                                    </span>
                                        </div>
                                    </a>

                                    <div class="col-md-3 col-sm-3 col-xs-3">

                                        <!-- LIKE SECTION START -->
                                        <div class="pull-right for_like">

                                                <span class="countlike<?php echo $product_id ?>"
                                                      id="countlike"><?php $db->countlikesArt($product_id); ?>
                                                </span>


                                            <?php if (isset($_SESSION['login_user'])) { // Checking Session user
                                                $user_id = $_SESSION['login_user'];
                                                $db->hasUserLiked($user_id, $product_id); // Checking that user has liked painting or not
                                                if (($db->hasUserLiked($user_id, $product_id)) == true) {
                                                    echo "<a class='go_dislike' href='javascript:void(0)' data-id='$product_id'><i class=\"fa fa-heart heart\" ></i></a>";
                                                } else {
                                                    echo "<a class='go_like' href='javascript:void(0)' data-id='$product_id'><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                }
                                            } else { //When user is not logined
                                                echo " <a class='login-required' href='#login-modal-shortlist' data-toggle='modal' ><i class=\"fa fa-heart-o heart\" ></i></a>";
                                            }
                                            ?>
                                        </div>
                                        <!-- LIKE SECTION END -->

                                    </div>


                                </li>

                            <?php
                            $painting_id = $row["tot_likes"]; //saving last Painting Id in a varibale
                            }
                            ?>

                                <script type="text/javascript">
                                    var last_painting_id =<?php echo $painting_id ?>
                                </script>

                                <script type="text/javascript">
                                    $(function () {
                                        localStorage.setItem("last_id", last_painting_id);
                                        localStorage.setItem("last_title_name", "All")
                                    })
                                </script>


                            <?php } else {
                                echo "<b class='no_result'><div class='col-md-12' >Sorry No result Found</div></b>";

                            }
                            ?>
                    </ul>
                </div>
                <!--LOAD MORE SECTION START -->


                <?php include '../layout/login_modal.php' ?>


                <div id="load_data_table"></div> <!-- LOAD RESULT WORK ON THIS ID -->

                <div class="ws-more-btn-holder col-sm-12">
                    <div id="remove_row">
                        <a class="btn ws-more-btn"
                           name="btn_more"
                           id="btn_more"
                           data-vid=''>Load More
                        </a>
                    </div>
                </div>
                <!--LOAD MORE SECTION ENDED -->


            </div>
        </div>


    </div> <!-- END OF THE CONTAINER -->
</div>


<!--********************************************************************
 **********************AJAX CALL FOR LIKE START **********************
***********************************************************************-->

<script type="text/javascript">

    $('a.go_like').on('click', function () {
        if ($(this).hasClass('go_dislike')) {
            console.log("Dislike");
            var thisForm = this;
            var painting_id = $(thisForm).attr('data-id');
            $.ajax({
                url: 'dislike.php',
                method: "POST",
                data: {painting_id: painting_id},
                success: function (data) {
                    $(thisForm).toggleClass('go_dislike');
                    var _html = "<i class=\"fa fa-heart-o heart\"></i>";
                    $(thisForm).html(_html);

                    /*AJAX CALL COUNT*/
                    $.ajax({
                        type: "POST",
                        url: "likecount.php",
                        data: {painting_id: painting_id},
                        success: function (data) {
                            console.log('count updated');
                            $('.countlike' + painting_id).html(data);
                        }


                    });
                    return true;
                }
            })
        }
        else {
            console.log("like");
            var thisForm = this;
            var painting_id = $(thisForm).attr('data-id');
            $.ajax({
                url: 'like.php',
                method: "POST",
                data: {painting_id: painting_id},
                success: function (data) {
                    $(thisForm).toggleClass('go_dislike');
                    var _html = "<i class=\"fa fa-heart heart\" ></i>";
                    $(thisForm).html(_html);
                    /*AJAX CALL COUNT*/
                    $.ajax({
                        type: "POST",
                        url: "likecount.php",
                        data: {painting_id: painting_id},
                        success: function (data) {
                            console.log('count updated');
                            $('.countlike' + painting_id).html(data);
                        }

                    });

                    return true;
                }
            });
        }

    });


    $('a.go_dislike').on('click', function () {
        if ($(this).hasClass('go_like')) {
            console.log("like");
            var thisForm = this;
            var painting_id = $(thisForm).attr('data-id');
            $.ajax({
                url: 'like.php',
                method: "POST",
                data: {painting_id: painting_id},
                success: function (data) {
                    $(thisForm).toggleClass('go_like');
                    var _html = "<i class=\"fa fa-heart heart\" ></i>";
                    $(thisForm).html(_html);
                    /*AJAX CALL COUNT*/
                    $.ajax({
                        type: "POST",
                        url: "likecount.php",
                        data: {painting_id: painting_id},
                        success: function (data) {
                            console.log(data);
                            console.log('count updated');
                            $('.countlike' + painting_id).html(data);
                        }


                    });

                    return true;
                }
            });

        }
        else {
            console.log("Dislike");
            var thisForm = this;
            var painting_id = $(thisForm).attr('data-id');
            $.ajax({
                url: 'dislike.php',
                method: "POST",
                data: {painting_id: painting_id},
                success: function (data) {
                    $(thisForm).toggleClass('go_like');
                    var _html = "<i class=\"fa fa-heart-o heart\" ></i>";
                    $(thisForm).html(_html);
                    /*AJAX CALL COUNT*/
                    $.ajax({
                        type: "POST",
                        url: "likecount.php",
                        data: {painting_id: painting_id},
                        success: function (data) {

                            console.log('count updated');
                            $('.countlike' + painting_id).html(data);
                        }


                    });
                    return true;
                }
            });
        }
    });


    function like() {
        console.log($(this));
        var thisForm = this;
        var painting_id = $(thisForm).attr('data-id');
        $.ajax({
            url: 'like.php',
            method: "POST",
            data: {painting_id: painting_id},
            success: function (data) {
                $(thisForm).toggleClass('go_dislike');
                var _html = "Liked";
                $(thisForm).html(_html);
                return true;
            }

        });
    }


    function dislike() {
        var thisForm = this;
        var painting_id = $(thisForm).attr('data-id');
        $.ajax({
            url: 'dislike.php',
            method: "POST",
            data: {painting_id: painting_id},
            success: function (data) {
                $(thisForm).toggleClass('go_like');
                var _html = "Like";
                $(thisForm).html(_html);
                return true;
            }
        });
    }
</script>


<script type="text/javascript">

     $('#menushow').click(function() {
     if ($('.ws-parallax-header').hasClass('paddingadded')){
     $('.ws-parallax-header').removeClass('paddingadded');
     } else {
     $('.ws-parallax-header').addClass('paddingadded');
     }
     });

</script>


<!--*******************************************************
**********************AJAX CALL FOR LIKE END *********************
***********************************************************************-->


<?php include '../layout/footer.php'; ?>

<script type="text/javascript" src="painting.js"></script>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/frontend/cart-fragments.min.js?ver=2.6.9'></script>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>
