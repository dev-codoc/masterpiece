<!-- Register Form-->
<style type="text/css">
    input.form-control.submit {
        background: #C2A476;
        color: white;
        text-transform: uppercase;
        text-shadow: none;
        box-shadow: none;
        font-family: Montserrat;
    }

    @media screen and (max-width: 479px) {
        .modal-content {
            padding-right: 25px;
            padding-left: 25px;
        }

        #ws-register-modal .modal-header {
            border: none;
            margin-right: -25px;
            padding: 5px 10px 0 0;
        }

    }
    .ws-register-form input.form-control{
        height:45px;
    }
    #country{
        width: 100%;
        height: 45px;
        font-size: 16px;
        padding: 6px 12px;
        border-radius: 0;
        border: none;
        background-color: #f5f5f5;
    }

    .inline-form-phone{
        display:inline-flex;
        width:100%;
    }
    input.form-control.bfh-phone{
        width:15%;
    }
    input.form-control.mobile{
        width:85%;
    }

    span.required {
        font-size: 15px;
    }

    @media screen and (max-width:479px){
        input.form-control.bfh-phone {
            width: 28%;
        }
    }
</style>

<!-- Register Modal -->
<?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$link_array = explode('-', $actual_link);
$product_id = end($link_array);

?>
<div class="modal fade" id="ws-register-modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
            </div>

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">

                    <div class="modal-body">
                        <!-- Register Form -->
                        <form class="ws-register-form"
                              action="<?php echo $webroot ?>/Paintings/request_buy_form_submit.php" method="post">

                            <h3 style="margin-top: -40px">Request for Purchase</h3>
                            <div class="ws-separator"></div>
                            <!-- Name -->
                            <div class="form-group">
                                <label class="control-label">Name<span>*</span></label>
                                <input type="text" class="form-control" name="name" required>
                            </div>

                            <!-- Email -->
                            <div class="form-group">
                                <label class="control-label">Email Address<span>*</span></label>
                                <input type="email" class="form-control" name="email" required>
                            </div>


                            <!--City -->
                            <div class="form-group">
                                <label class="control-label">City<span></span></label>
                                <input type="text" class="form-control" name="city">
                            </div>

                            <!-- Country -->
                            <div class="form-group">
                                <label class="control-label">Country<span class="required">*</span></label>
                                <select class="form-control bfh-countries" data-country="IN" name="country" id="country" required></select>
                            </div>

                            <input type="hidden" class="form-control" name="artpath" value="<?php echo $product_id; ?>" readonly/>

                            <!-- Mobile -->
                            <div class="form-group">
                                <label class="control-label">Mobile Number with country
                                    code<span></span></label>

                                <div class="inline-form-phone">
                                    <input type="text" class="form-control bfh-phone"  data-country="country" name="auto-country-code" readonly required>
                                    <input type="number" class="form-control mobile" name="mobile" value="<?php echo $mobile ?>">
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="control-label">Message</label>
                                <textarea name="message" class="form-control" placeholder="Message"></textarea>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="form-control submit" value="Submit" name="request_submit">
                            </div>


                        </form>
                    </div>


                </div>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo $webroot?>/auth/user/js/curr_code.js"></script>