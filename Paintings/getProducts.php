<?php
if(isset($_POST['price_range'])) {

    //Include database configuration file
     include '../layout/db.php'; 
    //set conditions for filter by price range
    $whereSQL = $orderSQL = '';
    $priceRange = $_POST['price_range'];
    if (!empty($priceRange)) {
        $priceRangeArr = explode(',', $priceRange);
        $whereSQL = "WHERE status!=3 AND (price BETWEEN " . $priceRangeArr[0] . " AND " . $priceRangeArr[1] . ")";
        $orderSQL = " ORDER BY price ASC ";
    } else {
        $orderSQL = " ORDER BY id DESC ";
    }


    //get product rows
    $sql = "SELECT * FROM tasks $whereSQL $orderSQL";
   // echo $sql;//fetching 12 Result Only from Users Table
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $product_id=$row['id'];
            $art_image = $row['image_path'];
            $art_name = $row['task'];
            $art_description = $row['task_description'];
            $art_technique = $row['technique'];
            $art_price = $row['price'];
            ?>

            <li class="post-1250 product type-product status-publish has-post-thumbnail product_cat-acryclic last instock shipping-taxable purchasable product-type-simple"
                data-sr='wait 0.1s, ease-in 20px'>
                <a href="painting_details.php?id=<?php echo $product_id ;?>" class="woocommerce-LoopProduct-link">
                        <figure class="ws-product-bg fixed_image">
                            <img width="300"
                                 height="300"
                                 src="<?php echo $art_image; ?>"
                                 class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                 alt="<?php echo $art_name; ?>">
                        </figure>
                        <span class="ws-item-subtitle"><?php echo $art_technique; ?></span>
                        <h3><?php echo $art_name; ?></h3>
                        <span class="price">
                                <span class="woocs_price_code" data-product-id="1318">
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol"></span>
                                        <?php
                                        if ($art_price=="0") //When Price is Zero
                                        {
                                            echo "Price Unavailable";
                                        }else
                                        {
                                            if (ctype_digit($art_price)) //Checking Price numeric or containing any symbol like usd or Pkr
                                            {
                                                //When Price is only Number
                                                $profit = ($art_price * 10) / 100; //Advantage Price After 10%
                                                $art_new_price = $art_price + $profit; //Adding profit in original Price
                                                echo "$" . $art_new_price;
                                            }
                                            else
                                            {
                                                // When Price is something Like 1250$,1200Usd,1200PKR
                                                $Price_number = preg_replace('/[^0-9]/', '', $art_price);
                                                $Price_symbol =preg_replace('/[^a-zA-Z $ ₹]/', '', $art_price);

                                                $profit=($Price_number*10)/100; //Advantage Price After 10%
                                                $art_new_price=$Price_number+$profit; //Adding profit in original Price
                                                echo $Price_symbol;
                                                echo " ";
                                                echo $art_new_price;
                                            }
                                        }
                                        ?>
                                    </span>
                                </span>
                            </span>
                    </a>
                </a>
            </li>

        <?php }
    } else {
        echo 'Product(s) not found';
    }
}?>