<?php include $_SERVER['DOCUMENT_ROOT'].'/Masterpiece/layout/db.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/Masterpiece/layout/common_layout.php';?>
<link href="http://demos.codexworld.com/includes/css/style.css" rel="stylesheet">
<link href="http://demos.codexworld.com/price-range-slider-jquery-ajax-php-mysql/jquery.range.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://demos.codexworld.com/includes/js/bootstrap.js"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="http://demos.codexworld.com/price-range-slider-jquery-ajax-php-mysql/jquery.range.js"></script>

<div class="container">
    <div class="filter-panel">
        <p><input type="hidden" class="price_range" value="0,500" /></p>
        <input type="button" onclick="filterProducts()" value="FILTER" />
    </div>
    <div id="productContainer">
        <?php

        //get product rows
        $sql = "SELECT * FROM tasks where LIMIT 8"; //fetching 12 Result Only from Users Table
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $product_id=$ROW['id'];
                $art_image = $row['image_path'];
                $art_name = $row['task'];
                $art_description = $row['task_description'];
                $art_technique = $row['technique'];
                $art_price = $row['price'];
                ?>

                <li class="post-1250 product type-product status-publish has-post-thumbnail product_cat-acryclic last instock shipping-taxable purchasable product-type-simple"
                    data-sr='wait 0.1s, ease-in 20px'>
                    <a href="painting_details.php?id=<?php echo $product_id ;?>" class="woocommerce-LoopProduct-link">
                        <a href="painting_details.php?id=<?php echo $product_id ;?>" class="woocommerce-LoopProduct-link">
                            <figure class="ws-product-bg fixed_image">
                                <img width="300"
                                     height="300"
                                     src="<?php echo $art_image; ?>"
                                     class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                     alt="<?php echo $art_name; ?>">
                            </figure>
                            <span class="ws-item-subtitle"><?php echo $art_technique; ?></span>
                            <h3><?php echo $art_name; ?></h3>
                            <span class="price">
                                <span class="woocs_price_code" data-product-id="1318">
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol">&#36;</span>&nbsp;<?php echo $art_price; ?> </span>
                                </span>
                            </span>
                        </a>
                </li>


            <?php }
        }else{
            echo 'Product(s) not found';
        } ?>
    </div>
</div>

<script type="text/javascript">
    $('.price_range').jRange({
        from: 0,
        to: 500,
        step: 50,
        format: '%s USD',
        width: 300,
        showLabels: true,
        isRange : true
    });
</script>
<script type="text/javascript">
    function filterProducts() {
        var price_range = $('.price_range').val();
        $.ajax({
            type: 'POST',
            url: 'getProducts.php',
            data:'price_range='+price_range,
            beforeSend: function () {
                $('.container').css("opacity", ".5");
            },
            success: function (html) {
                $('#productContainer').html(html);
                $('.container').css("opacity", "");
            }
        });
    }
</script>
