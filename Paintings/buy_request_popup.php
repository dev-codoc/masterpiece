<style type="text/css">

    .nav-tabs {
        margin-bottom: 15px;
    }
    .sign-with {
        margin-top: 25px;
        padding: 20px;
    }
    div#OR {
        height: 30px;
        width: 30px;
        border: 1px solid #C2C2C2;
        border-radius: 50%;
        font-weight: bold;
        line-height: 28px;
        text-align: center;
        font-size: 12px;
        float: right;
        position: absolute;
        right: -16px;
        top: 40%;
        z-index: 1;
        background: #DFDFDF;
    }

</style>

<?php
$product_id=$_GET["id"];
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title" id="myModalLabel">
                    Send Email For Buy this painting - <a href="http://yourmasterpieces.com" style="
                    color:black">yourmasterpieces.com</a></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">

                            <li class="active"><a href="#Registration" data-toggle="tab">Request for Purchase</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div class="tab-pane active" id="Registration">
                                <form role="form" class="form-horizontal" action="request_buy_form_submit.php" method="post">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label">
                                            Name</label>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <select class="form-control">
                                                        <option>Mr.</option>
                                                        <option>Ms.</option>
                                                        <option>Mrs.</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="Name" name="name" required/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">
                                            Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" name="email" placeholder="Email" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile" class="col-sm-2 control-label">
                                            Mobile</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="mobile" placeholder="Mobile With Country Code " required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="country" class="col-sm-2 control-label">
                                            Country</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="country" placeholder="City of Delivery" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="art name" class="col-sm-2 control-label">
                                            Message</label>
                                        <div class="col-sm-10">
                                            <textarea name="message" name="message">Request Message here...</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="art name" class="col-sm-2 control-label">
                                            Art ID </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="artpath" value="<?php echo $product_id;?>" readonly />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-10">
                                            <input type="submit" class="btn btn-success btn-block" style="background:black;margin-top:15px;margin-bottom:15px;box-shadow: none;color:white">
                                            </input>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
