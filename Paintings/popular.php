<?php session_start(); // Starting Session ?>
<?php include '../layout/db.php'; ?>
<?php include '../layout/functions.php'; ?>
<?php include '../auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler();

//get product rows
$popular_Painting = "SELECT * FROM `tasks` WHERE status=0 ORDER BY tot_likes DESC LIMIT 12";
$sql_pop = "$popular_Painting";
//var_dump($sql_pop);
$result = $conn->query($sql_pop);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $product_id = $row['id'];
        $art_image = $row['image_path'];
        $art_name = $row['task'];
        $art_description = $row['task_description'];
        $art_technique = $row['technique'];
        $art_price = $row['price'];
        $art_new_price=$row['price_new'];
        $tot_likes = $row['tot_likes'];
        $curr_code = $row['curr_code'];

        ?>

        <li class="post-1250 product type-product status-publish has-post-thumbnail product_cat-acryclic last instock shipping-taxable purchasable product-type-simple"
            data-sr='wait 0.1s, ease-in 20px'>
            <!-- LIKE SECTION START -->
            <div class="pull-right for_like">
                <?php if (isset($_SESSION['login_user'])) { // Checking Session user
                    $user_id = $_SESSION['login_user'];
                    $db->hasUserLiked($user_id, $product_id); // Checking that user has liked painting or not
                    if (($db->hasUserLiked($user_id, $product_id)) == true) {
                        echo "<a class='go_dislike' href='javascript:void(0)' data-id='$product_id'><i class=\"fa fa-heart heart\" ></i></a>";
                    } else {
                        echo "<a class='go_like' href='javascript:void(0)' data-id='$product_id'><i class=\"fa fa-heart-o heart\" ></i></a>";
                    }
                } else { //When user is not logined
                    echo " <a class='login-required' href='#login-modal-shortlist' data-toggle='modal' ><i class=\"fa fa-heart-o heart\" ></i></a>";
                }
                ?>
            </div>
            <!-- LIKE SECTION END -->

            <a href="painting_details.php/<?php echo friendlyURL($product_id, $art_name); ?>"
               class="woocommerce-LoopProduct-link">
                <a href="painting_details.php/<?php echo friendlyURL($product_id, $art_name); ?>"
                   class="woocommerce-LoopProduct-link">
                    <figure class="ws-product-bg fixed_image">
                        <img width="300"
                             height="300"
                             src="<?php echo $art_image; ?>"
                             class="attachment-shop_catalog size-shop_catalog wp-post-image"
                             alt="<?php echo $art_name; ?>">
                    </figure>
                    <span class="ws-item-subtitle"><?php echo $art_technique; ?></span>
                    <h3><?php echo $art_name; ?></h3>
                    <span class="price">
                                <span class="woocs_price_code" data-product-id="1318">
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol"></span>
                                        <!-- PRICE IF CURR CODE AVAILBALE -->
                                        <?php if ((!empty($curr_code)) && (!empty($art_new_price)))
                                            echo "$curr_code " . $$art_new_price;
                                        else get_painting_price($art_price);
                                        ?>
                                    </span>

                                </span>
                            </span>
                </a>
        </li>

        <?php
        $painting_id = $row["tot_likes"]; //saving last Painting Id in a varibale
    }
}
?>
<script type="text/javascript">
    var last_painting_id = "<?php echo $painting_id ?>";
</script>

<script type="text/javascript">
    $(function () {
        localStorage.setItem("last_id", last_painting_id);
        localStorage.setItem("last_title_name", "popular")
    })
</script>

<script src="js/like.js"></script>