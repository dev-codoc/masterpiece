
$('a.go_like').on('click', function () {

    if ($(this).hasClass('go_dislike')) {
        console.log("Dislike");
        var thisForm = this;
        var painting_id = $(thisForm).attr('data-id');
        $.ajax({
            url: HOST+"/Paintings/dislike.php",
            method: "POST",
            data: {painting_id: painting_id},
            success: function (data) {
                $(thisForm).toggleClass('go_dislike');
                var _html = "<i class=\"fa fa-heart-o heart\"></i>";
                $(thisForm).html(_html);

                /*AJAX CALL COUNT*/
                $.ajax({
                    type: "POST",
                    url: HOST+"/Paintings/likecount.php",
                    data: {painting_id: painting_id},
                    success: function (data) {
                        console.log('count updated');
                        $('.countlike'+painting_id).html(data);
                    }


                });
                return true;
            }
        })
    }
    else {
        console.log("like");
        var thisForm = this;
        var painting_id = $(thisForm).attr('data-id');
        $.ajax({
            url: HOST+"/Paintings/like.php",
            method: "POST",
            data: {painting_id: painting_id},
            success: function (data) {
                $(thisForm).toggleClass('go_dislike');
                var _html = "<i class=\"fa fa-heart heart\" ></i>";
                $(thisForm).html(_html);
                /*AJAX CALL COUNT*/
                $.ajax({
                    type: "POST",
                    url: HOST+"/Paintings/likecount.php",
                    data: {painting_id: painting_id},
                    success: function (data) {
                        console.log('count updated');
                        $('.countlike'+painting_id).html(data);
                    }

                });

                return true;
            }
        });
    }

});


$('a.go_dislike').on('click', function () {
    if ($(this).hasClass('go_like')) {
        console.log("like");
        var thisForm = this;
        var painting_id = $(thisForm).attr('data-id');
        $.ajax({
            url: HOST+"/Paintings/like.php",
            method: "POST",
            data: {painting_id: painting_id},
            success: function (data) {
                $(thisForm).toggleClass('go_like');
                var _html = "<i class=\"fa fa-heart heart\" ></i>";
                $(thisForm).html(_html);
                /*AJAX CALL COUNT*/
                $.ajax({
                    type: "POST",
                    url: HOST+"/Paintings/likecount.php",
                    data: {painting_id: painting_id},
                    success: function (data) {
                        console.log(data);
                        console.log('count updated');
                        $('.countlike'+painting_id).html(data);
                    }


                });

                return true;
            }
        });

    }
    else {
        console.log("Dislike");
        var thisForm = this;
        var painting_id = $(thisForm).attr('data-id');
        $.ajax({
            url: HOST+"/Paintings/dislike.php",
            method: "POST",
            data: {painting_id: painting_id},
            success: function (data) {
                $(thisForm).toggleClass('go_like');
                var _html = "<i class=\"fa fa-heart-o heart\" ></i>";
                $(thisForm).html(_html);
                /*AJAX CALL COUNT*/
                $.ajax({
                    type: "POST",
                    url: HOST+"/Paintings/likecount.php",
                    data: {painting_id: painting_id},
                    success: function (data) {

                        console.log('count updated');
                        $('.countlike'+painting_id).html(data);
                    }


                });
                return true;
            }
        });
    }
});


function like() {
    console.log($(this));
    var thisForm = this;
    var painting_id = $(thisForm).attr('data-id');
    $.ajax({
        url: 'like.php',
        method: "POST",
        data: {painting_id: painting_id},
        success: function (data) {
            $(thisForm).toggleClass('go_dislike');
            var _html = "Liked";
            $(thisForm).html(_html);
            return true;
        }

    });
}


function dislike() {
    var thisForm = this;
    var painting_id = $(thisForm).attr('data-id');
    $.ajax({
        url: 'dislike.php',
        method: "POST",
        data: {painting_id: painting_id},
        success: function (data) {
            $(thisForm).toggleClass('go_like');
            var _html = "Like";
            $(thisForm).html(_html);
            return true;
        }
    });
}
