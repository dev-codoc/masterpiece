<?php session_start(); // Starting Session ?>
<?php include '../layout/db.php'; ?>
<?php include '../layout/functions.php'; ?>
<?php include '../auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler(); ?>

<div class="container ws-page-container load_product" id="load_product">
    <!-- LOAD PRODUCT CLASS IS USED TO HIDE IN FUNCTION CLASS WHEN CLICKED -->
    <div class="row">
        <div class="ws-journal-container">
            <div class="col-sm-12">
                <div class="row">
                    <ul class="products">
                        <div id="productContainer">

                            <?php
                            $last_paint_id = $_POST['last_painting_id'];
                            $painting_id = '';
                            $lat = '';
                            $lng = '';
                            $address = '';
                            $cateogry = $_POST['cateogry']; // Fetching Cateogry Name Via Ajax call Result -> LAtest, Popular, Year 2016 etc

                            //echo "cateogry name" . $cateogry;


                            if ($cateogry == "latest_painting") {
                                $sql = "SELECT * FROM `tasks` WHERE id<" . $last_paint_id . " AND  status='0'  ORDER BY id DESC LIMIT 12";
                            } elseif ($cateogry == "popular" || $cateogry == "All") {

                                $sql = "SELECT * FROM `tasks` WHERE tot_likes<" . $last_paint_id . " AND status='0' ORDER BY tot_likes DESC LIMIT 12";
                            } elseif ($cateogry == "acrylic") {

                                $sql = "SELECT * FROM `tasks` WHERE tot_likes<" . $last_paint_id . " AND status=0 AND technique LIKE 'Acrylic%' ORDER BY tot_likes DESC LIMIT 12";
                            } elseif ($cateogry == "oil") {

                                $sql = "SELECT * FROM `tasks` WHERE  tot_likes<" . $last_paint_id . " AND status='0' AND technique LIKE 'Oil%' ORDER BY tot_likes DESC LIMIT 12";
                            } elseif ($cateogry == "water") {
                                $sql = "SELECT * FROM `tasks` WHERE  tot_likes<" . $last_paint_id . " AND status='0' AND technique LIKE 'water%' ORDER BY tot_likes DESC LIMIT 12";

                            } elseif ($cateogry == "pencil") {

                                $sql = "SELECT * FROM `tasks` WHERE  tot_likes<" . $last_paint_id . " AND status='0' AND technique LIKE 'pencil%' ORDER BY tot_likes DESC LIMIT 12";

                            } elseif ($cateogry == "graphite") {

                                $sql = "SELECT * FROM `tasks` WHERE  tot_likes<" . $last_paint_id . " AND status='0' AND technique LIKE 'graphite%' ORDER BY tot_likes DESC LIMIT 12";

                            } elseif ($cateogry == "2016" || $cateogry == "2017" || $cateogry == "2018") {
                                //$sql = "SELECT * FROM `tasks` WHERE id<" . $last_paint_id . " AND   status!='3' AND created_at LIKE '$cateogry%' LIMIT 12";
                                $sql = "SELECT * FROM `tasks` WHERE created_at >'" . $last_paint_id . "' AND status='0' AND created_at LIKE '$cateogry%' ORDER BY created_at ASC LIMIT 12";

                            } elseif ($cateogry == "top") {

                                $sql = "SELECT * FROM `tasks` WHERE tot_likes<" . $last_paint_id . " AND status='0' ORDER BY tot_likes DESC LIMIT 38";
                            }


                            //var_dump($sql);
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = $result->fetch_assoc()) {

                                    $painting_id = $row["id"];
                                    $last_id = $row['tot_likes'];
                                    $art_image = $row['image_path'];
                                    $art_name = $row['task'];
                                    $art_description = $row['task_description'];
                                    $art_technique = $row['technique'];
                                    $art_price = $row['price'];
                                    $art_new_price = $row['price_new'];
                                    $tot_likes = $row['tot_likes'];
                                    $curr_code = $row['curr_code'];
                                    ?>


                                    <li class="post-1250 product type-product status-publish has-post-thumbnail product_cat-acryclic last instock shipping-taxable purchasable product-type-simple"
                                        data-sr='wait 0.1s, ease-in 20px'>

                                        <a href="<?php echo $webroot ?>/Paintings/painting_details.php/<?php echo friendlyURL($painting_id, $art_name); ?>"
                                           class="woocommerce-LoopProduct-link">
                                            <a href="<?php echo $webroot ?>/Paintings/painting_details.php/<?php echo friendlyURL($painting_id, $art_name); ?>"
                                               class="woocommerce-LoopProduct-link">
                                                <figure class="ws-product-bg fixed_image">
                                                    <img width="300"
                                                         height="300"
                                                         src="<?php echo $art_image; ?>"
                                                         class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                         alt="<?php echo $art_name; ?>">
                                                </figure>

                                                <div class="col-md-9 col-sm-9 col-xs-9 text-left">
                                                    <h3><?php echo $art_name; ?></h3>
                                                    <span class="ws-item-subtitle"><?php echo $art_technique; ?></span>
                                                    <span class="price">
                                                        <span class="woocs_price_code" data-product-id="1318">
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span class="woocommerce-Price-currencySymbol"></span>
                                                                <!-- PRICE IF CURR CODE AVAILBALE -->
                                                                <?php if ((!empty($curr_code)) && (!empty($art_new_price)))
                                                                    echo "$curr_code " . $art_new_price;
                                                                else get_painting_price($art_price);
                                                                ?>
                                                            </span>
                                                        </span>
                                                    </span>
                                                </div>

                                                <div class="col-md-3 col-sm-3 col-xs-3">

                                                    <div class="pull-right for_like">
                                                        <span class="countlike<?php echo $painting_id ?>"
                                                              id="countlike"><?php $db->countlikesArt($painting_id); ?></span>


                                                        <?php if (isset($_SESSION['login_user'])) { // Checking Session user
                                                            $user_id = $_SESSION['login_user'];
                                                            $db->hasUserLiked($user_id, $painting_id); // Checking that user has liked painting or not
                                                            if (($db->hasUserLiked($user_id, $painting_id)) == true) {
                                                                echo "<a class='go_dislike' href='javascript:void(0)' data-id='$painting_id'><i class=\"fa fa-heart heart\" ></i></a>";
                                                            } else {
                                                                echo "<a class='go_like' href='javascript:void(0)' data-id='$painting_id'><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                            }
                                                        } else { //When user is not logined
                                                            echo " <a class='login-required' href='#login-modal-shortlist' data-toggle='modal' ><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                        }
                                                        ?>
                                                    </div>

                                                </div>
                                            </a>
                                    </li>


                                <?php
                                if ($cateogry == "All" || $cateogry == "popular" || $cateogry == "acrylic" || $cateogry == "top" || $cateogry == "oil" || $cateogry == "water" || $cateogry == "pencil" || $cateogry == "graphite") {
                                    //echo "No thanks";
                                    $painting_id = $row['tot_likes'];
                                } elseif ($cateogry == "latest_painting") {

                                    //echo "Thanks";
                                    $painting_id = $row['id'];

                                } elseif ($cateogry == "2016" || $cateogry == "2017" || $cateogry || "2018") {

                                    $painting_id = $row['created_at'];

                                }
                                ?>


                                    <script type="text/javascript">

                                        var last_painting_id = "<?php echo $painting_id ?>";
                                    </script>

                                <?php } ?>


                            <?php } else {
                                echo "<div class='col-md-12 text-center' id='not-found' style='font-size:20px;font-weight: 600;margin-top: 40px'>NO RESULT FOUND</div> ";

                            }
                            ?>

                        </div>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="remove_row">
    <div class="ws-more-btn-holder col-sm-12">
        <button type="button" name="btn_more" data-vid=""
                id="btn_more" class="btn ws-more-btn" style="box-shadow: none">LOAD MORE
        </button>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        localStorage.setItem("last_id", last_painting_id);
    })
</script>


<script type="text/javascript">

    $(function () {
        var last_id = localStorage.getItem('last_id');
        $('#btn_more').attr('data-vid', last_id);
    });
</script>

<script src="js/like.js"></script>