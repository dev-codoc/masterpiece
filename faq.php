<?php session_start(); ?>
<?php require_once './layout/db.php'; ?>
<?php require_once './Artist/lat_long_conversion.php' ?>
<?php require_once './layout/functions.php'; ?>
<?php require_once './auth/Artican/include/DbHandler.php'; ?>
<?php $db = new DbHandler(); ?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">


    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png" type="image/x-icon">


    <title>FAQ - masterpiece</title>

    <?php include './Artist/artist_page_external_style.php'; ?>
    <?php include './google-analytics.php'; ?>
</head>
<body class="page-template page-template-template-page page-template-template-page-php page page-id-169 wpb-js-composer js-comp-ver-4.12.1 vc_responsive currency-usd">

<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

<!-- Loader Start -->
<div id="preloader">
    <div class="preloader-container">
        <div class="ws-spinner"></div>
    </div>
</div>
<!-- End Loader Start -->


<!-- Top Bar Start -->
<?php include './layout/header.php'; ?>
<!-- End Header -->

<!-- Page Parallax Header -->
<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="<?php echo $webroot ?>/images/masterpiece_faq.jpg">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>Got Questions?</h1>
            </div>
        </div>
    </div>
</div>
<!-- End Page Parallax Header -->

<!-- Container Start -->
<div class="container ws-page-container">

    <!-- Row Start -->
    <div class="row">

        <div class="col-sm-12">


            <article id="post-169" class="post-169 page type-page status-publish hentry">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-8">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="vc_tta-container" data-vc-action="collapse">
                                    <div class="vc_general vc_tta vc_tta-tabs vc_tta-color-grey vc_tta-style-outline vc_tta-shape-rounded vc_tta-spacing-1 vc_tta-gap-5 vc_tta-tabs-position-top vc_tta-controls-align-center">
                                        <div class="vc_tta-tabs-container">
                                            <ul class="vc_tta-tabs-list">
                                                <li class="vc_tta-tab vc_active" data-vc-tab><a
                                                            href="#1450886386648-e8568866-bfbf" data-vc-tabs
                                                            data-vc-container=".vc_tta"><span class="vc_tta-title-text">Frequently Asked Question</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="vc_tta-panels-container">
                                            <div class="vc_tta-panels">
                                                <div class="vc_tta-panel vc_active" id="1450886386648-e8568866-bfbf"
                                                     data-vc-content=".vc_tta-panel-body">
                                                    <div class="vc_tta-panel-heading"><h4 class="vc_tta-panel-title"><a
                                                                    href="#1450886386648-e8568866-bfbf"
                                                                    data-vc-accordion
                                                                    data-vc-container=".vc_tta-container"><span
                                                                        class="vc_tta-title-text">Frequently Asked Question</span></a>
                                                        </h4></div>
                                                    <div class="vc_tta-panel-body">
                                                        <div class="ws-heading text-center"><h2>Registration</h2>
                                                            <div class="ws-separator"></div>
                                                        </div>
                                                        <div id='1450886461569-a8b1f4a1-5196'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md vc_toggle_active">
                                                            <div class="vc_toggle_title"><h4>How do I register as an
                                                                    artist on Masterpieces?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content"><p>Registering with us is a
                                                                    simple and fast process. Click <a
                                                                            href="<?php echo $webroot ?>/auth/register.php"
                                                                            class="link">here</a>
                                                                    or you
                                                                    may visit <a
                                                                            href="<?php echo $webroot ?>/auth/login.php"
                                                                            class="link">My Account </a> section and
                                                                    fill in your basic
                                                                    details and click on
                                                                    ‘Register’. Once you have successfully registered,
                                                                    please tell us
                                                                    about yourself in the ‘Bio’ section and your
                                                                    country, city and contact
                                                                    number in the ‘Edit Profile’ section and start
                                                                    uploading your artwork
                                                                    by clicking on ‘Upload Art’ button. Please visit our
                                                                    <a href="terms&conditions.php" class="link">Terms
                                                                        and
                                                                        Conditions</a> for more details</p>
                                                            </div>
                                                        </div>
                                                        <div id='1451044196445-1aa1a52a-7b5f'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4>Is there any eligibility
                                                                    criteria for artist registration on
                                                                    Masterpieces?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content"><p>We at Masterpieces believe
                                                                    no factors can stop the Artist in you!
                                                                    However, to make the best use of the platform, below
                                                                    are some
                                                                    criteria to keep into consideration
                                                                    We only accept original works of art, no
                                                                    reproductions or copied
                                                                    works.
                                                                    We do not compromise in quality and consistency
                                                                    while reviewing
                                                                    our artists.
                                                                    The users are expected to be 18 years and above. In
                                                                    case of age
                                                                    less than the mentioned, we request the users to use
                                                                    the site under
                                                                    parental guidance. </p>
                                                            </div>
                                                        </div>
                                                        <div id='1451044261452-312bf1d8-0856'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4> Is there a registration
                                                                    fee?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content"><p>We are an open Platform
                                                                    for Artists, Buyers, and Art lovers and
                                                                    everyone can register with us without any
                                                                    registration fee.</p>
                                                            </div>
                                                        </div>
                                                        <div id='1451044260573-e3972c2d-87f3'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4>How do I register as a
                                                                    buyer on Masterpieces?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content"><p>Registering with us is a
                                                                    simple and fast process. Click <a
                                                                            href="<?php echo $webroot ?>/auth/register.php"
                                                                            class="link">here</a> or you
                                                                    may visit <a
                                                                            href="<?php echo $webroot ?>/auth/login.php"
                                                                            class="link">My Account </a>  section and fill in your basic
                                                                    details and click on
                                                                    ‘Register’. Once you have successfully registered,
                                                                    please tell us
                                                                    about yourself in the ‘Bio’ section and your
                                                                    country, city and contact
                                                                    number in the ‘Edit Profile’ section. On successful
                                                                    registration, you
                                                                    may start browsing through our range of artworks and
                                                                    buy what you
                                                                    like.</p>
                                                            </div>
                                                        </div>

                                                        <div id='1451044260573-e3972c2d-87f3'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default
                                                             vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4>How do I report fraud and
                                                                    inappropriate content on the
                                                                    platform?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content"><p>If you would like to
                                                                    report someone or some artwork for fraud
                                                                    or as an inappropriate content, please contact us at
                                                                    <a href="mailto:contact@yourmasterpieces.com"
                                                                       class="link">contact@yourmasterpieces.com</a>
                                                                    with a valid
                                                                    explanation,
                                                                    evidence if possible of the violation.</p>
                                                            </div>
                                                        </div>

                                                        <div id='1451044260573-e3972c2d-87f3'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default
                                                             vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4>What qualifies for fraud
                                                                    and inappropriate content on the
                                                                    platform?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content"><p>Artists have been
                                                                    depicting the nude form for millennia, and we at
                                                                    Masterpieces obviously supports and celebrates the
                                                                    representation
                                                                    of the human body in art. We will, however, take
                                                                    steps to remove
                                                                    any images that depict or otherwise promote the
                                                                    sexual exploitation,
                                                                    use of strong language, etc.
                                                                    All users are given the option to block/unblock
                                                                    inappropriate content
                                                                    from their view when they are logged on to the site.
                                                                    For instructions
                                                                    on how to block adult content from your personal
                                                                    view, if there is a
                                                                    work you'd like to call to our attention, please
                                                                    contact us at
                                                                    <a href="mailto:support@yourmasterpieces.com"
                                                                       class="link">support@yourmasterpieces.com</a>
                                                                    Our curators decide which works qualify as
                                                                    inappropriate content. </p>
                                                            </div>
                                                        </div>

                                                        <div id='1451044260573-e3972c2d-87f3'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default
                                                             vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4>Can a User outside India
                                                                    register on Your Masterpieces
                                                                    platform?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content"><p>
                                                                    Yes, Masterpieces is an Open and Equal
                                                                    opportunity platform.
                                                                    Any user who is an artist, a buyer or even an art
                                                                    lover can register
                                                                    with us without any registration fee </p>
                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 40px"><span
                                                                    class="vc_empty_space_inner"></span></div>
                                                        <div class="ws-heading text-center"><h2>ACCOUNT</h2>
                                                            <div class="ws-separator"></div>
                                                        </div>
                                                        <div id='1475821804317-16862ac3-acfb'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4>Can I add personal profile
                                                                    link to my bio?</h4><i class="vc_toggle_icon"></i>
                                                            </div>
                                                            <div class="vc_toggle_content">
                                                                <p>
                                                                    Masterpieces family believes in giving you complete
                                                                    freedom and
                                                                    control over the sale of your art, so you can put up
                                                                    your work on
                                                                    multiple sites. However, we do insist that the
                                                                    specific works put up on
                                                                    Masterpieces are not put up for sale elsewhere. This
                                                                    is only to
                                                                    ensure that buyers don’t end up purchasing artworks
                                                                    that have
                                                                    already been sold.
                                                                    If you do register with multiple sites, do remember
                                                                    that :
                                                                    Your pricing must be similar across all your places
                                                                    of sale.
                                                                    Data suggests that the best chances you have of
                                                                    selling your art is by
                                                                    showcasing as many works of your art with us as
                                                                    possible. In this
                                                                    way, buyers have more choice and can get to know
                                                                    your style a little
                                                                    better.
                                                                    If you happen to put up your work somewhere else,
                                                                    you have to keep
                                                                    us notified in case the artwork has been sold out.
                                                                    This is to avoid
                                                                    inconvenience caused to our buyers.
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div id='1475840287866-f07a54e4-11c3'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default  vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4>How do I add artworks to my
                                                                    favourite list?</h4><i class="vc_toggle_icon"></i>
                                                            </div>
                                                            <div class="vc_toggle_content"><p>We are happy you liked the
                                                                    artwork on our site. You can add it to
                                                                    your favorite list simply by clicking on the 'heart'
                                                                    icon next to the
                                                                    painting of your choice.
                                                                    You can view the list of your favourite paintings by
                                                                    clicking the 'heart'.</p>
                                                            </div>
                                                        </div>
                                                        <div id='1475840359162-2d462e00-e2f4'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default
                                                             vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4>How do I change my
                                                                    account’s password, email, shipping
                                                                    address, and billing information?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content"><p>You can update all of your
                                                                    account details by visiting the Profile
                                                                    section in <a
                                                                            href="<?php echo $webroot ?>/auth/login.php"
                                                                            class="link">My Account </a> anytime</p>
                                                            </div>
                                                        </div>

                                                        <div id='1475840359162-2d462e00-e2f4'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default
                                                             vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4> I forgot my password to
                                                                    log in. How can I request a new one?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content">
                                                                <p>
                                                                    Go to the <a
                                                                            href="<?php echo $webroot ?>/auth/forgot_password/index.php"
                                                                            class="link">Forgot Password</a> page.
                                                                    Follow the on-screen
                                                                    instructions
                                                                    in order to reset your password and we will send you
                                                                    an email with a
                                                                    link to reset your password.
                                                                    If you do not receive an email from Your
                                                                    Masterpieces within a few
                                                                    minutes, please follow the steps below:
                                                                    Check your email account's spam or junk folder to
                                                                    see if our email
                                                                    was filtered there.
                                                                    Request a new password again and be sure that you
                                                                    are entering
                                                                    your email address correctly. (If there are any
                                                                    typos, our email will
                                                                    not reach you.)
                                                                    Please also add <a
                                                                            href="mailto:support@yourmasterpieces.com"
                                                                            class="link">support@yourmasterpieces.com</a>
                                                                    to your
                                                                    contacts list
                                                                    to ensure delivery of our emails to your inbox.
                                                                    If you've checked all the above and still do not
                                                                    receive your email,
                                                                    please contact us at <a
                                                                            href="mailto:support@yourmasterpieces.com"
                                                                            class="link">support@yourmasterpieces.com</a>

                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div id='1475840359162-2d462e00-e2f4'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default
                                                             vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4> Can I switch my profile
                                                                    from a Buyer to an Artist?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content">
                                                                <p>
                                                                    Masterpieces is an open platform, you are
                                                                    registered with us as
                                                                    a user who is free to browse artworks, upload your
                                                                    artwork and even
                                                                    buy the artworks.
                                                                    To Upload an artwork go to Profile under
                                                                    <a
                                                                            href="<?php echo $webroot ?>/auth/login.php"
                                                                            class="link">My Account </a>
                                                                    section and
                                                                    click on ‘Upload Art’ button, isn’t that simple?
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div id='1475840359162-2d462e00-e2f4'
                                                             class="vc_toggle vc_toggle_arrow vc_toggle_color_default
                                                             vc_toggle_size_md">
                                                            <div class="vc_toggle_title"><h4> Can I switch my profile
                                                                    from an Artist to a Buyer?</h4><i
                                                                        class="vc_toggle_icon"></i></div>
                                                            <div class="vc_toggle_content">
                                                                <p>
                                                                    Masterpieces is an open platform, you are
                                                                    registered with us as
                                                                    a user who is free to browse artworks, upload your
                                                                    artwork and even
                                                                    buy the artworks.
                                                                    To Buy an artwork click on the  <a href="<?php echo $webroot?>/Paintings/paintings.php" class="link">Paintings</a> browse
                                                                    through our various
                                                                    Paintings on our platform.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-2">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper"></div>
                                        </div>
                                    </div>
                                </div>
            </article>


        </div>


        <style type="text/css">

            .link {
                color: #C2A476;
            }

        </style>


    </div><!-- Row End -->
</div><!-- Container End -->


<!-- Footer Start -->
<?php include './layout/footer.php'; ?>
<!-- Footer Bar End -->


<div id="fb-root"></div>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>


<script type='text/javascript'>
    function vc_js() {
        vc_toggleBehaviour(), vc_tabsBehaviour(), vc_accordionBehaviour(), vc_teaserGrid(), vc_carouselBehaviour(), vc_slidersBehaviour(), vc_prettyPhoto(), vc_googleplus(), vc_pinterest(), vc_progress_bar(), vc_plugin_flexslider(), vc_google_fonts(), vc_gridBehaviour(), vc_rowBehaviour(), vc_googleMapsPointer(), vc_ttaActivation(), jQuery(document).trigger("vc_js"), window.setTimeout(vc_waypoints, 500)
    }
    function getSizeName() {
        var screen_w = jQuery(window).width();
        return screen_w > 1170 ? "desktop_wide" : screen_w > 960 && 1169 > screen_w ? "desktop" : screen_w > 768 && 959 > screen_w ? "tablet" : screen_w > 300 && 767 > screen_w ? "mobile" : 300 > screen_w ? "mobile_portrait" : ""
    }
    function loadScript(url, $obj, callback) {
        var script = document.createElement("script");
        script.type = "text/javascript", script.readyState && (script.onreadystatechange = function () {
            ("loaded" === script.readyState || "complete" === script.readyState) && (script.onreadystatechange = null, callback())
        }), script.src = url, $obj.get(0).appendChild(script)
    }
    function vc_ttaActivation() {
        jQuery("[data-vc-accordion]").on("show.vc.accordion", function (e) {
            var $ = window.jQuery, ui = {};
            ui.newPanel = $(this).data("vc.accordion").getTarget(), window.wpb_prepare_tab_content(e, ui)
        })
    }
    function vc_accordionActivate(event, ui) {
        if (ui.newPanel.length && ui.newHeader.length) {
            var $pie_charts = ui.newPanel.find(".vc_pie_chart:not(.vc_ready)"),
                $round_charts = ui.newPanel.find(".vc_round-chart"), $line_charts = ui.newPanel.find(".vc_line-chart"),
                $carousel = ui.newPanel.find('[data-ride="vc_carousel"]');
            "undefined" != typeof jQuery.fn.isotope && ui.newPanel.find(".isotope, .wpb_image_grid_ul").isotope("layout"), ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
                var grid = jQuery(this).data("vcGrid");
                grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
            }), vc_carouselBehaviour(ui.newPanel), vc_plugin_flexslider(ui.newPanel), $pie_charts.length && jQuery.fn.vcChat && $pie_charts.vcChat(), $round_charts.length && jQuery.fn.vcRoundChart && $round_charts.vcRoundChart({reload: !1}), $line_charts.length && jQuery.fn.vcLineChart && $line_charts.vcLineChart({reload: !1}), $carousel.length && jQuery.fn.carousel && $carousel.carousel("resizeAction"), ui.newPanel.parents(".isotope").length && ui.newPanel.parents(".isotope").each(function () {
                jQuery(this).isotope("layout")
            })
        }
    }
    function initVideoBackgrounds() {
        return window.console && window.console.warn && window.console.warn("this function is deprecated use vc_initVideoBackgrounds"), vc_initVideoBackgrounds()
    }
    function vc_initVideoBackgrounds() {
        jQuery(".vc_row").each(function () {
            var youtubeUrl, youtubeId, $row = jQuery(this);
            $row.data("vcVideoBg") ? (youtubeUrl = $row.data("vcVideoBg"), youtubeId = vcExtractYoutubeId(youtubeUrl), youtubeId && ($row.find(".vc_video-bg").remove(), insertYoutubeVideoAsBackground($row, youtubeId)), jQuery(window).on("grid:items:added", function (event, $grid) {
                $row.has($grid).length && vcResizeVideoBackground($row)
            })) : $row.find(".vc_video-bg").remove()
        })
    }
    function insertYoutubeVideoAsBackground($element, youtubeId, counter) {
        if ("undefined" == typeof YT.Player)return counter = "undefined" == typeof counter ? 0 : counter, counter > 100 ? void console.warn("Too many attempts to load YouTube api") : void setTimeout(function () {
            insertYoutubeVideoAsBackground($element, youtubeId, counter++)
        }, 100);
        var $container = $element.prepend('<div class="vc_video-bg vc_hidden-xs"><div class="inner"></div></div>').find(".inner");
        new YT.Player($container[0], {
            width: "100%",
            height: "100%",
            videoId: youtubeId,
            playerVars: {
                playlist: youtubeId,
                iv_load_policy: 3,
                enablejsapi: 1,
                disablekb: 1,
                autoplay: 1,
                controls: 0,
                showinfo: 0,
                rel: 0,
                loop: 1,
                wmode: "transparent"
            },
            events: {
                onReady: function (event) {
                    event.target.mute().setLoop(!0)
                }
            }
        }), vcResizeVideoBackground($element), jQuery(window).bind("resize", function () {
            vcResizeVideoBackground($element)
        })
    }
    function vcResizeVideoBackground($element) {
        var iframeW, iframeH, marginLeft, marginTop, containerW = $element.innerWidth(),
            containerH = $element.innerHeight(), ratio1 = 16, ratio2 = 9;
        ratio1 / ratio2 > containerW / containerH ? (iframeW = containerH * (ratio1 / ratio2), iframeH = containerH, marginLeft = -Math.round((iframeW - containerW) / 2) + "px", marginTop = -Math.round((iframeH - containerH) / 2) + "px", iframeW += "px", iframeH += "px") : (iframeW = containerW, iframeH = containerW * (ratio2 / ratio1), marginTop = -Math.round((iframeH - containerH) / 2) + "px", marginLeft = -Math.round((iframeW - containerW) / 2) + "px", iframeW += "px", iframeH += "px"), $element.find(".vc_video-bg iframe").css({
            maxWidth: "1000%",
            marginLeft: marginLeft,
            marginTop: marginTop,
            width: iframeW,
            height: iframeH
        })
    }
    function vcExtractYoutubeId(url) {
        if ("undefined" == typeof url)return !1;
        var id = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
        return null !== id ? id[1] : !1
    }
    function vc_googleMapsPointer() {
        var $ = window.jQuery, $wpbGmapsWidget = $(".wpb_gmaps_widget");
        $wpbGmapsWidget.click(function () {
            $("iframe", this).css("pointer-events", "auto")
        }), $wpbGmapsWidget.mouseleave(function () {
            $("iframe", this).css("pointer-events", "none")
        }), $(".wpb_gmaps_widget iframe").css("pointer-events", "none")
    }
    document.documentElement.className += " js_active ", document.documentElement.className += "ontouchstart" in document.documentElement ? " vc_mobile " : " vc_desktop ", function () {
        for (var prefix = ["-webkit-", "-moz-", "-ms-", "-o-", ""], i = 0; i < prefix.length; i++)prefix[i] + "transform" in document.documentElement.style && (document.documentElement.className += " vc_transform ")
    }(), "function" != typeof window.vc_plugin_flexslider && (window.vc_plugin_flexslider = function ($parent) {
        var $slider = $parent ? $parent.find(".wpb_flexslider") : jQuery(".wpb_flexslider");
        $slider.each(function () {
            var this_element = jQuery(this), sliderSpeed = 800,
                sliderTimeout = 1e3 * parseInt(this_element.attr("data-interval")),
                sliderFx = this_element.attr("data-flex_fx"), slideshow = !0;
            0 === sliderTimeout && (slideshow = !1), this_element.is(":visible") && this_element.flexslider({
                animation: sliderFx,
                slideshow: slideshow,
                slideshowSpeed: sliderTimeout,
                sliderSpeed: sliderSpeed,
                smoothHeight: !0
            })
        })
    }), "function" != typeof window.vc_googleplus && (window.vc_googleplus = function () {
        0 < jQuery(".wpb_googleplus").length && !function () {
            var po = document.createElement("script");
            po.type = "text/javascript", po.async = !0, po.src = "//apis.google.com/js/plusone.js";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(po, s)
        }()
    }), "function" != typeof window.vc_pinterest && (window.vc_pinterest = function () {
        0 < jQuery(".wpb_pinterest").length && !function () {
            var po = document.createElement("script");
            po.type = "text/javascript", po.async = !0, po.src = "//assets.pinterest.com/js/pinit.js";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(po, s)
        }()
    }), "function" != typeof window.vc_progress_bar && (window.vc_progress_bar = function () {
        "undefined" != typeof jQuery.fn.waypoint && jQuery(".vc_progress_bar").waypoint(function () {
            jQuery(this).find(".vc_single_bar").each(function (index) {
                var $this = jQuery(this), bar = $this.find(".vc_bar"), val = bar.data("percentage-value");
                setTimeout(function () {
                    bar.css({width: val + "%"})
                }, 200 * index)
            })
        }, {offset: "85%"})
    }), "function" != typeof window.vc_waypoints && (window.vc_waypoints = function () {
        "undefined" != typeof jQuery.fn.waypoint && jQuery(".wpb_animate_when_almost_visible:not(.wpb_start_animation)").waypoint(function () {
            jQuery(this).addClass("wpb_start_animation")
        }, {offset: "85%"})
    }), "function" != typeof window.vc_toggleBehaviour && (window.vc_toggleBehaviour = function ($el) {
        function event(e) {
            e && e.preventDefault && e.preventDefault();
            var title = jQuery(this), element = title.closest(".vc_toggle"),
                content = element.find(".vc_toggle_content");
            element.hasClass("vc_toggle_active") ? content.slideUp({
                duration: 300, complete: function () {
                    element.removeClass("vc_toggle_active")
                }
            }) : content.slideDown({
                duration: 300, complete: function () {
                    element.addClass("vc_toggle_active")
                }
            })
        }

        $el ? $el.hasClass("vc_toggle_title") ? $el.unbind("click").click(event) : $el.find(".vc_toggle_title").unbind("click").click(event) : jQuery(".vc_toggle_title").unbind("click").on("click", event)
    }), "function" != typeof window.vc_tabsBehaviour && (window.vc_tabsBehaviour = function ($tab) {
        if (jQuery.ui) {
            var $call = $tab || jQuery(".wpb_tabs, .wpb_tour"),
                ver = jQuery.ui && jQuery.ui.version ? jQuery.ui.version.split(".") : "1.10",
                old_version = 1 === parseInt(ver[0]) && 9 > parseInt(ver[1]);
            $call.each(function (index) {
                var $tabs, interval = jQuery(this).attr("data-interval"), tabs_array = [];
                if ($tabs = jQuery(this).find(".wpb_tour_tabs_wrapper").tabs({
                        show: function (event, ui) {
                            wpb_prepare_tab_content(event, ui)
                        }, beforeActivate: function (event, ui) {
                            1 !== ui.newPanel.index() && ui.newPanel.find(".vc_pie_chart:not(.vc_ready)")
                        }, activate: function (event, ui) {
                            wpb_prepare_tab_content(event, ui)
                        }
                    }), interval && interval > 0)try {
                    $tabs.tabs("rotate", 1e3 * interval)
                } catch (e) {
                    window.console && window.console.log && console.log(e)
                }
                jQuery(this).find(".wpb_tab").each(function () {
                    tabs_array.push(this.id)
                }), jQuery(this).find(".wpb_tabs_nav li").click(function (e) {
                    return e.preventDefault(), old_version ? $tabs.tabs("select", jQuery("a", this).attr("href")) : $tabs.tabs("option", "active", jQuery(this).index()), !1
                }), jQuery(this).find(".wpb_prev_slide a, .wpb_next_slide a").click(function (e) {
                    if (e.preventDefault(), old_version) {
                        var index = $tabs.tabs("option", "selected");
                        jQuery(this).parent().hasClass("wpb_next_slide") ? index++ : index--, 0 > index ? index = $tabs.tabs("length") - 1 : index >= $tabs.tabs("length") && (index = 0), $tabs.tabs("select", index)
                    } else {
                        var index = $tabs.tabs("option", "active"), length = $tabs.find(".wpb_tab").length;
                        index = jQuery(this).parent().hasClass("wpb_next_slide") ? index + 1 >= length ? 0 : index + 1 : 0 > index - 1 ? length - 1 : index - 1, $tabs.tabs("option", "active", index)
                    }
                })
            })
        }
    }), "function" != typeof window.vc_accordionBehaviour && (window.vc_accordionBehaviour = function () {
        jQuery(".wpb_accordion").each(function (index) {
            var $tabs, $this = jQuery(this),
                active_tab = ($this.attr("data-interval"), !isNaN(jQuery(this).data("active-tab")) && 0 < parseInt($this.data("active-tab")) ? parseInt($this.data("active-tab")) - 1 : !1),
                collapsible = !1 === active_tab || "yes" === $this.data("collapsible");
            $tabs = $this.find(".wpb_accordion_wrapper").accordion({
                header: "> div > h3",
                autoHeight: !1,
                heightStyle: "content",
                active: active_tab,
                collapsible: collapsible,
                navigation: !0,
                activate: vc_accordionActivate,
                change: function (event, ui) {
                    "undefined" != typeof jQuery.fn.isotope && ui.newContent.find(".isotope").isotope("layout"), vc_carouselBehaviour(ui.newPanel)
                }
            }), !0 === $this.data("vcDisableKeydown") && ($tabs.data("uiAccordion")._keydown = function () {
            })
        })
    }), "function" != typeof window.vc_teaserGrid && (window.vc_teaserGrid = function () {
        var layout_modes = {fitrows: "fitRows", masonry: "masonry"};
        jQuery(".wpb_grid .teaser_grid_container:not(.wpb_carousel), .wpb_filtered_grid .teaser_grid_container:not(.wpb_carousel)").each(function () {
            var $container = jQuery(this), $thumbs = $container.find(".wpb_thumbnails"),
                layout_mode = $thumbs.attr("data-layout-mode");
            $thumbs.isotope({
                itemSelector: ".isotope-item",
                layoutMode: "undefined" == typeof layout_modes[layout_mode] ? "fitRows" : layout_modes[layout_mode]
            }), $container.find(".categories_filter a").data("isotope", $thumbs).click(function (e) {
                e.preventDefault();
                var $thumbs = jQuery(this).data("isotope");
                jQuery(this).parent().parent().find(".active").removeClass("active"), jQuery(this).parent().addClass("active"), $thumbs.isotope({filter: jQuery(this).attr("data-filter")})
            }), jQuery(window).bind("load resize", function () {
                $thumbs.isotope("layout")
            })
        })
    }), "function" != typeof window.vc_carouselBehaviour && (window.vc_carouselBehaviour = function ($parent) {
        var $carousel = $parent ? $parent.find(".wpb_carousel") : jQuery(".wpb_carousel");
        $carousel.each(function () {
            var $this = jQuery(this);
            if (!0 !== $this.data("carousel_enabled") && $this.is(":visible")) {
                $this.data("carousel_enabled", !0);
                var carousel_speed = (getColumnsCount(jQuery(this)), 500);
                jQuery(this).hasClass("columns_count_1") && (carousel_speed = 900);
                var carousele_li = jQuery(this).find(".wpb_thumbnails-fluid li");
                carousele_li.css({"margin-right": carousele_li.css("margin-left"), "margin-left": 0});
                var fluid_ul = jQuery(this).find("ul.wpb_thumbnails-fluid");
                fluid_ul.width(fluid_ul.width() + 300), jQuery(window).resize(function () {
                    var before_resize = screen_size;
                    screen_size = getSizeName(), before_resize != screen_size && window.setTimeout("location.reload()", 20)
                })
            }
        })
    }), "function" != typeof window.vc_slidersBehaviour && (window.vc_slidersBehaviour = function () {
        jQuery(".wpb_gallery_slides").each(function (index) {
            var $imagesGrid, this_element = jQuery(this);
            if (this_element.hasClass("wpb_slider_nivo")) {
                var sliderSpeed = 800, sliderTimeout = 1e3 * this_element.attr("data-interval");
                0 === sliderTimeout && (sliderTimeout = 9999999999), this_element.find(".nivoSlider").nivoSlider({
                    effect: "boxRainGrow,boxRain,boxRainReverse,boxRainGrowReverse",
                    slices: 15,
                    boxCols: 8,
                    boxRows: 4,
                    animSpeed: sliderSpeed,
                    pauseTime: sliderTimeout,
                    startSlide: 0,
                    directionNav: !0,
                    directionNavHide: !0,
                    controlNav: !0,
                    keyboardNav: !1,
                    pauseOnHover: !0,
                    manualAdvance: !1,
                    prevText: "Prev",
                    nextText: "Next"
                })
            } else this_element.hasClass("wpb_image_grid") && (jQuery.fn.imagesLoaded ? $imagesGrid = this_element.find(".wpb_image_grid_ul").imagesLoaded(function () {
                $imagesGrid.isotope({itemSelector: ".isotope-item", layoutMode: "fitRows"})
            }) : this_element.find(".wpb_image_grid_ul").isotope({
                itemSelector: ".isotope-item",
                layoutMode: "fitRows"
            }))
        })
    }), "function" != typeof window.vc_prettyPhoto && (window.vc_prettyPhoto = function () {
        try {
            jQuery && jQuery.fn && jQuery.fn.prettyPhoto && jQuery('a.prettyphoto, .gallery-icon a[href*=".jpg"]').prettyPhoto({
                animationSpeed: "normal",
                hook: "data-rel",
                padding: 15,
                opacity: .7,
                showTitle: !0,
                allowresize: !0,
                counter_separator_label: "/",
                hideflash: !1,
                deeplinking: !1,
                modal: !1,
                callback: function () {
                    var url = location.href, hashtag = url.indexOf("#!prettyPhoto") ? !0 : !1;
                    hashtag && (location.hash = "")
                },
                social_tools: ""
            })
        } catch (err) {
            window.console && window.console.log && console.log(err)
        }
    }), "function" != typeof window.vc_google_fonts && (window.vc_google_fonts = function () {
        return !1
    }), window.vcParallaxSkroll = !1, "function" != typeof window.vc_rowBehaviour && (window.vc_rowBehaviour = function () {
        function fullWidthRow() {
            var $elements = $('[data-vc-full-width="true"]');
            $.each($elements, function (key, item) {
                var $el = $(this);
                $el.addClass("vc_hidden");
                var $el_full = $el.next(".vc_row-full-width");
                if ($el_full.length || ($el_full = $el.parent().next(".vc_row-full-width")), $el_full.length) {
                    var el_margin_left = parseInt($el.css("margin-left"), 10),
                        el_margin_right = parseInt($el.css("margin-right"), 10),
                        offset = 0 - $el_full.offset().left - el_margin_left, width = $(window).width();
                    if ($el.css({
                            position: "relative",
                            left: offset,
                            "box-sizing": "border-box",
                            width: $(window).width()
                        }), !$el.data("vcStretchContent")) {
                        var padding = -1 * offset;
                        0 > padding && (padding = 0);
                        var paddingRight = width - padding - $el_full.width() + el_margin_left + el_margin_right;
                        0 > paddingRight && (paddingRight = 0), $el.css({
                            "padding-left": padding + "px",
                            "padding-right": paddingRight + "px"
                        })
                    }
                    $el.attr("data-vc-full-width-init", "true"), $el.removeClass("vc_hidden")
                }
            }), $(document).trigger("vc-full-width-row", $elements)
        }

        function parallaxRow() {
            var vcSkrollrOptions, callSkrollInit = !1;
            return window.vcParallaxSkroll && window.vcParallaxSkroll.destroy(), $(".vc_parallax-inner").remove(), $("[data-5p-top-bottom]").removeAttr("data-5p-top-bottom data-30p-top-bottom"), $("[data-vc-parallax]").each(function () {
                var skrollrSpeed, skrollrSize, skrollrStart, skrollrEnd, $parallaxElement, parallaxImage, youtubeId;
                callSkrollInit = !0, "on" === $(this).data("vcParallaxOFade") && $(this).children().attr("data-5p-top-bottom", "opacity:0;").attr("data-30p-top-bottom", "opacity:1;"), skrollrSize = 100 * $(this).data("vcParallax"), $parallaxElement = $("<div />").addClass("vc_parallax-inner").appendTo($(this)), $parallaxElement.height(skrollrSize + "%"), parallaxImage = $(this).data("vcParallaxImage"), youtubeId = vcExtractYoutubeId(parallaxImage), youtubeId ? insertYoutubeVideoAsBackground($parallaxElement, youtubeId) : "undefined" != typeof parallaxImage && $parallaxElement.css("background-image", "url(" + parallaxImage + ")"), skrollrSpeed = skrollrSize - 100, skrollrStart = -skrollrSpeed, skrollrEnd = 0, $parallaxElement.attr("data-bottom-top", "top: " + skrollrStart + "%;").attr("data-top-bottom", "top: " + skrollrEnd + "%;")
            }), callSkrollInit && window.skrollr ? (vcSkrollrOptions = {
                forceHeight: !1,
                smoothScrolling: !1,
                mobileCheck: function () {
                    return !1
                }
            }, window.vcParallaxSkroll = skrollr.init(vcSkrollrOptions), window.vcParallaxSkroll) : !1
        }

        function fullHeightRow() {
            var $element = $(".vc_row-o-full-height:first");
            if ($element.length) {
                var $window, windowHeight, offsetTop, fullHeight;
                $window = $(window), windowHeight = $window.height(), offsetTop = $element.offset().top, windowHeight > offsetTop && (fullHeight = 100 - offsetTop / (windowHeight / 100), $element.css("min-height", fullHeight + "vh"))
            }
            $(document).trigger("vc-full-height-row", $element)
        }

        function fixIeFlexbox() {
            var ua = window.navigator.userAgent, msie = ua.indexOf("MSIE ");
            (msie > 0 || navigator.userAgent.match(/Trident.*rv\:11\./)) && $(".vc_row-o-full-height").each(function () {
                "flex" === $(this).css("display") && $(this).wrap('<div class="vc_ie-flexbox-fixer"></div>')
            })
        }

        var $ = window.jQuery;
        $(window).off("resize.vcRowBehaviour").on("resize.vcRowBehaviour", fullWidthRow).on("resize.vcRowBehaviour", fullHeightRow), fullWidthRow(), fullHeightRow(), fixIeFlexbox(), vc_initVideoBackgrounds(), parallaxRow()
    }), "function" != typeof window.vc_gridBehaviour && (window.vc_gridBehaviour = function () {
        jQuery.fn.vcGrid && jQuery("[data-vc-grid]").vcGrid()
    }), "function" != typeof window.getColumnsCount && (window.getColumnsCount = function (el) {
        for (var find = !1, i = 1; !1 === find;) {
            if (el.hasClass("columns_count_" + i))return find = !0, i;
            i++
        }
    });
    var screen_size = getSizeName();
    "function" != typeof window.wpb_prepare_tab_content && (window.wpb_prepare_tab_content = function (event, ui) {
        var $ui_panel, $google_maps, panel = ui.panel || ui.newPanel,
            $pie_charts = panel.find(".vc_pie_chart:not(.vc_ready)"), $round_charts = panel.find(".vc_round-chart"),
            $line_charts = panel.find(".vc_line-chart"), $carousel = panel.find('[data-ride="vc_carousel"]');
        if (vc_carouselBehaviour(), vc_plugin_flexslider(panel), ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
                var grid = jQuery(this).data("vcGrid");
                grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
            }), panel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && panel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
                var grid = jQuery(this).data("vcGrid");
                grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
            }), $pie_charts.length && jQuery.fn.vcChat && $pie_charts.vcChat(), $round_charts.length && jQuery.fn.vcRoundChart && $round_charts.vcRoundChart({reload: !1}), $line_charts.length && jQuery.fn.vcLineChart && $line_charts.vcLineChart({reload: !1}), $carousel.length && jQuery.fn.carousel && $carousel.carousel("resizeAction"), $ui_panel = panel.find(".isotope, .wpb_image_grid_ul"), $google_maps = panel.find(".wpb_gmaps_widget"), 0 < $ui_panel.length && $ui_panel.isotope("layout"), $google_maps.length && !$google_maps.is(".map_ready")) {
            var $frame = $google_maps.find("iframe");
            $frame.attr("src", $frame.attr("src")), $google_maps.addClass("map_ready")
        }
        panel.parents(".isotope").length && panel.parents(".isotope").each(function () {
            jQuery(this).isotope("layout")
        })
    }), "function" != typeof window.vc_googleMapsPointer, jQuery(document).ready(function ($) {
        window.vc_js()
    });


</script>
</body>
</html>
