<?php require_once '../../layout/functions.php' ?>
<?php
include('config.php');
include('function.php');

$uemail = $_GET['email'];
$token = $_GET['token'];

$userID = UserID($uemail);

$verifytoken = verifytoken($userID, $token);

//var_dump($verifytoken);


if (isset($_POST['submit'])) {
    require_once '../Artican/include/PassHash.php';

    $new_password = $_POST['new_password'];
    $password_hash1 = PassHash::hash($new_password);
    //var_dump($password_hash1);

    $update_query = "UPDATE users SET password_hash = '$password_hash1' WHERE id = $userID";
    //var_dump($update_query);

    $update_password = mysqli_query($db, $update_query);
    if ($update_password) {
        //mysqli_query($db, "UPDATE recovery_keys SET valid = 0 WHERE userID = $userID AND token ='$token'");
        $msg = 'Your password has changed successfully. Please login with your new password.';
        $msgclass = 'bg-success';
    }

}


?>


<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta charset="UTF-8"/>
    <meta name="description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings.">
    <meta name="keywords"
          content="Paintings,Your Masterpieces, Non Digital Art, Buy paintings, Sell paintings, Sell Art, Buy Art, Bulk paintings buying, Foreign paintings, Art collectors, Collect Art, Buy painting online, Sell painting online, Painter profile, Artist profile"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta property="og:title" content="Your Masterpieces"/>
    <meta property="og:type" content=""/>
    <meta property="og:url" content="<?php echo $webroot; ?>"/>
    <meta property="og:image" content="http://www.yourmasterpieces.com/images/favicon.jpeg"/>
    <meta property="og:site_name" content="Your Masterpieces"/>
    <meta property="fb:admins" content="Your Masterpieces"/>
    <meta property="og:locale" content="en_us"/>
    <meta property="og:description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings."/>
    <link rel="canonical" href="<?php echo $webroot; ?>"/>
    <link rel='stylesheet' id='contact-form-7-css'
          href='<?php echo $webroot ?>/layout/assets/css/style.css?ver=4.7'
          type='text/css' media='all'/>

    <?php include '../../Artist/artist_page_external_style.php'; ?>
    <?php include '../../google-analytics.php'; ?>

    <style type="text/css">

        .success_login {
            color: green;
            font-size: 17px;
            font-weight: 500;
        }

        .login_failed {
            color: red;
            font-size: 17px;
            font-weight: 500;
            margin-top: -25px;
        }

    </style>


<body class="page page-id-88 page-template page-template-template-page page-template-template-page-php woocommerce-account woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<a class="skip-link screen-reader-text" href="#content">Skip to content</a>


<!-- Header Start -->
<?php require_once '../../layout/header.php' ?>
<!-- HEADER END -->

<!-- Page Parallax Header -->
<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="http://wossthemes.com/artday/wp-content/uploads/2016/09/new_header_image.jpg">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>Update Password</h1>
            </div>
        </div>
    </div>
</div>
<!-- End Page Parallax Header -->


<!-- Container Start -->
<div class="container ws-page-container">
    <!-- Row Start -->
    <div class="row">
        <div class="col-sm-12">

            <?php if ($verifytoken == 1) { ?>


                <article id="post-88" class="post-88 page type-page status-publish hentry">
                    <div class="woocommerce">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <form method="post" class="lost_reset_password">


                                    <div class="text-center">
                                        <p>Update your new password</p>
                                    </div>
                                    <div class="ws-separator"></div>

                                    <p><label for="user_login">New Password</label>

                                        <input class="input-text" type="password" name="new_password"
                                               placeholder="Enter password" id="user_login" required/>
                                    </p>


                                    <div class="clear"></div>


                                    <div class="padding-top-x20"></div>
                                    <p>
                                        <input type="submit" name="submit" class="btn ws-btn-fullwidth"
                                               value="Update Password"/>
                                    </p>


                                    <div class="error-msg padding-top-x20">
                                        <?php if (isset($msg)) { ?>
                                            <div class="<?php echo $msgclass; ?> text-center"
                                                 style="padding:5px;"><?php echo $msg; ?></div>
                                        <?php } ?>

                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>
                </article>


            <?php } else { ?>
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="text-center">
                        <h2>Invalid or Broken Token</h2>
                        <div class="ws-separator"></div>
                        <p>Opps! The link you have come with is maybe broken or already used. Please make sure that you
                            copied the link correctly .</p>
                    </div>
                </div>
            <?php } ?>


        </div>
    </div>
</div>


<?php include '../../layout/footer.php'; ?>


<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>


</body>
</html>