<?php

function checkUser($email)
{
	global $db;
	
	$query = mysqli_query($db, "SELECT id FROM users WHERE email = '$email'");

	if(mysqli_num_rows($query) > 0)
	{
		return 'true';
	}else
	{
		return 'false';
	}
}

function UserName($email){
    global $db;

    $query = mysqli_query($db, "SELECT name FROM users WHERE email = '$email'");
    $row = mysqli_fetch_assoc($query);

    return $row['name'];
}

function UserID($email)
{
	global $db;
	
	$query = mysqli_query($db, "SELECT id FROM users WHERE email = '$email'");
	$row = mysqli_fetch_assoc($query);
	
	return $row['id'];
}


function generateRandomString($length = 20) {
	// This function has taken from stackoverflow.com
    
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return md5($randomString);
}

function send_mail($to, $token,$username)
{
	require 'PHPMailer/PHPMailerAutoload.php';
	
	$mail = new PHPMailer;
	//var_dump($mail);
	
	$mail->isSMTP();
	$mail->Host = 'smtp.gmail.com';
	$mail->SMTPAuth = true;
	$mail->Username = 'support@yourmasterpieces.com';
	$mail->Password = 'YourSupport{789}';
	$mail->SMTPSecure = 'ssl';
	$mail->Port = 465;
	
	$mail->From = 'your_email@gmail.com';
	$mail->FromName = 'Masterpieces';
	$mail->addAddress($to);
	$mail->addReplyTo('support@yourmasterpieces.com', 'Masterpieces');
	
	$mail->isHTML(true);
	
	$mail->Subject = 'Masterpieces: Password Recovery Instruction';
	$link = 'http://yourmasterpieces.com/auth/forgot_password/forget.php?email='.$to.'&token='.$token;
	$mail->Body    = "
<html>
<body style=\"margin:0;padding;0\">
<div style=\"background-color: #e6e6e6;font-family:sans-serif;font-size:14px;color: #666;font-weight: 300;margin:0;padding: 40px 20px;\">
<table style=\"width:580px;margin: 0 auto;\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
    <tr>
    <td style=\"padding: 20px;background-color: #fff;\">
        <table style=\"width:100%;margin: 0 auto;padding: 0;\">
            <!-- Header Starts -->
            <tr>
                <td style=\"padding-bottom: 20px;border-bottom: 1px solid #e6e6e6;\">
                    <img height=\"50\" src=\"http://yourmasterpieces.com/images/masterpiece_logo.png\" title=\"Masterpieces Logo\">
                </td>
                <td style=\"padding-bottom: 20px;border-bottom: 1px solid #e6e6e6;\" align=\"right\">
                    <h1 style=\"font-size: 14px; color: #333;margin-bottom: 5px;\">Masterpieces account</h1>
                        <p style=\"margin-top: 5px;font-size: 13px;color: #666;font-weight: 300;\">Reset password</p>
                    </h1>
                </td>
            </tr>
            <!-- Header Ends -->
            <tr>
                <td colspan=\"2\" style=\"padding: 20px 0;\">
                    <img src=\"http://i65.tinypic.com/153p6aa.png\" style=\"width: 144px;height: 144px;display: block;margin: 20px auto 40px;\">
                    <h1 style=\"font-size: 18px;color: #333\">Hi $username</h1>
                    <p style=\"font-size: 14px; line-height: 20px;font-weight: 300;\">We received a reset password request for your account on Masterpieces, use the link below to reset your password. In case you did not request a password reset do not do anything.</p>
                    <!-- CTA Link/s Starts -->
                    <a href='$link' style=\"text-decoration: none;vertical-align: middle; display: inline-block;background-color: #BEA57C;color: #fff;line-height: 42px;font-size: 14px; font-weight: 700;letter-spacing: 2px;text-transform: uppercase;padding: 0 20px;\">Reset Password</a>

                </td>
            </tr>

            <!-- Footer Nav Starts -->
            <tr>
                <td style=\"padding-top: 20px;border-top: 1px solid #e6e6e6;\">
                    <span style=\"text-transform: uppercase;display: inline-block;border-right: 1px solid #e6e6e6;font-size: 12px;padding-right: 10px;color: #999;\">BROWSE</span>
                    <a href=\"http://yourmasterpieces.com/Artist/index.php\" style=\"text-transform: uppercase;color: #333;font-weight: 700;font-size: 12px;text-decoration: none;margin-left: 10px\">All Artists</a><a href=\"http://yourmasterpieces.com/Paintings/paintings.php\" style=\"text-transform: uppercase;color: #333;font-weight: 700;font-size: 12px;text-decoration: none;margin-left: 10px\">All Artworks</a>
                </td>
                <td style=\"padding-top: 20px;border-top: 1px solid #e6e6e6;\" align=\"right\">
                    <span style=\"text-transform: uppercase;display: inline-block;border-right: 1px solid #e6e6e6;font-size: 12px;padding-right: 10px;color: #999;\">CONNECT</span>
                    <a href=\"https://www.facebook.com/yourmasterpieces/\" style=\"display: inline-block;vertical-align: middle;text-transform: uppercase;color: #333;font-weight: 700;font-size: 12px;text-decoration: none;margin-left: 10px\"><img width=\"20\" src=\"http://i64.tinypic.com/rw5e00.png\"></a><a href=\"https://www.instagram.com/yourmasterpieces/\" style=\"display: inline-block;vertical-align: middle;text-transform: uppercase;color: #333;font-weight: 700;font-size: 12px;text-decoration: none;margin-left: 10px\"><img width=\"20\" src=\"http://i65.tinypic.com/24dihlk.png\"></a><a href=\"https://www.youtube.com/channel/UCIejbLItmDDMzmbDErRffWw\" style=\"display: inline-block;vertical-align: middle;text-transform: uppercase;color: #333;font-weight: 700;font-size: 12px;text-decoration: none;margin-left: 10px\"><img width=\"20\" src=\"http://i65.tinypic.com/1zn0arn.png\"></a>

                </td>
            </tr>
            <!-- Footer Nav Ends -->
        </table>
    </td>
    </tr>
    <!-- Email Meta Starts -->
    <tr>
        <td style=\"padding: 20px;font-size: 13px;color: #999;text-align: center;line-height: 18px;font-weight: 300;\">
            <p>You received this email because you opted in for this. Add us to your contacts to prevent this email from landing in spam. <a style=\"color: #666;text-decoration: none;border-bottom: 1px dashed #666\" href=\"mailto:support@yourmasterpieces.com?Subject='Unsubscribe'\">Unsubscribe</a> from such communication in the future.<br><br>Copyright &copy; 2016 - 2018, <a style=\"color: #666;text-decoration: none;border-bottom: 1px dashed #666\" href=\"http://yourmasterpieces.com\">www.yourmasterpieces.com</a></p>
        </td>
    </tr>
    <!-- Email Meta Ends -->
</table>
</div>
</body>
</html>
";
	
	$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
	
	if(!$mail->send()) {
		return 'fail';
	} else {
		return 'success';
	}
}

function verifytoken($userID, $token)
{	
	global $db;
	$sql="SELECT  count(*) as total FROM users WHERE id='$userID' AND auth_token = '$token'";
	//var_dump($sql);
	$query = mysqli_query($db, $sql);
	//var_dump($query);
	$row = mysqli_fetch_assoc($query);
	
	if(mysqli_num_rows($query) > 0)
	{
		if($row['total'] == 1)
		{
		    //echo "Done";
			return 1;
		}else
		{
		    //echo "error";
			return 0;
		}
	}else
	{
	    //echo "failed";
		return 0;
	}
	
}
?>