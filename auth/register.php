<?php session_start(); ?>
<?php require_once '../layout/db.php'?>
<?php require_once '../auth/Artican/include/DbHandler.php' ?>
<?php require_once '../auth/register_function.php' ?>
<?php require_once '../layout/functions.php' ?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8"/>
    <meta name="description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings.">
    <meta name="keywords"
          content="Paintings,Your Masterpieces, Non Digital Art, Buy paintings, Sell paintings, Sell Art, Buy Art, Bulk paintings buying, Foreign paintings, Art collectors, Collect Art, Buy painting online, Sell painting online, Painter profile, Artist profile"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta property="og:title" content="Your Masterpieces"/>
    <meta property="og:type" content=""/>
    <meta property="og:url" content="<?php echo $webroot; ?>"/>
    <meta property="og:image" content="http://www.yourmasterpieces.com/images/favicon.jpeg"/>
    <meta property="og:site_name" content="Your Masterpieces"/>
    <meta property="fb:admins" content="Your Masterpieces"/>
    <meta property="og:locale" content="en_us"/>
    <meta property="og:description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings."/>
    <link rel="canonical" href="<?php echo $webroot; ?>"/>
    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png"
          type="image/x-icon">

    <title>Your Masterpieces - Register </title>

    <?php include '../Artist/artist_page_external_style.php'; ?>
    <?php include '../google-analytics.php'; ?>

    <style type="text/css">

        .success_register {
            color: green;
            font-size: 17px;
            font-weight: 500;
        }

        .register_failed {
            color: red;
            font-size: 17px;
            font-weight: 500;
        }

        span.required {
            font-size: 15px;
        }

        label.inline.terms {
            margin-left: -15px;
            text-decoration: none;
            text-transform: none;
            text-align: justify;
            font-size: 11px;
            color: #353535;
        }

        .term_text {
            color: #C2A476;
            font-size: 11px;
        }

        select#country {
            height: 55px;
        }

        .password-grouping{
            display: inline-table;
        }
        .password-grouping .input-group-btn{
            background: #f5f5f5;
        }
        #show_password{
            background: none;
            box-shadow: none;
            outline:none !important;
        }

        .glyphicon{
            color:black !important;
        }



        @media screen and (max-width: 479px) {
            label.inline.terms {
                font-size: 10px;
            }

            .term_text {
                font-size: 10px;
            }
            .ws-register-link{
                font-size: 12px;
                padding-top: 3px;
            }
        }


    </style>

</head>
<body class="page page-id-88 page-template page-template-template-page page-template-template-page-php woocommerce-account woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

<!-- Loader Start -->
<div id="preloader">
    <div class="preloader-container">
        <div class="ws-spinner"></div>
    </div>
</div>
<!-- End Loader Start -->


<!-- Header Start -->
<?php include '../layout/header.php' ?>
<!-- End Header -->

<!-- Page Parallax Header -->
<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="http://wossthemes.com/artday/wp-content/uploads/2016/09/new_header_image.jpg">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>My Account</h1>
            </div>
        </div>
    </div>
</div>
<!-- End Page Parallax Header -->


<!-- REGISTER FUNCTION START -->

<?php
if (isset($_POST['register'])) {
    $name = $_POST["name"];
    $firstname = $_POST["firstname"];
    $email = $_POST["email"];
    $password = $_POST["password"];
    $fb_id = "nil";
    $country = $_POST['country'];

    $db = new DbHandler();
    $res = $db->createUser($name, $firstname, $email, $password, $fb_id, $country);

    if ($fb_id == "nil") {
        // get the user by email
        $user = $db->getUserByEmail($email);
        $response['apiKey'] = $user['api_key'];
        $response['status'] = $user['status'];
        $response['otp'] = $user['otp'];
        $response['userId'] = $user['id'];
    } else {
        $user = $db->getUserByFbId($fb_id);
        $response['apiKey'] = $user['api_key'];
        $response['status'] = $user['status'];
        $response['otp'] = $user['otp'];
        $response['userId'] = $user['id'];
    }

    if ($res == USER_CREATED_SUCCESSFULLY) {
        $response["error"] = false;
        $response["message"] = 'You are successfully registered Please <a href="login.php">Login to Continue</a>';
        echoRespnse(201, $response);
    } else if ($res == USER_CREATE_FAILED) {
        $response["error"] = true;
        $response["message"] = "Oops! An error occurred while registering";
        echoRespnse(200, $response);
    } else if ($res == USER_ALREADY_EXISTED) {
        $response["error"] = true;
        $response["message"] = "Sorry, this email already exists";
        echoRespnse(200, $response);
    }
}
?>


<!-- REGISTER FUNCTION END -->


<!-- Container Start -->
<div class="container ws-page-container">

    <!-- Row Start -->
    <div class="row">

        <div class="col-sm-12">

            <article id="post-88" class="post-88 page type-page status-publish hentry">
                <div class="woocommerce">


                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">

                            <form method="post" class="ws-login-form">


                                <!-- Full NAME START -->
                                <div class="form-group">
                                    <label for="username" class="control-label">Full Name<span
                                                class="required">*</span></label>
                                    <input type="text" class="form-control" name="name" id="name" value="" required/>
                                </div>
                                <!-- NAME END -->


                                <!-- FIRST NAME OR USER NAME START -->
                                <div class="form-group">
                                    <label for="username" class="control-label">Display Name<span
                                                class="required">*</span></label>
                                    <input type="text" class="form-control" name="firstname" id="firstname" value=""
                                           required/>
                                </div>
                                <!-- FIRST NAME END -->


                                <!-- Country-->
                                <div class="form-group">
                                    <label class="control-label">Country<span class="required">*</span></label>
                                    <select class="form-control bfh-countries" data-country="IN" name="country"
                                            id="country"></select>
                                </div>
                                <!-- COUNTRY END -->

                                <!-- EMAIL START -->
                                <div class="form-group">
                                    <label for="username" class="control-label">email address<span
                                                class="required">*</span></label>
                                    <input type="email" class="form-control" name="email" id="email" value="" required/>
                                </div>
                                <!-- EMAIL END -->

                                <!-- PASSWORD START -->
                                <div class="form-group">
                                    <label for="password" class="control-label">Password<span class="required">*</span></label>
                                    <div class="password-grouping">
                                        <input class="form-control" type="password" name="password" id="password"
                                               required/>
                                        <span class="input-group-btn">
                                        <button id="show_password" class="btn btn-secondary" type="button">
                                            <span class="glyphicon glyphicon-eye-close"></span>
                                        </button>
                                    </span>
                                    </div>
                                </div>
                                <!-- PASSWORD END -->

                                <script type="text/javascript">

                                   /* $('#show_password').hover(function functionName() {
                                            //Change the attribute to text
                                            $('#password').attr('type', 'text');
                                            $('.glyphicon').removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
                                        }, function () {
                                            //Change the attribute back to password
                                            $('#password').attr('type', 'password');
                                            $('.glyphicon').removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
                                        }
                                    );*/

                                </script>

                                <script type="text/javascript">

                                    $('#show_password').click(function () {

                                        if (($('#password').attr('type')) == "password") {
                                            $('#password').attr('type', 'text');
                                            $('.glyphicon').removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');

                                        } else {
                                            $('#password').attr('type', 'password');
                                            $('.glyphicon').removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');

                                        }
                                        //Change the attribute to text
                                    });


                                </script>

                                <!-- TERMS AND CONDITION CHECK -->

                                <div class="pull-left" style="margin-top: -15px;padding-bottom: 10px">
                                    <div class="checkbox">
                                        <label for="rememberme" class="inline terms">
                                            By "Register" I agree that I have read and accepted the <a
                                                    href="<?php echo $webroot ?>/terms&conditions.php" target="_blank"
                                                    class="term_text">TERMS AND CONDITIONS</a>
                                            and that the Masterpieces family may keep me informed with emails about
                                            products and services.
                                        </label>
                                    </div>
                                </div>

                                <!-- TERMS AND CONDITION CHECK-->

                                <div class="clearfix"></div>


                                <!-- Button -->
                                <input type="submit"
                                       class="btn ws-btn-fullwidth"
                                       name="register"
                                       value="Register"/>
                                <div class="padding-top-x20"></div>


                                <!-- Facebook Button -->
                                <?php include 'fb/fbindex.php' ; ?>


                            </form>
                            <!-- End Register Form -->


                            <!-- LOGIN Form-->
                            <div class="ws-register-form">

                                <!-- Link -->
                                <div class="ws-register-link">
                                    <a href="<?php echo $webroot ?>/auth/login.php">Already Registered? Click here to
                                        Login
                                    </a>
                                </div>

                                <!-- Register Modal -->
                                <div class="modal fade" id="ws-register-modal" tabindex="-1">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <a class="close" data-dismiss="modal" aria-label="Close"><span
                                                            aria-hidden="true">&times;</span></a>
                                            </div>

                                            <div class="modal-body">

                                                <div class="row">
                                                    <div class="ws-register-modal-content">
                                                        <!-- Register Form -->
                                                        <form method="post" class="ws-register-form">

                                                            <h3>Create An Account</h3>
                                                            <div class="ws-separator"></div>


                                                            <!-- Email -->
                                                            <div class="form-group">
                                                                <label for="reg_email" class="control-label">Email
                                                                    address <span class="required">*</span></label>
                                                                <input type="email" class="form-control" name="email"
                                                                       id="reg_email" value=""/>
                                                            </div>


                                                            <div class="form-group">
                                                                <label for="reg_password" class="control-label">Password
                                                                    <span class="required">*</span></label>
                                                                <input type="password" class="form-control"
                                                                       name="password" id="reg_password"/>
                                                            </div>


                                                            <!-- Spam Trap -->
                                                            <div style="left: -999em; position: absolute;"><label
                                                                        for="trap">Anti-spam</label><input type="text"
                                                                                                           name="email_2"
                                                                                                           id="trap"
                                                                                                           tabindex="-1"/>
                                                            </div>


                                                            <div class="modal-footer">
                                                                <div class="padding-top-x30"></div>
                                                                <!-- Button -->
                                                                <input type="hidden" id="woocommerce-register-nonce"
                                                                       name="woocommerce-register-nonce"
                                                                       value="7491cd5198"/><input type="hidden"
                                                                                                  name="_wp_http_referer"
                                                                                                  value="/artday/my-account/"/>
                                                                <input type="submit" class="btn ws-btn-fullwidth"
                                                                       name="register" value="Create Account"/>
                                                                <div class="padding-top-x20"></div>

                                                                <!-- Link -->
                                                                <div class="ws-register-link">
                                                                    <a href="#ws-register-modal" data-toggle="modal">Already
                                                                        have an account? Sign in here.</a>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- End Register Modal -->
                            </div>
                            <!-- End Register -->

                        </div>
                    </div>

                </div>
            </article>


        </div>


    </div><!-- Row End -->
</div><!-- Container End -->


<!-- Footer Start -->
<?php include '../layout/footer.php' ?>
<!-- Footer End -->

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>


<script type="text/javascript" src="<?php echo $webroot ?>/auth/user/js/curr_code.js"></script>


</body>
</html>
