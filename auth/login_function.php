

<?php
if (isset($_POST['login'])) {

    // reading post params
    $email = $_POST['email'];
    $password = $_POST['password'];
    $fb_id = 'nil';

    $response = array();

    $db = new DbHandler();
    // check for correct email and password
    if ($db->checkLogin($email, $password)) {
        // get the user by email
        //$user = $db->getUserByFbId($fb_id);
        if ($fb_id == "nil") {
            // get the user by email
            $user = $db->getUserByEmail($email);
        } else {
            $user = $db->getUserByFbId($fb_id);
        }
        $user_id = $user['id'];
        $tot_likes = $db->getTotLikes($user_id);
        $tot_uploads = $db->getTotUploads($user_id);
        $tot_created = $db->getTotCreated($user_id);


        if ($user != NULL) {

            session_start();
            $_SESSION['login_user'] = $user['id']; // SET SESSION USER ID

            //echo "user".$_SESSION['user'];
            header("location:$webroot/auth/user/profile.php ");


            $response["error"] = false;
            $response['name'] = $user['name'];
            $response['firstname'] = $user['firstname'];
            $response['email'] = $user['email'];
            $response['apiKey'] = $user['api_key'];
            $response['status'] = $user['status'];
            $response['otp'] = $user['otp'];
            $response['createdAt'] = $user['created_at'];
            $response['total_likes'] = $tot_likes['total'];
            $response['total_uploads'] = $tot_uploads['total'];
            $response['total_created'] = $tot_created['total'];
            $response['userId'] = $user['id'];
            $response['userBio'] = $user['bio'];
            $response['message'] = "Successfully logined";


        } else {
            // unknown error occurred
            $response['error'] = true;
            $response['message'] = "An error occurred. Please try again";
        }
    } else {
        // user credentials are wrong
        $response['error'] = true;
        $response['message'] = 'Login failed. Incorrect credentials';
    }

}
?>
<!-- Login FUNCTION END -->


<?php
function echoRespnse($status_code, $response)
{
    if ($status_code == "200") {

        echo "</br><div class='col-md-12 text-center login_failed'>" . json_encode($response['message']) . "</div></br>";

    }
    if ($status_code != "200") {
        echo "</br><div class='col-md-12 text-center success_login'>" . json_encode($response) . "</div></br>";
    }
}

?>