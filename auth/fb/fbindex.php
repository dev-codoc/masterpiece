<?php
$url = $_SERVER['REQUEST_URI'];
$link_array = explode('/', $url);
$urlend = end($link_array);
?>
<?php include 'functions.php'; ?>

<?php if ($urlend == 'login.php') { ?>
    <a class="btn ws-btn-facebook" data-toggle="modal" data-target="#fbmodal"><i class="fa fa-facebook"
                                                                                 style="font-size: 17px"></i>
        &nbsp Sign In with Facebook</a>
<?php } ?>

<?php if ($urlend == 'register.php') { ?>
    <a class="btn ws-btn-facebook" data-toggle="modal" data-target="#fbmodal">Sign Up with Facebook</a>
<?php } ?>


<style type="text/css">
    body {
        font-family: 'Varela Round', sans-serif;
    }

    .modal-confirm {
        color: #636363;
        width: 505px;
        height: 700px
    }

    .modal-confirm .modal-content {
        padding: 20px;
        border-radius: 5px;
        border: none;
    }

    .modal-confirm .modal-header {
        border-bottom: none;
        position: relative;
    }

    .modal-confirm h4 {
        text-align: center;
        font-size: 26px;
        margin: 30px 0 -15px;
    }

    .modal-confirm .form-control, .modal-confirm .btn {
        min-height: 40px;
        border-radius: 3px;
    }

    .modal-confirm .close {
        position: absolute;
        top: -5px;
        right: -5px;
    }

    .modal-confirm .modal-footer {
        border: none;
        text-align: center;
        border-radius: 5px;
        font-size: 13px;
    }

    .modal-confirm .icon-box {
        color: #fff;
        position: absolute;
        margin: 0 auto;
        left: 0;
        right: 0;
        top: -48px;
        width: 55px;
        height: 55px;
        border-radius: 50%;
        z-index: 9;
        background: #82ce34;
        padding: 9px;
        text-align: center;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
    }

    .modal-confirm .icon-box i {
        font-size: 35px;
        position: relative;
        top: 3px;
    }

    .modal-confirm.modal-dialog {
        margin-top: 80px;
    }

    .ws-call-btn {
        background: #c2a476 !important;
        margin-bottom: 25px;
    }

    .ws-call-btn a {
        border-style: none !important;
        margin-top: 5px;
    }

    .modal-title {
        font-size: 28px !important;
        font-weight: 600;
        letter-spacing: 1px;
        color: #353535;
        font-family: Montserrat;
    }

    .trigger-btn {
        display: inline-block;
        margin: 100px auto;
    }

    @media screen and (max-width: 479px) {
        .modal-confirm {
            color: #636363;
            width: 345px;
        }
    }
</style>


<!-- Modal HTML -->
<div id="fbmodal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="icon-box" style="background: #C2A476">
                    <i class="fa fa-facebook"></i>
                </div>
                <h4 class="modal-title">Login with Facebook coming soon.</h4>
            </div>
            <div class="modal-body">
                <p class="text-center" style="font-family: Montserrat">If you wish to login using Facebook, download
                    Masterpiece mobile app </p>
            </div>

            <div class="modal-footer">

                <div class="col-md-12" style="margin-top: 15px">

                    <div class="col-md-6" style="margin-bottom: 15px">

                        <a href="https://play.google.com/store/apps/details?id=com.codon.masterpiece&amp;hl=en">

                            <img src="<?php echo $webroot ?>/assets/img/android.png" style="height: 52px;border: none;">

                        </a>

                    </div>

                    <div class="col-md-6" style="margin-bottom: 15px">

                        <a href="https://itunes.apple.com/in/app/masterpiece-platform-for-non/id1089890769?mt=8">
                            <img src="<?php echo $webroot ?>/assets/img/ios.svg" style="height: 64px;border: none;margin-top: -5px; width: 175px">
                        </a>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

