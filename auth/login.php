<?php session_start();?>
<?php require_once '../layout/db.php' ;?>
<?php require_once '../auth/Artican/include/DbHandler.php' ?>
<?php require_once '../auth/Artican/include/PassHash.php' ?>
<?php require_once '../layout/functions.php' ?>
<?php require_once '../auth/login_function.php' ?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Your Masterpieces - Login </title>
    <meta charset="UTF-8"/>
    <meta name="description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings.">
    <meta name="keywords"
          content="Paintings,Your Masterpieces, Non Digital Art, Buy paintings, Sell paintings, Sell Art, Buy Art, Bulk paintings buying, Foreign paintings, Art collectors, Collect Art, Buy painting online, Sell painting online, Painter profile, Artist profile"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta property="og:title" content="Your Masterpieces"/>
    <meta property="og:type" content=""/>
    <meta property="og:url" content="<?php echo $webroot; ?>"/>
    <meta property="og:image" content="http://www.yourmasterpieces.com/images/favicon.jpeg"/>
    <meta property="og:site_name" content="Your Masterpieces"/>
    <meta property="fb:admins" content="Your Masterpieces"/>
    <meta property="og:locale" content="en_us"/>
    <meta property="og:description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings."/>
    <link rel="canonical" href="<?php echo $webroot; ?>"/>
    <link rel='stylesheet' id='contact-form-7-css'
          href='<?php echo $webroot?>/layout/assets/css/style.css?ver=4.7'
          type='text/css' media='all'/>

    <?php include '../Artist/artist_page_external_style.php'; ?>
    <?php include '../google-analytics.php' ; ?>

    <style type="text/css">

        .success_login {
            color: green;
            font-size: 17px;
            font-weight: 500;
        }
        .login_failed {
            color: red;
            font-size: 17px;
            font-weight: 500;
            margin-top: -25px;
        }
        span.required {
            font-size: 15px;
        }

        .text-or{
            text-align: center;
            border-bottom: 1px solid #000;
            line-height: 0.1em;
            margin: 10px 0 20px;
        }
        .ws-btn-facebook {
            color: #fff;
            background-color: #3b5998;
            font-family: Montserrat;
            text-shadow: none;
        }
        .ws-btn-facebook:hover {
            color: #fff;
            background-color: #3b5998;
            opacity: 0.8;
        }
        .password-grouping{
            display: inline-table;
        }
        .password-grouping .input-group-btn{
            background: #f5f5f5;
        }
        #show_password{
            background: none;
            box-shadow: none;
            outline:none !important;
        }

        .glyphicon{
            color:black !important;
        }
        .or-text{
            font-family: Montserrat;

        }


        @media screen and (max-width: 479px) {
            label.inline.terms {
                font-size: 10px;
            }

            .term_text {
                font-size: 10px;
            }
            .or-text{
                font-family: Montserrat;
                margin-top: 25px;
            }
            .ws-register-link{
                font-size: 12px;
                padding-top: 3px;
            }
            .modal-confirm{
                width:95% !important;
            }
        }



    </style>

</head>
<body class="page page-id-88 page-template page-template-template-page page-template-template-page-php woocommerce-account woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

<!-- Loader Start -->
<div id="preloader">
    <div class="preloader-container">
        <div class="ws-spinner"></div>
    </div>
</div>
<!-- End Loader Start -->



<!-- Header Start -->
<?php include '../layout/header.php' ?>

<!-- End Header -->

<!-- Page Parallax Header -->
<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="http://wossthemes.com/artday/wp-content/uploads/2016/09/new_header_image.jpg">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>My Account</h1>
            </div>
        </div>
    </div>
</div>
<!-- End Page Parallax Header -->



<!-- Container Start -->
<div class="container ws-page-container">

    <!-- Row Start -->
    <div class="row">

        <div class="col-sm-12">

            <article id="post-88" class="post-88 page type-page status-publish hentry">
                <div class="woocommerce">


                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">

                            <form method="post" class="ws-login-form">

                                <div id="error_login"><?php
                                    if (isset($_POST['login'])) {
                                        echoRespnse(200, $response);
                                    }
                                    ?></div>

                                <!-- EMAIL START -->
                                <div class="form-group">
                                    <label for="username" class="control-label">email address<span
                                                class="required">*</span></label>
                                    <input type="email" class="form-control" name="email" id="email" value=""/>
                                </div>
                                <!-- EMAIL END -->

                                <!-- PASSWORD START -->
                                <div class="form-group">
                                    <label for="password" class="control-label">Password<span class="required">*</span></label>

                                    <div class="password-grouping">
                                        <input class="form-control" type="password" name="password" id="password"
                                               required/>
                                        <span class="input-group-btn">
                                        <button id="show_password" class="btn btn-secondary" type="button">
                                            <span class="glyphicon glyphicon-eye-close"></span>
                                        </button>
                                    </span>
                                    </div>


                                </div>
                                <!-- PASSWORD END -->

                                <!-- STAY LOGINED -->
                                <div class="pull-left">
                                    <div class="checkbox">
                                        <label for="rememberme" class="inline">
                                            <input name="rememberme" type="checkbox" id="rememberme" value="forever">
                                            Stay signed in
                                        </label>
                                    </div>
                                </div>
                                <!-- STAY LOGINED -->



                                <div class="clearfix"></div>


                                <!-- Button -->
                                <input type="submit"
                                       class="btn ws-btn-fullwidth"
                                       name="login"
                                       value="Login"/>
                                <div class="padding-top-x10"></div>

                                <!-- Facebook Button -->


                                <!-- FORGOT PAASWORD LINK START -->
                                <div class="pull-left">
                                    <div class="ws-register-link">
                                        <a href="<?php echo $webroot ?>/auth/forgot_password/index.php">Forgot password?</a>
                                    </div>
                                </div>
                                <!-- FORGOT PASSWORD LINK EDN-->

                                <!-- FORGOT PAASWORD LINK START -->
                                <div class="pull-right">
                                    <div class="ws-register-link">
                                        <a href="<?php echo $webroot ?>/auth/register.php">Don't have an account?</a>
                                    </div>
                                </div>
                                <!-- FORGOT PASSWORD LINK EDN-->





                                <div class="col-md-12 text-center or-text padding-top-x20 padding-bottom-x15">
                                     OR
                                </div>






                                <?php include 'fb/fbindex.php' ;?>


                            </form>
                            <!-- End Register Form -->


                            <!-- LOGIN Form-->

                            <!-- End Register -->

                        </div>
                    </div>

                </div>
            </article>


        </div>


    </div><!-- Row End -->
</div><!-- Container End -->

<script type="text/javascript">

    $('#show_password').hover(function functionName() {
            //Change the attribute to text
            $('#password').attr('type', 'text');
            $('.glyphicon').removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
        }, function () {
            //Change the attribute back to password
            $('#password').attr('type', 'password');
            $('.glyphicon').removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
        }
    );

</script>


<!-- Footer Start -->
<?php include '../layout/footer.php' ?>
<!-- Footer End -->

<script type = 'text/javascript' src = '<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8' ></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>

</body>
</html>
