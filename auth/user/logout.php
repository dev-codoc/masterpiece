<?php include '../../layout/conf.php' ;?>

<?php
session_start();
if(session_destroy()) // Destroying All Sessions
{
    header("Location:$webroot/auth/login.php"); // Redirecting To Home Page
}
?>