<?php include 'sessions.php'; ?>
<?php include '../../layout/db.php'; ?>
<?php include '../../Artist/lat_long_conversion.php'; ?>
<?php include '../../layout/functions.php'; ?>
<?php include '../Artican/include/DbHandler.php' ?>
<?php $user_profile_id = $login_user_id; ?>
<?php $db = new DbHandler(); ?>


<!DOCTYPE html>
<html lang="en">
<head>

    <?php
    $sql = "SELECT * FROM users where id='$user_profile_id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $fb_id = $row['fb_id'];
            ?>
            <title><?php echo $row['name'] ?></title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content=" Artist on Your Masterpieces || <?php echo $row['bio']; ?>">
            <meta property="og:image" content="<?php get_artist_profile_image($conn, $artist_detail_id, $fb_id); ?>">
        <?php }
    } ?>

    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png" type="image/x-icon">
    <?php include '../../Artist/artist_page_external_style.php'; ?>
    <link rel="stylesheet" href="css/profile.css">
    <script type="text/javascript" src="<?php echo $webroot ?>/layout/config.js"></script>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php include '../../google-analytics.php' ?>


</head>

<!--Header File For Masterpiece -->
<?php include '../../layout/header.php'; ?>


<?php
$url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
// URL of page for share PAGE FOR ARTIST
?>

<!-- Container Start -->

<div class="ws-footer" class="img-responsive">

    <?php
    $sql = "SELECT * FROM users where id='$user_profile_id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
    $facebook = $row['fb_id'];
    $artist_name = $row['firstname']; //Storing name
    $location = $row['location']; //storing latlong value into a variable
    $city = $row['city'];
    $country = $row['country'];

    ?>

    <div class="col-md-12 pull-right">
        <a class="edit_profile pull-right" data-toggle="modal" data-target="#edit-profile-modal">Edit Profile</a>
    </div>


    <center class="image-profile-container">
        <img src="<?php get_artist_profile_image($conn, $user_profile_id, $facebook); ?>"
             class="img-circle-artist-page">


        <form method="post" action="../../auth/Artican/v1/ProfilePicUpload/profilepicUpload.php"
              enctype="multipart/form-data" id="upload_profile_img">

            <!-- PROFILE PIC CHNAGE START-->
            <div class="box">
                <input type="hidden" class="form-control" name="user_id" value="<?php echo $row['id'] ?>" required>
                <input type="file" name="image" id="upload_profile" class="inputfile inputfile-1">
                <label for="upload_profile">
                    <span><i class="fa fa-edit"></i></span>
                </label>
            </div>

            <script type="text/javascript">


                $('#upload_profile').change(function () {
                    $('#upload_profile_img').submit();
                })
            </script>
            <!-- PROFILE PIC CHANGE END -->

        </form>
    </center>
    <br/>
    <h2 class="name"> <?php echo $artist_name; ?> </h2>
    <center>
        <div class="map-text">
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <b style="color:#C2A476"></b>

            <!-- ARTIST LOCATION START WITH COUNTRY AND CITY -->
            <?php if ((empty($city)) && (empty($country))) { ?>
                <?php get_artist_location($location) ?>
            <?php } else {
                echo ucwords($city);
                if (!empty($country) && !empty($city)) {
                    echo ",";
                }
                echo ucwords($country);
            } ?>
            <!-- ARTIST LOCATION END WITH COUNTRY AND CITY -->
            <br/>

            <!--
            Total Art Uploaded : <?php /*echo $db->getTotUploads($user_profile_id); */ ?>


            <br>

            Total Created : <?php /*echo $db->getTotCreated($user_profile_id); */ ?>
-->


        </div>
        <div class="col-sm-12 ws-footer-col">
            <div id="text-3" class="bar widget-space widget_text">
                <div class="textwidget"
                     style=" padding-right: 25px;padding-left:25px;">
                    <p class="artist_bio_details"><?php echo $row['bio']; ?> </p>

                </div>
            </div>
        </div>
        <br/>
        <?php }
        }; ?>

        <!-- SHARE LINKS FOR ARTIST PROFILE -->


        <!-- ICON ADDED -->

        <div class="text-center icon-new">


                <span class="uplaod-icon">
                    <img src="<?php echo $webroot ?>/auth/user/images/upload-icon.png" class="icon-trend">
                    <span class="text-trend"><?php echo $db->getTotUploads($user_profile_id); ?></span>
                    <p class="grey-text">Uploaded</p>
                </span>
            <span class="uplaod-icon">
                    <img src="<?php echo $webroot ?>/auth/user/images/created-icon.png" class="icon-trend">
                        <span class="text-trend"><?php echo $db->getTotCreated($user_profile_id); ?></span>
                        <p class="grey-text">Created</p>
                </span>

            <span class="uplaod-icon">
                    <img src="<?php echo $webroot ?>/auth/user/images/like-icon.png" class="icon-trend">
                        <span class="text-trend"><?php echo $db->getTotLikes($user_profile_id); ?></span>
                        <p class="grey-text">Likes</p>
                </span>

        </div>


</div>

<?php include '../../auth/user/edit_profile.php' ?>

<div class="col-md-12 pull-right upload-btn hidden-xs">
    <a class="edit_profile pull-right"
       href="upload_painting.php">Upload Art</a>
</div>




<div class="container ws-page-container" style="padding-top:unset !important">
    <div class="row">
        <div class="col-sm-12">
            <article id="post-1786" class="post-1786 page type-page status-publish hentry">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                    <div class="wpb_wrapper">

                                        <div class="vc_row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="woocommerce columns-4">
                                                            <div class="row">


                                                                <div class="col-md-12 pull-right upload-btn visible-xs">
                                                                    <a class="edit_profile pull-right"
                                                                       href="upload_painting.php">Upload Art</a>
                                                                </div>



                                                                <ul class="products">

                                                                    <?php

                                                                    // Storing Artist ID DOn't Confuse
                                                                    $sql = " SELECT distinct t.* FROM tasks t, user_tasks ut WHERE ut.user_id =$user_profile_id AND t.id = ut.task_id AND status=0 ";

                                                                    //var_dump($sql);
                                                                    $result = $conn->query($sql);
                                                                    if ($result->num_rows > 0) {
                                                                    // output data of each row
                                                                    while ($row = $result->fetch_assoc()) {
                                                                    $task_id = $row['id'];
                                                                    $art_image = $row['image_path'];
                                                                    $art_name = $row['task'];
                                                                    $art_price = $row['price'];
                                                                    $art_new_price = $row['price_new'];
                                                                    $tot_likes = $row['tot_likes'];
                                                                    $curr_code = $row['curr_code'];
                                                                    ?>

                                                                    <li class="first  post-<?php echo $task_id; ?> product type-product status-publish has-post-thumbnail product_cat-water-color-painting instock shipping-taxable product-type-simple"
                                                                        data-sr='wait 0.1s, ease-in 20px'>
                                                                        <a href="<?php echo $webroot; ?>/auth/user/edit_painting_details.php?id=<?php echo $task_id ?>"
                                                                           class="woocommerce-LoopProduct-link">

                                                                            <div class="image">

                                                                                <div class="absolute-in-image">


                                                                                    <!-- DELETE BUTTON CODE -->
                                                                                    <a class="delete pull-left"
                                                                                       href="#deleteArt"
                                                                                       data-toggle="modal"
                                                                                       data-id="<?php echo $task_id ?>">
                                                                                        <img src="images/delete-trend.png"
                                                                                             class="delete">
                                                                                    </a>
                                                                                    <!-- END DELETE CODE-->
                                                                                </div>

                                                                                <figure class="ws-product-bg">

                                                                                    <a href="<?php echo $webroot; ?>/auth/user/edit_painting_details.php?id=<?php echo $task_id ?>"
                                                                                       class="woocommerce-LoopProduct-link">


                                                                                        <img width="300"
                                                                                             height="300"
                                                                                             src="<?php echo $art_image; ?>"
                                                                                             class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                                                             alt=""/>

                                                                                </figure>
                                                                        </a>
                                                            </div>


                                                            <script>
                                                                $('.delete').click(function () {
                                                                    var id = $(this).data('id');
                                                                    // alert(id);
                                                                    localStorage.setItem('deleteArt', id);
                                                                });
                                                            </script>

                                                            <div class="col-md-9 col-sm-9 col-xs-9 text-left">

                                                                <h3><?php echo $art_name; ?></h3>
                                                                <span class="ws-item-subtitle"><?php echo $row['technique']; ?></span>
                                                                <span class="price">
                                                                                            <span class="woocs_price_code"
                                                                                                  data-product-id="1318">
                                                                                                <span class="woocommerce-Price-amount amount">
                                                                                                    <span class="woocommerce-Price-currencySymbol"></span>

                                                                                                    <!-- PRICE IF CURR CODE AVAILBALE -->
                                                                                                    <?php if ((!empty($curr_code)) && (!empty($art_new_price)))
                                                                                                        echo "$curr_code " . $art_new_price;
                                                                                                    else get_painting_price($art_price);
                                                                                                    ?>
                                                                                                    <!-- END PRICE CODE -->
                                                                                                </span>
                                                                                            </span>
                                                                                        </span>
                                                            </div>

                                                            <div class="col-md-3 col-sm-3
                                                                                        col-xs-3">
                                                                                        <span class="countlike<?php echo $task_id ?>"
                                                                                              id="countlike">
                                                    <?php $db->countlikesArt($task_id); ?>
                                                </span>

                                                                <!-- LIKE SECTION START -->
                                                                <div class="pull-right for_like">
                                                                    <?php if (isset($_SESSION['login_user'])) { // Checking Session user
                                                                        $user_id = $_SESSION['login_user'];

                                                                        $db->hasUserLiked($user_id, $task_id); // Checking that user has liked painting or not
                                                                        if (($db->hasUserLiked($user_id, $task_id)) == true) {
                                                                            echo "<a class='go_dislike' href='javascript:void(0)' data-id='$task_id'><i class=\"fa fa-heart heart\" ></i></a>";
                                                                        } else {
                                                                            echo "<a class='go_like' href='javascript:void(0)' data-id='$task_id'><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                                        }
                                                                    } else { //When user is not logined
                                                                        echo " <a class='login-required' href='#login-modal-shortlist' data-toggle='modal' ><i class=\"fa fa-heart-o heart\" ></i></a>";
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <!-- LIKE SECTION END -->

                                                                <!-- LIKE SECTION END -->


                                                                </a>
                                                                </li>

                                                                <!--DELETE PAINTING START !-->


                                                                <!--<a href="" onclick="<?php /*$db->reportTask($user_profile_id, $task_id)*/ ?>" class="btn btn-danger">DELETE </a>
-->
                                                                <!-- DELETE ART END -->

                                                                <?php }
                                                                }; ?>
                                                                </ul>

                                                                <?php include 'delete_confirm_modal.php' ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            </article>


        </div>

    </div><!-- Row End -->
</div><!-- Container End -->


<?php include '../../layout/footer.php'; ?>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/frontend/cart-fragments.min.js?ver=2.6.9'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src="<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8"></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/auth/user/js/disable_inspect_element.js'></script>
<script type="text/javascript" src="<?php echo $webroot ?>/Paintings/js/like.js"></script>

