<!--
<script type="text/javascript"
        src="js/country.js"></script>-->
<!-- Register Form-->
<style type="text/css">
    input.form-control.submit {
        background: #C2A476;
        color: white;
        text-transform: uppercase;
        box-shadow: none;
        text-shadow:none;
    }

    .modal-header {
        border-bottom: none !important;
    }

    .heading_edit {
        font-size: 15px;
        margin-top: -40px;
    }

    .ws-register-form input.form-control {
        height: 45px;
    }
    .submit-btn{
        background: #CCB48E !important;
        color:white;
        font-family: Montserrat;
        text-shadow: none;
    }

    @media screen and (max-width: 479px) {
        .modal-content {
            padding-right: 25px;
            padding-left: 25px;
        }

        #ws-register-modal .modal-header {
            border: none;
            margin-right: -25px;
            padding: 5px 10px 0 0;
        }

    }

    #country{
        width: 100%;
        height: 45px;
        font-size: 16px;
        padding: 6px 12px;
        border-radius: 0;
        border: none;
        background-color: #f5f5f5;
    }

    .inline-form-phone{
        display:inline-flex;
        width:100%;
    }
    input.form-control.bfh-phone{
        width:15%;
    }
    input.form-control.mobile{
        width:85%;
    }

    span.required {
        font-size: 15px;
    }

    @media screen and (max-width:479px){
        input.form-control.bfh-phone {
            width: 28%;
        }
    }

</style>

<!-- Register Modal -->


<?php
$sql = "SELECT * FROM users where id='$user_profile_id' or fb_id='$user_profile_id'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
    while ($row = $result->fetch_assoc()) {

        $facebook = $row['fb_id'];
        $artist_user_name = $row['firstname'];
        $artist_full_name = $row['name']; //Storing name
        $location = $row['location']; //storing latlong value into a variable
        $bio = $row['bio'];
        $city = $row['city'];
        $country = $row['country'];
        $mobile = $row['mobile_num'];
        ?>

        <div class="modal fade" id="edit-profile-modal" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    </div>

                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">

                            <div class="modal-body">
                                <!-- Register Form -->
                                <form class="ws-register-form"
                                      action="<?php echo $webroot ?>/auth/user/edit_profile_submit.php" method="post">

                                    <h3 class="heading_edit">Update Information</h3>
                                    <div class="ws-separator"></div>

                                    <!-- USER ID -->

                                    <input type="hidden" class="form-control" name="user_id"
                                           value="<?php echo $row['id'] ?>">

                                    <!-- User Name -->
                                    <div class="form-group">
                                        <label class="control-label">User Name<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="user-name"
                                               value="<?php echo $artist_user_name ?>" required>
                                    </div>

                                    <!-- Full Name -->
                                    <div class="form-group">
                                        <label class="control-label">Full Name<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="full-name"
                                               value="<?php echo $artist_full_name; ?>" required>
                                    </div>

                                    <!--City -->
                                    <div class="form-group">
                                        <label class="control-label">City<span></span></label>
                                        <input type="text" class="form-control" name="city"
                                               value="<?php echo $city; ?>">
                                    </div>
                                    <!-- Country-->
                                    <div class="form-group">
                                        <label class="control-label">Country<span class="required">*</span></label>
                                        <select class="form-control bfh-countries" data-country="<?php echo $country ?>" name="country" id="country" required></select>
                                    </div>

                                    <!-- Mobile Number -->
                                    <div class="form-group">
                                        <label class="control-label">Mobile Number with country
                                            code<span></span></label>

                                        <div class="inline-form-phone">
                                        <input type="text" class="form-control bfh-phone"  data-country="country" name="auto-country-code" readonly required>
                                        <input type="number" class="form-control mobile" name="mobile" value="<?php echo $mobile ?>">
                                        </div>
                                    </div>

                                    <!-- ABOUT OR BIO -->
                                    <div class="form-group">
                                        <label class="control-label">Enter Your Bio<span></span></label>
                                        <input type="text" class="form-control" name="bio" value="<?php echo $bio ?>">

                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <input type="submit" class="form-control submit submit-btn" value="Update Info"
                                               name="request_submit">
                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    <?php }
} ?>

<script type="text/javascript" src="js/curr_code.js"></script>