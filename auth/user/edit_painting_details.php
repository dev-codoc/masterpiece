<?php include 'sessions.php'; ?>
<?php include '../../layout/db.php'; ?>
<?php include_once '../../layout/functions.php' ?>

<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1); ?>

<?php
$userid = $login_user_id; //Getting user id from session variable
$product_id = $_GET["id"]; //Getting Product id from url id

//Checking task id with user id if not then redirecting into profilepage
$check_task = "SELECT * FROM `user_tasks` WHERE user_id=$userid AND task_id=$product_id ";
//var_dump($check_task);
$result = $conn->query($check_task);
if ($result->num_rows == 0) {
    header("location:$webroot/auth/user/profile.php");
}
?>


<!--CODE FOR FETCHING PAINTING VALUES-->
<?php

$sql = "SELECT * FROM tasks where id='$product_id'";
$result = $conn->query($sql);
if ($result->num_rows > 0)   {
// output data of each row
while ($row = $result->fetch_assoc()) {
$product_id = $row['id'];
$art_image = $row['image_path'];
$art_name = $row['task'];
$art_description = $row['task_description'];
$art_technique = $row['technique'];
$art_tags = $row['tags'];
$curr_code = $row['curr_code'];
$art_price = $row['price'];
$art_year = $row['year'];
$art_dimension = $row['dimension'];
$created_by_me = $row['createdbyme'];
$wish_to_sell = $row['wishtosell'];
$art_sell_mail = $row['contact_info'];
?>


<!--CODE END FOR FETCHING PAINTING VALUES -->

<!DOCTYPE html>
<html lang="en-US">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?php echo $webroot ?>/images/favicon.png"
          type="image/x-icon">


    <title>Your Masterpieces - Edit Painting </title>


    <!-- This site is optimized with the Yoast SEO plugin v5.1 - https://yoast.com/wordpress/plugins/seo/ -->
    <meta charset="UTF-8"/>
    <meta name="description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings.">
    <meta name="keywords"
          content="Paintings,Your Masterpieces, Non Digital Art, Buy paintings, Sell paintings, Sell Art, Buy Art, Bulk paintings buying, Foreign paintings, Art collectors, Collect Art, Buy painting online, Sell painting online, Painter profile, Artist profile"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <meta property="og:title" content="Your Masterpieces"/>
    <meta property="og:type" content=""/>
    <meta property="og:url" content="<?php echo $webroot; ?>"/>
    <meta property="og:image" content="http://www.yourmasterpieces.com/images/favicon.jpeg"/>
    <meta property="og:site_name" content="Your Masterpieces"/>
    <meta property="fb:admins" content="Your Masterpieces"/>
    <meta property="og:locale" content="en_us"/>
    <meta property="og:description"
          content="Platform for non-digital art. An open platform for artists, art collectors, art galleries and NGOs. Artists from all over the world, irrespective of their age, expertise or experience,share their paintings."/>
    <link rel="canonical" href="<?php echo $webroot; ?>"/>

    <!-- / Yoast SEO plugin. -->
    <link rel="stylesheet" href="css/iconstylesheet.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="css/upload_painting_stylesheet.css">


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/Masterpiecelive/google-analytics.php' ?>

    <?php include '../../Artist/artist_page_external_style.php'; ?>

    <script src="<?php echo $webroot ?>/layout/config.js" type="text/javascript"></script>


</head>
<body class="page-template page-template-template-page page-template-template-page-php page page-id-566 wpb-js-composer js-comp-ver-4.12.1 vc_responsive currency-usd">

<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

<!-- Loader Start -->
<div id="preloader">
    <div class="preloader-container">
        <div class="ws-spinner"></div>
    </div>
</div>
<!-- End Loader Start -->


<!-- Top Bar Start -->

<?php include '../../layout/header.php'; ?>
<!-- End Header -->

<!-- Page Parallax Header -->
<div class="ws-parallax-header parallax-window" data-parallax="scroll"
     data-image-src="<?php echo $webroot ?>/images/masterpiece_contact.jpg"
     data-img-alt="Your Masterpieces">
    <div class="ws-overlay">
        <div class="ws-parallax-caption">
            <div class="ws-parallax-holder">
                <h1>Edit Painting Details</h1>
            </div>
        </div>
    </div>
</div>
<!-- End Page Parallax Header -->


<!-- Container Start -->
<div class="container ws-page-container">
    <!-- Row Start -->
    <div class="row">
        <div class="col-sm-12">

            <article id="post-693" class="post-693 page type-page status-publish hentry">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element">
                                    <h3 class="step-text">Step 1</h3>
                                    <div class="wpb_wrapper">
                                        <div class="ws-contact-info">
                                            <div class="images ws-product-bg fixed_detail_img result"
                                                 style="height:382px">

                                                <!-- AJAX IMAGE WILL LOAD HERE-->
                                                <img src="<?php echo $art_image ?>" id="previewing"
                                                     style="height:382px;object-fit: cover;width:100%"
                                                     class="attachment-shop_single size-shop_single wp-post-image"
                                                     alt="" title="">
                                                <!--END AJAX FILE LOAD-->
                                            </div>


                                            <!-- UPLOAD IMAGE START -->

                                            <form id="uploadimage"  method="post"
                                                  enctype="multipart/form-data" class="wpcf7-form form-horizontal ws-contact-form">

                                            <div class="col-sm-12" style="margin-top: 4%;">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <div id="selectImage">
                                                                <div class="input-file-container">
                                                                        <span class="" aria-hidden="true">

                                                                            <!-- IMAGE UPLOAD BUTTON -->
                                                                            <input type="file"
                                                                                   class="inputfile inputfile-4"
                                                                                   id="file" name="file"
                                                                                   value="<?php echo $data['imagepath']; ?>"
                                                                                   required="yes">


                                                                            <label for="file">



                                                                                    <img src="images/upload.png" style="cursor: pointer">

                                                                            </label>

                                                                                <!-- IMAGE UPLOAD CODE END -->

                                                                        </span>
                                                                </div>
                                                                <p class="file-return"></p>
                                                                <br/>

                                                                <!-- SUBMIT PICTURE IMAGE -->
                                                                <div class="col-sm-12">
                                                                    <input type="submit" value="Upload image" name="submitimage" class="wpcf7-form-control wpcf7-submit ws-big-btn">
                                                                </div>

                                                            </div>
                                                        </center>
                                                    </div>
                                            </form>
                                            <center>
                                                <h4 id='loading1'>Updating Image ... Please Wait !! </h4>
                                            </center>
                                            <center>
                                                <h4 id='loading2'>"Your data is being uploaded for
                                                    processing..."</h4>
                                                <div id="message"><span id='time'></span>
                                            </center>
                                            <center>

                                                <!-- UPLOAD IMAGE END -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <h3 class="step-text">Step 2</h3>
                            <div role="form" class="wpcf7" id="" lang="en-US" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form method="post"
                                      class="wpcf7-form form-horizontal ws-contact-form" id="form_info">
                                    <input type="hidden" name="userid" value="<?php echo $userid; ?>">
                                    <input type="hidden" name="taskid" value="<?php echo $product_id; ?>">
                                    <div class="row ws-contact-form">
                                        <!-- ART NAME START-->
                                        <div class="col-sm-12">
                                                <span class="wpcf7-form-control-wrap text-1">
                                                   <!-- IMAGE VALUE IS COMING HERE WHEN CHANGE PHOTO BUTTON CLICK-->
                                                    <input type="hidden" id="file_location" name="image_art"
                                                           value="<?php echo $art_image ?>">
                                                    <!-- END  CHANNGE ART  BUTTON CLICK-->
                                                    <input type="text"
                                                           name="task"
                                                           class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"
                                                           aria-required="true"
                                                           aria-invalid="false"
                                                           placeholder="Art Name"
                                                           required="yes"
                                                           value="<?php echo $art_name; ?>"
                                                    />
                                                </span>
                                        </div>
                                        <!-- ART NAME END-->

                                        <!-- ART TECHNIQUE START-->
                                        <div class="col-sm-6">
                                                <span class="wpcf7-form-control-wrap text-1">
                                                    <input type="text"
                                                           name="technique"
                                                           class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"
                                                           aria-required="true"
                                                           aria-invalid="false"
                                                           placeholder="Technique"
                                                           value="<?php echo $art_technique; ?>"
                                                           required="required"/>
                                                </span>
                                        </div>
                                        <!-- ART TECHNIQUE END-->

                                        <!-- ART DIMENSION START-->
                                        <div class="col-sm-6">
                                                <span class="wpcf7-form-control-wrap text-2">
                                                    <input type="text"
                                                           name="dimension"
                                                           class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"
                                                           aria-required="true"
                                                           aria-invalid="false"
                                                           placeholder="Dimension"
                                                           value="<?php echo $art_dimension; ?>"
                                                           required="required"
                                                    /></span>
                                        </div>
                                        <!-- ART DIMENSION END-->

                                        <!-- ART YEAR START-->
                                        <div class="col-sm-6">
                                                <span class="wpcf7-form-control-wrap email-1">
                                                    <input type="number"
                                                           name="year"
                                                           class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"
                                                           aria-required="true"
                                                           aria-invalid="false"
                                                           placeholder="Year"
                                                           value="<?php echo $art_year; ?>"
                                                           required="required"
                                                    /></span>
                                        </div>
                                        <!-- ART YEAR END-->

                                        <!-- ART YEAR START-->
                                        <div class="col-sm-6">
                                                <span class="wpcf7-form-control-wrap email-1">
                                                    <input type="text"
                                                           name="tags"
                                                           class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"
                                                           aria-required="true"
                                                           aria-invalid="false"
                                                           placeholder="Tag 1, Tag 2, Tag 3 ..."
                                                           value="<?php echo $art_tags; ?>"
                                                    /></span>
                                        </div>
                                        <!-- ART YEAR END-->


                                        <!-- CREATED BY ME FIELD STARRT-->

                                        <div class="col-sm-6" style="height:65px">
                                              <span class="wpcf7-form-control-wrap email-1">

                                                    <select class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"
                                                            aria-required="true" style="height:40px" id="created"
                                                            name="created-by-me">
                                                        <option disabled>Created by Me ?</option>
                                                          <option value="1" <?php if ($created_by_me == "1") echo "Selected" ?>>Created by me</option>
                                                          <option value="0" <?php if ($created_by_me == "0") echo "Selected" ?>>Not created by me</option>
                                                </select>
                                                </span>
                                        </div>
                                        <!-- CREATED BY ME FIELD END -->


                                        <!-- ART SELL OPTION START-->
                                        <div class="col-sm-6" style="height:65px">
                                                <span class="wpcf7-form-control-wrap email-1">
                                                    <select class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"
                                                            aria-required="true" style="height:40px" id="wish-sell"
                                                            name="wish-to-sell"
                                                            onchange="hide_remove();">
                                                        <option disabled>Wish To Sell ?</option>
                                                          <option value="1" <?php if ($wish_to_sell == "1") echo "Selected" ?>>I wish to sell</option>
                                                          <option value="0" <?php if ($wish_to_sell == "0") echo "Selected" ?>>I don't wish to sell</option>
                                                </select>
                                                </span>
                                        </div>
                                        <script type="text/javascript">


                                            window.onload = function () {
                                                hide_remove();
                                            };


                                            function hide_remove() {
                                                var sel = $('#wish-sell').val();

                                                if (sel == "1") {
                                                    $('.wish').removeClass('hidden_field');
                                                }
                                                if (sel == "0") {
                                                    $('.wish').addClass('hidden_field');
                                                }

                                            }


                                        </script>
                                        <!-- ART SELL OPTION END-->

                                        <!-- ART PRICE START-->
                                        <div class="col-sm-6 wish ">
                                                <span class="wpcf7-form-control-wrap email-1">

                                                    <select class="form-control bfh-currencies" name="curr_code"
                                                            data-currency="<?php if (!empty($curr_code)) echo $curr_code; else echo "USD" ?>"
                                                            style="height: 40px;margin-bottom: 25px;"
                                                            id="curr_code">

                                                    </select>
                                                </span>
                                        </div>
                                        <!-- ART PRICE END-->

                                        <!-- ART PRICE START-->
                                        <div class="col-sm-6 wish ">
                                                <span class="wpcf7-form-control-wrap email-1">
                                                    <input type="number"
                                                           name="price"
                                                           class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"
                                                           aria-required="true"
                                                           aria-invalid="false"
                                                           placeholder="Enter Selling Price"
                                                           value="<?php echo $art_price; ?>"
                                                    /></span>
                                        </div>
                                        <!-- ART PRICE END-->

                                        <!-- ART EMAIL START-->
                                        <div class="col-sm-12 wish ">
                                                <span class="wpcf7-form-control-wrap email-1">
                                                    <input type="email"
                                                           name="email"
                                                           class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"
                                                           aria-required="true"
                                                           aria-invalid="false"
                                                           placeholder="Enter your Email address and / or Phone no. with country code"
                                                           value="<?php echo $art_sell_mail; ?>"
                                                    />
                                                </span>
                                        </div>
                                        <!-- ART EMAIL END -->


                                        <!-- ART INFORMATION START-->
                                        <div class="col-sm-12">
                                                <span class="wpcf7-form-control-wrap textarea-1"><textarea
                                                            name="art-info" cols="40" rows="10"
                                                            class="wpcf7-form-control wpcf7-textarea form-control"
                                                            aria-invalid="false"
                                                            placeholder="About Art Information"><?php echo $art_description; ?></textarea></span>
                                        </div>
                                        <!-- ART INFORMATION END -->


                                        <div class="col-sm-12">
                                            <input type="button" value="Update Art Info" name="upload_details"
                                                   class="wpcf7-form-control wpcf7-submit ws-big-btn"
                                                   id="upload_painting_details"/>
                                        </div>
                                    </div>


                                    <div id="error_text" class="text-center"></div>


                            </div>

                            </form>


                        </div>
                    </div>
                </div>
        </div>
    </div>
    </article>


</div>


</div><!-- Row End -->
</div><!-- Container End -->

<?php }
} ?>
<!-- Footer Start -->

<?php include '../../layout/footer.php'; ?>
<!-- Footer Bar End -->

<div id="fb-root"></div>
</body>
</html>


<!-- AJAX CALL FOR SUBMIT PAINTING -->
<script type="text/javascript">
    $("#upload_painting_details").click(function () {

        var image_art = $("input[name=image_art]").val();
        var taskname = $("input[name=task]").val();
        var technique = $("input[name=technique]").val();
        var dimension = $("input[name=dimension]").val();
        var year = $("input[name=year]").val();
        var tags = $("input[name=tags]").val();
        var wish_sell = $('#wish-sell').val();
        var created_by_me = $('#created').val();
        var price = $("input[name=price]").val();
        var curr_code = $("#curr_code").val();
        var email = $("input[name=email]").val();


        //Image Null Checking
        if (image_art == "") {
            console.log("Image is not uploaded");
            $('#error_text').html("Please Upload Image First");
            return false;
        }

        // task name null checking
        if (taskname == "") {
            console.log("Please enter task name");
            $('#error_text').html("Please Enter Art name");
            return false;
        }

        // technique null checking
        if (technique == "") {
            console.log("Please enter technique name");
            $('#error_text').html("Please Enter technique");
            return false;
        }

        //Dimension checking
        if (dimension == "") {
            console.log("Please enter Dimension");
            $('#error_text').html("Please Enter Dimension");
            return false;
        }

        //year checking
        if (year == "") {
            console.log("Please enter year");
            $('#error_text').html("Please Enter Year");
            return false;
        }

        /*Created null check*/
        if (created_by_me == "Created by Me ?") {
            console.log("Please select created by me");
            $('#error_text').html("Please select created by me");
            return false;
        }


        /*Wish to sell Null check*/
        if (wish_sell == "Wish To Sell ?") {
            console.log("Please Select Wish to sell");
            $('#error_text').html("Please Select Wish to Sell");
            return false;
        }


        if (image_art != "") {
            if (wish_sell == 1) {

                if (price == "") {
                    $('#error_text').html("Please Enter Your Price");
                    return false;
                }
                else if (price != "") {
                    if (email == "") {
                        $('#error_text').html("Please Enter Your Email");
                        return false;
                    }
                }
            }
        }
        // if wish to sell is not selected then curr code blank set

        if (image_art != "") {
            if (wish_sell == 1 && price != "" && email != "") {
                console.log("All fields filled");
                $.ajax({
                    type: "POST",
                    url: "edit_painting_function.php",
                    data: $("#form_info").serialize(),
                    success: function (response) {
                        console.log(response);
                        window.location.href = HOST +"/auth/user/profile.php";
                    }
                });
            }
        }

        if (image_art != "" && wish_sell == 0) {

            // alert("wish no");
            console.log("All fields filled");
            $.ajax({
                type: "POST",
                url: "edit_painting_function.php",
                data: $("#form_info").serialize(),
                success: function (response) {
                    console.log(response);
                    window.location = HOST+"/auth/user/profile.php";
                    setTimeout(5000);

                }


            });
        }
    });


</script>

<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/bootstrap/js/bootstrap.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/plugins/parallax.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/scrollReveal.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/owl-carousel/owl.carousel.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/bootstrap-dropdownhover.min.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/plugins/jquery.sticky.js?ver=4.7.8'></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/layout/assets/js/main.js?ver=4.7.8'></script>
<script type='text/javascript' src="art_upload.js"></script>
<script type="text/javascript" src="js/curr_code.js"></script>
<script type='text/javascript'
        src='<?php echo $webroot ?>/auth/user/js/disable_inspect_element.js'></script>
