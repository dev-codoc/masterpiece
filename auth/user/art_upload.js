$(document).ready(function (e) {
    $("#chart").hide();
    $("#loading1").hide();
    $("#loading2").hide();
    $('#shadow').removeAttr('src');
    $("#submit").hide();



    $("#uploadimage").on('submit',(function(e) {
        e.preventDefault();
        $("#message").empty();

        //$("chart").empty();

        $('#loading1').show();
        submitButt = $("#submit");
        submitButt.prop("disabled",false);
        $.ajax({
            url: "fileUpload.php", // Url to which the request is send
            type: "POST",
            dataType: 'json',             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
//alert(data);
                console.log(data);
                console.log(data.upload);
                $("#loading1").hide();
//var json = JSON.parse(data);
                $("#file_location").val(data.upload);
//alert(data.upload);
                $("#message").html("<b><span id='success'>Image Uploaded Successfully...!!</span><br/></b>");
                $("#submit").show();
            }
        });
    }));

// Function to preview image after validation

    $(function() {
        $("#file").change(function() {

            var fileSize = this.files[0].size / 1024 / 1024;
            if (fileSize > 5) {
                $('#errorImage').css('display', 'block');
                $('#imageUpload').attr('disabled','true');
                return false;

            }


            $("#message").empty(); // To remove the previous error message
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {
                $('#previewing').attr('src','noimage.png');
                $("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                return false;
            }
            else
            {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    function imageIsLoaded(e) {
        $("#file").css("color","green");
        $('#image_preview').css("display", "block");
        $('#previewing').attr('src', e.target.result);
        $('#previewing').attr('width', '250px');
//$('#previewing').attr('height', '230px');
    };
});