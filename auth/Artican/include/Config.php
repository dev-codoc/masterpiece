<?php
/**
 * Database configuration  
 * Note: This file contains the entire project configuration like database connection parameters and other variables.
 * Sharmeen Sahibole
 */
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_NAME', 'liveart');
 
define('USER_CREATED_SUCCESSFULLY', 0);
define('USER_CREATE_FAILED', 1);
define('USER_ALREADY_EXISTED', 2);
?>