<?php
/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 * Sharmeen Sahibole
 */

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class DbHandler
{

    private $conn;

    function __construct()
    {
        //require_once dirname(__FILE__) . './DbConnect.php';
        require_once dirname(__FILE__) . '/DbConnect.php';

        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();

    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($name, $firstname, $email, $password, $fb_id,$country)
    {
        //require_once 'PassHash.php'; 
        require_once dirname(__FILE__) . '/PassHash.php';
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($fb_id, $email)) {
            // Generating password hash
            $password_hash = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            //Generate OTP while creating user
            $string = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $string_shuffled = str_shuffle($string);
            $otp = substr($string_shuffled, 1, 7);

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(name, firstname, email, password_hash, api_key, status, fb_id, otp,country) values(?, ?, ?, ?, ?, 0, ?, ?,?)");
            $stmt->bind_param("ssssssss", $name, $firstname, $email, $password_hash, $api_key, $fb_id, $otp,$country);

            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db 
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password)
    {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password_hash FROM users WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }


    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($fb_id, $email)
    {
        $stmt = $this->conn->prepare("SELECT id from users WHERE fb_id = ? AND email = ?");
        $stmt->bind_param("ss", $fb_id, $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email)
    {
        //id, name, firstname, email, fb_id, api_key, otp, status, created_at
        $stmt = $this->conn->prepare("SELECT id, name, firstname, email, fb_id, api_key, otp, status, created_at, bio FROM users WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user by fb_id
     * @param String $fb_id User Facebook id
     */
    public function getUserByFbId($fb_id)
    {
        //id, name, firstname, email, fb_id, api_key, otp, status, created_at
        $stmt = $this->conn->prepare("SELECT id, name, firstname, email, fb_id, api_key, otp, status, created_at, bio FROM users WHERE fb_id = ?");
        $stmt->bind_param("s", $fb_id);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id)
    {
        $stmt = $this->conn->prepare("SELECT api_key FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            $api_key = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getUserNamebyId($user_id)
    {
        $stmt = $this->conn->prepare("SELECT firstname FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            $first_name = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $first_name;
        } else {
            return NULL;
        }
    }

    public function getUserBiobyId($user_id)
    {
        $stmt = $this->conn->prepare("SELECT bio FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            $bio = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $bio;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key)
    {
        $stmt = $this->conn->prepare("SELECT id FROM users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key)
    {
        $stmt = $this->conn->prepare("SELECT id from users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey()
    {
        return md5(uniqid(rand(), true));
    }

    /* ------------- `tasks` table method ------------------ */

    /**
     * Creating new task
     * @param String $user_id user id to whom task belongs to
     * @param String $task task text
     */


    public function createTask($user_id, $task, $taskdescription, $image_path, $year, $technique, $dimension, $tags, $createdbyme, $wishtosell, $curr_code, $price_new, $price, $contact_info)
    {


        $stmt = $this->conn->prepare("INSERT INTO tasks(task, task_description, image_path, year, technique, dimension, tags, createdbyme, wishtosell, curr_code, price_new, price, contact_info, tot_likes) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, 0)");

        $stmt->bind_param("ssssssssssiss", $task, $taskdescription, $image_path, $year, $technique, $dimension, $tags, $createdbyme, $wishtosell, $curr_code,$price_new, $price, $contact_info);

        $result = $stmt->execute();
        $stmt->close();
        //echo "art info updated <br>";

        if ($result) {
            // task row created
            // now assign the task to user
            $new_task_id = $this->conn->insert_id;
//            echo "$new_task_id";
  //          echo "Usertask is going to update <br>";
            $res = $this->createUserTask($user_id, $new_task_id, $wishtosell, $price, $contact_info);
            if ($res) {
                // task created successfully 
                return $new_task_id;
            } else {
                // task failed to create
                return NULL;
            }
        } else {
            // task failed to create
            return NULL;
        }

    }


    /**
     * Fetching single task
     * @param String $task_id id of the task
     */
    public function getTask($task_id)
    {
        /* $stmt = $this->conn->prepare("SELECT t.* FROM tasks t WHERE t.id = ? ORDER BY t.id DESC");
         $stmt->bind_param("i", $task_id);
         if ($stmt->execute()) {
             $task = $stmt->get_result()->fetch_assoc();
             $stmt->close();
             return $task;
         } else {
             return NULL;
         }*/

        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t WHERE t.id = ? AND t.status = 0 ORDER BY t.id DESC");
        $stmt->bind_param("i", $task_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    /**
     * Fetching all user tasks
     * @param String $user_id id of the user
     */
    public function getAllUserTasks($user_id)
    {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_tasks ut WHERE t.id = ut.task_id AND ut.user_id = ? AND t.status = 0 ORDER BY t.id DESC");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    /**
     * Fetching all user tasks
     * @param String $user_id id of the user
     * -Sharmeen
     */
    public function getAllOtherUsersTasks($user_id)
    {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_tasks ut WHERE t.id = ut.task_id AND t.status = 0 ORDER BY t.tot_likes DESC LIMIT 50");
        // SELECT * FROM table ORDER BY id DESC LIMIT 50
        // $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    /**
     * Fetching all latest user tasks
     * @param String $user_id id of the user
     * -Sharmeen
     */
    public function getAllOtherUsersTasksLatest($user_id)
    {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_tasks ut WHERE t.id = ut.task_id AND t.status = 0 ORDER BY id DESC LIMIT 50");
        // SELECT * FROM table ORDER BY id DESC LIMIT 50
        // $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }


    /**
     * Updating task
     * @param String $task_id id of the task
     * @param String $task task text
     * @param String $status task status
     */
    /* public function updateTask($user_id, $task_id, $status) {
         $stmt = $this->conn->prepare("UPDATE tasks t, user_tasks ut set t.status = ? WHERE t.id = ? AND t.id = ut.task_id AND ut.user_id = ?");
         $stmt->bind_param("iii", $status, $task_id, $user_id);
         $stmt->execute();
         $num_affected_rows = $stmt->affected_rows;
         $stmt->close();
         return $num_affected_rows > 0;
     }*/

    public function updateTask($user_id, $task_id, $task, $taskdescription, $image_path, $year, $technique, $dimension, $tags, $createdbyme, $wishtosell, $curr_code, $price, $contact_info)

    {

        $priceapp=$curr_code .' '. $price;
        $stmt = $this->conn->prepare("UPDATE tasks t, user_tasks ut SET t.task = ?, t.task_description = ?, t.image_path = ?, t.year = ?, t.technique = ?, t.dimension = ?, t.tags = ?, t.createdbyme = ?, t.wishtosell = ?, t.curr_code=?, t.price_new = ?,t.price = ?, t.contact_info = ?
										WHERE t.id = ? AND t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("ssssssssssissii", $task, $taskdescription, $image_path, $year, $technique, $dimension, $tags, $createdbyme, $wishtosell, $curr_code, $price,$priceapp, $contact_info, $task_id, $user_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    /**
     * Deleting a task
     * @param String $task_id id of the task to delete
     */
    public function deleteTask($user_id, $task_id)
    {
        //-Sharmeen
        // Also delete task from other tables such as "user_buyTasks", "user_sellTasks", "user_likeTasks"
        $stmt = $this->conn->prepare("DELETE t FROM tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        //("DELETE t FROM tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?);


        //user_buyTasks bt WHERE bt.task_id = t.id AND bt.user_id = ut.user_id,
        //user_sellTasks st WHERE st.task_id = t.id AND st.user_id = ut.user_id,
        //user_likeTasks lt WHERE lt.task_id = t.id AND lt.user_id = ut.user_id");
        $stmt->bind_param("ii", $task_id, $user_id);
        // $stmt->execute();
        //$num_affected_rows = $stmt->affected_rows;
        //$stmt->close();
        //return $num_affected_rows > 0;

        $result = $stmt->execute();
        $stmt->close();
        //return $result;

        if ($result) {
            $new_res = $this->deleteTask1($user_id, $task_id);
            if ($new_res)
                return $new_res;
            else
                return NULL;
        } else {
            return NULL;
        }
    }

    public function deleteTask1($user_id, $task_id)
    {
        //-Sharmeen
        // Also delete task from other tables such as "user_buyTasks", "user_sellTasks", "user_likeTasks"
        $stmt = $this->conn->prepare("DELETE ut FROM user_tasks ut WHERE ut.task_id = ? AND ut.user_id = ?");
        //("DELETE t FROM tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?);


        //user_buyTasks bt WHERE bt.task_id = t.id AND bt.user_id = ut.user_id,
        //user_sellTasks st WHERE st.task_id = t.id AND st.user_id = ut.user_id,
        //user_likeTasks lt WHERE lt.task_id = t.id AND lt.user_id = ut.user_id");
        $stmt->bind_param("ii", $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        header('location:'.$webroot.'/auth/user/profile.php');
        return $num_affected_rows > 0;
    }

    /* ------------- `user_tasks` table method ------------------ */

    /**
     * Function to assign a task to user
     * @param String $user_id id of the user
     * @param String $task_id id of the task
     */
    /* public function createUserTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("INSERT INTO user_tasks(user_id, task_id) values(?, ?)"); 
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result; 
    }*/

    public function createUserTask($user_id, $task_id, $wishtosell, $price, $contact_info)
    {
        $stmt = $this->conn->prepare("INSERT INTO user_tasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();
        $stmt->close();
        echo "User Task Table updated <br>";

        echo "wishto sell<br>" . $wishtosell;

        //-Sharmeen
        if ($wishtosell == "1") {
            echo "wish to sell is active <br>";

            if ($result) {
                // task row created
                //now assign task for wishtosell to user in "user_sellTasks" table
                echo "Going to create User sell task";

                $res = $this->createUserSellTask($user_id, $task_id, $wishtosell, $price, $contact_info);


                if ($res) {
                    // wishtosell added successfully
                    echo "<br>wish to sell added";
                    return true;
                    header('location:'.$webroot.'/auth/user/profile.php');
                } else {
                    // task failed to create
                    return true;
                }
            } else {
                // task failed to create
                return true;
            }
        } else {
            return true;
            header('location:'.$webroot.'/auth/user/profile.php');
        }

        // return $result;
    }

    //Sharmeen

    public function updateTaskImage($task_id, $imageurl)
    {
        $stmt = $this->conn->prepare("UPDATE tasks t set t.image_path = ? WHERE t.id = ? ");
        $stmt->bind_param("si", $imageurl, $task_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function updateProfilePic($user_id, $imageurl)
    {
        $stmt = $this->conn->prepare("UPDATE users u set u.profilepic_path = ? WHERE u.id = ? ");
        $stmt->bind_param("si", $imageurl, $user_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result;

        /* $stmt->execute();
         $num_affected_rows = $stmt->affected_rows;
         $stmt->close();
         return $num_affected_rows > 0;*/

    }


    public function createUserLikeTask($user_id, $task_id, $like)
    {

        //check if this entry already exists in "user_likeTasks" table
        $stmt = $this->conn->prepare("SELECT id from user_likeTasks ul WHERE ul.user_id = ? AND ul.task_id = ?");
        $stmt->bind_param("ii", $user_id, $task_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();

        if ($num_rows > 0) {
            //update "user_likeTasks" table
            $res = $this->updateUserLikeTask($user_id, $task_id, $like);
            if ($res) {
                return $res;
            } else {
                // task failed to create
                return NULL;
            }
        } else {
            //create "user_likeTasks" table
            $stmt_new = $this->conn->prepare("INSERT INTO user_likeTasks (user_id, task_id, like_val) VALUES(?, ?, ?)");
            $stmt_new->bind_param("iis", $user_id, $task_id, $like);
            $result_new = $stmt_new->execute();
            $stmt_new->close();

            if ($result_new) {
                $new_res = $this->updateTaskLikes($user_id, $task_id, $like);
                if ($new_res)
                    return $new_res;
                else
                    return NULL;
            } else {
                return NULL;
            }
            //return $result;
        }
        //return $num_rows > 0;
    }

    public function updateUserLikeTask($user_id, $task_id, $like)
    {
        $stmt = $this->conn->prepare("UPDATE user_likeTasks u set u.like_val = ? WHERE u.user_id = ? AND u.task_id = ?");
        $stmt->bind_param("sii", $like, $user_id, $task_id);
        /* $stmt->execute();
         $num_affected_rows = $stmt->affected_rows;
         $stmt->close();*/
        $result = $stmt->execute();
        $stmt->close();
        // return $result;

        //if($num_affected_rows > 0){
        if ($result) {
            $new_res = $this->updateTaskLikes($user_id, $task_id, $like);
            if ($new_res)
                return $new_res;
            else
                return NULL;
        } else {
            return NULL;
        }

//	   return $num_affected_rows > 0; 
    }

    /*
        */
    public function createUserBuyTask($user_id, $task_id, $wishtobuy)
    {
        //check if this entry already exists in "user_buyTasks" table
        $stmt = $this->conn->prepare("SELECT id from user_buyTasks ub WHERE ub.user_id = ? AND ub.task_id = ?");
        $stmt->bind_param("ii", $user_id, $task_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();

        if ($num_rows > 0) {
            //update "user_buyTasks" table
            $res = $this->updateUserBuyTask($user_id, $task_id, $wishtobuy);
            if ($res) {
                return $res;
            } else {
                // task failed to create 
                return NULL;
            }
        } else {
            //create "user_buyTasks" table
            $stmt = $this->conn->prepare("INSERT INTO user_buyTasks(user_id, task_id, wishtobuy) values(?, ?, ?)");
            $stmt->bind_param("iis", $user_id, $task_id, $wishtobuy);
            $result = $stmt->execute();
            $stmt->close();

            if ($result) {
                //now assign task for like to user in "user_buyTasks" table
                $like = "1";                                                        //## -Sharmeen
                $res = $this->createUserLikeTask($user_id, $task_id, $like);

                if ($res) {
                    // like added successfully alongwith wishtobuy !
                    return $result;
                } else {
                    // task failed to create
                    return NULL;
                }
            } else {
                // task failed to create
                return NULL;
            }
            //return $result;
        }

        //return $num_rows > 0;
    }

    public function updateUserBuyTask($user_id, $task_id, $wishtobuy)
    {
        $stmt = $this->conn->prepare("UPDATE user_buyTasks u set u.wishtobuy = ? WHERE u.user_id = ? AND u.task_id = ?");
        $stmt->bind_param("sii", $wishtobuy, $user_id, $task_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    /**
     * Updating task tot_likes
     * @param String $task_id id of the task
     * @param String $tot_likes task likes total
     */
    public function updateTaskLikes($user_id, $task_id, $like)
    {
        if ($like == "1") {
            //Add +1 to Likes
            $stmt = $this->conn->prepare("UPDATE tasks t SET t.tot_likes = (t.tot_likes + 1) WHERE t.id = ?");
            $stmt->bind_param("i", $task_id);
            $stmt->execute();
            $num_affected_rows = $stmt->affected_rows;
            $stmt->close();
            return $num_affected_rows > 0;
        } else {
            //Add -1 to Likes
            $stmt = $this->conn->prepare("UPDATE tasks t SET t.tot_likes = (t.tot_likes - 1) WHERE t.id = ?");
            $stmt->bind_param("i", $task_id);
            $stmt->execute();
            $num_affected_rows = $stmt->affected_rows;
            $stmt->close();
            return $num_affected_rows > 0;

        }
    }

    public function hasUserWishedToBuy($user_id, $task_id)
    {
        $stmt = $this->conn->prepare("SELECT id from user_buyTasks ub WHERE ub.user_id = ? AND ub.task_id = ? AND ub.wishtobuy = '1'");
        $stmt->bind_param("ii", $user_id, $task_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();

        return $num_rows > 0;
    }

    public function hasUserLiked($user_id, $task_id)
    {
        $stmt = $this->conn->prepare("SELECT id FROM user_likeTasks ul WHERE ul.user_id = ? AND ul.task_id = ? AND ul.like_val = '1'");
        $stmt->bind_param("ii", $user_id, $task_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();

        /* return $num_rows > 0;*/
        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Fetching user by task id
     * @param String $task_id
     */
    public function getUserByTaskId($task_id)
    {
        $stmt = $this->conn->prepare("SELECT u.* FROM users u, user_tasks ut WHERE ut.task_id = ? AND u.id = ut.user_id");
        $stmt->bind_param("i", $task_id);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user['firstname'];
        } else {
            return NULL;
        }
    }

    /**
     * Fetching all user liked tasks
     * @param String $user_id id of the user
     */
    public function getAllUserLikedTasks($user_id)
    {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_likeTasks ult WHERE t.id = ult.task_id AND ult.user_id = ? AND ult.like_val = '1' AND t.status = 0 ORDER BY t.id DESC");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    /**
     * Fetching all user liked tasks count
     * @param String $user_id id of the user
     */
    public function getTotLikedArts($user_id)
    {
        //SELECT COUNT(*) AS total FROM user_tasks ut  WHERE ut.user_id = ?
        $stmt = $this->conn->prepare("SELECT COUNT(*) AS total FROM user_likeTasks ult WHERE ult.user_id = ? AND ult.like_val = '1'");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $values = $stmt->get_result()->fetch_assoc();
        $stmt->close();

        $stmt = $this->conn->prepare("UPDATE users u set u.totLiked = ? WHERE u.id = ? ");
        $stmt->bind_param("si", $values['total'], $user_id);
        $result = $stmt->execute();
        $stmt->close();

        return $values;
    }



    /*Fetching total likes of art
     * @param String $art_id id of the art
    */
    public function countlikesArt($art_id)
    {
        $stmt = $this->conn->prepare("SELECT tot_likes AS total FROM tasks where id=?");
        $stmt->bind_param("i", $art_id);
        $stmt->execute();
        $values = $stmt->get_result()->fetch_assoc();
        $stmt->close();
        echo $values['total'];
    }

    /**
     * Fetching total likes received
     * @param String $user_id id of the user
     */
    public function getTotLikes($user_id)
    {
        $stmt = $this->conn->prepare("SELECT SUM(tot_likes) AS total FROM tasks t, user_tasks ut  WHERE ut.user_id = ? AND t.id = ut.task_id AND t.status = 0");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $values = $stmt->get_result()->fetch_assoc();
        $stmt->close();

        $stmt = $this->conn->prepare("UPDATE users u set u.totLikes = ? WHERE u.id = ? ");
        $stmt->bind_param("si", $values['total'], $user_id);
        $result = $stmt->execute();
        $stmt->close();

        return $values['total'];

    }

    /**
     * Fetching total uploads
     * @param String $user_id id of the user
     */
    public function getTotUploads($user_id)
    {
        $stmt = $this->conn->prepare("SELECT COUNT(*) AS total FROM user_tasks ut, tasks t  WHERE ut.user_id = ? AND t.id = ut.task_id AND t.status = 0");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $values = $stmt->get_result()->fetch_assoc();
        $stmt->close();

        $stmt = $this->conn->prepare("UPDATE users u set u.totUploaded = ? WHERE u.id = ? ");
        $stmt->bind_param("si", $values['total'], $user_id);
        $result = $stmt->execute();
        $stmt->close();

        return $values['total'];
    }

    /**
     * Fetching total arts created by user
     * @param String $user_id id of the user
     */
    public function getTotCreated($user_id)
    {
        $stmt = $this->conn->prepare("SELECT COUNT(*) AS total FROM tasks t, user_tasks ut  WHERE ut.user_id = ? AND t.id = ut.task_id AND t.createdbyme = '1' AND t.status = 0");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $values = $stmt->get_result()->fetch_assoc();
        $stmt->close();

        $stmt = $this->conn->prepare("UPDATE users u set u.totCreated = ? WHERE u.id = ? ");
        $stmt->bind_param("si", $values['total'], $user_id);
        $result = $stmt->execute();
        $stmt->close();

        return $values['total'];

    }

    /**
     * Fetching fb_id by user_id
     * @param String $user_id User Facebook id
     */
    public function getFbId($user_id)
    {
        $stmt = $this->conn->prepare("SELECT fb_id AS fbid FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $values = $stmt->get_result()->fetch_assoc();
        $stmt->close();

        return $values;


        /* if ($stmt->execute()) {
             $fb_id = $stmt->get_result()->fetch_assoc();
             $stmt->close();
             return $fb_id;
         } else {
             return NULL;
         }*/
    }

    public function createUserSellTask($user_id, $task_id, $wishtosell, $price, $contact_info)
    {

        /*//Create new
        $stmt = $this->conn->prepare("INSERT INTO user_sellTasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result; */

        //check if this entry already exists in "user_sellTasks" table
        //echo "call";
       // $sql = "SELECT id from user_sellTasks ub WHERE ub.user_id = $user_id AND ub.task_id = $task_id";
       // echo "create user sell task query" . $sql;

        $stmt = $this->conn->prepare("SELECT id from user_sellTasks ub WHERE ub.user_id = ? AND ub.task_id = ?");
        $stmt->bind_param("ii", $user_id, $task_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
       // echo "Value usersell task fetched<br>";


        if ($num_rows > 0) {
         //   echo "sell taskid is already found";
            //update price in tasks table
            $res = $this->updateUserSellTask($user_id, $task_id, $wishtosell, $price, $contact_info);
            if ($res) {
                return $res;
            } else {
                // task failed to create
                return NULL;
            }
        } else {
            //create "user_sellTasks" table
            //$sql = "INSERT INTO user_sellTasks(user_id, task_id) values($user_id, $task_id)";
           // echo "insert userselltask " . $sql;
            $stmt = $this->conn->prepare("INSERT INTO user_sellTasks(user_id, task_id) values(?, ?)");
            $stmt->bind_param("ii", $user_id, $task_id);
            $result = $stmt->execute();
            $stmt->close();
            echo "User sell task created <br>";

            // return $result;
            if ($result) {
                //check if task id entry already exists in "tasks" table
                $stmt = $this->conn->prepare("SELECT id from tasks t WHERE t.id = ?");
                $stmt->bind_param("i", $task_id);
                $stmt->execute();
                $stmt->store_result();
                $num_rows = $stmt->num_rows;
                $stmt->close();

                if ($num_rows > 0) {
                    //update price in tasks table
                    $res = $this->updateUserSellTask($user_id, $task_id, $wishtosell, $price, $contact_info);
                    if ($res) {
                        return $result;
                    } else {
                        // task failed to create
                        return NULL;
                    }
                } else {
                    return $result;
                }
            } else {
                return NULL;
            }
        }
    }

    public function updateUserSellTask($user_id, $task_id, $wishtosell, $price, $contact_info)
    {
        $stmt = $this->conn->prepare("UPDATE tasks t SET t.price = ?, t.wishtosell = ?, t.contact_info = ? WHERE t.id = ?");
        $stmt->bind_param("sssi", $price, $wishtosell, $contact_info, $task_id);
        //$stmt->execute();
        //$num_affected_rows = $stmt->affected_rows;  
        $result = $stmt->execute();
        $stmt->close();
        //return $num_affected_rows > 0;
        return $result;
    }

    /*
    Verify OTP
    */

    public function updateUserMobile($user_id, $fb_id, $mobile)
    {
        $stmt = $this->conn->prepare("UPDATE users u SET u.mobile_num = ? WHERE u.fb_id = ?");
        $stmt->bind_param("ss", $mobile, $fb_id);
        //$stmt->execute();
        //$num_affected_rows = $stmt->affected_rows;  
        $result = $stmt->execute();
        $stmt->close();
        //return $num_affected_rows > 0;
        return $result;
    }


    /*NEW IMAGE TASK ADDED IN DB*/
    public function insertTaskImage($imageurl)
    {
        $stmt = $this->conn->prepare("INSERT INTO `tasks`(`image_path`) VALUE (?)");
        $stmt->bind_param("s", $imageurl);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }


    public function verifyOTP($user_id, $otp)
    {
        $stmt = $this->conn->prepare("SELECT id from users u WHERE u.id = ? AND u.otp = ?");
        $stmt->bind_param("is", $user_id, $otp);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        //return $num_rows > 0;

        if ($num_rows > 0) {
            // OTP verified
            // Now update user table as verified
            $res = $this->update_verified_users($user_id);
            if ($res) {
                return TRUE;
            } else {
                return FALSE;
            }

        } else {
            return FALSE;
        }
    }

    private function update_verified_users($user_id)
    {
        $stmt = $this->conn->prepare("UPDATE users u SET u.status = 1 WHERE u.id = ?");
        $stmt->bind_param("i", $user_id);
        //$stmt->execute();
        //$num_affected_rows = $stmt->affected_rows;  
        $result = $stmt->execute();
        $stmt->close();
        //return $num_affected_rows > 0;
        return $result;

    }


    /* Check if User Mobile is verified */
    public function isUserVerified($user_id, $mobile)
    {
        $stmt = $this->conn->prepare("SELECT id from users u WHERE u.id = ? AND u.mobile = ? AND u.status = 1");
        $stmt->bind_param("is", $user_id, $mobile);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        //return $num_rows > 0; 

        if ($num_rows > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function updateUserLocation($user_id, $location)
    {
        $stmt = $this->conn->prepare("UPDATE users u set u.location = ? WHERE u.id = ? ");
        $stmt->bind_param("si", $location, $user_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result;

        /*	$stmt->execute();
            $num_affected_rows = $stmt->affected_rows;
            $stmt->close();
            return $num_affected_rows > 0; */
    }

    public function updateUserName($user_id, $name)
    {
        $stmt = $this->conn->prepare("UPDATE users u set u.name = ? WHERE u.id = ? ");
        $stmt->bind_param("si", $name, $user_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    public function updateUserFirstName($user_id, $firstname)
    {
        $stmt = $this->conn->prepare("UPDATE users u set u.firstname = ? WHERE u.id = ? ");
        $stmt->bind_param("si", $firstname, $user_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    public function update_contact($user_id, $user_contact_info)
    {
        $stmt = $this->conn->prepare("UPDATE users u set u.user_contact_info = ? WHERE u.id = ? ");
        $stmt->bind_param("si", $user_contact_info, $user_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result;

    }

    public function updateUserBio($user_id, $bio)
    {
        $stmt = $this->conn->prepare("UPDATE users u set u.bio = ? WHERE u.id = ? ");
        $stmt->bind_param("si", $bio, $user_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result;

    }


    public function reportTask($user_id, $task_id)
    {
        $stmt = $this->conn->prepare("INSERT INTO user_reportTasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();
        $stmt->close();
        // return $result;

        if ($result) {
            $stmt = $this->conn->prepare("UPDATE tasks t SET t.status = 3 WHERE t.id = ?");
            $stmt->bind_param("i", $task_id);
            $res = $stmt->execute();
            $stmt->close();
            return $res;
        } else {
            return FALSE;
        }
    }

    public function DeleteTaskbyStatus($user_id, $task_id)
    {
        $stmt = $this->conn->prepare("INSERT INTO user_reportTasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();
        $stmt->close();
        // return $result;

        if ($result) {
            $stmt = $this->conn->prepare("UPDATE tasks t SET t.status = 4 WHERE t.id = ?");
            $stmt->bind_param("i", $task_id);
            $res = $stmt->execute();
            $stmt->close();
            header('location:'.$webroot.'/auth/user/profile.php');
            return $res;
        } else {
            return FALSE;
        }
    }

    //Search Algo

    /**
     * Fetching all user tasks that match search string
     * -Sharmeen
     */
    public function getSearch($search_val)
    {
        //SELECT * FROM 'mytable' WHERE INSTR('mycol', 'abc') > 0
        //INSTR('task', '$search_val') > 0
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_tasks ut WHERE t.id = ut.task_id AND t.status = 0 AND (INSTR(`task`, '$search_val') > 0 OR 
		INSTR(`task_description`, '$search_val') > 0 OR INSTR(`technique`, '$search_val') > 0 ) ORDER BY t.tot_likes DESC LIMIT 50");
        // $stmt->bind_param("s", $search_val);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    //Search Algo

    /**
     * Fetching all user tasks that match search string
     * -Sharmeen
     */
    public function getSearchArtist($search_val)
    {
        //SELECT * FROM 'mytable' WHERE INSTR('mycol', 'abc') > 0
        //INSTR('task', '$search_val') > 0
        $stmt = $this->conn->prepare("SELECT u.* FROM users u WHERE INSTR(`firstname`, '$search_val') > 0 ORDER BY u.totLikes DESC LIMIT 50");
        // $stmt->bind_param("s", $search_val);
        $stmt->execute();
        $users = $stmt->get_result();
        $stmt->close();
        return $users;
    }

    public function getuserEmailbyId($id){

        $sql="Select email from users where id=$id ";
        $result=mysqli_query($this->conn,$sql);
        $result=mysqli_fetch_assoc($result);
        return $result['email'];
    }

    public function UnReportTaskById($id){

        $sql="UPDATE `tasks` SET `status`=0 WHERE id=$id";
        var_dump($sql);
        $result=mysqli_query($this->conn,$sql);
        if($result){
            return "Unreported Succefully";
        }else{
            return "Something Went Wrong";
        }

    }

}


?>


