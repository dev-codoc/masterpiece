<?php

//profilepicUpload.php
//-Sharmeen Sahibole
//-Created on 16-04-16

require_once'../.././include/DbHandler.php';
//require_once dirname(__FILE__) . '/DbHandler.php';

function compress_image($source_url, $destination_url, $quality) {
$info = getimagesize($source_url);

	//Get sizes
list($width,$height)=getimagesize($source_url);
if($width > $height)
	$newwidth=200;
else
	$newwidth= 200;

$newheight=($height/$width)*$newwidth;
//$newwidth = 256;
//$newheight = 256;  

// Load
$thumb = imagecreatetruecolor($newwidth, $newheight);
//$source = imagecreatefromjpeg($source_url);	 


	if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
	elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
	elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

	imagecopyresized($thumb, $image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

	//save it
	imagejpeg($thumb, $destination_url, $quality);

	imagedestroy($thumb);
	imagedestroy($image);

	//return destination file url
	return $destination_url;
}



// Path to move uploaded files
$target_path = "uploads/";
$target_path_thumbnails = "uploads_thumbnails/";

// array for final json response
$response = array();

// getting server ip address
//$server_ip = gethostbyname(gethostname()); 
$server_ip = 'localhost/Masterpiecelive/auth/Artican/v1';

//var_dump($server_ip);
//die();

// final file url that is being uploaded
$file_upload_url = 'http://' . $server_ip . '/' . 'ProfilePicUpload' . '/' . $target_path_thumbnails; //$target_path;

//var_dump($file_upload_url);

if (isset($_FILES['image']['name'])) {
    $target_path = $target_path . basename($_FILES['image']['name']);
	$target_path_thumbnails = $target_path_thumbnails . basename($_FILES['image']['name']);



    // reading other post parameters
    $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
   // $task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';


    //$response['file_name'] = basename($_FILES['image']['name']);
    $response['user_id'] = $user_id;
  //  $response['task_id'] = $task_id;

	$db = new DbHandler();

    try {
        // Throws exception incase file is not being moved
        if (!move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
            // make error flag true
            $response['error'] = true;
            $response['message'] = 'Could not upload the photo!';
			//$db->deleteTask($user_id, $task_id);
        } else{


		//##start

	$source_photo = $target_path; //$thumb; //$target_path; //$target_path . basename($_FILES['image']['name']);
	$dest_photo = $target_path_thumbnails; //$target_path_thumbnails; //$target_path . basename($_FILES['image']['name']);

	$target_path = compress_image($source_photo, $dest_photo, 80);
	var_dump($target_path);
	//die();
	//##end


        // File successfully uploaded
		$imageurl = $file_upload_url . basename($_FILES['image']['name']);
		//$result = $db->updateTaskImage($task_id, $imageurl);
		$result = $db->updateProfilePic($user_id, $imageurl);

		if ($result) {
                // task updated successfully
               // $response['message'] = 'File uploaded successfully!';
				$response['message'] = 'Your Profile Pic is uploaded successfully!';
        $response['error'] = false;
        $response['file_path'] = $file_upload_url . basename($_FILES['image']['name']);
		$response['user_id'] = $user_id;
		//$response['task_id'] = $task_id;
            } else {
                // task failed to update
                $response['error'] = true;
                $response['message'] = "Image failed to upload. Please try again!";
				//$db->deleteTask($user_id, $task_id);
            }
           // echoRespnse(200, $response);
		}

    } catch (Exception $e) {
        // Exception occurred. Make error flag true
        $response['error'] = true;
        $response['message'] = $e->getMessage();
		//$db->deleteTask($user_id, $task_id);
    }
} else {
    // File parameter is missing
    $response['error'] = true;
    $response['message'] = 'No image received.';
	//$db->deleteTask($user_id, $task_id);
}

// Echo final json response to client
//echo json_encode($response);

header("location:http://localhost/masterpiecelive/auth/user/profile.php");
?>