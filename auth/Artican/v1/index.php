<?php

//use \Psr\Http\Message\ServerRequestInterface as Request;
//use \Psr\Http\Message\ResponseInterface as Response;
/*
 * Note: Here we are including required libraries and other helper functions.  
 * Sharmeen Sahibole
 */


require_once'./include/DbHandler.php';
require_once './include/PassHash.php';
require '.././libs/Slim/Slim.php';
//require '.././libs/Slim/vendor/autoload.php'; 
 
\Slim\Slim::registerAutoloader(); 

use \Slim\Slim AS Slim;
//$app = new Slim();
 
//$app = new \Slim\Slim();   

//$app = new \Slim\App;

//$app = new \Slim\App();

$app = new \Slim\Slim();

 
// User id from db - Global Variable
$user_id = NULL;

//Print something on webpage

 
/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty 
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance(); 
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(200, $response);
        $app->stop();
    }
}
 
/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}
 
/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
}
 
//$app->run();

//##########################################################################################
/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */

$app->post('/register', function() use ($app) { 
            // check for required params
            verifyRequiredParams(array('name', 'firstname','fb_id','password')); 
 
            $response = array();
 
            // reading post params
            $name = $app->request->post('name');
			$firstname = $app->request->post('firstname');
            $email = $app->request->post('email');
            $password = $app->request->post('password');
			$fb_id = $app->request->post('fb_id');
 
            // validating email address
           // validateEmail($email);
 
            $db = new DbHandler();
            $res = $db->createUser($name, $firstname, $email, $password, $fb_id);
			
			if($fb_id == "nil")
			{
			 // get the user by email 
			 $user = $db->getUserByEmail($email);
			 $response['apiKey'] = $user['api_key']; 
					$response['status'] = $user['status']; 
					$response['otp'] = $user['otp'];
					$response['userId'] = $user['id'];	 
			} else {
                $user = $db->getUserByFbId($fb_id); 
				$response['apiKey'] = $user['api_key']; 
					$response['status'] = $user['status']; 
					$response['otp'] = $user['otp'];
					$response['userId'] = $user['id'];	 
			}
 
            if ($res == USER_CREATED_SUCCESSFULLY) {
					$response["error"] = false;
					$response["message"] = "You are successfully registered";
					//$response['apiKey'] = $user['api_key']; 
					//$response['status'] = $user['status'];  
					//$response['otp'] = $user['otp'];
					//$response['userId'] = $user['id'];	 				
					echoRespnse(201,$response);				           
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
                echoRespnse(200, $response);
            } else if ($res == USER_ALREADY_EXISTED) {
                $response["error"] = true;
                $response["message"] = "Sorry, this email already exists";
                echoRespnse(200, $response); 
            }
        });
		
		/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/login', function() use ($app) {
			// check for required params
            verifyRequiredParams(array('email','fb_id','password'));
 
            // reading post params
            $email = $app->request()->post('email');
            $password = $app->request()->post('password');
			$fb_id = $app->request->post('fb_id');
			$location = $app->request->post('location');
			$name = $app->request->post('name');
			$first_name = $app->request->post('firstname'); 
            $response = array();
 
            $db = new DbHandler(); 
            // check for correct email and password
            if ($db->checkLogin($email, $password)) { 
                // get the user by email
                //$user = $db->getUserByFbId($fb_id);  
			if($fb_id == "nil")
			{
			 // get the user by email 
			 $user = $db->getUserByEmail($email);
			} else {
                $user = $db->getUserByFbId($fb_id);
			}				
				$user_id = $user['id'];
				$tot_likes = $db->getTotLikes($user_id);
				$tot_uploads = $db->getTotUploads($user_id);
				$tot_created = $db->getTotCreated($user_id);	
				$db->updateUserLocation($user_id,$location );
				$db->updateUserName($user_id,$name );
				$db->updateUserFirstName($user_id,$first_name);	 	
				
 
                if ($user != NULL) {
                    $response["error"] = false;
                    $response['name'] = $user['name'];
					$response['firstname'] = $user['firstname'];
                    $response['email'] = $user['email'];
                    $response['apiKey'] = $user['api_key'];
					$response['status'] = $user['status']; 
					$response['otp'] = $user['otp'];
                    $response['createdAt'] = $user['created_at'];
					$response['total_likes'] = $tot_likes['total'];
					$response['total_uploads'] = $tot_uploads['total'];
					$response['total_created'] = $tot_created['total'];  
					$response['userId'] = $user['id'];
					$response['userBio'] = $user['bio'];
				
                } else {
                    // unknown error occurred 
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
            }
 
            echoRespnse(200, $response);
        });
		
		/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();
 
    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();
 
        // get the api key
        $api_key = $headers['Authorization'];
        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user = $db->getUserId($api_key);
            if ($user != NULL)
                $user_id = $user["id"];
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Creating new task in db
 * method POST
 * params - name
 * url - /tasks/
 */
$app->post('/tasks_new', 'authenticate', function() use ($app) { 
	
	 // check for required params
            verifyRequiredParams(array('task', 'technique', 'dimension')); 
 
            $response = array();
            $task = $app->request->post('task'); 
			$taskdescription = $app->request->post('taskdescription');
			$lat = $app->request->post('lat');
			$lng = $app->request->post('lng'); 
			$year = $app->request->post('year');
			$technique = $app->request->post('technique');
			$dimension = $app->request->post('dimension');
			$createdbyme = $app->request->post('createdbyme');
			$wishtosell = $app->request->post('wishtosell');
			$price = $app->request->post('price');
			$contact_info = $app->request->post('contact_info');	
if($contact_info == NULL)
		$contact_info = "na";
 
            global $user_id;
            $db = new DbHandler();
 
            // creating new task 
            $task_id = $db->createTask($user_id, $task, $taskdescription, $lat, $lng, $year, $technique, $dimension, $createdbyme, $wishtosell, $price, $contact_info);  
 
            if ($task_id != NULL) {  
				
                $response["error"] = false; 
                $response["message"] = "Art uploaded successfully!";
                $response["task_id"] = $task_id; 
				$response["user_id"] = $user_id;  
            } else {
                $response["error"] = true;
                $response["message"] = "Failed to upload art. Please try again"; 
            }
            echoRespnse(201, $response);
        });
		
		/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/tasks', 'authenticate', function() {
            global $user_id;
            $response = array(); 
            $db = new DbHandler();
 
            // fetching all user tasks
            $result = $db->getAllUserTasks($user_id);
			
			$tot_likes = $db->getTotLikes($user_id);
			$tot_uploads = $db->getTotUploads($user_id);
			$tot_created = $db->getTotCreated($user_id); 
			$tot_liked = $db->getTotLikedArts($user_id); 
			$user_bio = $db->getUserBiobyId($user_id);
 
            $response["error"] = false;
            $response["tasks"] = array(); 
			$response['total_likes'] = $tot_likes['total']; 
			$response['total_uploads'] = $tot_uploads['total'];
			$response['total_created'] = $tot_created['total']; 
			$response['total_liked'] = $tot_liked['total'];
			$response['userBio'] = $user_bio['bio']; 
 
            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["id"] = $task["id"];
                $tmp["task"] = $task["task"]; 
				$tmp["taskdescription"] = $task["task_description"];   
				$tmp["year"] = $task["year"];	 
				$tmp["technique"] = $task["technique"];	
				$tmp["dimension"] = $task["dimension"];	
				$tmp["createdbyme"] = $task["createdbyme"];	
				$tmp["wishtosell"] = $task["wishtosell"];
				$tmp["tot_likes"] = $task["tot_likes"];	 
				$tmp["imageurl"] = $task["image_path"];	
                $tmp["status"] = $task["status"];
                $tmp["createdAt"] = $task["created_at"]; 
				//if($db->hasUserLiked($user_id, $task["id"])) $tmp["hasLiked"] = '1';
				//else $tmp["hasLiked"] = '0';
				$tmp["hasLiked"] = $db->hasUserLiked($user_id, $task["id"]); //## -Sharmeen 
				$user = $db->getUserByTaskId($task["id"]);
				$tmp["user_fbid"] = $user['fb_id'];
				$tmp["user_name"] = $user['firstname'];
				$tmp["user_id"] = $user['id']; 
				$tmp["price"] = $task["price"];	
				$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user_id, $task["id"]); //## -Sharmeen 
				$tmp["userBio"] = $user['bio'];
				
                array_push($response["tasks"], $tmp);
            }
 
            echoRespnse(200, $response);
        });
		
		
		/**
 * Listing all tasks of ALL Other users
 * method GET
 * url /othertasks          
 */
$app->get('/othertasks', 'authenticate', function() {
            global $user_id;
            $response = array(); 
            $db = new DbHandler();
 
            // fetching all user tasks
            $result = $db->getAllOtherUsersTasks($user_id);
 
            $response["error"] = false;
            $response["tasks"] = array();
 
            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {				
                $tmp = array();
                $tmp["id"] = $task["id"];
                $tmp["task"] = $task["task"];
				$tmp["taskdescription"] = $task["task_description"]; 
				$tmp["year"] = $task["year"];	 
				$tmp["technique"] = $task["technique"];	
				$tmp["dimension"] = $task["dimension"];	
				$tmp["createdbyme"] = $task["createdbyme"];	
				$tmp["wishtosell"] = $task["wishtosell"];	
				$tmp["tot_likes"] = $task["tot_likes"];	
				$tmp["imageurl"] = $task["image_path"];				
                $tmp["status"] = $task["status"];
                $tmp["createdAt"] = $task["created_at"];  
				$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user_id, $task["id"]); //## -Sharmeen  
				//if($db->hasUserLiked($user_id, $task["id"])) $tmp["hasLiked"] = '1';
				//else $tmp["hasLiked"] = '0';
				$tmp["hasLiked"] = $db->hasUserLiked($user_id, $task["id"]); //## -Sharmeen  
				$user = $db->getUserByTaskId($task["id"]);
				$tmp["user_fbid"] = $user['fb_id'];
				$tmp["user_name"] = $user['firstname'];
				$tmp["user_id"] = $user['id']; 
				$tmp["price"] = $task["price"];
				$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user_id, $task["id"]); //## -Sharmeen 
				$tmp["userBio"] = $user['bio'];
				
                array_push($response["tasks"], $tmp); 
            }
 
            echoRespnse(200, $response);
        });
		
		
		
		/**
 * Listing single task of particual user
 * method GET
 * url /tasks/:id
 * Will return 404 if the task doesn't belongs to user
 */
$app->get('/tasks/:id', 'authenticate', function($task_id) {
            global $user_id;
            $response = array();
            $db = new DbHandler();
 
            // fetch task
            $result = $db->getTask($task_id, $user_id); 
 
            if ($result != NULL) {
                $response["error"] = false;
                $response["id"] = $result["id"];
                $tmp["task"] = $task["task"];
				$tmp["taskdescription"] = $task["task_description"]; 
				$tmp["year"] = $task["year"];	 
				$tmp["technique"] = $task["technique"];	
				$tmp["dimension"] = $task["dimension"];	
				$tmp["createdbyme"] = $task["createdbyme"];	
				$tmp["wishtosell"] = $task["wishtosell"];	
				$tmp["tot_likes"] = $task["tot_likes"];	
				$response["imageurl"] = $result["image_path"];
                $response["status"] = $result["status"];
                $response["createdAt"] = $result["created_at"]; 
				$tmp["price"] = $task["price"];
				$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user_id, $task["id"]); //## -Sharmeen 
				
                echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested art doesn't exists";  
                echoRespnse(404, $response);
            }
        });
		
		/**
 * Updating existing task
 * method PUT
 * params task, status
 * url - /tasks/:id
 * /updatetask
 */
$app->post('/updatetask', 'authenticate', function() use($app) {
            // check for required params 
            verifyRequiredParams(array('task_id'));    
 
            global $user_id; 
			$task_id = 	$app->request->put('task_id');	 
            $task = $app->request->put('task');
            $taskdescription = $app->request->put('taskdescription');
			$year = $app->request->put('year');
			$technique = $app->request->put('technique');
			$dimension = $app->request->put('dimension');
			$createdbyme = $app->request->put('createdbyme'); 
			$wishtosell = $app->request->put('wishtosell'); 
			$price = $app->request->put('price');
			$contact_info = $app->request->post('contact_info');
 
            $db = new DbHandler();
            $response = array(); 
 
            // updating task 
            $result = $db->updateTask($user_id, $task_id, $task, $taskdescription, $year, $technique, $dimension, $createdbyme, $wishtosell, $price, $contact_info); 
			//updateTask($user_id, $task_id, $task, $status); 
            if ($result) {
                // task updated successfully
                $response["error"] = false;
                $response["message"] = "Your Art updated successfully!";
            } else {
                // task failed to update
                $response["error"] = true;
                $response["message"] = "Your Art failed to update. Please try again.."; 
            }
            echoRespnse(200, $response); 
        });
		
		/**
 * Deleting task. Users can delete only their tasks
 * method DELETE
 * url /deletetask
 */
$app->post('/deletetask','authenticate', function() use($app) { 
			// check for required params  
            verifyRequiredParams(array('task_id'));   
 
            global $user_id; 
			$task_id = 	$app->request->put('task_id');

			$db = new DbHandler();
            $response = array();
 
            // deleting task
            $result = $db->deleteTask($user_id, $task_id);  
			
			 if ($result) {
                // task deleted successfully
                $response["error"] = false;
                $response["message"] = "Your Art is deleted succesfully";
            } else {
                // task failed to delete
                $response["error"] = true;
                $response["message"] = "Your Art failed to delete. Please try again!";
            }
            echoRespnse(200, $response);			
			
        });
		
		/**
 * Updating User Mobile number
 * method PUT
 * params mobile, fb_id 
 * url - /updatemobile
 */
$app->post('/updatemobile', 'authenticate', function() use($app) {
            
			// check for required params
            verifyRequiredParams(array('fb_id', 'mobile'));
 
            global $user_id;            
            $fb_id = $app->request->put('fb_id');
            $mobile = $app->request->put('mobile'); 
 
            $db = new DbHandler();
            $response = array();
 
            // updating task
            $result = $db->updateUserMobile($user_id, $fb_id, $mobile);    
			
            if ($result) {
                // task updated successfully
                $response["error"] = false;
                $response["message"] = "Mobile number updated successfully!";
				//$response["user_id"] = $user_id;  
            } else {
                // task failed to update
                $response["error"] = true;
                $response["message"] = "Mobile number failed to update. Please try again!";
            }
            echoRespnse(201, $response); 
        });
		
/**
 * Creating new like request in db
 * method POST
 * params - 'task_id', 'like'
 * url - /like/
 */
$app->post('/like_new', 'authenticate', function() use ($app) { 
	
          
            // check for required params
            verifyRequiredParams(array('task_id', 'like_val'));  
 
            $response = array();
			$task_id = $app->request->post('task_id'); 
			$like = $app->request->post('like_val');
			
            global $user_id;
            $db = new DbHandler();
 
            // liking a task 
			$result = $db->createUserLikeTask($user_id, $task_id, $like);  
			
			if ($result) {
                // task liked successfully
                $response["error"] = false;
                $response["message"] = "Art liked successfully!";
				
            } else {
                
				$response["error"] = true;
                $response["message"] = "Please try again!"; 
            }
            echoRespnse(201, $response); 
		
        });
		
		/**
 * Creating new buy request in db
 * method POST
 * params - 'task_id', 'wishtobuy' 
 * url - /buy/
 */
$app->post('/buy', 'authenticate', function() use ($app) { 
	
          
            // check for required params
            verifyRequiredParams(array('task_id', 'wishtobuy')); 
 
            $response = array();
			$task_id = $app->request->post('task_id');
			$wishtobuy = $app->request->post('wishtobuy');
			
            global $user_id;
            $db = new DbHandler();
 
            // liking a task 
			$result = $db->createUserBuyTask($user_id, $task_id, $wishtobuy);  
			
			if ($result) {
                // task wish to buy successfully
                $response["error"] = false;
                $response["message"] = "Your request to purchase this art is successful. We will email you shortly.";
				
            } else {
                
				$response["error"] = true;
                $response["message"] = "Please try again!";  
            }
            echoRespnse(201, $response);  
		
        });
		
		/**
 * Creating new sell request in db
 * method POST
 * params - 'task_id', 'wishtosell' 
 * url - /sell/
 */
$app->post('/sell', 'authenticate', function() use ($app) {
	
          
            // check for required params
            verifyRequiredParams(array('task_id', 'wishtosell')); 
 
            $response = array();
			$task_id = $app->request->post('task_id');
			$wishtosell = $app->request->post('wishtosell'); 
			$price = $app->request->post('price'); 
			$contact_info = $app->request->post('contact_info');
			if($contact_info == NULL)
		$contact_info = "na";
			
            global $user_id;
            $db = new DbHandler();
 
            // liking a task 
			$result = $db->createUserSellTask($user_id, $task_id, $wishtosell, $price, $contact_info);     
			
			if ($result) {
                // task wished to sell successfully
                $response["error"] = false;
                $response["message"] = "Art wished to sell successfully."; 
				
            } else {
                
				$response["error"] = true;
                $response["message"] = "Please try again!";  
            }
            echoRespnse(201, $response);  
		
        });
		
		/**
 * Listing all tasks liked by user
 * method GET
 * url  Tasks          
 */
 $app->get('/likedTasks', 'authenticate', function() {
            global $user_id;
            $response = array(); 
            $db = new DbHandler();
 
            // fetching all user tasks
            $result = $db->getAllUserLikedTasks($user_id);
 
            $response["error"] = false;
            $response["tasks"] = array();
 
            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {				
                $tmp = array();
                $tmp["id"] = $task["id"];
                $tmp["task"] = $task["task"];
				$tmp["taskdescription"] = $task["task_description"]; 
				$tmp["year"] = $task["year"];	 
				$tmp["technique"] = $task["technique"];	
				$tmp["dimension"] = $task["dimension"];	
				$tmp["createdbyme"] = $task["createdbyme"];	
				$tmp["wishtosell"] = $task["wishtosell"];	 
				$tmp["tot_likes"] = $task["tot_likes"];	
				$tmp["imageurl"] = $task["image_path"];				
                $tmp["status"] = $task["status"];
                $tmp["createdAt"] = $task["created_at"];  
				$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user_id, $task["id"]); //## -Sharmeen 
				//if($db->hasUserLiked($user_id, $task["id"])) $tmp["hasLiked"] = '1';
				//else $tmp["hasLiked"] = '0';
				$tmp["hasLiked"] = $db->hasUserLiked($user_id, $task["id"]); //## -Sharmeen 
				$user = $db->getUserByTaskId($task["id"]);
				$tmp["user_fbid"] = $user['fb_id'];
				$tmp["user_name"] = $user['firstname'];
				$tmp["user_id"] = $user['id']; 
				$tmp["price"] = $task["price"];
				$tmp["userBio"] = $user['bio'];
				
                array_push($response["tasks"], $tmp); 
            }
 
            echoRespnse(200, $response);
        });
		
		/**
 * Listing single task of particual user
 * method POST
 * url /taskdescription
 
 */
$app->post('/taskdescription', 'authenticate', function() use ($app) {
			
			verifyRequiredParams(array('task_id'));
            $response = array();
            $db = new DbHandler();
			
			global $user_id;
			
			$task_id = $app->request->post('task_id'); 
 
            // fetch task
            $result = $db->getTask($task_id); 
			
			$response["error"] = false;
            $response["tasks"] = array();
 
            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {				
                $tmp = array();
                $tmp["id"] = $task["id"]; 
                $tmp["task"] = $task["task"];
				$tmp["taskdescription"] = $task["task_description"]; 
				$tmp["year"] = $task["year"];	 
				$tmp["technique"] = $task["technique"];	
				$tmp["dimension"] = $task["dimension"];	
				$tmp["createdbyme"] = $task["createdbyme"];	
				$tmp["wishtosell"] = $task["wishtosell"]; 	
				$tmp["tot_likes"] = $task["tot_likes"];	
				$tmp["imageurl"] = $task["image_path"];				
                $tmp["status"] = $task["status"];
                $tmp["createdAt"] = $task["created_at"];  
				//$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user_id, $task["id"]); //## -Sharmeen 
				  
				$user = $db->getUserByTaskId($task["id"]);
				$tmp["user_fbid"] = $user['fb_id'];
				$tmp["user_name"] = $user['firstname'];
				$tmp["user_id"] = $user['id']; 
				$tmp["price"] = $task["price"];
				$tmp["hasLiked"] = $db->hasUserLiked($user_id, $task["id"]); //## -Sharmeen 
				$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user['id'], $task["id"]); //## -Sharmeen 
				$tmp["userBio"] = $user['bio'];
				
                array_push($response["tasks"], $tmp); 
            }
 
            echoRespnse(200, $response);
			
        });	

		/**
 * Listing all tasks of particual user
 * method GET
 * url /usertasks          
 */
$app->post('/usertasks', 'authenticate', function() use ($app) {
			
			verifyRequiredParams(array('user_id'));
            $response = array();
            $db = new DbHandler();
			
			global $user_id; 
			
			$user_id_1 = $app->request->post('user_id'); 
 
            // fetch task
            $result = $db->getAllUserTasks($user_id_1);
			$tot_likes = $db->getTotLikes($user_id_1);
			$tot_uploads = $db->getTotUploads($user_id_1);
			$tot_created = $db->getTotCreated($user_id_1); 
			$user_fb_id = $db->getFbId($user_id_1);
			$user_name = $db->getUserNamebyId($user_id_1); 
			$tot_liked = $db->getTotLikedArts($user_id_1);
			$user_bio = $db->getUserBiobyId($user_id_1);
 
            $response["error"] = false;
            $response["tasks"] = array(); 
			$response['user_fbid'] = $user_fb_id['fbid'];     
			$response['total_likes'] = $tot_likes['total'];
			$response['total_uploads'] = $tot_uploads['total'];
			$response['total_created'] = $tot_created['total']; 
			$response['user_name'] = $user_name['firstname']; 
			$response['total_liked'] = $tot_liked['total']; 
			$response['userBio'] = $user_bio['bio']; 			
	 		
 
            // looping through result and preparing tasks array 
            while ($task = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["id"] = $task["id"];
                $tmp["task"] = $task["task"];
				$tmp["taskdescription"] = $task["task_description"]; 
				$tmp["year"] = $task["year"];	 
				$tmp["technique"] = $task["technique"];	 
				$tmp["dimension"] = $task["dimension"];	
				$tmp["createdbyme"] = $task["createdbyme"];	
				$tmp["wishtosell"] = $task["wishtosell"];	
				$tmp["tot_likes"] = $task["tot_likes"];	 
				$tmp["imageurl"] = $task["image_path"];	
                $tmp["status"] = $task["status"];
                $tmp["createdAt"] = $task["created_at"]; 
				//if($db->hasUserLiked($user_id, $task["id"])) $tmp["hasLiked"] = '1';
				//else $tmp["hasLiked"] = '0';
				$tmp["hasLiked"] = $db->hasUserLiked($user_id, $task["id"]); //## -Sharmeen   
				$user = $db->getUserByTaskId($task["id"]);
				$tmp["user_fbid"] = $user['fb_id'];
				$tmp["user_name"] = $user['firstname'];
				$tmp["user_id"] = $user['id'];
				$tmp["price"] = $task["price"];	
				$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user_id, $task["id"]); //## -Sharmeen  
				$tmp["userBio"] = $user['bio']; 
								
                array_push($response["tasks"], $tmp); 
            }
 
            echoRespnse(200, $response); 
        });
		
		
$app->post('/verifyotp', 'authenticate', function() use($app) {
            
			// check for required params
            verifyRequiredParams(array('otp'));
 
            global $user_id;            
            $otp = $app->request->put('otp');           
 
            $db = new DbHandler();
            $response = array();
 
            // updating task
            $result = $db->verifyOTP($user_id, $otp);    
			
            if ($result) {
              // otp verified successfully
                $response["error"] = false;
                $response["message"] = "Mobile number verified succesfully!";
            } else {
                 // otp not verified
                $response["error"] = true;
                $response["message"] = "Mobile verification failed. Please try again.";
            }
            echoRespnse(201, $response); 
        });
		
		/**
 * Listing all Latest tasks of ALL users
 * method GET
 * url /othertaskslatest          
 */
$app->get('/othertaskslatest', 'authenticate', function() {  
            global $user_id;
            $response = array(); 
            $db = new DbHandler();
 
            // fetching all user tasks
            $result = $db->getAllOtherUsersTasksLatest($user_id);
 
            $response["error"] = false;
            $response["tasks"] = array();
 
            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {				
                $tmp = array();
                $tmp["id"] = $task["id"];
                $tmp["task"] = $task["task"];
				$tmp["taskdescription"] = $task["task_description"]; 
				$tmp["year"] = $task["year"];	 
				$tmp["technique"] = $task["technique"];	
				$tmp["dimension"] = $task["dimension"];	
				$tmp["createdbyme"] = $task["createdbyme"];	
				$tmp["wishtosell"] = $task["wishtosell"]; 	
				$tmp["tot_likes"] = $task["tot_likes"];	
				$tmp["imageurl"] = $task["image_path"];				
                $tmp["status"] = $task["status"];
                $tmp["createdAt"] = $task["created_at"];  
				$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user_id, $task["id"]); //## -Sharmeen
				//if($db->hasUserLiked($user_id, $task["id"])) $tmp["hasLiked"] = '1';
				//else $tmp["hasLiked"] = '0';				
				$tmp["hasLiked"] = $db->hasUserLiked($user_id, $task["id"]); //## -Sharmeen 
				$user = $db->getUserByTaskId($task["id"]);
				$tmp["user_fbid"] = $user['fb_id'];
				$tmp["user_name"] = $user['firstname'];
				$tmp["user_id"] = $user['id']; 
				$tmp["price"] = $task["price"];
				$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user_id, $task["id"]); //## -Sharmeen 
				$tmp["userBio"] = $user['bio']; 
				
                array_push($response["tasks"], $tmp); 
            }
 
            echoRespnse(200, $response);
        });
		
		/**
 * Reporting task. Users can report any Art
 * method POST
 * url /reporttask
 */
$app->post('/reporttask', 'authenticate', function() use ($app) { 
	
          
            // check for required params
            verifyRequiredParams(array('task_id'));   
 
            $response = array(); 
			$task_id = $app->request->post('task_id'); 
			//$reason = $app->request->post('reason');
			
            global $user_id;
            $db = new DbHandler();
 
            // liking a task 
			$result = $db->reportTask($user_id, $task_id);   
			
			if ($result) {
                // task reported successfully
                $response["error"] = false;
                $response["message"] = "Art reported successfully!"; 
				
            } else {
                
				$response["error"] = true;
                $response["message"] = "Please try again!"; 
            }
            echoRespnse(201, $response); 
		
        });
		
		/**
 * Update Users Contact Information during buying.
 * method POST
 * url /updateinfo
 */
$app->post('/updateinfo', 'authenticate', function() use ($app) { 
	
          
            // check for required params 
            verifyRequiredParams(array('user_contact_info'));   
 
            $response = array(); 
			$user_contact_info = $app->request->post('user_contact_info'); 
			//$reason = $app->request->post('reason');
			if($user_contact_info == NULL)
				$user_contact_info = "na"; 
			
            global $user_id;
            $db = new DbHandler();
 
            // update contact info
			$result = $db->update_contact($user_id, $user_contact_info);    
			
			if ($result) {
                // 
                $response["error"] = false;
                $response["message"] = "Contact info updated successfully!";  
				
            } else {
                
				$response["error"] = true;
                $response["message"] = "Please try again!";  
            }
            echoRespnse(201, $response); 
		
        });
		
		/**
 * Search ART
 * method POST
 * url /searchArt
 */
$app->post('/searchArt', 'authenticate', function() use ($app) { 
	
          
            // check for required params 
            verifyRequiredParams(array('search_val'));   
 
            $response = array(); 
			$search_val = $app->request->post('search_val'); 
			if($search_val == NULL)
				$search_val = ""; 
			
            global $user_id;
            $db = new DbHandler();
 
            // fetching all search tasks
			$result = $db->getSearch($search_val);    
			
			$response["error"] = false;
            $response["tasks"] = array();
 
            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {				
                $tmp = array();
                $tmp["id"] = $task["id"];
                $tmp["task"] = $task["task"];
				$tmp["taskdescription"] = $task["task_description"]; 
				$tmp["year"] = $task["year"];	 
				$tmp["technique"] = $task["technique"];	
				$tmp["dimension"] = $task["dimension"];	
				$tmp["createdbyme"] = $task["createdbyme"];	
				$tmp["wishtosell"] = $task["wishtosell"];	 
				$tmp["tot_likes"] = $task["tot_likes"];	
				$tmp["imageurl"] = $task["image_path"];				
                $tmp["status"] = $task["status"];
                $tmp["createdAt"] = $task["created_at"];  
				$tmp["hasWishedtobuy"] = $db->hasUserWishedToBuy($user_id, $task["id"]); //## -Sharmeen 
				//if($db->hasUserLiked($user_id, $task["id"])) $tmp["hasLiked"] = '1';
				//else $tmp["hasLiked"] = '0';
				$tmp["hasLiked"] = $db->hasUserLiked($user_id, $task["id"]); //## -Sharmeen 
				$user = $db->getUserByTaskId($task["id"]);
				$tmp["user_fbid"] = $user['fb_id'];
				$tmp["user_name"] = $user['firstname'];
				$tmp["user_id"] = $user['id']; 
				$tmp["price"] = $task["price"]; 
				$tmp["userBio"] = $user['bio']; 
				
                array_push($response["tasks"], $tmp); 
            }
			
           echoRespnse(200, $response);
		
        });
		
		/**
 * Search Artists
 * method POST
 * url /searchArtist
 */
$app->post('/searchArtist', 'authenticate', function() use ($app) { 
	
          
            // check for required params 
            verifyRequiredParams(array('search_val'));   
 
            $response = array(); 
			$search_val = $app->request->post('search_val'); 
			if($search_val == NULL)
				$search_val = ""; 
			
            global $user_id;
            $db = new DbHandler();
 
            // fetching all search tasks
			$result = $db->getSearchArtist($search_val);     
			
			$response["error"] = false;
            $response["users"] = array(); 
 
            // looping through result and preparing tasks array
            while ($user = $result->fetch_assoc()) {	 
			
				$user_id = $user['id'];
				$tot_likes = $db->getTotLikes($user_id);
				$tot_uploads = $db->getTotUploads($user_id);
				$tot_created = $db->getTotCreated($user_id);
				$tot_liked = $db->getTotLikedArts($user_id);			
 			
                $tmp = array();
				$tmp["user_id"] = $user['id']; 
				$tmp["user_fbid"] = $user['fb_id'];
				$tmp['firstname'] = $user['firstname'];
				$tmp['total_likes'] = $tot_likes['total'];
				$tmp['total_uploads'] = $tot_uploads['total'];
				$tmp['total_created'] = $tot_created['total'];
				$tmp['total_liked'] = $tot_liked['total'];  
				$tmp["userBio"] = $user['bio']; 
				
                array_push($response["users"], $tmp);  
            }
			
           echoRespnse(200, $response);
		
        });
		

 /**
 * Update User Profile
 * method POST
 * url /updateprofile
 */
$app->post('/updateprofile', 'authenticate', function() use($app) {
            // check for required params 
           // verifyRequiredParams(array('user_id'));      
 
            global $user_id; 
			
			// reading post params
            $name = $app->request->post('name');
			$first_name = $app->request->post('firstname'); 
			$bio = $app->request->post('bio');
 
            $db = new DbHandler();
            $response = array(); 
 
            // updating user profile 
			$db->updateUserName($user_id,$name );
			$db->updateUserFirstName($user_id,$first_name);
			$db->updateUserBio($user_id,$bio);
			
			$response["error"] = false;
            $response["message"] = "Your Profile is updated successfully!";
            echoRespnse(200, $response); 
        });
	
		
$app->run();		
?>